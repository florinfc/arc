package org.ejb3unit.hibernate.dialect;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The class <code>CUBRIDDialectTest</code> contains tests for the class
 * <code>{@link CUBRIDDialect}</code>.
 * 
 * @generatedBy CodePro at 6/30/13 8:04 PM
 */
public class CUBRIDDialectTest {
	/**
	 * Run the CUBRIDDialect() constructor test.
	 * 
	 * @throws Exception
	 * 
	 * @generatedBy CodePro at 6/30/13 8:04 PM
	 */
	@Test
	public void testCUBRIDDialect() throws Exception {

		CUBRIDDialect result = new CUBRIDDialect();

		assertNotNull(result);
		assertEquals(false, result.dropConstraints());
		assertEquals(false, result.hasAlterTable());
		assertEquals(false, result.supportsTemporaryTables());
		assertEquals('[', result.openQuote());
		assertEquals(']', result.closeQuote());
		assertEquals("add", result.getAddColumnString());
		assertEquals(false, result.supportsCommentOn());
		assertEquals(true, result.supportsSequences());
		assertEquals("select name from db_serial",
				result.getQuerySequencesString());
		assertEquals(true, result.supportsLimit());
		assertEquals(true, result.bindLimitParametersInReverseOrder());
		assertEquals(true, result.useMaxForLimit());
		assertEquals(true, result.forUpdateOfColumns());
		assertEquals(" ", result.getForUpdateString());
		assertEquals(true, result.supportsUnionAll());
		assertEquals(true, result.supportsCurrentTimestampSelection());
		assertEquals("select systimestamp from table({1}) as T(X)",
				result.getCurrentTimestampSelectString());
		assertEquals(false, result.isCurrentTimestampSelectStringCallable());
		assertEquals("org.ejb3unit.hibernate.dialect.CUBRIDDialect",
				result.toString());
		assertEquals(true, result.supportsUniqueConstraintInCreateAlterTable());
		assertEquals("", result.getNullColumnString());
		assertEquals(true, result.supportsUnique());
		assertEquals(true, result.supportsNotNullUnique());
		assertEquals(true, result.supportsColumnCheck());
		assertEquals("create table", result.getCreateTemporaryTableString());
		assertEquals("", result.getCreateTemporaryTablePostfix());
		assertEquals("create table", result.getCreateTableString());
		assertEquals("create table", result.getCreateMultisetTableString());
		assertEquals(true, result.hasDataTypeInIdentityColumn());
		assertEquals(true, result.supportsTableCheck());
		assertEquals("", result.getTableTypeString());
		assertEquals(false, result.supportsIfExistsBeforeTableName());
		assertEquals("", result.getCascadeConstraintsString());
		assertEquals(false, result.supportsIfExistsAfterTableName());
		assertEquals(10, result.getMaxAliasLength());
		assertEquals(false, result.supportsIdentityColumns());
		assertEquals(false, result.supportsInsertSelectIdentity());
		assertEquals(null, result.getIdentityInsertString());
		assertEquals(false, result.supportsPooledSequences());
		assertEquals(true, result.supportsLimitOffset());
		assertEquals(true, result.supportsVariableLimit());
		assertEquals(false, result.bindLimitParametersFirst());
		assertEquals(false, result.forceLimitUsage());
		assertEquals(true, result.supportsLockTimeouts());
		assertEquals(false, result.isLockTimeoutParameterized());
		assertEquals(" ", result.getForUpdateNowaitString());
		assertEquals(true, result.supportsOuterJoinForUpdate());
		assertEquals("drop table", result.getDropTemporaryTableString());
		assertEquals(null, result.performTemporaryTableDDLInIsolation());
		assertEquals(true, result.dropTemporaryTableAfterUse());
		assertEquals("current_timestamp",
				result.getCurrentTimestampSQLFunctionName());
		assertEquals("values ( )", result.getNoColumnsInsertString());
		assertEquals("lower", result.getLowercaseFunction());
		assertEquals(true, result.qualifyIndexName());
		assertEquals(" drop constraint ", result.getDropForeignKeyString());
		assertEquals(false, result.hasSelfReferentialForeignKeyBug());
		assertEquals(true, result.supportsCascadeDelete());
		assertEquals(" cross join ", result.getCrossJoinSeparator());
		assertEquals(true, result.supportsEmptyInList());
		assertEquals(false, result.areStringComparisonsCaseInsensitive());
		assertEquals(false, result.supportsRowValueConstructorSyntax());
		assertEquals(false, result.supportsRowValueConstructorSyntaxInInList());
		assertEquals(true, result.useInputStreamToInsertBlob());
		assertEquals(true, result.supportsParametersInInsertSelect());
		assertEquals(false,
				result.replaceResultVariableInOrderByClauseWithPosition());
		assertEquals(false, result.requiresCastingOfParametersInSelectClause());
		assertEquals(
				true,
				result.supportsResultSetPositionQueryMethodsOnForwardOnlyCursor());
		assertEquals(true, result.supportsCircularCascadeDeleteConstraints());
		assertEquals(true, result.supportsSubselectAsInPredicateLHS());
		assertEquals(true, result.supportsExpectedLobUsagePattern());
		assertEquals(true, result.supportsLobValueChangePropogation());
		assertEquals(true, result.supportsUnboundedLobLocatorMaterialization());
		assertEquals(true, result.supportsSubqueryOnMutatingTable());
		assertEquals(true, result.supportsExistsInSelect());
		assertEquals(false,
				result.doesReadCommittedCauseWritersToBlockReaders());
		assertEquals(false,
				result.doesRepeatableReadCauseReadersToBlockWriters());
		assertEquals(true, result.supportsBindAsCallableArgument());
		assertEquals(false, result.supportsTupleCounts());
		assertEquals(true, result.supportsTupleDistinctCounts());
	}

	/**
	 * Run the String getCreateSequenceString(String) method test.
	 * 
	 * @throws Exception
	 * 
	 * @generatedBy CodePro at 6/30/13 8:04 PM
	 */
	@Test
	public void testGetCreateSequenceString() throws Exception {
		CUBRIDDialect fixture = new CUBRIDDialect();
		String sequenceName = "";

		String result = fixture.getCreateSequenceString(sequenceName);

		assertEquals("create serial ", result);
	}

	/**
	 * Run the String getDropSequenceString(String) method test.
	 * 
	 * @throws Exception
	 * 
	 * @generatedBy CodePro at 6/30/13 8:04 PM
	 */
	@Test
	public void testGetDropSequenceString() throws Exception {
		CUBRIDDialect fixture = new CUBRIDDialect();
		String sequenceName = "";

		String result = fixture.getDropSequenceString(sequenceName);

		assertEquals("drop serial ", result);
	}

	/**
	 * Run the String getIdentitySelectString(String,String,int) method test.
	 * 
	 * @throws Exception
	 * 
	 * @generatedBy CodePro at 6/30/13 8:04 PM
	 */
	@Test
	public void testGetIdentitySelectString() throws Exception {
		CUBRIDDialect fixture = new CUBRIDDialect();
		String table = "";
		String column = "";
		int type = 1;

		String result = fixture.getIdentitySelectString(table, column, type);

		assertEquals("select current_val from db_serial where name = '_ai_'",
				result);
	}

	/**
	 * Run the String getLimitString(String,boolean) method test.
	 * 
	 * @throws Exception
	 * 
	 * @generatedBy CodePro at 6/30/13 8:04 PM
	 */
	@Test
	public void testGetLimitString() throws Exception {
		CUBRIDDialect fixture = new CUBRIDDialect();
		String sql = "";
		boolean hasOffset = false;

		String result = fixture.getLimitString(sql, hasOffset);

		assertEquals(" limit ?", result);
	}

	/**
	 * Run the String getSequenceNextValString(String) method test.
	 * 
	 * @throws Exception
	 * 
	 * @generatedBy CodePro at 6/30/13 8:04 PM
	 */
	@Test
	public void testGetSequenceNextValString() throws Exception {
		CUBRIDDialect fixture = new CUBRIDDialect();
		String sequenceName = "";

		String result = fixture.getSequenceNextValString(sequenceName);

		assertEquals("select .next_value from table({1}) as T(X)", result);
	}

	/**
	 * Perform pre-test initialization.
	 * 
	 * @throws Exception
	 *             if the initialization fails for some reason
	 * 
	 * @generatedBy CodePro at 6/30/13 8:04 PM
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Perform post-test clean-up.
	 * 
	 * @throws Exception
	 *             if the clean-up fails for some reason
	 * 
	 * @generatedBy CodePro at 6/30/13 8:04 PM
	 */
	@After
	public void tearDown() throws Exception {
	}
}
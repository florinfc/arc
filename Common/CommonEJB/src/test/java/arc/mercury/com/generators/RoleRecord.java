package arc.mercury.com.generators;

import arc.mercury.com.entities.Role;
import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.SingleBeanGenerator;

/**
 * Generator for one Role record
 */
@GeneratorType(className = Role.class, fieldType = FieldType.ALL_TYPES)
public final class RoleRecord extends SingleBeanGenerator<Role> {

	public RoleRecord() {
		super(Role.class);
	}

	public final static RoleRecord instance() {
		return new RoleRecord();
	}
}

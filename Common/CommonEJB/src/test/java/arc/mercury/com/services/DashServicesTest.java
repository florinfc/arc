package arc.mercury.com.services;

import arc.mercury.com.entities.*;
import arc.mercury.com.enums.MailName;
import com.bm.testsuite.BaseSessionBeanFixture;
import com.bm.testsuite.dataloader.CSVInitialDataSet;
import com.bm.testsuite.dataloader.DateFormats;
import junit.framework.Assert;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.JUnit4ClassRunner;
import org.junit.runner.RunWith;

import javax.persistence.PersistenceException;
import java.util.HashMap;
import java.util.List;

@RunWith(JUnit4ClassRunner.class)
public class DashServicesTest extends BaseSessionBeanFixture<DashServices> {
	private static final Class<?>[] beans = {

			User.class, Role.class, Team.class, State.class, Mail.class

	};
	private static CSVInitialDataSet<?>[] data = {

			new CSVInitialDataSet<State>(State.class, "Csv/State.csv", "id",
					"name", "code", "active"),
			new CSVInitialDataSet<User>(User.class, "Csv/User.csv", "id",
					"name", "password", "mail", "fname", "lname", "role"),
			new CSVInitialDataSet<Role>(Role.class, "Csv/Role.csv", "id",
					"name"),
			new CSVInitialDataSet<Team>(Team.class, "Csv/Team.csv", "id",
					"name"),
			new CSVInitialDataSet<Mail>(Mail.class, "Csv/Mail.csv", "id",
					"name", "stateId", "subject", "content"),

	};

	static {
		for (int i = 0; i < data.length; ++i) {
			DateFormats.USER_DATE.setUserDefinedFomatter("MM-dd-yyyy");
			data[i].addDateFormat(DateFormats.USER_DATE);
		}
	}

	private final Logger log = Logger.getLogger(DashServicesTest.class);
	private DashServices service;

	public DashServicesTest() {
		super(DashServices.class, beans, data);
	}

	@Override
	@Before
	public void setUp() throws Exception {
		super.setUp();
		this.service = this.getBeanToTest();
		Assert.assertNotNull(service);
	}

	@Override
	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	private <T> void showItems(List<T> items) {
		log.info(getName() + "\t : records = " + items.size());
		for (T item : items) {
			log.info(item.toString());
		}
	}

	@Test
	public void testGetAllUsers() throws Exception {
		List<User> result = service.getAllUsers();
		Assert.assertNotNull(result);
		Assert.assertTrue(0 < result.size());
		showItems(result);
	}

//	@Test
//	public void testGetAnonymous() throws Exception {
//		List<User> result = service.getAnonymous();
//		Assert.assertTrue(0 < result.size());
//		showItems(result);
//	}

	@Test
	public void testGetUserById() throws Exception {
		Long id = null;
		User result = service.getUserById(id);
		Assert.assertNull(result);

		id = new Long(-1L);
		result = service.getUserById(id);
		Assert.assertNull(result);

		id = new Long(1L);
		result = service.getUserById(id);
		Assert.assertNotNull(result);
	}

	@Test
	public void testGetUserByMail() throws Exception {
		String mail = null;
		User result = service.getUserByMail(mail);
		Assert.assertNull(result);

		mail = "";
		result = service.getUserByMail(mail);
		Assert.assertNull(result);

		mail = "xxx";
		result = service.getUserByMail(mail);
		Assert.assertNull(result);
	}

	@Test
	public void testGetUserByName() throws Exception {
		String name = null;
		User result = service.getUserByName(name);
		Assert.assertNull(result);

		name = "";
		result = service.getUserByName(name);
		Assert.assertNull(result);

		name = "xxx";
		result = service.getUserByName(name);
		Assert.assertNull(result);
	}

	@Test
	public void testSaveUser() {
		User user = null;
		User result = service.saveUser(user);
		Assert.assertNull(result);

		try {
			user = new User();
			result = service.saveUser(user);
		} catch (PersistenceException e) {
			// should throw exception (mail is required)
			Assert.assertTrue(true);
		}

		user.setId(5L);
		user.setMail("x.x@x.x");
		service.saveUser(user);
	}

	@Test
	public void testDeleteUser() {
		User user = null;
		service.deleteUser(user);

		try {
			user = new User();
			service.deleteUser(user);
		} catch (IllegalArgumentException e) {
			// should throw exception (id is required)
			Assert.assertTrue(true);
		}

		user.setId(5L);
		service.deleteUser(user);
	}

	@Test
	public void testGetStates() {
		List<State> result = service.getStates();
		Assert.assertTrue(0 < result.size());
		showItems(result);
	}

	@Test
	public void testGetStateById() {
		Long id = null;
		State result = service.getStateById(id);
		Assert.assertNull(result);

		id = new Long(-1L);
		result = service.getStateById(id);
		Assert.assertNull(result);

		id = new Long(1L);
		result = service.getStateById(id);
		Assert.assertNotNull(result);
	}

	private String fillPdf(String formName) {
		String formPath = System.getProperty("file.separator") + "Forms";
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("comName", "123456789-123456789-123456789-123456789-123456789-123456789-123456789");
		map.put("agName", "123456789-123456789-123456789-123456789-123456789-123456789-123456789");
//		map.put("agAddress", "123456789-123456789-123456789-123456789-123456789-123456789-12345");
//		map.put("agCity", "123456789-123456789-123456789-123456789-123456789-123456789-12345");
//		map.put("agState", "CA");
//		map.put("agZip", "11111");
		map.put("initAddress", "123456789-123456789-123456789-123456789-123456789-123456789-12345");
		map.put("initCity", "123456789-123456789-123456789-123456789-123456789-123456789-12345");
		map.put("initState", "CA");
		map.put("initZip", "22222");
		map.put("mailAddress", "123456789-123456789-123456789-123456789-123456789-123456789-12345");
		map.put("mailCity", "123456789-123456789-123456789-123456789-123456789-123456789-12345");
		map.put("mailState", "CA");
		map.put("mailZip", "33333");
		map.put("frontAddress", "123456789-123456789-123456789-123456789-123456789-123456789-12345");
		map.put("frontCity", "123456789-123456789-123456789-123456789-123456789-123456789-12345");
		map.put("frontState", "CA");
		map.put("frontZip", "99999");
		map.put("manName", "123456789-123456789-123456789-123456789-123456789-123456789-123456789");
		map.put("manAddress", "123456789-123456789-123456789-123456789-123456789-123456789-12345");
		map.put("manCity", "123456789-123456789-123456789-123456789-123456789-123456789-12345");
		map.put("manState", "CA");
		map.put("manZip", "44444");
		map.put("comPurpose", "123456789-123456789-123456789-123456789-123456789-123456789-12345");
		map.put("Public", "On");
		map.put("Charitable", "On");
		map.put("mOne", "On");
		map.put("mMore", "On");
		map.put("mAll", "On");
		map.put("comShares", "9999999");
		map.put("comOwners", "9");
		map.put("userName", "123456789-123456789-123456789-123456789.");
		map.put("font", "Courier-Bold");
		map.put("fontsize", "10");
		return service.createPdf(formPath, formName, map);
	}

	@Test
	public void testCreateAll() {
		testCreateARTS_CID();
		testCreateARTS_CL();
		testCreateARTS_GS();
		testCreateARTS_MU();
		testCreateARTS_PC();
		testCreateARTS_RE();
		testCreateARTS_PB();
		testCreateARTS_LLC1();
	}

	@Test
	public void testCreateARTS_CID() {
		String result = fillPdf("arts-cid.pdf");
		Assert.assertNotNull(result);
	}

	@Test
	public void testCreateARTS_CL() {
		String result = fillPdf("arts-cl.pdf");
		Assert.assertNotNull(result);
	}

	@Test
	public void testCreateARTS_GS() {
		String result = fillPdf("arts-gs.pdf");
		Assert.assertNotNull(result);
	}

	@Test
	public void testCreateARTS_LLC1() {
		String result = fillPdf("arts-llc1.pdf");
		Assert.assertNotNull(result);
	}

	@Test
	public void testCreateARTS_MU() {
		String result = fillPdf("arts-mu.pdf");
		Assert.assertNotNull(result);
	}

	@Test
	public void testCreateARTS_PB() {
		String result = fillPdf("arts-pb.pdf");
		Assert.assertNotNull(result);
	}

	@Test
	public void testCreateARTS_PC() {
		String result = fillPdf("arts-pc.pdf");
		Assert.assertNotNull(result);
	}

	@Test
	public void testCreateARTS_RE() {
		String result = fillPdf("arts-re.pdf");
		Assert.assertNotNull(result);
	}

	@Test
	public void testSendSimpleMail() {
		User to = new User();
		to.setMail("florin.filipoiu@arnia.ro");
		to.setFname("florin");
		to.setLname("FILIPOIU");
//		to.setMail("dilet.ibram@arnia.ro");
//		to.setFname("Dilet");
//		to.setLname("IBRAM");
		String subject = "testHtml";
		String[] bodyLines = {"plain", "<b>bold</b>", "<i>italic</i>"};
		String sep = System.getProperty("file.separator");
		String formPath = sep + "Forms";
		if (sep.equals("\\")) {
			formPath = "C:" + formPath;
		}
		String fileFullName = formPath + sep + "PDF-out" + sep + "arts-pb.pdf";
		String[] attFiles = {fileFullName};
		String result = service
				.sendSimpleMail(to, subject, bodyLines, attFiles);
		Assert.assertNotNull(result);
		Assert.assertTrue(result.length() > 5);
		Assert.assertEquals("Sent:", result.substring(0, 5));
	}

	@Test
	public void testSendSaveMail() {
		User user = new User();
//		user.setMail("dilet.ibram@arnia.ro");
//		user.setFname("Dilet");
//		user.setLname("IBRAM");
		user.setMail("florin.filipoiu@arnia.ro");
		user.setFname("florin");
		user.setLname("FILIPOIU");
		user.setId(5L);

		User[] toList = new User[1];
		toList[0] = user;

		Mail mail = service.getMailByName(MailName.Save.getValue());

		String subject = mail.getSubject();
		String url = "http://dash.server:8081/dashboard/incwiz/start.xhtml?id="
				+ user.getId().toString();

		String line = String.format(mail.getContent(), url);
		String[] bodyLines = {line};
		String result = service.sendSimpleMail(user, subject, bodyLines, null);

		Assert.assertNotNull(result);
		Assert.assertTrue(result.length() > 5);
		Assert.assertEquals("Sent:", result.substring(0, 5));
	}

	@Test
	public void testSendFinishMail() {
		User user = new User();
//		user.setMail("dilet.ibram@arnia.ro");
//		user.setFname("Dilet");
//		user.setLname("IBRAM");
		user.setMail("florin.filipoiu@arnia.ro");
		user.setFname("florin");
		user.setLname("FILIPOIU");

		User[] toList = new User[1];
		toList[0] = user;

		Mail mail = service.getMailByName(MailName.Finish.getValue());

		String subject = mail.getSubject();
		String[] bodyLines = {mail.getContent()};

		String sep = System.getProperty("file.separator");
		String formPath = sep + "Forms";
		if (sep.equals("\\")) {
			formPath = "C:" + formPath;
		}
		String fileFullName = formPath + sep + "PDF-out" + sep + "arts-llc1.pdf";
		String[] attFiles = {fileFullName};

		String result = service.sendSimpleMail(user, subject, bodyLines,
				attFiles);

		Assert.assertNotNull(result);
		Assert.assertTrue(result.length() > 5);
		Assert.assertEquals("Sent:", result.substring(0, 5));
	}
}
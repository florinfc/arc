package arc.mercury.com.generators;

import arc.mercury.com.entities.Team;
import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.SingleBeanGenerator;

/**
 * Generator for one Team record
 */
@GeneratorType(className = Team.class, fieldType = FieldType.ALL_TYPES)
public final class TeamRecord extends SingleBeanGenerator<Team> {

	public TeamRecord() {
		super(Team.class);
	}

	public final static TeamRecord instance() {
		return new TeamRecord();
	}
}

package arc.mercury.com.managers;

import arc.mercury.com.entities.Role;
import arc.mercury.com.entities.State;

import arc.mercury.com.entities.Team;
import arc.mercury.com.entities.User;
import com.bm.testsuite.BaseSessionBeanFixture;
import com.bm.testsuite.dataloader.CSVInitialDataSet;
import com.bm.testsuite.dataloader.DateFormats;
import junit.framework.Assert;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.PersistenceException;
import java.util.List;

public class StateManagerTest extends BaseSessionBeanFixture<StateManager> {
    private final Logger log = Logger.getLogger(StateManagerTest.class);
	private static final Class<?>[] beans = { State.class };
    private static CSVInitialDataSet<?>[] data = {

            new CSVInitialDataSet<State>(State.class, "Csv/State.csv", "id",
                    "name", "code", "active")

    };

    static {
        for (int i = 0; i < data.length; ++i) {
            DateFormats.USER_DATE.setUserDefinedFomatter("MM-dd-yyyy");
            data[i].addDateFormat(DateFormats.USER_DATE);
        }
    }

    private StateManager manager;
    
	public StateManagerTest() {
		super(StateManager.class, beans, data);
	}

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        this.manager = this.getBeanToTest();
        Assert.assertNotNull(manager);
    }

    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    private <T> void showItems(List<T> items) {
        log.info(getName() + "\t : records = " + items.size());
        for (T item : items) {
            log.info(item.toString());
        }
    }

    @Test
    public void testFindAll() {
        List<State> result = manager.findAll();
        Assert.assertTrue(0 < result.size());
        showItems(result);
    }

    @Test
    public void testFindById() {
        Long id = null;
        State result = manager.findById(id);
        Assert.assertNull(result);

        id = new Long(-1L);
        result = manager.findById(id);
        Assert.assertNull(result);

        id = new Long(1L);
        result = manager.findById(id);
        Assert.assertNotNull(result);
    }

}
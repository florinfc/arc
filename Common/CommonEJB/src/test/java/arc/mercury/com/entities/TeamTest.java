package arc.mercury.com.entities;

import arc.mercury.com.generators.UserCollection;

import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;

public class TeamTest extends BaseEntityFixture<Team> {
	private static final Generator<?>[] SPECIAL_GENERATORS = {

	UserCollection.instance()

	};

	public TeamTest() {
		super(Team.class, SPECIAL_GENERATORS);
	}
}

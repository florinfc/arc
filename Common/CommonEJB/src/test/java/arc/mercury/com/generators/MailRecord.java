package arc.mercury.com.generators;

import arc.mercury.com.entities.Mail;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.SingleBeanGenerator;

/**
 * Generator for one Mail record
 */
@GeneratorType(className = Mail.class, fieldType = FieldType.ALL_TYPES)
public final class MailRecord extends SingleBeanGenerator<Mail> {

	public MailRecord() {
		super(Mail.class);
	}

	public final static MailRecord instance() {
		return new MailRecord();
	}
}

package arc.mercury.com.managers;

import java.util.List;
import java.util.UUID;

import arc.mercury.com.entities.Mail;
import arc.mercury.com.enums.MailName;

import com.bm.testsuite.BaseSessionBeanFixture;
import com.bm.testsuite.dataloader.CSVInitialDataSet;
import com.bm.testsuite.dataloader.DateFormats;

import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MailManagerTest extends BaseSessionBeanFixture<MailManager> {
    private final Logger log = Logger.getLogger(MailManagerTest.class);
    private static final Class<?>[] beans = { Mail.class };
    private static CSVInitialDataSet<?>[] data = {

    	new CSVInitialDataSet<Mail>(Mail.class, "Csv/Mail.csv", "id",
				"name", "stateId", "subject", "content"),

    };

    static {
        for (int i = 0; i < data.length; ++i) {
            DateFormats.USER_DATE.setUserDefinedFomatter("MM-dd-yyyy");
            data[i].addDateFormat(DateFormats.USER_DATE);
        }
    }

    private MailManager manager;
    
    private final static Long stateIdTest = 5L;

    public MailManagerTest() {
        super(MailManager.class, beans, data);
    }

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        this.manager = this.getBeanToTest();
        Assert.assertNotNull(manager);
    }

    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    private <T> void showItems(List<T> items) {
        log.info(getName() + "\t : records = " + items.size());
        for (T item : items) {
            log.info(item.toString());
        }
    }
    
    @Test
    public void testFindAll() {
        List<Mail> result = manager.findAll();
        Assert.assertTrue(0 < result.size());
        showItems(result);
    }
    
    @Test
    public void testFindById() {
        Long id = null;
        Mail result = manager.findById(id);
        Assert.assertNull(result);

        id = new Long(-1L);
        result = manager.findById(id);
        Assert.assertNull(result);

        id = new Long(1L);
        result = manager.findById(id);
        Assert.assertNotNull(result);
    }
    
    @Test
    public void testFindByName() {
    	String name = MailName.Save.getValue();
        Mail result = manager.findByName(name);
        Assert.assertNotNull(result);
        
        name = MailName.Finish.getValue();
        result = manager.findByName(name);
        Assert.assertNotNull(result);
        
        name = UUID.randomUUID().toString();
        result = manager.findByName(name);
        Assert.assertNull(result);
    }
    
    @Test
    public void testFindByStateId() {
        List<Mail> result = manager.findByStateId(stateIdTest);
        Assert.assertTrue(0 < result.size());
        showItems(result);
    }
    
    @Test
    public void testFindLikeName() {
    	String name = MailName.Save.getValue();
    	name = name.substring(0, name.length() - 2);
        List<Mail> result = manager.findLikeName(name);
        Assert.assertTrue(0 < result.size());
        showItems(result);
        
        name = MailName.Finish.getValue();
        name = name.substring(name.length() - 2, name.length());
        result = manager.findLikeName(name);
        Assert.assertTrue(0 < result.size());
        showItems(result);
        
        name = UUID.randomUUID().toString();
        result = manager.findLikeName(name);
        Assert.assertTrue(0 == result.size());
    }

}
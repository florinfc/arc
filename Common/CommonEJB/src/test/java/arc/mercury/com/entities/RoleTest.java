package arc.mercury.com.entities;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.junit.After;
import org.junit.Before;

import arc.mercury.com.generators.UserCollection;

import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;

public class RoleTest extends BaseEntityFixture<Role> {
	private static final Generator<?>[] SPECIAL_GENERATORS = {

	UserCollection.instance(),

	};

	public RoleTest() {
		super(Role.class, SPECIAL_GENERATORS);
	}

	public RoleTest(Generator<?>[] SPECIAL_GENERATORS) {
		super(Role.class, SPECIAL_GENERATORS);
	}

	@PersistenceUnit(unitName = "cubrid")
	private EntityManagerFactory entityManagerFactory;

	protected void Clear() {
		// to do later!
		// EntityManager em = getEntityManager();
		EntityManager em = (EntityManager) this;
		em.clear();
		em.close();
	}

	// RoleTest rolPtTest;

	@Override
	@Before
	public void setUp() throws Exception {
		// rolPtTest = new RoleTest(SPECIAL_GENERATORS);
		super.setUp();
		// EntityManager em = (EntityManager) this;
		// sau
		// EntityManager em = entityManagerFactory.createEntityManager();
	}

	@Override
	@After
	public void tearDown() {
	}
}
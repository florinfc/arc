package arc.mercury.com.managers;

import arc.mercury.com.entities.Role;
import arc.mercury.com.entities.Team;
import arc.mercury.com.entities.User;
import com.bm.testsuite.BaseSessionBeanFixture;
import com.bm.testsuite.dataloader.CSVInitialDataSet;
import com.bm.testsuite.dataloader.DateFormats;
import junit.framework.Assert;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;

import javax.persistence.EntityManager;
import java.util.List;

public class TeamManagerTest extends BaseSessionBeanFixture<TeamManager> {
    private final Logger log = Logger.getLogger(TeamManagerTest.class);
    private static final Class<?>[] beans = {

            User.class, Team.class,
    };
    private static CSVInitialDataSet<?>[] data = {

            new CSVInitialDataSet<User>(User.class, "Csv/User.csv", "id",
                    "name", "password", "mail", "fname", "lname"),
            new CSVInitialDataSet<Team>(Team.class, "Csv/Team.csv", "id",
                    "name"),

    };

    static {
        for (int i = 0; i < data.length; ++i) {
            DateFormats.USER_DATE.setUserDefinedFomatter("MM-dd-yyyy");
            data[i].addDateFormat(DateFormats.USER_DATE);
        }
    }

    private TeamManager manager;

    public TeamManagerTest() {
        super(TeamManager.class, beans, data);
    }

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        this.manager = this.getBeanToTest();
        Assert.assertNotNull(manager);
    }

    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    private <T> void showItems(List<T> items) {
        log.info(getName() + "\t : records = " + items.size());
        for (T item : items) {
            log.info(item.toString());
        }
    }

}
package arc.mercury.com.entities;

import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;

public class MailTest extends BaseEntityFixture<Mail> {
	private static final Generator<?>[] SPECIAL_GENERATORS = {
	};

	public MailTest() {
		super(Mail.class, SPECIAL_GENERATORS);
	}
}

package arc.mercury.com.generators;


import arc.mercury.com.entities.User;
import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.SingleBeanGenerator;

/**
 * Generator for one User record
 */
@GeneratorType(className = User.class, fieldType = FieldType.ALL_TYPES)
public final class UserRecord extends SingleBeanGenerator<User> {

	public UserRecord() {
		super(User.class);
	}

	public final static UserRecord instance() {
		return new UserRecord();
	}
}

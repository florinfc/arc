package arc.mercury.com.generators;

import arc.mercury.com.entities.Mail;
import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

/**
 * Generator for more Mail records
 */
@GeneratorType(className = Mail.class, fieldType = FieldType.ALL_TYPES)
public final class MailCollection extends BeanCollectionGenerator<Mail> {

	public MailCollection() {
		super(Mail.class, 5);
	}

	public final static MailCollection instance() {
		return new MailCollection();
	}
}

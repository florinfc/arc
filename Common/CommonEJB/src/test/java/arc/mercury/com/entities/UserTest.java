package arc.mercury.com.entities;

import org.junit.After;
import org.junit.Before;

import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;

public class UserTest extends BaseEntityFixture<User> {
	private static final Generator<?>[] SPECIAL_GENERATORS = {

	// UserCollection.instance(),
	// RoleRecord.instance(), TeamRecord.instance()

	};

	public UserTest() {
		super(User.class, SPECIAL_GENERATORS);
	}

	@Override
	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@Override
	@After
	public void tearDown() {
		// to do later
	}

}
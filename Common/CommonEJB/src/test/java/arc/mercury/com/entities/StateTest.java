package arc.mercury.com.entities;

import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;

public class StateTest extends BaseEntityFixture<State> {
	public static final Generator<?>[] SPECIAL_GENERATORS = {
	};

	public StateTest() {
		super(State.class, SPECIAL_GENERATORS);
	}
}
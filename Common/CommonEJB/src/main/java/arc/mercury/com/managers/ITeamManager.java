package arc.mercury.com.managers;

import arc.mercury.com.entities.Team;

import javax.ejb.Local;
import javax.persistence.EntityManager;
import java.util.List;

@Local
public interface ITeamManager {

	public EntityManager getManager();

	public List<Team> findAll();

	public Team findById(Long id);

	public Team findByName(String name);

	public List<Team> findLikeName(String name);
}

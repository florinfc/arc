package arc.mercury.com.services;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.jboss.ws.api.annotation.WebContext;

import arc.mercury.com.base.Constants;
import arc.mercury.com.common.Address;
import arc.mercury.com.common.AnswerValue;
import arc.mercury.com.common.Board;
import arc.mercury.com.entities.Mail;
import arc.mercury.com.entities.State;
import arc.mercury.com.entities.User;
import arc.mercury.com.managers.IMailManager;
import arc.mercury.com.managers.IRoleManager;
import arc.mercury.com.managers.IStateManager;
import arc.mercury.com.managers.IUserManager;

import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.AcroFields.Item;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

@SuppressWarnings("serial")
@Stateless
@Remote(IDashServices.class)
@WebService(targetNamespace = "http://dash.server:8081/")
@WebContext(contextRoot = "/common", urlPattern = "/services")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public class DashServices implements IDashServices {

	Logger log = Logger.getLogger("DashServices");
	@EJB(mappedName = "java:module/UserManager")
	private IUserManager uManager;
	@EJB(mappedName = "java:module/RoleManager")
	private IRoleManager rManager;
	@EJB(mappedName = "java:module/StateManager")
	private IStateManager sManager;
	@EJB(mappedName = "java:module/MailManager")
	private IMailManager mManager;

	public DashServices() {
	}

	@WebMethod
	public List<User> getAllUsers() {
		List<User> result = new ArrayList<User>();
		List<User> items = uManager.findAll();
		for (User item : items) {
			result.add(item);
		}
		return result;
	}

	@WebMethod
	public List<User> getAnonymous() {
		return uManager.findAnonymous();
	}

	@WebMethod
	public User getUserById(Long id) {
		return uManager.findById(id);
	}

	@WebMethod
	public User getUserByName(String name) {
		User user = uManager.findByName(name);
		return user;
	}

	@WebMethod
	public User getUserByMail(String mail) {
		return uManager.findByMail(mail);
	}

	@WebMethod
	public User saveUser(User user) {
		User newuser = uManager.update(user);
		return newuser;
	}

	@WebMethod
	public void deleteUser(User user) {
		uManager.delete(user);
	}

	@Override
	@WebMethod
	public List<State> getStates() {
		List<State> result = new ArrayList<State>();
		List<State> items = sManager.findAll();
		for (State item : items) {
			result.add(item);
		}
		return result;
	}

	@Override
	@WebMethod
	public State getStateById(Long id) {
		return sManager.findById(id);
	}

	@WebMethod
	public Mail getMailByName(String name) {
		return mManager.findByName(name);
	}

	@Override
	public List<Mail> getMailByStateId(Long stateId) {
		return mManager.findByStateId(stateId);
	}

	@WebMethod
	public String sendSimpleMail(User to, String subject, String[] bodyLines,
			String[] attFiles) {
		User[] toList = new User[1];
		toList[0] = to;
		User[] ccList = new User[1];
		User admin = uManager.findByName("florin");
		ccList[0] = admin;
		return sendMail(null, toList, ccList, null, subject, bodyLines,
				attFiles);
	}

	@WebMethod
	public String sendMail(User from, User[] toList, User[] ccList,
			User[] bccList, String subject, String[] bodyLines,
			String[] attFiles) {

		if (from == null) {
			from = new User();
			from.setMail("testing@ez-inc.org");
			from.setFname("EasyInc");
			from.setLname("");
		}
		if (toList == null || toList.length == 0) {
			return "TO: may not be null or empty";
		}
		String result = "NOK: unknown!";
		try {
			String local = InetAddress.getLocalHost().getCanonicalHostName();
			Properties props = System.getProperties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", "mail.ez-inc.org");
			props.put("mail.smtp.localhost", local);
			props.put("mail.smtp.starttls.enable", "false");
			props.put("mail.smtp.auth", "true");
			props.put("mail.debug", "false");
			Session session = Session.getInstance(props, new Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication("testing@ez-inc.org",
							"WEN.{Dv2-OPw");
				}
			});

			MimeMessage msg = new MimeMessage(session);
			msg.setHeader("Content-Type", "text/html; charset=utf-8");
			msg.setHeader("Content-Transfer-Encoding", "7bit");
			msg.setSentDate(new Date());
			msg.setFrom(new InternetAddress(from.getMail(), from.getFullName()));
			for (User to : toList) {
				msg.addRecipient(Message.RecipientType.TO, new InternetAddress(
						to.getMail(), to.getFullName()));
			}
			if (ccList != null) {
				for (User cc : ccList) {
					msg.addRecipient(Message.RecipientType.CC,
							new InternetAddress(cc.getMail(), cc.getFullName()));
				}
			}
			if (bccList != null) {
				for (User bcc : bccList) {
					msg.addRecipient(
							Message.RecipientType.BCC,
							new InternetAddress(bcc.getMail(), bcc
									.getFullName()));
				}
			}
			if (subject == null) {
				subject = "";
			}
			msg.setSubject(subject);

			Multipart master = new MimeMultipart();
			MimeBodyPart bdy = new MimeBodyPart();
			StringBuffer sb = new StringBuffer();
			if (bodyLines == null) {
				bodyLines = new String[0];
			}
			for (String line : bodyLines) {
				sb.append("<p>" + line + "<p/>");
			}
			bdy.setContent(sb.toString(), "text/html");
			master.addBodyPart(bdy);

			if (attFiles != null) {
				for (String file : attFiles) {
					MimeBodyPart att = new MimeBodyPart();
					// JavaMail 1.3
					// DataSource source = new
					// FileDataSource(file);
					// att.setDataHandler(new DataHandler(source));
					// att.setFileName(file.getName());
					// // att.setContent(att, "application/pdf");
					// // att.setDisposition(MimePart.ATTACHMENT);

					// JavaMail 1.4
					att.attachFile(file);
					master.addBodyPart(att);
				}
			}

			msg.setContent(master);
			msg.saveChanges();
			Transport.send(msg);
			result = "Sent: message with " + bodyLines.length
					+ " line(s), and "
					+ ((attFiles == null) ? 0 : attFiles.length)
					+ " attachment(s).";
		} catch (Exception e) {
			log.severe(e.toString());
			result = "NOK: " + e.toString();
		}
		return result;
	}

	private void fillAddress(Map<String, String> map, Address a, String prefix) {
		String adr = a.getAddress1().toUpperCase();
		if (!(a.getAddress2().equals(Constants.defAddress2))) {
			adr += " " + a.getAddress2().toUpperCase();
		}
		map.put(prefix + "Address", adr);
		map.put(prefix + "City", a.getCity().toUpperCase());
		String sCode = "";
		try {
			Long sid = Long.valueOf(a.getState());
			State s = sManager.findById(sid);
			if (s != null) {
				sCode = s.getCode();
			}
		} catch (NumberFormatException e) {
			sCode = a.getState();
		}
		map.put(prefix + "State", sCode.toUpperCase());
		map.put(prefix + "Zip", a.getZip().toUpperCase());
	}

	@WebMethod
	public String fillPdf(String formPath, String formName, AnswerValue com,
			String userName) {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("comName", com.getName1());
		if (com.getHelpASP() == 0) {
			map.put("agName", com.getAgName());
			if (com.getAgRegistered() < 0) {
				fillAddress(map, com.getAgAddress(), "ag");
			}
		} else {
			map.put("agName", "ARC MERCURY");
			map.put("agAddress", "1011 Brioso Drive Suite 107");
			map.put("agCity", "Costa Mesa");
			map.put("agState", "CA");
			map.put("agZip", "92627");
		}
		if (!com.getAddress().isEmpty()) {
			if (com.getAddress().get(0) != null) {
				fillAddress(map, com.getAddress().get(0).getAddress(), "init");
			}
			if (com.getMailDiff() != null && com.getMailDiff()) {
				if (com.getAddress().get(1) != null) {
					fillAddress(map, com.getAddress().get(1).getAddress(),
							"mail");
				}
			}
			if (com.getPhysDiff() != null && com.getPhysDiff()) {
				if (com.getAddress().get(3) != null) {
					fillAddress(map, com.getAddress().get(3).getAddress(),
							"front");
				}
			}
		}
		if (com.getBoard() != null) {
			Board b = com.getBoard().get(0);
			if (b != null) {
				map.put("manName", b.getFullName());
				fillAddress(map, b.getIdent().getAddress(), "man");
			}
		}
		if (com.getNbShares() != null) {
			map.put("comShares", com.getNbShares().toString());
		}
		if (com.getNbOwners() != null) {
			map.put("comOwners", com.getNbOwners().toString());
		}
		map.put("comPurpose", com.getPurposeName());
		if (com.getNonProfitType() != null) {
			if (com.getNonProfitType() == Constants.Public) {
				if (com.getPublicBenefit() == 1) {
					map.put("Public", "On");
				}
				if (com.getPublicBenefit() == 2) {
					map.put("Charitable", "On");
				}
			}
		}
		if (com.getManaged() != null) {
			Integer m = com.getManaged();
			switch (m) {
			case 1:
				map.put("mOne", "On");
				break;
			case 2:
				map.put("mMore", "On");
				break;
			case 3:
				map.put("mAll", "On");
				break;
			}

		}
		map.put("userName", userName);
		return createPdf(formPath, formName, map);
	}

	@WebMethod
	public String createPdf(String formPath, String formName,
			HashMap<String, String> map) {
		String fontType = map.get("font");
		if (fontType == null || fontType.isEmpty()) {
			fontType = BaseFont.COURIER_BOLD;
		}
		String fontSize = map.get("fontSize");
		if (fontSize == null || fontSize.isEmpty()) {
			fontSize = "10";
		}
		String sep = System.getProperty("file.separator");
		if (sep.equals("\\")) {
			formPath = "C:" + formPath;
		}
		String formFullName = formPath + System.getProperty("file.separator")
				+ "PDF-in" + System.getProperty("file.separator") + formName;
		String fileFullName = formPath + System.getProperty("file.separator")
				+ "PDF-out" + System.getProperty("file.separator") + formName;
		OutputStream writer = null;
		try {
			writer = new FileOutputStream(fileFullName);
			PdfReader reader = new PdfReader(formFullName);
			PdfStamper ps = new PdfStamper(reader, writer);
			AcroFields form = ps.getAcroFields();
			BaseFont font = BaseFont.createFont(fontType, BaseFont.CP1250,
					BaseFont.EMBEDDED);
			form.addSubstitutionFont(font);
			Map<String, Item> keys = form.getFields();
			for (String key : keys.keySet()) {
				form.setFieldProperty(key, "textfont", font, null);
				form.setFieldProperty(key, "textsize", new Float(fontSize),
						null);
				if (!key.equals("agState")) {
					form.setFieldProperty(key, "bgcolor", null, null);
				}
				if (key.equals("comOwners")) {
					form.setFieldProperty(key, "align", "center", null);
				}
				if (key.equals("comShares")) {
					form.setFieldProperty(key, "align", "center", null);
				}
				String value = map.get(key);
				switch (form.getFieldType(key)) {
				case AcroFields.FIELD_TYPE_CHECKBOX:
				case AcroFields.FIELD_TYPE_RADIOBUTTON:
					String[] values = form.getAppearanceStates(key);
					for (int x = 0; x < values.length; ++x) {
						value = map.get(values[x]);
						if ("On".equals(value)) {
							form.setField(key, values[x]);
							break;
						}
					}
					break;
				case AcroFields.FIELD_TYPE_COMBO:
				case AcroFields.FIELD_TYPE_LIST:
				case AcroFields.FIELD_TYPE_NONE:
				case AcroFields.FIELD_TYPE_PUSHBUTTON:
				case AcroFields.FIELD_TYPE_SIGNATURE:
				case AcroFields.FIELD_TYPE_TEXT:
				default:
					if (value != null) {
						form.setField(key, value.toUpperCase());
					}
				}
			}
			ps.setFormFlattening(true);
			ps.close();
			writer.close();
			reader.close();
		} catch (Exception e) {
			log.severe(e.toString());
			return null;
		}
		return fileFullName;
	}

}

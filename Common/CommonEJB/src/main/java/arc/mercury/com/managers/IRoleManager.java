package arc.mercury.com.managers;

import java.util.List;

import javax.ejb.Local;
import javax.persistence.EntityManager;

import arc.mercury.com.entities.Role;

@Local
public interface IRoleManager {

	public EntityManager getManager();

	public List<Role> findAll();

	public Role findById(Long id);

	public Role findByName(String name);

	public List<Role> findLikeName(String name);
}

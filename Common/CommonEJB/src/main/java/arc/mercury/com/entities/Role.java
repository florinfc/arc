package arc.mercury.com.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import arc.mercury.com.base.BaseClass;
import arc.mercury.com.base.Constants;

@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "TRole")
@NamedQueries({

		@NamedQuery(name = "Role.findAll", query = "select item from Role item order by item.id"),
		@NamedQuery(name = "Role.findById", query = "select item from Role item where item.id = :param"),
		@NamedQuery(name = "Role.findByName", query = "select item from Role item where item.name = :param"),
		@NamedQuery(name = "Role.findLikeName", query = "select item from Role item where item.name like :param order by item.name")

})
@SuppressWarnings("serial")
public class Role extends BaseClass<Role> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "name", length = Constants.MAXLEN)
	private String name;

	// @ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)
	// private List<User> users;

	public Role() {
		super();
	}

}

package arc.mercury.com.managers;

import arc.mercury.com.entities.Team;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Stateless
@SuppressWarnings("unchecked")
public class TeamManager implements ITeamManager {

	public TeamManager() {
	}

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;

	/**
	 * {@inheritDoc}
	 */
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Team> findAll() {
		List<Team> result = null;
		Query query = getManager().createNamedQuery("Team.findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Team>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public Team findById(Long id) {
		if (id == null) {
			return null;
		}
		return (Team) getManager().find(Team.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	public Team findByName(String name) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		Team result = null;
		Query query = getManager().createNamedQuery("Team.findByName");
		query.setParameter("param", name);
		try {
			result = (Team) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Team> findLikeName(String name) {
		if (name == null || name.isEmpty()) {
			return new ArrayList<Team>(0);
		}
		List<Team> result = null;
		Query query = getManager().createNamedQuery("Team.findLikeName");
		query.setParameter("param", "%" + name + "%");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Team>(0);
		}
		return result;
	}

}

@XmlSchema(
		xmlns = {}, namespace = "common", elementFormDefault = XmlNsForm.UNSET, attributeFormDefault = XmlNsForm.UNSET
) package arc.mercury.com.entities;

import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
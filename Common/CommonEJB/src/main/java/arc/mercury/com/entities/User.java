package arc.mercury.com.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import arc.mercury.com.base.BaseClass;
import arc.mercury.com.base.Constants;

@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "TUser")
@NamedQueries({

		@NamedQuery(name = "User.findAll", query = "select item from User item order by item.id"),
		@NamedQuery(name = "User.findById", query = "select item from User item where item.id = :param"),
		@NamedQuery(name = "User.findByName", query = "select item from User item where item.name = :param"),
		@NamedQuery(name = "User.findLikeName", query = "select item from User item where item.name like :param order by item.name"),
		@NamedQuery(name = "User.findByMail", query = "select item from User item where item.mail = :param")

})
@SuppressWarnings("serial")
public class User extends BaseClass<User> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "name", length = Constants.MAXLEN, unique = true, nullable = true)
	private String name;
	@Column(name = "password", length = Constants.MAXLEN)
	private String password;
	@Column(name = "mail", length = Constants.MAXLEN, unique = true, nullable = false)
	private String mail;
	@Column(name = "fname", length = Constants.MAXLEN)
	private String fname;
	@Column(name = "lname", length = Constants.MAXLEN)
	private String lname;
	@Column(name = "photo")
	private byte[] photo;
	// @ManyToMany(targetEntity = Role.class, fetch = FetchType.LAZY)
	// private List<Role> roles;
	// @ManyToMany(targetEntity = Team.class, fetch = FetchType.LAZY)
	// private List<Team> teams;
	@OneToOne(targetEntity = Role.class)
	@JoinColumn(name = "roleid")
	private Role role;
	@Transient
	private Team team;

	public User() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public String getFullName() {
		return getFname() + " " + getLname();
	}

}

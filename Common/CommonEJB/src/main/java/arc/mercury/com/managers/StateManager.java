package arc.mercury.com.managers;

import arc.mercury.com.entities.State;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@SuppressWarnings("unchecked")
public class StateManager implements IStateManager {

	public StateManager() {
	}

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;

	/**
	 * {@inheritDoc}
	 */
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<State> findAll() {
		List<State> result = null;
		Query query = getManager().createNamedQuery("State.findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<State>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public State findById(Long id) {
		if (id == null) {
			return null;
		}
		return (State) getManager().find(State.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	public State findByName(String name) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		State result = null;
		Query query = getManager().createNamedQuery("State.findByName");
		query.setParameter("param", name);
		try {
			result = (State) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<State> findLikeName(String name) {
		if (name == null || name.isEmpty()) {
			return new ArrayList<State>(0);
		}
		List<State> result = null;
		Query query = getManager().createNamedQuery("State.findLikeName");
		query.setParameter("param", "%" + name + "%");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<State>(0);
		}
		return result;
	}

}

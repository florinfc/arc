package arc.mercury.com.managers;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import arc.mercury.com.entities.Role;

@Stateless
@SuppressWarnings("unchecked")
public class RoleManager implements IRoleManager {

	public RoleManager() {
	}

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;

	/**
	 * {@inheritDoc}
	 */
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Role> findAll() {
		List<Role> result = null;
		Query query = getManager().createNamedQuery("Role.findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Role>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public Role findById(Long id) {
		if (id == null) {
			return null;
		}
		return (Role) getManager().find(Role.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	public Role findByName(String name) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		Role result = null;
		Query query = getManager().createNamedQuery("Role.findByName");
		query.setParameter("param", name);
		try {
			result = (Role) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Role> findLikeName(String name) {
		if (name == null || name.isEmpty()) {
			return new ArrayList<Role>(0);
		}
		List<Role> result = null;
		Query query = getManager().createNamedQuery("Role.findLikeName");
		query.setParameter("param", "%" + name + "%");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Role>(0);
		}
		return result;
	}

}

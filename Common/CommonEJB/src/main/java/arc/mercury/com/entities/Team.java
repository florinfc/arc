package arc.mercury.com.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import arc.mercury.com.base.BaseClass;

/**
 * Class Team User: florin Date: 29.08.2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "TTeam")
@NamedQueries({
		@NamedQuery(name = "Team.findAll", query = "select item from Team item order by item.id"),
		@NamedQuery(name = "Team.findById", query = "select item from Team item where item.id = :param"),
		@NamedQuery(name = "Team.findByName", query = "select item from Team item where item.name = :param"),
		@NamedQuery(name = "Team.findLikeName", query = "select item from Team item where item.name like :param order by item.name")

})
@SuppressWarnings("serial")
public class Team extends BaseClass<Team> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	protected Long id;
	@Column(name = "name")
	protected String name;

	// @ManyToMany(mappedBy = "teams", fetch = FetchType.LAZY)
	// private List<User> users;

	public Team() {
		super();
	}

}

package arc.mercury.com.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import arc.mercury.com.base.BaseClass;
import arc.mercury.com.base.Constants;

/**
 * Class State User: florin Date: 29.08.2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "TState")
@NamedQueries({

		@NamedQuery(name = "State.findAll", query = "select item from State item order by item.id"),
		@NamedQuery(name = "State.findById", query = "select item from State item where item.id = :param"),
		@NamedQuery(name = "State.findByName", query = "select item from State item where item.name = :param"),
		@NamedQuery(name = "State.findLikeName", query = "select item from State item where item.name like :param order by item.name")

})
@SuppressWarnings("serial")
public class State extends BaseClass<State> {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "name", length = Constants.MAXLEN)
	private String name;
	@Column(name = "code")
	private String code;
	@Column(name = "active")
	private Integer active;
	@Column(name = "minWage")
	private Float minWage;

	public State() {
		super();
	}

	public Float getMinWage() {
		return minWage;
	}

	public void setMinWage(Float minWage) {
		this.minWage = minWage;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}
}

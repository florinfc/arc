package arc.mercury.com.managers;

import java.util.List;

import javax.ejb.Local;
import javax.persistence.EntityManager;

import arc.mercury.com.entities.Mail;

@Local
public interface IMailManager {
	
	public EntityManager getManager();

	/**
	 * Find all the mails in the system
	 * 
	 * @return List of mail
	 */
	public List<Mail> findAll();

	/**
	 * Find mail by id
	 * 
	 * @param id
	 * @return Mail
	 */
	public Mail findById(Long id);

	/**
	 * Find mail by name (exact match)
	 * 
	 * @param name
	 * @return Mail
	 */
	public Mail findByName(String name);

	/**
	 * Find all mails that contains a pattern in the name
	 * 
	 * @param pattern
	 * @return List of mails
	 */
	public List<Mail> findLikeName(String name);
	
	/**
	 * Find all mails that have a certain state id
	 * 
	 * @param stateId
	 * @return Mail
	 */
	public List<Mail> findByStateId(Long stateId);
	

}

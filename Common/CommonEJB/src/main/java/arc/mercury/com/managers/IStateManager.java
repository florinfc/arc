package arc.mercury.com.managers;

import arc.mercury.com.entities.State;

import java.util.List;

import javax.ejb.Local;
import javax.persistence.EntityManager;

@Local
public interface IStateManager {

	public EntityManager getManager();

	public List<State> findAll();

	public State findById(Long id);

	public State findByName(String name);

	public List<State> findLikeName(String name);

}

package arc.mercury.com.managers;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import arc.mercury.com.entities.User;

@Stateless
@SuppressWarnings("unchecked")
public class UserManager implements IUserManager {

	public UserManager() {
	}

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;

	/**
	 * {@inheritDoc}
	 */
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	public User insert(User item) {
		if (item == null) {
			return null;
		}
		getManager().persist(item);
		return item;
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(User item) {
		if (item == null) {
			return;
		}
		item = getManager().find(User.class, item.getId());
		if (item != null) {
			getManager().remove(item);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Long id) {
		if (id == null) {
			return;
		}
		User item = getManager().getReference(User.class, id);
		if (item != null) {
			getManager().remove(item);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public User update(User item) {
		if (item == null) {
			return null;
		}
		item = getManager().merge(item);
		return item;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<User> findAll() {
		List<User> result = null;
		Query query = getManager().createNamedQuery("User.findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<User>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public User findById(Long id) {
		if (id == null) {
			return null;
		}
		return (User) getManager().find(User.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<User> findAnonymous() {
		List<User> result = null;
		Query query = getManager().createNamedQuery("User.findByName");
		query.setParameter("param", "");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<User>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public User findByName(String name) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		User result = null;
		Query query = getManager().createNamedQuery("User.findByName");
		query.setParameter("param", name);
		try {
			result = (User) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<User> findLikeName(String name) {
		if (name == null || name.isEmpty()) {
			return new ArrayList<User>(0);
		}
		List<User> result = null;
		Query query = getManager().createNamedQuery("User.findLikeName");
		query.setParameter("param", "%" + name + "%");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<User>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public User findByMail(String mail) {
		if (mail == null || mail.isEmpty()) {
			return null;
		}
		User result = null;
		Query query = getManager().createNamedQuery("User.findByMail");
		query.setParameter("param", mail);
		try {
			result = (User) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

}

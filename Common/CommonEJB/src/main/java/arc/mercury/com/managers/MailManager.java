package arc.mercury.com.managers;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import arc.mercury.com.entities.Mail;


@Stateless
@SuppressWarnings("unchecked")
public class MailManager implements IMailManager{
	
	public MailManager() {
	}

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;
	
	/**
	 * {@inheritDoc}
	 */
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Mail> findAll()	{
		List<Mail> result = null;
		Query query = getManager().createNamedQuery("Mail.findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Mail>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public Mail findById(Long id)	{
		if (id == null) {
			return null;
		}
		return (Mail) getManager().find(Mail.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	public Mail findByName(String name)	{
		if (name == null || name.isEmpty()) {
			return null;
		}
		Mail result = null;
		Query query = getManager().createNamedQuery("Mail.findByName");
		query.setParameter("param", name);
		try {
			result = (Mail) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Mail> findLikeName(String name)	{
		if (name == null || name.isEmpty()) {
			return new ArrayList<Mail>(0);
		}
		List<Mail> result = null;
		Query query = getManager().createNamedQuery("Mail.findLikeName");
		query.setParameter("param", "%" + name + "%");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Mail>(0);
		}
		return result;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<Mail> findByStateId(Long stateId) {
		if (stateId == null) {
			return null;
		}
		List<Mail> result = null;
		Query query = getManager().createNamedQuery("Mail.findByStateId");
		query.setParameter("param", stateId);
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Mail>(0);
		}
		return result;
	}
}

package arc.mercury.com.services;

import arc.mercury.com.common.AnswerValue;
import arc.mercury.com.entities.Mail;
import arc.mercury.com.entities.State;
import arc.mercury.com.entities.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

@WebService(targetNamespace = "http://dash.server:8081/")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public interface IDashServices extends Serializable {

	@WebMethod
	public List<User> getAllUsers();

	@WebMethod
	public List<User> getAnonymous();

	@WebMethod
	public User getUserById(Long id);

	@WebMethod
	public User getUserByName(String name);

	@WebMethod
	public User getUserByMail(String mail);

	@WebMethod
	public User saveUser(User user);

	@WebMethod
	public void deleteUser(User user);

	@WebMethod
	public List<State> getStates();

	@WebMethod
	public State getStateById(Long id);

	@WebMethod
	public List<Mail> getMailByStateId(Long stateId);

	@WebMethod
	public Mail getMailByName(String name);

	@WebMethod
	public String sendSimpleMail(User to, String subject, String[] bodyLines,
								 String[] attFiles);

	@WebMethod
	public String sendMail(@WebParam(name = "from") User from,
						   @WebParam(name = "toList") User[] toList,
						   @WebParam(name = "ccList") User[] ccList,
						   @WebParam(name = "bccList") User[] bccList,
						   @WebParam(name = "subject") String subject,
						   @WebParam(name = "bodyLines") String[] bodyLines,
						   @WebParam(name = "attFiles") String[] attFiles);

	@WebMethod
	public String fillPdf(@WebParam(name = "formPath") String formPath,
						  @WebParam(name = "formName") String formName,
						  @WebParam(name = "com") AnswerValue com,
						  @WebParam(name = "userName") String userName) throws Exception;

	@WebMethod
	public String createPdf(@WebParam(name = "formPath") String formPath,
							@WebParam(name = "formName") String formName,
							@WebParam(name = "map") HashMap<String, String> map) throws Exception;

}

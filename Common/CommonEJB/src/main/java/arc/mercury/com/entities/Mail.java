package arc.mercury.com.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import arc.mercury.com.base.BaseClass;
import arc.mercury.com.base.Constants;

@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "TMail")
@NamedQueries({

		@NamedQuery(name = "Mail.findAll", query = "select item from Mail item order by item.id"),
		@NamedQuery(name = "Mail.findById", query = "select item from Mail item where item.id = :param"),
		@NamedQuery(name = "Mail.findByName", query = "select item from Mail item where item.name = :param"),
		@NamedQuery(name = "Mail.findLikeName", query = "select item from Mail item where item.name like :param order by item.name"),
		@NamedQuery(name = "Mail.findByStateId", query = "select item from Mail item where item.stateId = :param")

})
@SuppressWarnings("serial")
public class Mail extends BaseClass<Mail> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "name", length = Constants.MAXLEN)
	private String name;
	@Column(name = "stateId")
	private Long stateId;
	@Column(name = "subject")
	private String subject;
	@Column(name = "content")
	private String content;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getStateId() {
		return stateId;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}

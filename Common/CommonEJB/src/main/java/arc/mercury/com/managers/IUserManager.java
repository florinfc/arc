package arc.mercury.com.managers;

import java.util.List;

import javax.ejb.Local;
import javax.persistence.EntityManager;

import arc.mercury.com.entities.User;

@Local
public interface IUserManager {

	public EntityManager getManager();

	/**
	 * Insert an user item in the system
	 * 
	 * @param item
	 * @return new user
	 */
	public User insert(User item);

	/**
	 * Update an user item in the system
	 * 
	 * @param item
	 * @return new user
	 */
	public User update(User item);

	/**
	 * Delete an user item from the system
	 * 
	 * @param item
	 * @return
	 */
	public void delete(User item);

	/**
	 * Delete an user item by id from the system
	 * 
	 * @param id
	 * @return
	 */
	public void delete(Long id);

	/**
	 * Find all the users in the system
	 * 
	 * @return List of users
	 */
	public List<User> findAll();

	/**
	 * Find user by id
	 * 
	 * @param id
	 * @return User
	 */
	public User findById(Long id);

	/**
	 * Find anonymous users
	 * 
	 * @return List of users
	 */
	public List<User> findAnonymous();

	/**
	 * Find user by name (exact match)
	 * 
	 * @param name
	 * @return User
	 */
	public User findByName(String name);

	/**
	 * Find all users that contains a pattern in the name
	 * 
	 * @param pattern
	 * @return List of users
	 */
	public List<User> findLikeName(String name);

	/**
	 * Find user by e-mail
	 * 
	 * @param mail
	 * @return User
	 */
	public User findByMail(String mail);

}

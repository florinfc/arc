package arc.mercury.com.enums;

public enum BranchType {

	None(0), InternalHub(1), InternalSat(2), Franchise(3), JVHub(4);
	private Integer value;

	BranchType(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}

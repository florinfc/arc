package arc.mercury.com.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

public class Config {

	private static final Logger log = Logger.getLogger("Config");

	private static String FileName = "cubrid.properties";
	private final Properties properties = new Properties();

	public Config() {
		InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream(FileName);
		if (in == null) {
			log.warning("Properties file '" + FileName
					+ "' not found; defaults will apply!");
		} else {
			try {
				properties.load(in);
				in.close();
			} catch (IOException e) {
				log.severe(e.toString());
			} catch (Exception e) {
				log.severe(e.toString());
			}
		}
	}

	public Properties getProperties() {
		return properties;
	}

	public String getProperty(String key) {
		return properties.getProperty(key);
	}

	public String getTimestampFormat() {
		return properties.getProperty("timestampFormat",
				"yyyy-MM-dd HH:mm:ss.SSS");
	}

	public String getDatetimeFormat() {
		return properties.getProperty("datetimeFormat", "yyyy-MM-dd HH:mm:ss");
	}

	public String getDateFormat() {
		return properties.getProperty("dateFormat", "yyyy-MM-dd");
	}

	public String getTimeFormat() {
		return properties.getProperty("timeFormat", "HH:mm:ss");
	}

	public String getFieldSeparator() {
		return properties.getProperty("fieldSeparator",
				System.getProperty("line.separator"));
	}

	public String getLineSeparator() {
		return properties.getProperty("lineSeparator",
				System.getProperty("line.separator"));
	}

	public String getFileName() {
		return FileName;
	}

	public void setFileName(String fileName) {
		FileName = fileName;
	}

	static final long serialVersionUID = 5673040466066010365L;
}

package arc.mercury.com.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import arc.mercury.com.base.BaseClass;

@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class ITeam extends BaseClass<ITeam> {

	private Long id;
	private String name;

	// @XmlTransient
	// private List<IUser> users;

	public ITeam() {
		this.id = 0L;
		this.name = "none";
	}

	public ITeam(String name) {
		this.name = name;
	}

	public ITeam(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

package arc.mercury.com.base;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectReader;
import org.codehaus.jackson.map.ObjectWriter;

public class BaseJson {

	public static String toJson(Object obj) {
		if (obj instanceof String) {
			return (String) obj;
		}
		ObjectWriter ow = new ObjectMapper().writer().withType(obj.getClass());
		String json = "";
		try {
			json = ow.writeValueAsString(obj);
		} catch (Exception e) {
			json = obj.toString();
		}
		return json;
	}

	public static Object toObject(String json, Class<?> clazz) {
		if (json == null) {
			return null;
		}
		ObjectReader or = new ObjectMapper().reader().withType(clazz);
		// ObjectMapper or = new ObjectMapper();
		Object obj = null;
		try {
			obj = or.readValue(json);
		} catch (Exception e) {
			obj = null;
		}
		return obj;
	}

}

package arc.mercury.com.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import arc.mercury.com.base.BaseClass;

@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class IRole extends BaseClass<IRole> {

	private Long id;
	private String name;

	// @XmlTransient
	// private List<IUser> users;

	public IRole() {
		this.id = 0L;
		this.name = "anonymous";
	}

	public IRole(String name) {
		this.name = name;
	}

	public IRole(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

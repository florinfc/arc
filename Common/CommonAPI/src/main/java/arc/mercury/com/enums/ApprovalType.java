package arc.mercury.com.enums;

public enum ApprovalType {

	No(0), Yes(1), YesAmount(2), YesPercent(3);
	private Integer value;

	ApprovalType(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}

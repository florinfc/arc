package arc.mercury.com.entities;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import arc.mercury.com.base.BaseClass;

@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class IQuestion extends BaseClass<IQuestion> {

	private Long id;
	private String name;
	private IPage page;
	private Integer ord;
	private String label;
	private String pattern;
	private String hint;
	private String defaultValue;
	private List<IRestrict> restricts;

	private IAnswer answer;
	@XmlTransient
	private Boolean show = true;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getShow() {
		if (show == null) {
			show = true;
		}
		return show;
	}

	public void setShow(Boolean show) {
		this.show = show;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getHint() {
		return hint;
	}

	public void setHint(String hint) {
		this.hint = hint;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public Integer getOrd() {
		return ord;
	}

	public void setOrd(Integer ord) {
		this.ord = ord;
	}

	public IPage getPage() {
		return page;
	}

	public void setPage(IPage page) {
		this.page = page;
	}

	public List<IRestrict> getRestricts() {
		if (restricts == null) {
			restricts = new ArrayList<IRestrict>(0);
		}
		return restricts;
	}

	public void setRestricts(List<IRestrict> restricts) {
		this.restricts = restricts;
	}

	public IAnswer getAnswer() {
		return answer;
	}

	public void setAnswer(IAnswer answer) {
		this.answer = answer;
	}

	// public boolean isText() {
	// return this.viewType.equals(ViewType.Text.getValue());
	// }
	//
	// public boolean isCombo() {
	// return this.viewType.equals(ViewType.Combo.getValue());
	// }
	//
	// public boolean isCheck() {
	// return this.viewType.equals(ViewType.Check.getValue());
	// }
	//
	// public boolean isRadio() {
	// return this.viewType.equals(ViewType.Radio.getValue());
	// }
	//
	// public boolean isButton() {
	// return this.viewType.equals(ViewType.Button.getValue());
	// }
	//
	// public boolean isOutput() {
	// return this.viewType.equals(ViewType.Label.getValue());
	// }

}

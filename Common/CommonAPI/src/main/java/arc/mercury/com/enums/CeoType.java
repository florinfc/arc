package arc.mercury.com.enums;

public enum CeoType {

	President("President"), CEO("CEO");

	private String value;

	CeoType(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
}

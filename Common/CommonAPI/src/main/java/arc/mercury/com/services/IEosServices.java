package arc.mercury.com.services;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import arc.mercury.com.types.ListItem;

@WebService(wsdlLocation = "http://eos.server:8082/EOS.asmx?wsdl", targetNamespace = "http://arcmercury.com/websevices", serviceName = "EOS", portName = "EOSsoap")
// @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use =
// SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public interface IEosServices {

	public static class Client {
		public Integer code;
		public String name;

		public Integer getCode() {
			return code;
		}

		public void setCode(Integer code) {
			this.code = code;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

	}

	@WebMethod
	public Object GetAllClientsForUser(@WebParam(name = "Key") String key,
			@WebParam(name = "UserID") Integer uid);

	@WebMethod
	public String getObject(String key, int classID, int parentID,
			int objectID, int userID);

	@WebMethod
	public void insertObject(String key, int classID, int parentID,
			int objectID, String attributes);

	@WebMethod
	public List<ListItem> getAllClientsForUser(String key, int userID);

}

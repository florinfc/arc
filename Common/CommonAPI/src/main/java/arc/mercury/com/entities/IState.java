package arc.mercury.com.entities;

import arc.mercury.com.base.BaseClass;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class IState extends BaseClass<IState> {

	private Long id;
	private String name;
	private String code;
	private Integer active;
	private Float minWage;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Float getMinWage() {
		return minWage;
	}

	public void setMinWage(Float minWage) {
		this.minWage = minWage;
	}
}

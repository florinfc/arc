package arc.mercury.com.common;

import arc.mercury.com.enums.SubType;

/**
 * Class SubContractor
 * User: florin
 * Date: 29.08.2013
 */
public class SubContractor {
	private Long companyId;
	private SubType type;
	private boolean t1099;
	private String ssn;

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public SubType getType() {
		return type;
	}

	public void setType(SubType type) {
		this.type = type;
	}

	public boolean isT1099() {
		return t1099;
	}

	public void setT1099(boolean t1099) {
		this.t1099 = t1099;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SubContractor that = (SubContractor) o;

		if (t1099 != that.t1099) return false;
		if (companyId != null ? !companyId.equals(that.companyId) : that.companyId != null) return false;
		if (ssn != null ? !ssn.equals(that.ssn) : that.ssn != null) return false;
		if (type != that.type) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = companyId != null ? companyId.hashCode() : 0;
		result = 31 * result + (type != null ? type.hashCode() : 0);
		result = 31 * result + (t1099 ? 1 : 0);
		result = 31 * result + (ssn != null ? ssn.hashCode() : 0);
		return result;
	}
}

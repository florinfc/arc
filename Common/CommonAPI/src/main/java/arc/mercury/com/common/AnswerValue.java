package arc.mercury.com.common;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import arc.mercury.com.base.BaseClass;
import arc.mercury.com.base.Constants;

@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class AnswerValue extends BaseClass<AnswerValue> {

	private Integer state = 0;
	private Integer needLicense = -1;
	private Integer LLC = 2;
	private Integer managed = 0;
	private Integer nonProfit = -1;
	private Integer nonProfitType = 1;
	private Integer publicBenefit = 2;
	private Integer purpose = 0;
	private String purposeName = null;
	private Integer benefit = 0;
	private Integer userBenefit = 0;
	private Integer homeOwner = 0;
	private Integer cid = 0;
	private Integer nbInterests = 1;
	private Integer nbOwners = 1;
	private Integer closeCorp = 0;
	private Integer closeMonth = 11;
	private Integer nbHired = 0;
	private Date startPay = new Date();
	private Integer taxYear;
	private Integer ss4 = 0;
	private String primProduct;
	private String primDesc = Constants.defPrimDesc;
	private String name1 = Constants.defName1;
	private String name2 = Constants.defName2;
	private String name3 = Constants.defName3;
	private Integer trade = 0;
	private String tradeName = Constants.defTradeName;
	private List<Identity> address;
	private String agName = Constants.defAgentName;
	private Integer agRegistered = -1;
	private Address agAddress;
	private Integer nbShares = 1000000;
	private Integer subscribe = 1;
	private List<Board> owners;
	private Integer married = 0;
	private Integer disregardLLC = 0;
	private List<Board> board;
	private Boolean hasPrevEIN = false;
	private String prevEIN = Constants.defPrevEIN;
	private Integer contact = 0;
	private Integer owner = 0;
	private String eidNo;
	private Address eidAddress;
	private Integer quarter = 0;
	private Integer relative = 0;
	private Integer naics = 0;
	private String other;
	private Integer fileBEID = 1;
	private Integer vehicle = 0;
	private Integer casino = 0;
	private Integer sell = 0;
	private Integer sellWhat = 0;
	private Integer sellStart = 0;
	private Integer sellInternet = 0;
	private Integer sales12months = 0;
	private Integer salesTaxable = 0;
	private Integer helpAOF = 1;
	private Integer helpEIN = 1;
	private Integer helpSOI = 1;
	private Integer helpEID = 1;
	private Integer helpRSN = 1;
	private Integer helpSUB = 1;
	private Integer helpASP = 1;
	private Integer helpBMM = 1;
	private Integer helpPCP = 1;
	private Integer helpWSP = 1;
	private Integer helpLCP = 1;
	private Integer helpBCP = 1;
	private Integer helpACC = 1;
	private Boolean mailDiff = false;
	private Boolean officeDiff = false;
	private Boolean physDiff = false;
	private Integer reason = 0;
	private String otherReason = Constants.defReason;
	private Integer form = 1;
	private Integer ownerEID = 0;
	private String CEID = Constants.defEID;
	private String businessName = Constants.defBusinessName;
	private Address businessAddress;
	private String prevOwnerName = Constants.defPrevOwner;
	private String prevBusinessName = Constants.defBusinessName;
	private Integer price;
	private String prevEID = Constants.defPrevEID;
	private Date transferDate;
	private List<Board> personal;
	private List<Board> suppliers;
	private String supplierContact = Constants.defSupplierContact;
	private String bankContact = Constants.defBankContact;
	private String bankName = Constants.defBankName;
	private Identity bankAddress;
	private Integer acceptCC = 0;
	private String processor = Constants.defProcName;
	private String account = Constants.defAccount;
	private Boolean agreement = false;
	private Boolean disclaimer = false;
	private Boolean boardCopied = false;
	private Boolean ownerCopied = false;

	public AnswerValue() {
		address = new ArrayList<Identity>(4);
		address.add(new Identity());
		address.add(new Identity());
		address.add(new Identity());
		address.add(new Identity());
		board = new ArrayList<Board>(3);
		board.add(new Board(1));
		board.add(new Board(2));
		board.add(new Board(3));
		owners = new ArrayList<Board>();
		owners.add(new Board());
		agAddress = new Address();
		bankAddress = new Identity();
		businessAddress = new Address();
		personal = new ArrayList<Board>(3);
		personal.add(new Board());
		personal.add(new Board());
		personal.add(new Board());
		suppliers = new ArrayList<Board>(3);
		suppliers.add(new Board());
		suppliers.add(new Board());
		suppliers.add(new Board());
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		taxYear = cal.get(Calendar.YEAR);
		eidAddress = new Address();
	}

	public boolean primDescIsEmpty() {
		return primDesc.equals(Constants.defPrimDesc);
	}

	public boolean name1IsEmpty() {
		return name1.equals(Constants.defName1);
	}

	public boolean name2IsEmpty() {
		return name2.equals(Constants.defName2);
	}

	public boolean name3IsEmpty() {
		return name3.equals(Constants.defName3);
	}

	public boolean tradeName1IsEmpty() {
		return tradeName.equals(Constants.defTradeName);
	}

	public boolean agentName1IsEmpty() {
		return agName.equals(Constants.defAgentName);
	}

	public boolean prevEIN1IsEmpty() {
		return prevEIN.equals(Constants.defPrevEIN);
	}

	public boolean eidIsEmpty() {
		return CEID.equals(Constants.defEID);
	}

	public boolean businessNameIsEmpty() {
		return businessName.equals(Constants.defBusinessName);
	}

	public boolean prevOwnerIsEmpty() {
		return prevOwnerName.equals(Constants.defPrevOwner);
	}

	public boolean prevBusinessIsEmpty() {
		return prevBusinessName.equals(Constants.defprevBusiness);
	}

	public boolean prevEIDIsEmpty() {
		return prevEID.equals(Constants.defPrevEID);
	}

	public boolean supplierContactIsEmpty() {
		return supplierContact.equals(Constants.defSupplierContact);
	}

	public boolean bankContactIsEmpty() {
		return bankContact.equals(Constants.defBankContact);
	}

	public boolean bankNameIsEmpty() {
		return bankName.equals(Constants.defBankName);
	}

	public boolean processorIsEmpty() {
		return processor.equals(Constants.defProcName);
	}

	public boolean accountIsEmpty() {
		return account.equals(Constants.defAccount);
	}

	public boolean comIsCID() {
		return (comIsNonProfit() && nonProfitType == Constants.CID);
	}

	public boolean comIsPublic() {
		return (comIsNonProfit() && nonProfitType == Constants.Public);
	}

	public boolean comIsMutual() {
		return (comIsNonProfit() && nonProfitType == Constants.Mutual);
	}

	public boolean comIsReligious() {
		return (comIsNonProfit() && nonProfitType == Constants.Religious);
	}

	public boolean comIsNonProfit() {
		return nonProfit == 1;
	}

	public String getName1() {
		return name1;
	}

	public void setName1(String name1) {
		this.name1 = name1;
	}

	public String getName2() {
		return name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}

	public String getName3() {
		return name3;
	}

	public void setName3(String name3) {
		this.name3 = name3;
	}

	public Integer getTrade() {
		return trade;
	}

	public void setTrade(Integer trade) {
		this.trade = trade;
	}

	public String getTradeName() {
		return tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	public List<Identity> getAddress() {
		return address;
	}

	public void setAddress(List<Identity> address) {
		this.address = address;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getNeedLicense() {
		return needLicense;
	}

	public void setNeedLicense(Integer needLicense) {
		this.needLicense = needLicense;
	}

	public Integer getLLC() {
		return LLC;
	}

	public void setLLC(Integer LLC) {
		this.LLC = LLC;
	}

	public Integer getManaged() {
		return managed;
	}

	public void setManaged(Integer managed) {
		this.managed = managed;
	}

	public Integer getNonProfit() {
		return nonProfit;
	}

	public void setNonProfit(Integer nonProfit) {
		this.nonProfit = nonProfit;
	}

	public Integer getPurpose() {
		return purpose;
	}

	public void setPurpose(Integer purpose) {
		this.purpose = purpose;
	}

	public Integer getPublicBenefit() {
		return publicBenefit;
	}

	public void setPublicBenefit(Integer publicBenefit) {
		this.publicBenefit = publicBenefit;
	}

	public Integer getBenefit() {
		return benefit;
	}

	public void setBenefit(Integer benefit) {
		this.benefit = benefit;
	}

	public Integer getUserBenefit() {
		return userBenefit;
	}

	public void setUserBenefit(Integer userBenefit) {
		this.userBenefit = userBenefit;
	}

	public Integer getHomeOwner() {
		return homeOwner;
	}

	public void setHomeOwner(Integer homeOwner) {
		this.homeOwner = homeOwner;
	}

	public Integer getCid() {
		return cid;
	}

	public void setCid(Integer cid) {
		this.cid = cid;
	}

	public Boolean getAgreement() {
		return agreement;
	}

	public void setAgreement(Boolean agreement) {
		this.agreement = agreement;
	}

	public Integer getNbInterests() {
		return nbInterests;
	}

	public void setNbInterests(Integer nbInterests) {
		this.nbInterests = nbInterests;
	}

	public Integer getNbOwners() {
		return nbOwners;
	}

	public void setNbOwners(Integer n) {
		this.nbOwners = n;
	}

	public Integer getCloseCorp() {
		return closeCorp;
	}

	public void setCloseCorp(Integer closeCorp) {
		this.closeCorp = closeCorp;
	}

	public Integer getCloseMonth() {
		return closeMonth;
	}

	public void setCloseMonth(Integer closeMonth) {
		this.closeMonth = closeMonth;
	}

	public void setCloseDay(Integer closeMonth) {
		this.closeMonth = closeMonth;
	}

	public Integer getNbHired() {
		return nbHired;
	}

	public void setNbHired(Integer nbHired) {
		this.nbHired = nbHired;
	}

	public Date getStartPay() {
		return startPay;
	}

	public void setStartPay(Date startPay) {
		this.startPay = startPay;
	}

	public Integer getSs4() {
		return ss4;
	}

	public void setSs4(Integer ss4) {
		this.ss4 = ss4;
	}

	public String getPrimProduct() {
		return primProduct;
	}

	public void setPrimProduct(String primProduct) {
		this.primProduct = primProduct;
	}

	public String getAgName() {
		return agName;
	}

	public void setAgName(String agName) {
		this.agName = agName;
	}

	public Integer getAgRegistered() {
		return agRegistered;
	}

	public void setAgRegistered(Integer agRegistered) {
		this.agRegistered = agRegistered;
	}

	public Address getAgAddress() {
		return agAddress;
	}

	public void setAgAddress(Address agAddress) {
		this.agAddress = agAddress;
	}

	public Integer getSubscribe() {
		return subscribe;
	}

	public void setSubscribe(Integer subscribe) {
		this.subscribe = subscribe;
	}

	public List<Board> getOwners() {
		return owners;
	}

	public void setOwners(List<Board> owners) {
		this.owners = owners;
	}

	public List<Board> getBoard() {
		return board;
	}

	public void setBoard(List<Board> board) {
		this.board = board;
	}

	public Integer getMarried() {
		return married;
	}

	public void setMarried(Integer married) {
		this.married = married;
	}

	public Integer getDisregardLLC() {
		return disregardLLC;
	}

	public void setDisregardLLC(Integer disregardLLC) {
		this.disregardLLC = disregardLLC;
	}

	public Integer getContact() {
		return contact;
	}

	public void setContact(Integer contact) {
		this.contact = contact;
	}

	public Integer getHelpEID() {
		return helpEID;
	}

	public void setHelpEID(Integer helpEID) {
		this.helpEID = helpEID;
	}

	public Integer getOwner() {
		return owner;
	}

	public void setOwner(Integer owner) {
		this.owner = owner;
	}

	public String getEidNo() {
		return eidNo;
	}

	public void setEidNo(String eidNo) {
		this.eidNo = eidNo;
	}

	public Address getEidAddress() {
		return eidAddress;
	}

	public void setEidAddress(Address eidAddress) {
		this.eidAddress = eidAddress;
	}

	public Integer getQuarter() {
		return quarter;
	}

	public void setQuarter(Integer quarter) {
		this.quarter = quarter;
	}

	public Integer getRelative() {
		return relative;
	}

	public void setRelative(Integer relative) {
		this.relative = relative;
	}

	public Integer getNaics() {
		return naics;
	}

	public void setNaics(Integer naics) {
		this.naics = naics;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public Integer getFileBEID() {
		return fileBEID;
	}

	public void setFileBEID(Integer fileBEID) {
		this.fileBEID = fileBEID;
	}

	public Integer getVehicle() {
		return vehicle;
	}

	public void setVehicle(Integer vehicle) {
		this.vehicle = vehicle;
	}

	public Integer getCasino() {
		return casino;
	}

	public void setCasino(Integer casino) {
		this.casino = casino;
	}

	public Integer getSell() {
		return sell;
	}

	public void setSell(Integer sell) {
		this.sell = sell;
	}

	public Integer getSellWhat() {
		return sellWhat;
	}

	public void setSellWhat(Integer sellWhat) {
		this.sellWhat = sellWhat;
	}

	public Integer getSellStart() {
		return sellStart;
	}

	public void setSellStart(Integer sellStart) {
		this.sellStart = sellStart;
	}

	public Integer getSellInternet() {
		return sellInternet;
	}

	public void setSellInternet(Integer sellInternet) {
		this.sellInternet = sellInternet;
	}

	public Integer getSales12months() {
		return sales12months;
	}

	public void setSales12months(Integer sales12months) {
		this.sales12months = sales12months;
	}

	public Integer getSalesTaxable() {
		return salesTaxable;
	}

	public void setSalesTaxable(Integer salesTaxable) {
		this.salesTaxable = salesTaxable;
	}

	public Integer getHelpACC() {
		return helpACC;
	}

	public void setHelpACC(Integer helpACC) {
		this.helpACC = helpACC;
	}

	public Integer getHelpSUB() {
		return helpSUB;
	}

	public void setHelpSUB(Integer helpSUB) {
		this.helpSUB = helpSUB;
	}

	public Integer getHelpSOI() {
		return helpSOI;
	}

	public void setHelpSOI(Integer helpSOI) {
		this.helpSOI = helpSOI;
	}

	public Boolean getMailDiff() {
		return mailDiff;
	}

	public void setMailDiff(Boolean mailDiff) {
		this.mailDiff = mailDiff;
	}

	public Integer getReason() {
		return reason;
	}

	public void setReason(Integer reason) {
		this.reason = reason;
	}

	public Boolean getHasPrevEIN() {
		return hasPrevEIN;
	}

	public void setHasPrevEIN(Boolean hasPrevEIN) {
		this.hasPrevEIN = hasPrevEIN;
	}

	public String getPrevEIN() {
		return prevEIN;
	}

	public void setPrevEIN(String prevEIN) {
		this.prevEIN = prevEIN;
	}

	public String getPrimDesc() {
		return primDesc;
	}

	public void setPrimDesc(String primDesc) {
		this.primDesc = primDesc;
	}

	public Integer getHelpEIN() {
		return helpEIN;
	}

	public void setHelpEIN(Integer helpEIN) {
		this.helpEIN = helpEIN;
	}

	public Integer getHelpRSN() {
		return helpRSN;
	}

	public void setHelpRSN(Integer helpRSN) {
		this.helpRSN = helpRSN;
	}

	public Integer getForm() {
		return form;
	}

	public void setForm(Integer form) {
		this.form = form;
	}

	public Integer getOwnerEID() {
		return ownerEID;
	}

	public void setOwnerEID(Integer ownerEID) {
		this.ownerEID = ownerEID;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public Address getBusinessAddress() {
		return businessAddress;
	}

	public void setBusinessAddress(Address businessAddress) {
		this.businessAddress = businessAddress;
	}

	public String getPrevOwnerName() {
		return prevOwnerName;
	}

	public void setPrevOwnerName(String prevOwnerName) {
		this.prevOwnerName = prevOwnerName;
	}

	public String getPrevBusinessName() {
		return prevBusinessName;
	}

	public void setPrevBusinessName(String prevBusinessName) {
		this.prevBusinessName = prevBusinessName;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public String getPrevEID() {
		return prevEID;
	}

	public void setPrevEID(String prevEID) {
		this.prevEID = prevEID;
	}

	public Date getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}

	public List<Board> getPersonal() {
		return personal;
	}

	public void setPersonal(List<Board> personal) {
		this.personal = personal;
	}

	public List<Board> getSuppliers() {
		return suppliers;
	}

	public void setSuppliers(List<Board> suppliers) {
		this.suppliers = suppliers;
	}

	public String getSupplierContact() {
		return supplierContact;
	}

	public void setSupplierContact(String supplierContact) {
		this.supplierContact = supplierContact;
	}

	public String getBankContact() {
		return bankContact;
	}

	public void setBankContact(String bankContact) {
		this.bankContact = bankContact;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Identity getBankAddress() {
		return bankAddress;
	}

	public void setBankAddress(Identity bankAddress) {
		this.bankAddress = bankAddress;
	}

	public Integer getAcceptCC() {
		return acceptCC;
	}

	public void setAcceptCC(Integer acceptCC) {
		this.acceptCC = acceptCC;
	}

	public String getProcessor() {
		return processor;
	}

	public void setProcessor(String processor) {
		this.processor = processor;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getOtherReason() {
		return otherReason;
	}

	public void setOtherReason(String otherReason) {
		this.otherReason = otherReason;
	}

	public Integer getNonProfitType() {
		return nonProfitType;
	}

	public void setNonProfitType(Integer nonProfitType) {
		this.nonProfitType = nonProfitType;
	}

	public Boolean getOfficeDiff() {
		return officeDiff;
	}

	public void setOfficeDiff(Boolean officeDiff) {
		this.officeDiff = officeDiff;
	}

	public Boolean getPhysDiff() {
		return physDiff;
	}

	public void setPhysDiff(Boolean physDiff) {
		this.physDiff = physDiff;
	}

	public Integer getTaxYear() {
		return taxYear;
	}

	public void setTaxYear(Integer taxYear) {
		this.taxYear = taxYear;
	}

	public Integer getHelpBMM() {
		return helpBMM;
	}

	public void setHelpBMM(Integer helpBMM) {
		this.helpBMM = helpBMM;
	}

	public Boolean getDisclaimer() {
		return disclaimer;
	}

	public void setDisclaimer(Boolean disclaimer) {
		this.disclaimer = disclaimer;
	}

	public Boolean getBoardCopied() {
		return boardCopied;
	}

	public void setBoardCopied(Boolean boardCopied) {
		this.boardCopied = boardCopied;
	}

	public Boolean getOwnerCopied() {
		return ownerCopied;
	}

	public void setOwnerCopied(Boolean ownerCopied) {
		this.ownerCopied = ownerCopied;
	}

	public Integer getNbShares() {
		return nbShares;
	}

	public void setNbShares(Integer nbShares) {
		this.nbShares = nbShares;
	}

	public Integer getHelpAOF() {
		return helpAOF;
	}

	public void setHelpAOF(Integer helpAOF) {
		this.helpAOF = helpAOF;
	}

	public Integer getHelpASP() {
		return helpASP;
	}

	public void setHelpASP(Integer helpASP) {
		this.helpASP = helpASP;
	}

	public Integer getHelpPCP() {
		return helpPCP;
	}

	public void setHelpPCP(Integer helpPCP) {
		this.helpPCP = helpPCP;
	}

	public Integer getHelpWSP() {
		return helpWSP;
	}

	public void setHelpWSP(Integer helpWSP) {
		this.helpWSP = helpWSP;
	}

	public Integer getHelpLCP() {
		return helpLCP;
	}

	public void setHelpLCP(Integer helpLCP) {
		this.helpLCP = helpLCP;
	}

	public Integer getHelpBCP() {
		return helpBCP;
	}

	public void setHelpBCP(Integer helpBCP) {
		this.helpBCP = helpBCP;
	}

	public String getCEID() {
		return CEID;
	}

	public void setCEID(String CEID) {
		this.CEID = CEID;
	}

	public String getPurposeName() {
		return purposeName;
	}

	public void setPurposeName(String purposeName) {
		this.purposeName = purposeName;
	}
}

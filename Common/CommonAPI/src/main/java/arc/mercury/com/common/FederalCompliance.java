package arc.mercury.com.common;

/**
 * Class FederalCompliance
 * User: florin
 * Date: 29.08.2013
 */
public class FederalCompliance {
	private Integer filing;
	private Integer allowanceNo;
	private Integer extraWH;
	private boolean exemptWH;
	private Integer exemptFUTA;

	public Integer getFiling() {
		return filing;
	}

	public void setFiling(Integer filing) {
		this.filing = filing;
	}

	public Integer getAllowanceNo() {
		return allowanceNo;
	}

	public void setAllowanceNo(Integer allowanceNo) {
		this.allowanceNo = allowanceNo;
	}

	public Integer getExtraWH() {
		return extraWH;
	}

	public void setExtraWH(Integer extraWH) {
		this.extraWH = extraWH;
	}

	public boolean isExemptWH() {
		return exemptWH;
	}

	public void setExemptWH(boolean exemptWH) {
		this.exemptWH = exemptWH;
	}

	public Integer getExemptFUTA() {
		return exemptFUTA;
	}

	public void setExemptFUTA(Integer exemptFUTA) {
		this.exemptFUTA = exemptFUTA;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		FederalCompliance that = (FederalCompliance) o;

		if (exemptWH != that.exemptWH) return false;
		if (allowanceNo != null ? !allowanceNo.equals(that.allowanceNo) : that.allowanceNo != null) return false;
		if (exemptFUTA != null ? !exemptFUTA.equals(that.exemptFUTA) : that.exemptFUTA != null) return false;
		if (extraWH != null ? !extraWH.equals(that.extraWH) : that.extraWH != null) return false;
		if (filing != null ? !filing.equals(that.filing) : that.filing != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = filing != null ? filing.hashCode() : 0;
		result = 31 * result + (allowanceNo != null ? allowanceNo.hashCode() : 0);
		result = 31 * result + (extraWH != null ? extraWH.hashCode() : 0);
		result = 31 * result + (exemptWH ? 1 : 0);
		result = 31 * result + (exemptFUTA != null ? exemptFUTA.hashCode() : 0);
		return result;
	}
}

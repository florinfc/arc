package arc.mercury.com.common;

import arc.mercury.com.base.Constants;
import arc.mercury.com.enums.CeoType;
import arc.mercury.com.enums.CfoType;
import arc.mercury.com.enums.SecType;

import java.util.Calendar;
import java.util.Date;

public class Board {

	private String firstName = Constants.defFname;
	private String lastName = Constants.defLname;
	private String title;
	private String ssn = Constants.defSSN;
	private String dln = Constants.defDLN;
	private Integer issueState;
	private Integer type;
	private Double percent;
	private Date taxEnd;
	private Boolean individual = true;
	private String company = Constants.defCompanyName;
	private String fein = Constants.defFEIN;
	private Identity ident;

	public Board() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.DAY_OF_MONTH, 31);
		cal.set(Calendar.MONTH, 11);
		taxEnd = cal.getTime();
		ident = new Identity();
		this.percent = 0.0;
	}

	public Board(Board b) {
		this.firstName = b.firstName;
		this.lastName = b.lastName;
		this.title = b.title;
		this.ssn = b.ssn;
		this.dln = b.dln;
		this.issueState = b.issueState;
		this.type = b.type;
		this.individual = b.individual;
		this.company = b.company;
		this.fein = b.fein;
		this.ident = new Identity(b.ident);
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.DAY_OF_MONTH, 31);
		cal.set(Calendar.MONTH, 11);
		taxEnd = cal.getTime();
		this.percent = 0.0;
	}

	public Board(int x) {
		if (x == 1) {
			this.title = CeoType.President.name();
		}
		if (x == 2) {
			this.title = CfoType.Treasurer.name();
		}
		if (x == 3) {
			this.title = SecType.Secretary.name();
		}
		ident = new Identity();
	}

	public String getFullName() {
		return firstName + " " + lastName;
	}

	public void setFullName(String name) {
		name = name.trim();
		int ix = name.lastIndexOf(' ');
		if (name.startsWith(Constants.defFname)) {
			ix = name.indexOf(Constants.defFname) + Constants.defFname.length();
		}
		firstName = name.substring(0, ix);
		lastName = name.substring(ix + 1);
	}

	public boolean firstNameIsEmpty() {
		return firstName.equals(Constants.defFname);
	}

	public boolean lastNameIsEmpty() {
		return lastName.equals(Constants.defLname);
	}

	public boolean ssnIsEmpty() {
		return ssn.equals(Constants.defSSN);
	}

	public boolean dlnIsEmpty() {
		return ssn.equals(Constants.defDLN);
	}

	public boolean companyIsEmpty() {
		return company.equals(Constants.defCompanyName);
	}

	public boolean feinIsEmpty() {
		return fein.equals(Constants.defFEIN);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getDln() {
		return dln;
	}

	public void setDln(String dln) {
		this.dln = dln;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Double getPercent() {
		return percent;
	}

	public void setPercent(Double percent) {
		this.percent = percent;
	}

	public Date getTaxEnd() {
		return taxEnd;
	}

	public void setTaxEnd(Date taxEnd) {
		this.taxEnd = taxEnd;
	}

	public Boolean getIndividual() {
		return individual;
	}

	public void setIndividual(Boolean individual) {
		this.individual = individual;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Identity getIdent() {
		return ident;
	}

	public void setIdent(Identity ident) {
		this.ident = ident;
	}

	public Integer getIssueState() {
		return issueState;
	}

	public void setIssueState(Integer issueState) {
		this.issueState = issueState;
	}

	public String getFein() {
		return fein;
	}

	public void setFein(String fein) {
		this.fein = fein;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((company == null) ? 0 : company.hashCode());
		result = prime * result + ((dln == null) ? 0 : dln.hashCode());
		result = prime * result + ((fein == null) ? 0 : fein.hashCode());
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((ident == null) ? 0 : ident.hashCode());
		result = prime * result
				+ ((individual == null) ? 0 : individual.hashCode());
		result = prime * result
				+ ((issueState == null) ? 0 : issueState.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((percent == null) ? 0 : percent.hashCode());
		result = prime * result + ((ssn == null) ? 0 : ssn.hashCode());
		result = prime * result + ((taxEnd == null) ? 0 : taxEnd.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Board other = (Board) obj;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		if (dln == null) {
			if (other.dln != null)
				return false;
		} else if (!dln.equals(other.dln))
			return false;
		if (fein == null) {
			if (other.fein != null)
				return false;
		} else if (!fein.equals(other.fein))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (ident == null) {
			if (other.ident != null)
				return false;
		} else if (!ident.equals(other.ident))
			return false;
		if (individual == null) {
			if (other.individual != null)
				return false;
		} else if (!individual.equals(other.individual))
			return false;
		if (issueState == null) {
			if (other.issueState != null)
				return false;
		} else if (!issueState.equals(other.issueState))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (percent == null) {
			if (other.percent != null)
				return false;
		} else if (!percent.equals(other.percent))
			return false;
		if (ssn == null) {
			if (other.ssn != null)
				return false;
		} else if (!ssn.equals(other.ssn))
			return false;
		if (taxEnd == null) {
			if (other.taxEnd != null)
				return false;
		} else if (!taxEnd.equals(other.taxEnd))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

}

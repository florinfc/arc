package arc.mercury.com.enums;

public enum ValueType {

	None(0), Amount(1), Percent(2);
	private Integer value;

	ValueType(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}

package arc.mercury.com.base;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import arc.mercury.com.base.BaseClass;

@Entity
@Table(name = "ForTest")
@NamedQueries({

		@NamedQuery(name = "ForTest.findAll", query = "select item from ForTest item order by item.id"),
		@NamedQuery(name = "ForTest.findById", query = "select item from ForTest item where item.id = :param")

})
@SuppressWarnings({ "serial" })
public class ForTest extends BaseClass<ForTest> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}

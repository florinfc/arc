package arc.mercury.com.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import arc.mercury.com.base.BaseClass;

@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class IUser extends BaseClass<IUser> {

	private Long id;
	private String name;
	private String password;
	private String mail;
	private String fname;
	private String lname;
	private byte[] photo;
	// @XmlTransient
	// private List<IRole> roles;
	// @XmlTransient
	// private List<ITeam> teams;
	private IRole role;
	private ITeam team;

	public IUser() {
		this.name = "anonymous";
		this.fname = this.name;
		this.lname = this.name;
		this.mail = "email";
		this.role = new IRole();
		// this.roles = new ArrayList<IRole>();
		// roles.add(new IRole());
		// this.teams = new ArrayList<ITeam>();
		// teams.add(new ITeam());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getFullName() {
		return getFname() + " " + getLname();
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public IRole getRole() {
		return role;
	}

	public void setRole(IRole role) {
		this.role = role;
	}

	public ITeam getTeam() {
		return team;
	}

	public void setTeam(ITeam team) {
		this.team = team;
	}

}

package arc.mercury.com.services;

import arc.mercury.com.entities.*;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService(targetNamespace = "http://dash.server:8081/")
public interface IIncServices {

	@WebMethod
	public IState getStateById(Long id);

	@WebMethod
	public List<INaics> getNaics();

	@WebMethod
	public List<INaics> getNaicsLike(String text);

	@WebMethod
	public INaics getNaicsById(Long id);

	@WebMethod
	public List<ISS4> getSS4();

	@WebMethod
	public List<IQuote> getQuote();

	@WebMethod
	public List<IHelp> getHelp();

	@WebMethod
	public List<IUser> getAllUsers();

	@WebMethod
	public IUser getUserById(Long id);

	@WebMethod
	public IUser getUserByName(String name);

	@WebMethod
	public IUser getUserByMail(String mail);

	@WebMethod
	public IUser saveUser(IUser user);

	@WebMethod
	public List<IForm> getForms(String code);

	@WebMethod
	public IForm getForm(Long formId);

	@WebMethod
	public void saveForm(IForm form);

	@WebMethod
	public void submitForm(IForm form);

	@WebMethod
	public IPage savePage(IPage page);

	@WebMethod
	public IAnswer newAnswer();

	@WebMethod
	public List<IAnswer> getAnswers();

	@WebMethod
	public IAnswer getAnswer(Long uid, Long fid);

	@WebMethod
	public IAnswer saveAnswer(IAnswer answer);

}

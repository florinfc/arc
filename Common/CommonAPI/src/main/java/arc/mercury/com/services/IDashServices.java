package arc.mercury.com.services;

import arc.mercury.com.common.AnswerValue;
import arc.mercury.com.entities.IMail;
import arc.mercury.com.entities.IState;
import arc.mercury.com.entities.IUser;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

@WebService(targetNamespace = "http://dash.server:8081/")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public interface IDashServices extends Serializable {

	@WebMethod
	public List<IUser> getAllUsers();

	@WebMethod
	public List<IUser> getAnonymous();

	@WebMethod
	public IUser getUserById(Long id);

	@WebMethod
	public IUser getUserByName(String name);

	@WebMethod
	public IUser getUserByMail(String mail);

	@WebMethod
	public IUser saveUser(IUser user);

	@WebMethod
	public Object deleteUser(IUser user);

	@WebMethod
	public List<IState> getStates();

	@WebMethod
	public IState getStateById(Long id);

	@WebMethod
	public List<IMail> getMailByStateId(Long stateId);

	@WebMethod
	public IMail getMailByName(String name);

	@WebMethod
	public String sendSimpleMail(IUser to, String subject, String[] bodyLines,
								 String[] attFiles);

	@WebMethod
	public String sendMail(IUser to, String subject, String[] bodyLines,
						   String[] attFiles);

	@WebMethod
	public String sendMail(@WebParam(name = "from") IUser from,
						   @WebParam(name = "toList") IUser[] toList,
						   @WebParam(name = "ccList") IUser[] ccList,
						   @WebParam(name = "bccList") IUser[] bccList,
						   @WebParam(name = "subject") String subject,
						   @WebParam(name = "bodyLines") String[] bodyLines,
						   @WebParam(name = "attFiles") String[] attFiles);

	@WebMethod
	public String fillPdf(@WebParam(name = "formPath") String formPath,
							@WebParam(name = "formName") String formName,
							@WebParam(name = "com") AnswerValue com,
							@WebParam(name = "userName") String userName) throws Exception;

	@WebMethod
	public String createPdf(@WebParam(name = "formPath") String formPath,
							@WebParam(name = "formName") String formName,
							@WebParam(name = "map") HashMap<String, String> map) throws Exception;

}

package arc.mercury.com.enums;

public enum NotifyMode {

	None(0), Test(1), Mail(2);
	private Integer value;

	NotifyMode(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}

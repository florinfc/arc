package arc.mercury.com.base;

public class Constants {

	public static final int MAXLEN = 255;
	public static final String KEY = "C841C7BC08D24A6B984F00AFBD7AED6E7019F56ECAD645A2BF2CB2E1BD23EB4E107B691822B1495F8C5883440192F973E1863BF418A3421E8BD467DE27A8E7A190DC8A703C274563B612422DA8A139B4A046E4FDCE8448269714FA921EF5163B89FCA4EF3C9543A196CB7E43812F631C6CE246D4F9B54FB1AC6D979F49511D15";
	/* Identity constants */
	public static final String defFname = "First Name";
	public static final String defLname = "Last Name";
	public static final String defMname = "Middle Name";
	public static final String defMail = "email";
	public static final String defPhone = "tel #";
	public static final String defFax = "fax #";
	public static final String defMobile = "mobile";
	/* Address constants */
	public static final String defAddress1 = "Address";
	public static final String defAddress2 = "Address2 (only if necessary)";
	public static final String defCity = "City";
	public static final String defState = "State";
	public static final String defZip = "Zip";
	public static final String defCountry = "Country";
	/* Board constants */
	public static final String defFEIN = "FEIN";
	public static final String defCompanyName = "Company Name";
	public static final String defSSN = "Social Security #";
	public static final String defDLN = "Driver's License #";
	/* Company constants */
	public static final String defReason = "";
	public static final String defPrimDesc = "Describe in a few brief words ...";
	public static final String defName1 = "Company Name 1";
	public static final String defName2 = "Company Name 2";
	public static final String defName3 = "Company Name 3";
	public static final String defTradeName = "Trade Name";
	public static final String defAgentName = "Agent Name";
	public static final String defPrevEIN = "previous EIN";
	public static final String defEID = "Enter # here";
	public static final String defBusinessName = "Business Name";
	public static final String defPrevOwner = "Previous Owner Name";
	public static final String defprevBusiness = "Previous Business Name";
	public static final String defPrevEID = "Previous State Employer ID #";
	public static final String defSupplierContact = "Contact Name";
	public static final String defBankContact = "Contact Name";
	public static final String defBankName = "Bank Name";
	public static final String defProcName = "Processor Name";
	public static final String defAccount = "Account Number";
	/* Non-profit types */
	public static final Integer Religious = 2;
	public static final Integer Public = 3;
	public static final Integer Mutual = 4;
	public static final Integer CID = 5;
	/* Help types */
	public static final Integer AOF = 1;
	public static final Integer EIN = 2;
	public static final Integer SOI = 3;
	public static final Integer EID = 4;
	public static final Integer RSN = 5;
	public static final Integer SUB = 6;
	public static final Integer ASP = 7;
	public static final Integer BMM = 8;
	public static final Integer PCP = 9;
	public static final Integer WSP = 10;
	public static final Integer LCP = 11;
	public static final Integer BCP = 12;
	public static final Integer ACC = 13;

}

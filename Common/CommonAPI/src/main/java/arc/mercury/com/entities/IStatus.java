package arc.mercury.com.entities;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import arc.mercury.com.base.BaseClass;

@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class IStatus extends BaseClass<IStatus> {

	private Long id;
	private String name;
	private Long next;
	private List<IForm> forms;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getNext() {
		return next;
	}

	public void setNext(Long next) {
		this.next = next;
	}

	public List<IForm> getForms() {
		if (forms == null) {
			forms = new ArrayList<IForm>(0);
		}
		return forms;
	}

	public void setForms(List<IForm> forms) {
		this.forms = forms;
	}

}

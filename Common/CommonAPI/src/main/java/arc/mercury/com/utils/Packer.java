package arc.mercury.com.utils;


public class Packer
{
	private static String Pack;

    public Packer()
    {
        Pack = "";
    }

    private static String EncodeText(String Text)
    {
        Text = Text.trim();

        Text = Text.replace("&", "&amp;");
        Text = Text.replace("\"", "&quot;");
        Text = Text.replace("<", "&lt;");
        Text = Text.replace(">", "&gt;");
        Text = Text.replace("'", "`");

        return Text;
    }

    public void Append(String Attribute, String Value)
    {
        Value = Value.replace("\r\n", "\\r\\n");
        
        Pack += "<Attribute Name=\"" + EncodeText(Attribute) + "\" Value=\"" + EncodeText(Value) + "\" />";
    }

    public String GetPack()
    {
        return "<Object>" + Pack + "</Object>";
    }

    public void Clear()
    {
        Pack = "";
    }
	
	
}
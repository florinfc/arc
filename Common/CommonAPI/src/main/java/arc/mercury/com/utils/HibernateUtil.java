package arc.mercury.com.utils;

import java.util.Properties;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public final class HibernateUtil {

	private static SessionFactory sessionFactory;
	private static Configuration configuration;

	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			initSessionFactory();
		}
		return sessionFactory;
	}

	private static synchronized void initSessionFactory() {
		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(getProperties()).buildServiceRegistry();
		sessionFactory = getConfiguration().buildSessionFactory(serviceRegistry);
	}

	public static Configuration getConfiguration() {
		if (configuration == null) {
			initConfiguration();
		}
		return configuration;
	}

	private static synchronized void initConfiguration() {
		configuration = new Configuration().configure();
	}

	public static Session getSession() {
		return getSessionFactory().getCurrentSession();
	}

	public static Properties getProperties() {
		return getConfiguration().getProperties();
	}

	public static String getUrl() {
		return getConfiguration().getProperty("hibernate.connection.url");
	}

	public static String getUser() {
		return getConfiguration().getProperty("hibernate.connection.username");
	}

	public static String getPassword() {
		return getConfiguration().getProperty("hibernate.connection.password");
	}
}

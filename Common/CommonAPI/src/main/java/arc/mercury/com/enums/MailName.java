package arc.mercury.com.enums;

public enum MailName {

	Save("save"), Finish("finish");

	private String value;

	MailName(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
}

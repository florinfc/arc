package arc.mercury.com.enums;

public enum CfoType {

	Treasurer("Treasurer"), CFO("CFO");

	private String value;

	CfoType(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
}

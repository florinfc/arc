package arc.mercury.com.enums;

public enum Roles {

	Anonymous(0), Admin(1), HRManager(2), TeamLeader(3), TeamSecondary(4), TeamMember(5), SubContractor(6);
	private Integer value;

	Roles(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}

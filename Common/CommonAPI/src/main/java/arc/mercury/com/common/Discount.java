package arc.mercury.com.common;

import arc.mercury.com.enums.ValueType;

/**
 * Class Discount
 * User: florin
 * Date: 29.08.2013
 */
public class Discount {
	private ValueType type;
	private Float value;
	private String reason;

	public ValueType getType() {
		return type;
	}

	public void setType(ValueType type) {
		this.type = type;
	}

	public Float getValue() {
		return value;
	}

	public void setValue(Float value) {
		this.value = value;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Discount discount = (Discount) o;

		if (reason != null ? !reason.equals(discount.reason) : discount.reason != null) return false;
		if (type != discount.type) return false;
		if (value != null ? !value.equals(discount.value) : discount.value != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = type != null ? type.hashCode() : 0;
		result = 31 * result + (value != null ? value.hashCode() : 0);
		result = 31 * result + (reason != null ? reason.hashCode() : 0);
		return result;
	}
}

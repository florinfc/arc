package arc.mercury.com.enums;

public enum CyclePeriod {

	Once(0), Daily(1), Weekly(2), BiWeekly(3), Monthly(4), SemiMonthly(5), Quarterly(6), Annualy(7), SemiAnnualy(8), SpecificDate(9);
	private Integer value;

	CyclePeriod(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}

package arc.mercury.com.utils;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.GregorianCalendar;
//import java.util.logging.Level;
//import java.util.logging.Logger;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

@SuppressWarnings("serial")
public class Convert extends Error {

	public static byte[] toByte(char[] chars) {
		if (chars == null) {
			return null;
		}
		ByteBuffer bBuffer = Charset.forName("UTF-8").encode(
				CharBuffer.wrap(chars));
		bBuffer.order();
		byte[] bytes = bBuffer.array();
		return bytes;
	}

	public static char[] toChar(byte[] bytes) {
		if (bytes == null) {
			return null;
		}
		ByteBuffer bBuffer = ByteBuffer.wrap(bytes);
		bBuffer.order();
		char[] chars = new char[bBuffer.remaining()];
		for (int i = 0; i < chars.length; i++)
			chars[i] = (char) (bytes[i + bBuffer.position()] & 0xFF);
		return chars;
	}

	public static String toString(char[] chars) {
		if (chars == null) {
			return null;
		}
		StringBuffer sBuffer = new StringBuffer();
		for (int i = 0; i < chars.length; ++i) {
			sBuffer.append(chars[i]);
		}
		return sBuffer.toString();
	}

	public static Integer toInteger(char[] chars) {
		if (chars == null) {
			return null;
		}
		Integer value = 0;
		ByteBuffer bBuffer = ByteBuffer.wrap(toByte(chars));
		if (bBuffer.getClass().equals(String.class)) {
			String str = toString(chars);
			if (!str.isEmpty()) {
				value = Integer.valueOf(str);
			}
		} else if (bBuffer.getClass().equals(Integer.class)) {
			value = bBuffer.getInt();
		}
		return value;
	}

	public static Long toLong(char[] chars) {
		if (chars == null) {
			return null;
		}
		Long value = 0L;
		ByteBuffer bBuffer = ByteBuffer.wrap(toByte(chars));
		if (bBuffer.getClass().equals(String.class)) {
			String str = toString(chars);
			if (!str.isEmpty()) {
				value = Long.valueOf(str);
			}
		} else if (bBuffer.getClass().equals(Long.class)) {
			value = bBuffer.getLong();
		}
		return value;
	}

	public static Boolean toBoolean(char[] chars) {
		if (chars == null) {
			return null;
		}
		String str = toString(chars);
		if (str.equalsIgnoreCase("FALSE")) {
			return false;
		}
		if (str.equalsIgnoreCase("TRUE")) {
			return true;
		}
		int b = toInteger(chars);
		return (b != 0) ? true : false;
	}

	public static char[] toChars(Object obj) {
		if (obj == null) {
			return null;
		}
		if (obj instanceof String) {
			String value = (String) obj;
			return toChar(value.getBytes());
		}
		if (obj instanceof byte[]) {
			byte[] value = (byte[]) obj;
			return toChar(value);
		}
		if (obj instanceof Short) {
			Short value = (Short) obj;
			ByteBuffer bBuffer = ByteBuffer.allocate(Short.SIZE / 8);
			bBuffer.putShort(value);
			return toChar(bBuffer.array());
		}
		if (obj instanceof Integer) {
			Integer value = (Integer) obj;
			ByteBuffer bBuffer = ByteBuffer.allocate(Integer.SIZE / 8);
			bBuffer.putInt(value);
			return toChar(bBuffer.array());
		}
		if (obj instanceof Long) {
			Long value = (Long) obj;
			ByteBuffer bBuffer = ByteBuffer.allocate(Long.SIZE / 8);
			bBuffer.putLong(value);
			return toChar(bBuffer.array());
		}
		if (obj instanceof Boolean) {
			Boolean value = (Boolean) obj;
			return toChar(((value) ? "TRUE" : "FALSE").getBytes());
		}
		return null;
	}

	public static XMLGregorianCalendar toXMLGregorianCalendar(Date date) {
		GregorianCalendar gCalendar = new GregorianCalendar();
		gCalendar.setTime(date);
		XMLGregorianCalendar xmlCalendar = null;

		try {
			xmlCalendar = DatatypeFactory.newInstance()
					.newXMLGregorianCalendar(gCalendar);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}

		return xmlCalendar;
	}

	public static Date toDate(XMLGregorianCalendar calendar) {
		if (calendar == null) {
			return null;
		}
		return calendar.toGregorianCalendar().getTime();
	}

}

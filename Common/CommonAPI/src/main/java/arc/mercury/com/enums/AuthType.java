package arc.mercury.com.enums;

public enum AuthType {

	None(0), Login(1), Register(2);

	private Integer value;

	AuthType(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}

package arc.mercury.com.base;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectReader;
import org.codehaus.jackson.map.ObjectWriter;

@SuppressWarnings({ "serial", "unchecked" })
public abstract class BaseClass<T> implements Serializable {

	protected static Class<?> clazz;
	protected Map<String, Object> fields = null;

	public BaseClass() {
		super();
		ParameterizedType superclass = (ParameterizedType) getClass()
				.getGenericSuperclass();
		clazz = (Class<T>) superclass.getActualTypeArguments()[0];
	}

	public BaseClass(Class<T> c) {
		super();
		clazz = c;
	}

	public static Class<?> getSubClass() {
		return clazz;
	}

	public static Class<?> instance(Class<?> c) {
		try {
			clazz = (Class<?>) c.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return clazz;
	}

	public String toJson() {
		ObjectWriter ow = new ObjectMapper().writer().withType(getSubClass());
		String json = "";
		try {
			json = ow.writeValueAsString(this);
		} catch (Exception e) {
			json = null;
		}
		return json;
	}

	public Class<T> toObject(String json) {
		ObjectReader or = new ObjectMapper().reader().withType(getSubClass());
		Class<T> obj = null;
		try {
			obj = or.readValue(json);
		} catch (Exception e) {
			obj = null;
		}
		return obj;
	}

	protected Object getField(String key) {
		fields = findFields(this);
		return fields.get(key);
	}

	public Map<String, Object> findFields(Object obj) {
		Field[] fields = obj.getClass().getDeclaredFields();
		Map<String, Object> map = new HashMap<String, Object>(fields.length);
		for (int x = 0; x < fields.length; ++x) {
			Field field = fields[x];
			boolean accessible = field.isAccessible();
			field.setAccessible(true);
			// Annotation[] annotations = field.getDeclaredAnnotations();
			// for (int i = 0; i < annotations.length; i++) {
			// if (!(annotations[i].annotationType().equals(Transient.class) ||
			// annotations[i]
			// .annotationType().equals(XmlTransient.class))) {
			Object value = null;
			try {
				value = field.get(obj);
			} catch (Exception e) {
				value = e.toString();
			}
			if (value instanceof ParameterizedType) {
				ParameterizedType pt = (ParameterizedType) value;
				Type type = pt.getActualTypeArguments()[0];
				value = type.toString();
			}
			map.put(field.getName(), value);
			// }
			// }
			field.setAccessible(accessible);
		}
		return map;
	}

	@Override
	public int hashCode() {
		HashCodeBuilder hb = new HashCodeBuilder();
		fields = findFields(this);
		for (Map.Entry<String, Object> field : fields.entrySet()) {
			hb.append(field.getValue());
		}
		return hb.toHashCode();
	}

	@Override
	public boolean equals(final Object other) {
		if (other == null) {
			return false;
		}
		EqualsBuilder eb = new EqualsBuilder();
		fields = findFields(this);
		Map<String, Object> that = findFields(other);
		for (Map.Entry<String, Object> field : fields.entrySet()) {
			eb.append(field.getValue(), that.get(field.getKey()));
		}
		return eb.isEquals();
	}

	@Override
	public String toString() {
		return toJson();
	}

}

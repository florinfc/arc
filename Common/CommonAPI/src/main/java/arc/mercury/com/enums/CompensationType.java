package arc.mercury.com.enums;

public enum CompensationType {

	None(0), Hourly(1), Commission(2), Flat(3), HourlyCommission(4), Sales(5);
	private Integer value;

	CompensationType(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}

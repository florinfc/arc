package arc.mercury.com.enums;

public enum NodeStatus {

	None(0), Completed(1), Archived(2);
	private Integer value;

	NodeStatus(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}

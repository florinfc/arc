package arc.mercury.com.enums;

public enum SubType {

	None(0), ThirdParty(1), OwnerLLC(2), OwnerSubS(3), SoleProprietor(4), W2(5);
	private Integer value;

	SubType(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}

package arc.mercury.com.common;

import arc.mercury.com.base.Constants;

/**
 * Class Address
 * User: florin
 * Date: 29.08.2013
 */
public class Address {

	private String address1 = Constants.defAddress1;
	private String address2 = Constants.defAddress2;
	private String city = Constants.defCity;
	private String state = Constants.defState;
	private String zip = Constants.defZip;
	private String country = Constants.defCountry;

	public Address() {
	}

	public Address(Address i) {
		address1 = i.address1;
		address2 = i.address2;
		city = i.city;
		state = i.state;
		zip = i.zip;
		country = i.country;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Address address = (Address) o;

		if (address1 != null ? !address1.equals(address.address1) : address.address1 != null) return false;
		if (address2 != null ? !address2.equals(address.address2) : address.address2 != null) return false;
		if (city != null ? !city.equals(address.city) : address.city != null) return false;
		if (country != null ? !country.equals(address.country) : address.country != null) return false;
		if (state != null ? !state.equals(address.state) : address.state != null) return false;
		if (zip != null ? !zip.equals(address.zip) : address.zip != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = address1 != null ? address1.hashCode() : 0;
		result = 31 * result + (address2 != null ? address2.hashCode() : 0);
		result = 31 * result + (city != null ? city.hashCode() : 0);
		result = 31 * result + (state != null ? state.hashCode() : 0);
		result = 31 * result + (zip != null ? zip.hashCode() : 0);
		result = 31 * result + (country != null ? country.hashCode() : 0);
		return result;
	}
}

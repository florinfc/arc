package arc.mercury.com.common;

/**
 * Class StateCompliance
 * User: florin
 * Date: 29.08.2013
 */
public class StateCompliance {
	private Integer filing;
	private Integer incomeTax;
	private Integer unemployment;
	private Integer exemptSUI;
	private Integer exemptNo;
	private Integer extraWH;

	public Integer getFiling() {
		return filing;
	}

	public void setFiling(Integer filing) {
		this.filing = filing;
	}

	public Integer getIncomeTax() {
		return incomeTax;
	}

	public void setIncomeTax(Integer incomeTax) {
		this.incomeTax = incomeTax;
	}

	public Integer getUnemployment() {
		return unemployment;
	}

	public void setUnemployment(Integer unemployment) {
		this.unemployment = unemployment;
	}

	public Integer getExemptSUI() {
		return exemptSUI;
	}

	public void setExemptSUI(Integer exemptSUI) {
		this.exemptSUI = exemptSUI;
	}

	public Integer getExemptNo() {
		return exemptNo;
	}

	public void setExemptNo(Integer exemptNo) {
		this.exemptNo = exemptNo;
	}

	public Integer getExtraWH() {
		return extraWH;
	}

	public void setExtraWH(Integer extraWH) {
		this.extraWH = extraWH;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		StateCompliance that = (StateCompliance) o;

		if (exemptNo != null ? !exemptNo.equals(that.exemptNo) : that.exemptNo != null) return false;
		if (exemptSUI != null ? !exemptSUI.equals(that.exemptSUI) : that.exemptSUI != null) return false;
		if (extraWH != null ? !extraWH.equals(that.extraWH) : that.extraWH != null) return false;
		if (filing != null ? !filing.equals(that.filing) : that.filing != null) return false;
		if (incomeTax != null ? !incomeTax.equals(that.incomeTax) : that.incomeTax != null) return false;
		if (unemployment != null ? !unemployment.equals(that.unemployment) : that.unemployment != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = filing != null ? filing.hashCode() : 0;
		result = 31 * result + (incomeTax != null ? incomeTax.hashCode() : 0);
		result = 31 * result + (unemployment != null ? unemployment.hashCode() : 0);
		result = 31 * result + (exemptSUI != null ? exemptSUI.hashCode() : 0);
		result = 31 * result + (exemptNo != null ? exemptNo.hashCode() : 0);
		result = 31 * result + (extraWH != null ? extraWH.hashCode() : 0);
		return result;
	}
}

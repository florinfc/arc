package arc.mercury.com.common;

public class Rate {

	private Integer hours;
	private Float hourly;
	private Float flat;
	private Float holiday;

	public Integer getHours() {
		return hours;
	}

	public void setHours(Integer hours) {
		this.hours = hours;
	}

	public Float getHourly() {
		return hourly;
	}

	public void setHourly(Float hourly) {
		this.hourly = hourly;
	}

	public Float getFlat() {
		return flat;
	}

	public void setFlat(Float flat) {
		this.flat = flat;
	}

	public Float getHoliday() {
		return holiday;
	}

	public void setHoliday(Float holiday) {
		this.holiday = holiday;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Rate rate = (Rate) o;

		if (flat != null ? !flat.equals(rate.flat) : rate.flat != null) return false;
		if (holiday != null ? !holiday.equals(rate.holiday) : rate.holiday != null) return false;
		if (hourly != null ? !hourly.equals(rate.hourly) : rate.hourly != null) return false;
		if (hours != null ? !hours.equals(rate.hours) : rate.hours != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = hours != null ? hours.hashCode() : 0;
		result = 31 * result + (hourly != null ? hourly.hashCode() : 0);
		result = 31 * result + (flat != null ? flat.hashCode() : 0);
		result = 31 * result + (holiday != null ? holiday.hashCode() : 0);
		return result;
	}
}

package arc.mercury.com.enums;

public enum InvoiceMethod {

	None(0), Hourly(1), Flat(2), FlatHourly(3), PerTask(4);
	private Integer value;

	InvoiceMethod(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}

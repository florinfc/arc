package arc.mercury.com.enums;

public enum NodeType {

	Idle(0), Program(1), Project(2), Task(3), Todo(4), Break(10), Lunch(11), Sick(12), Vacation(13), Holiday(14);
	private Integer value;

	NodeType(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}

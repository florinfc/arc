package arc.mercury.com.enums;

public enum SecType {

	Secretary("Secretary");

	private String value;

	SecType(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
}

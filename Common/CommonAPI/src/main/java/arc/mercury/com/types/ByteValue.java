package arc.mercury.com.types;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import arc.mercury.com.utils.Convert;

public class ByteValue extends XmlAdapter<char[], Object> {

	@Override
	public char[] marshal(Object item) throws Exception {
		return Convert.toChars(item);
	}

	@Override
	public Object unmarshal(char[] item) throws Exception {
		return Convert.toByte(item);
	}

}

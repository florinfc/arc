package arc.mercury.com.enums;

public enum InvoiceStatus {

	New(0), Working(1), Approved(7), Rejected(8), Locked(9);
	private Integer value;

	InvoiceStatus(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}

package arc.mercury.com.common;

/**
 * Class Benefit
 * User: florin
 * Date: 29.08.2013
 */
public class Benefit {
	private Integer accrual;
	private boolean paid;
	private Float paidRate;
	private boolean sick;
	private Float sickRate;
	private boolean vacation;
	private Float vacationRate;

	public Integer getAccrual() {
		return accrual;
	}

	public void setAccrual(Integer accrual) {
		this.accrual = accrual;
	}

	public boolean isPaid() {
		return paid;
	}

	public void setPaid(boolean paid) {
		this.paid = paid;
	}

	public Float getPaidRate() {
		return paidRate;
	}

	public void setPaidRate(Float paidRate) {
		this.paidRate = paidRate;
	}

	public boolean isSick() {
		return sick;
	}

	public void setSick(boolean sick) {
		this.sick = sick;
	}

	public Float getSickRate() {
		return sickRate;
	}

	public void setSickRate(Float sickRate) {
		this.sickRate = sickRate;
	}

	public boolean isVacation() {
		return vacation;
	}

	public void setVacation(boolean vacation) {
		this.vacation = vacation;
	}

	public Float getVacationRate() {
		return vacationRate;
	}

	public void setVacationRate(Float vacationRate) {
		this.vacationRate = vacationRate;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Benefit benefit = (Benefit) o;

		if (paid != benefit.paid) return false;
		if (sick != benefit.sick) return false;
		if (vacation != benefit.vacation) return false;
		if (accrual != null ? !accrual.equals(benefit.accrual) : benefit.accrual != null) return false;
		if (paidRate != null ? !paidRate.equals(benefit.paidRate) : benefit.paidRate != null) return false;
		if (sickRate != null ? !sickRate.equals(benefit.sickRate) : benefit.sickRate != null) return false;
		if (vacationRate != null ? !vacationRate.equals(benefit.vacationRate) : benefit.vacationRate != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = accrual != null ? accrual.hashCode() : 0;
		result = 31 * result + (paid ? 1 : 0);
		result = 31 * result + (paidRate != null ? paidRate.hashCode() : 0);
		result = 31 * result + (sick ? 1 : 0);
		result = 31 * result + (sickRate != null ? sickRate.hashCode() : 0);
		result = 31 * result + (vacation ? 1 : 0);
		result = 31 * result + (vacationRate != null ? vacationRate.hashCode() : 0);
		return result;
	}
}

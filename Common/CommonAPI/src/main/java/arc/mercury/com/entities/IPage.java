package arc.mercury.com.entities;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import arc.mercury.com.base.BaseClass;

@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class IPage extends BaseClass<IPage> {

	private Long id;
	private String name;
	private Integer ord;
	private IForm form;
	private List<IQuestion> questions;

	@XmlTransient
	private Integer prevPage;
	@XmlTransient
	private Integer nextPage;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOrd() {
		return ord;
	}

	public void setOrd(Integer ord) {
		this.ord = ord;
	}

	public IForm getForm() {
		return form;
	}

	public void setForm(IForm form) {
		this.form = form;
	}

	public List<IQuestion> getQuestions() {
		if (questions == null) {
			questions = new ArrayList<IQuestion>(0);
		}
		return questions;
	}

	public void setQuestions(List<IQuestion> questions) {
		this.questions = questions;
	}

	public Integer getPrevPage() {
		return prevPage;
	}

	public void setPrevPage(Integer prevPage) {
		this.prevPage = prevPage;
	}

	public Integer getNextPage() {
		return nextPage;
	}

	public void setNextPage(Integer nextPage) {
		this.nextPage = nextPage;
	}

}

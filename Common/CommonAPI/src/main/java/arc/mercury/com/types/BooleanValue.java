package arc.mercury.com.types;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class BooleanValue extends XmlAdapter<Boolean, Integer> {

	@Override
	public Boolean marshal(Integer item) throws Exception {
		return (item != 0) ? true : false;
	}

	@Override
	public Integer unmarshal(Boolean item) throws Exception {
		return (item) ? 1 : 0;
	}

}

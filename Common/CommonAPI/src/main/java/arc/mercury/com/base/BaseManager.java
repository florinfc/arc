package arc.mercury.com.base;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@SuppressWarnings("unchecked")
public class BaseManager<T> {

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;

	private Class<T> clazz;

	public BaseManager() {
		clazz = (Class<T>) BaseClass.getSubClass();
	}

	public BaseManager(Class<T> theClass) {
		clazz = theClass;
	}

	/**
	 * {@inheritDoc}
	 */
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	public T insert(T item) {
		if (item == null) {
			return null;
		}
		getManager().persist(item);
		return item;
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(T item) {
		if (item == null) {
			return;
		}
		Long id = (Long) ((BaseClass<T>) item).getField("id");
		delete(id);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Long id) {
		if (id == null) {
			return;
		}
		T item = (T) getManager().getReference(clazz, id);
		if (item != null) {
			getManager().remove(item);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public T update(T item) {
		if (item == null) {
			return null;
		}
		item = getManager().merge(item);
		return item;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<T> findAll() {
		List<T> result = null;
		Query query = getManager().createNamedQuery(
				clazz.getSimpleName() + ".findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<T>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public T findById(Long id) {
		if (id == null) {
			return null;
		}
		return (T) getManager().find(clazz, id);
	}
}

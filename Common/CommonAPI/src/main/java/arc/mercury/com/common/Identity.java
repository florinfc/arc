package arc.mercury.com.common;

import arc.mercury.com.base.Constants;

public class Identity {

	private String firstName = Constants.defFname;
	private String lastName = Constants.defLname;
	private String midName = Constants.defMname;
	private Address address;
	private String mail = Constants.defMail;
	private String phone = Constants.defPhone;
	private String fax = Constants.defFax;
	private String mobile = Constants.defMobile;
	private String ssn = Constants.defSSN;

	public Identity() {
		address = new Address();
	}

	public Identity(Identity i) {
		firstName = i.firstName;
		lastName = i.lastName;
		midName = i.midName;
		mail = i.mail;
		phone = i.phone;
		fax = i.fax;
		mobile = i.mobile;
		ssn = i.ssn;
		address = new Address(i.address);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMidName() {
		return midName;
	}

	public void setMidName(String midName) {
		this.midName = midName;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Identity identity = (Identity) o;

		if (address != null ? !address.equals(identity.address) : identity.address != null) return false;
		if (fax != null ? !fax.equals(identity.fax) : identity.fax != null) return false;
		if (firstName != null ? !firstName.equals(identity.firstName) : identity.firstName != null) return false;
		if (lastName != null ? !lastName.equals(identity.lastName) : identity.lastName != null) return false;
		if (mail != null ? !mail.equals(identity.mail) : identity.mail != null) return false;
		if (midName != null ? !midName.equals(identity.midName) : identity.midName != null) return false;
		if (mobile != null ? !mobile.equals(identity.mobile) : identity.mobile != null) return false;
		if (phone != null ? !phone.equals(identity.phone) : identity.phone != null) return false;
		if (ssn != null ? !ssn.equals(identity.ssn) : identity.ssn != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = firstName != null ? firstName.hashCode() : 0;
		result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
		result = 31 * result + (midName != null ? midName.hashCode() : 0);
		result = 31 * result + (address != null ? address.hashCode() : 0);
		result = 31 * result + (mail != null ? mail.hashCode() : 0);
		result = 31 * result + (phone != null ? phone.hashCode() : 0);
		result = 31 * result + (fax != null ? fax.hashCode() : 0);
		result = 31 * result + (mobile != null ? mobile.hashCode() : 0);
		result = 31 * result + (ssn != null ? ssn.hashCode() : 0);
		return result;
	}
}

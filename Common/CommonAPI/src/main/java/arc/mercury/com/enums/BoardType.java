package arc.mercury.com.enums;

public enum BoardType {

	Dir(0), CEO(1), CFO(2), SEC(3);

	private Integer value;

	BoardType(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}

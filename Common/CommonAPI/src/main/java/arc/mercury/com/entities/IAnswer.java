package arc.mercury.com.entities;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import arc.mercury.com.base.BaseClass;
import arc.mercury.com.base.BaseJson;
import arc.mercury.com.common.AnswerValue;

@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class IAnswer extends BaseClass<IAnswer> {

	private Long id;
	private String name;
	private Long userId;
	private Long formId;
	private Integer maxPage;
	private char[] pages = new char[16];
	private String value;
	private AnswerValue com;
	private List<IHelp> help;

	public IAnswer() {
		super();
		maxPage = 1;
		com = new AnswerValue();
	}

	public IAnswer(Long id, String name, Long uid, AnswerValue value) {
		super();
		this.id = id;
		this.name = name;
		this.userId = uid;
		this.com = value;
		this.value = BaseJson.toJson(value);
		for (int i = 0; i < pages.length; ++i) {
			pages[i] = '0';
		}
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getFormId() {
		return formId;
	}

	public void setFormId(Long formId) {
		this.formId = formId;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}

	public AnswerValue getCom() {
		return com;
	}

	public void setCom(AnswerValue com) {
		this.com = com;
	}

	public char[] getPages() {
		if (pages == null) {
			pages = new char[16];
		}
		return pages;
	}

	public void setPages(char[] pages) {
		this.pages = pages;
	}

	public List<IHelp> getHelp() {
		return help;
	}

	public void setHelp(List<IHelp> help) {
		this.help = help;
	}

}

package arc.mercury.com.base;

import junit.framework.Assert;
import junit.framework.TestSuite;

import org.junit.Test;

import arc.mercury.com.common.Address;
import arc.mercury.com.common.Board;

public class BaseJsonTest extends TestSuite {

	@Test
	public void testString() {
		String expect = "test";
		String json = BaseJson.toJson(expect);
		String actual = (String) BaseJson.toObject(json, String.class);
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void testLong() {
		Long expect = 1L;
		String json = BaseJson.toJson(expect);
		Long actual = (Long) BaseJson.toObject(json, Long.class);
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void testBoard() {
		Board expect = new Board();
		String json = BaseJson.toJson(expect);
		Board actual = (Board) BaseJson.toObject(json, Board.class);
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void testAddress() {
		Address expect = new Address();
		String json = BaseJson.toJson(expect);
		Address actual = (Address) BaseJson.toObject(json, Address.class);
		Assert.assertEquals(expect, actual);
	}
}

package arc.mercury.com.base;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The class <code>TestAll</code> builds a suite that can be used to run all of
 * the tests within its package as well as within any subpackages of its
 * package.
 * 
 * @generatedBy CodePro at 6/30/13 8:21 PM
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({

BaseJsonTest.class, BaseClassTest.class, BaseManagerTest.class

})
public class TestAll {
}

package arc.mercury.com.base;

import org.junit.After;
import org.junit.Before;

import com.bm.testsuite.PoJoFixture;

/**
 * Class BaseClassTest User: florin Date: 28.08.2013
 */
public class BaseClassTest extends PoJoFixture {

	// @SuppressWarnings("unchecked")
	// private Class<ForTest> clazz = (Class<ForTest>) BaseClass
	// .instance(BaseClass.getType());
	private static final Class<?>[] beans = { BaseClass.class };

	public BaseClassTest() {
		super(beans);
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

}

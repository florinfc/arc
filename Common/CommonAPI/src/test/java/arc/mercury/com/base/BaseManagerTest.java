package arc.mercury.com.base;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.bm.testsuite.BaseSessionBeanFixture;
import com.bm.testsuite.dataloader.CSVInitialDataSet;
import com.bm.testsuite.dataloader.DateFormats;

@SuppressWarnings("unchecked")
public class BaseManagerTest extends
		BaseSessionBeanFixture<BaseManager<ForTest>> {

	protected final Logger log = Logger.getLogger(BaseManagerTest.class);

	private static final BaseManager<ForTest> theClass = new BaseManager<ForTest>();
	private static final Class<?>[] beans = { ForTest.class };
	private static CSVInitialDataSet<?>[] data = {

	new CSVInitialDataSet<ForTest>(ForTest.class, "Csv/ForTest.csv", "id")

	};

	static {
		for (int i = 0; i < data.length; ++i) {
			DateFormats.USER_DATE.setUserDefinedFomatter("MM-dd-yyyy");
			data[i].addDateFormat(DateFormats.USER_DATE);
		}
	}

	protected int count = 0;
	protected EntityManager manager = null;
	protected BaseManager<ForTest> test = null;
	protected List<ForTest> items = null;
	protected ForTest item = null;
	protected ForTest rec = null;

	public BaseManagerTest() {
		super((Class<BaseManager<ForTest>>) theClass.getClass(), beans, data);
	}

	@Override
	@Before
	public void setUp() throws Exception {
		super.setUp();
		this.test = this.getBeanToTest();
		Assert.assertNotNull(test);
		Assert.assertNotNull(test.getManager());
		manager = test.getManager();
		manager.getTransaction().begin();
		items = test.findAll();
		count = items.size();
		showItems("Before");
		if (count > 0) {
			rec = items.get(0);
		}
	}

	@Override
	@After
	public void tearDown() throws Exception {
		EntityTransaction entityTransaction = manager.getTransaction();
		if (entityTransaction.isActive()) {
			if (entityTransaction.getRollbackOnly()) {
				entityTransaction.rollback();
			} else {
				entityTransaction.commit();
			}
		}
		showItems("After");
		manager.close();
		super.tearDown();
	}

	private void showItems(String when) {
		log.info(this.getName() + "\t" + when + " \t : records = "
				+ items.size());
		for (ForTest item : items) {
			log.info(item);
		}
	}

	@Test
	public void testInsert() throws Exception {
		item = test.insert(null);
		Assert.assertEquals(null, item);

		item = new ForTest();
		test.insert(item);
		items = test.findAll();
		Assert.assertEquals(++count, items.size());
	}

	@Test
	public void testDelete() throws Exception {
		test.delete((ForTest) null);
		items = test.findAll();
		Assert.assertEquals(count, items.size());

		item = new ForTest();
		test.delete(item);
		items = test.findAll();
		Assert.assertEquals(count, items.size());

		test.delete(rec);
		items = test.findAll();
		Assert.assertEquals(--count, items.size());
	}

	@Test
	public void testUpdate() {
		item = test.update(null);
		Assert.assertEquals(null, item);

		item = new ForTest();
		item = test.update(rec);
		Assert.assertEquals(rec, item);
	}

	@Test
	public void testFindAll() {
		items = test.findAll();
		Assert.assertEquals(count, items.size());
	}

	@Test
	public void testFindById() {
		Long id = null;
		item = test.findById(id);
		Assert.assertNull(item);

		id = -1L;
		item = test.findById(id);
		Assert.assertNull(item);

		id = 1L;
		item = test.findById(id);
		Assert.assertEquals(items.get(0), item);
	}

}

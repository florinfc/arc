package arc.mercury.com.generators;

import arc.mercury.com.entities.State;
import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

/**
 * Generator for more State records
 */
@GeneratorType(className = State.class, fieldType = FieldType.ALL_TYPES)
public final class StateCollection extends BeanCollectionGenerator<State> {

	public StateCollection() {
		super(State.class, 5);
	}

	public final static StateCollection instance() {
		return new StateCollection();
	}
}

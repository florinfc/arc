package arc.mercury.com.generators;

import arc.mercury.com.entities.Team;
import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

/**
 * Generator for more Team records
 */
@GeneratorType(className = Team.class, fieldType = FieldType.ALL_TYPES)
public final class TeamCollection extends BeanCollectionGenerator<Team> {

	public TeamCollection() {
		super(Team.class, 5);
	}

	public final static TeamCollection instance() {
		return new TeamCollection();
	}
}

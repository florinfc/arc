package arc.mercury.com.generators;

import arc.mercury.com.entities.Role;
import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

/**
 * Generator for more Role records
 */
@GeneratorType(className = Role.class, fieldType = FieldType.ALL_TYPES)
public final class RoleCollection extends BeanCollectionGenerator<Role> {

	public RoleCollection() {
		super(Role.class, 5);
	}

	public final static RoleCollection instance() {
		return new RoleCollection();
	}
}

package arc.mercury.com.generators;

import arc.mercury.com.entities.State;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.SingleBeanGenerator;

/**
 * Generator for one State record
 */
@GeneratorType(className = State.class, fieldType = FieldType.ALL_TYPES)
public final class StateRecord extends SingleBeanGenerator<State> {

	public StateRecord() {
		super(State.class);
	}

	public final static StateRecord instance() {
		return new StateRecord();
	}
}

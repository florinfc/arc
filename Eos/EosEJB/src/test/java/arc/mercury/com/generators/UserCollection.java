package arc.mercury.com.generators;


import arc.mercury.com.entities.User;
import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

/**
 * Generator for more User records
 */
@GeneratorType(className = User.class, fieldType = FieldType.ALL_TYPES)
public final class UserCollection extends BeanCollectionGenerator<User> {

	public UserCollection() {
		super(User.class, 5);
	}

	public final static UserCollection instance() {
		return new UserCollection();
	}
}

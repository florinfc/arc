package arc.mercury.eos.generators;

import arc.mercury.eos.entities.Node;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

@GeneratorType(className = Node.class, fieldType = FieldType.ALL_TYPES)
public final class NodeCollection extends BeanCollectionGenerator<Node> {

	public NodeCollection() {
		super(Node.class, 5);
	}

	public final static NodeCollection instance() {
		return new NodeCollection();
	}
}

package arc.mercury.eos.generators;

import arc.mercury.eos.entities.Branch;
import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.SingleBeanGenerator;

@GeneratorType(className = Branch.class, fieldType = FieldType.ALL_TYPES)
public final class BranchRecord extends SingleBeanGenerator<Branch> {

    public BranchRecord()
    {
        super(Branch.class);
    }

    public final static BranchRecord instance(){
        return new BranchRecord();
    }
}

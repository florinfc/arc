package arc.mercury.eos.managers;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.JUnit4ClassRunner;
import org.junit.runner.RunWith;

import arc.mercury.eos.entities.Invoice;
import arc.mercury.eos.entities.InvoiceItem;

import com.bm.testsuite.BaseSessionBeanFixture;
import com.bm.testsuite.dataloader.CSVInitialDataSet;

@RunWith(JUnit4ClassRunner.class)
public class InvoiceItemManagerTest extends
		BaseSessionBeanFixture<InvoiceItemManager> {

	protected static int count = 0;
	static Class<?>[] beans = {

	Invoice.class, InvoiceItem.class,

	};
	static CSVInitialDataSet<?>[] data = {

			new CSVInitialDataSet<Invoice>(Invoice.class, "Csv/Invoice.csv",
					"id", "name", "next"),
			new CSVInitialDataSet<InvoiceItem>(InvoiceItem.class,
					"Csv/InvoiceItem.csv", "id", "name"),

	};
	protected final Logger log = Logger.getLogger(InvoiceItemManagerTest.class);
	protected EntityManager manager = null;
	protected InvoiceItemManager test = null;
	protected List<InvoiceItem> items = null;
	protected InvoiceItem item = null;
	protected InvoiceItem first = null;

	public InvoiceItemManagerTest() {
		super(InvoiceItemManager.class, beans, data);
	}

	@Override
	@Before
	public void setUp() throws Exception {
		super.setUp();
		this.test = this.getBeanToTest();
		Assert.assertNotNull(test);
		Assert.assertNotNull(test.getManager());
		manager = test.getManager();
		manager.getTransaction().begin();
		items = test.findAll();
		count = items.size();
		// showItems("Before");
		if (count > 0) {
			first = items.get(0);
		}
	}

	@Override
	@After
	public void tearDown() throws Exception {
		EntityTransaction entityTransaction = manager.getTransaction();
		log.debug("entity manager open: " + manager.isOpen());
		log.debug("entity transaction active: " + entityTransaction.isActive());
		if (entityTransaction.isActive()) {
			if (entityTransaction.getRollbackOnly()) {
				entityTransaction.rollback();
			} else {
				entityTransaction.commit();
			}
		}
		showItems("After");
		manager.close();
		super.tearDown();
	}

	private void showItems(String when) {
		log.info(this.getClass().getCanonicalName() + "\t" + when
				+ " \t : records = " + items.size());
		for (InvoiceItem item : items) {
			log.info(item);
		}
	}

	@Test
	public void testInsert() throws Exception {
		item = test.insert(null);
		Assert.assertEquals(null, item);

		item = new InvoiceItem();
		// item.setId(null);
		item.setDescription("New");
		item = test.insert(item);
		Assert.assertEquals("New", item.getDescription());
		items = test.findAll();
		Assert.assertEquals(++count, items.size());
	}

	@Test
	public void testDelete() throws Exception {
		test.delete((InvoiceItem) null);
		items = test.findAll();
		Assert.assertEquals(count, items.size());

		item = new InvoiceItem();
		item.setId(99L);
		test.delete(item);
		items = test.findAll();
		Assert.assertEquals(count, items.size());

		test.delete(first);
		items = test.findAll();
		Assert.assertEquals(--count, items.size());
	}

	@Test
	public void testUpdate() {
		item = test.update(null);
		Assert.assertEquals(null, item);

		first.setDescription("Mod");
		item = test.update(first);
		Assert.assertEquals("Mod", item.getDescription());
	}

	@Test
	public void testFindAll() {
		items = test.findAll();
		Assert.assertEquals(count, items.size());
	}

	@Test
	public void testFindById() {
		Long id = null;
		item = test.findById(id);
		Assert.assertNull(item);

		id = -1L;
		item = test.findById(id);
		Assert.assertNull(item);

		id = first.getId();
		item = test.findById(id);
		Assert.assertEquals(items.get(0), item);
	}

	@Test
	public void testFindByNode() {
		Long id = null;
		item = test.findByNode(id);
		Assert.assertNull(item);

		id = -1L;
		item = test.findByNode(id);
		Assert.assertNull(item);

		id = 1L;
		item = test.findByNode(id);
		Assert.assertEquals(items.get(0), item);
	}

	@Test
	public void testFindInvoice() {
		Long id = null;
		item = test.findByInvoice(id);
		Assert.assertEquals(0, items.size());

		id = -1L;
		item = test.findByInvoice(id);
		Assert.assertEquals(0, items.size());

		id = 1L;
		item = test.findByInvoice(id);
		Assert.assertTrue(0 < items.size());
	}
}

package arc.mercury.eos.generators;

import arc.mercury.eos.entities.Assignment;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

@GeneratorType(className = Assignment.class, fieldType = FieldType.ALL_TYPES)
public final class AssignmentCollection extends
		BeanCollectionGenerator<Assignment> {

	public AssignmentCollection() {
		super(Assignment.class, 5);
	}

	public final static AssignmentCollection instance() {
		return new AssignmentCollection();
	}
}

package arc.mercury.eos.generators;

import arc.mercury.eos.entities.Activity;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

@GeneratorType(className = Activity.class, fieldType = FieldType.ALL_TYPES)
public final class ActivityCollection extends BeanCollectionGenerator<Activity> {

	public ActivityCollection() {
		super(Activity.class, 5);
	}

	public final static ActivityCollection instance() {
		return new ActivityCollection();
	}
}

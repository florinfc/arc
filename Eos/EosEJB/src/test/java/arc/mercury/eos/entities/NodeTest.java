package arc.mercury.eos.entities;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.junit.After;
import org.junit.Before;

import arc.mercury.eos.generators.CompensationRecord;
import arc.mercury.eos.generators.NodeRecord;
import arc.mercury.eos.generators.PersonRecord;

import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;

public class NodeTest extends BaseEntityFixture<Node> {

	private static final Generator<?>[] SPECIAL_GENERATORS = {
			NodeRecord.instance(), PersonRecord.instance(),
			CompensationRecord.instance() };

	public NodeTest() {
		super(Node.class, SPECIAL_GENERATORS);
	}

	public NodeTest(Generator<?>[] SPECIAL_GENERATORS) {
		super(Node.class, SPECIAL_GENERATORS);
	}

	@PersistenceUnit(unitName = "cubrid")
	private EntityManagerFactory entityManagerFactory;

	protected void Clear() {
		// to Do later!
		EntityManager em = (EntityManager) this;
		em.clear();
		em.close();
	}

	// NodeTest nodPtTest;

	@Override
	@Before
	public void setUp() throws Exception {
		// ?! SPECIAL_GENERATORS
		// nodPtTest = new NodeTest(SPECIAL_GENERATORS);
		super.setUp();
	}

	@Override
	@After
	public void tearDown() {
		// nodPtTest.Clear();
		this.Clear();
	}
}

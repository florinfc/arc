package arc.mercury.eos.generators;

import arc.mercury.eos.entities.Message;
import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.SingleBeanGenerator;

@GeneratorType(className = Message.class, fieldType = FieldType.ALL_TYPES)
public final class MessageRecord extends SingleBeanGenerator<Message> {

    public MessageRecord() {
        super(Message.class);
    }

    public final static MessageRecord instance() {
        return new MessageRecord();
    }
}

package arc.mercury.eos.generators;

import arc.mercury.eos.entities.Activity;
import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.SingleBeanGenerator;

@GeneratorType(className = Activity.class, fieldType = FieldType.ALL_TYPES)
public final class ActivityRecord extends SingleBeanGenerator<Activity> {
    public ActivityRecord() {
        super(Activity.class);
    }

    public final static ActivityRecord instance() {
        return new ActivityRecord();
    }
}

package arc.mercury.eos.entities;

import org.junit.After;
import org.junit.Before;

import arc.mercury.com.generators.UserRecord;
import arc.mercury.eos.generators.NodeRecord;

import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;

public class ActivityTest extends BaseEntityFixture<Activity> {

	private static final Generator<?>[] SPECIAL_GENERATORS = {
			NodeRecord.instance(), UserRecord.instance() };

	public ActivityTest() {
		super(Activity.class, SPECIAL_GENERATORS);
	}

	@Before
	@Override
	public void setUp() {
		try {
			super.setUp();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@After
	@Override
	public void tearDown() {
		try {
			super.tearDown();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
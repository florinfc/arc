package arc.mercury.eos.generators;

import arc.mercury.eos.entities.Client;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

@GeneratorType(className = Client.class, fieldType = FieldType.ALL_TYPES)
public final class ClientCollection extends BeanCollectionGenerator<Client> {

	public ClientCollection() {
		super(Client.class, 5);
	}

	public final static ClientCollection instance() {
		return new ClientCollection();
	}
}

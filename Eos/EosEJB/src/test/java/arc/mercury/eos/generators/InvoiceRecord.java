package arc.mercury.eos.generators;

import arc.mercury.eos.entities.Invoice;
import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.SingleBeanGenerator;

@GeneratorType(className = Invoice.class, fieldType = FieldType.ALL_TYPES)
public final class InvoiceRecord extends SingleBeanGenerator<Invoice> {

    public InvoiceRecord(){
        super(Invoice.class);
    }

    public final static InvoiceRecord instance(){
        return new InvoiceRecord();
    }
}

package arc.mercury.eos.generators;

import arc.mercury.eos.entities.Invoice;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

@GeneratorType(className = Invoice.class, fieldType = FieldType.ALL_TYPES)
public final class InvoiceCollection extends BeanCollectionGenerator<Invoice> {

	public InvoiceCollection() {
		super(Invoice.class, 5);
	}

	public final static InvoiceCollection instance() {
		return new InvoiceCollection();
	}
}

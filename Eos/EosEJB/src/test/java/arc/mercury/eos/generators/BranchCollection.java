package arc.mercury.eos.generators;

import arc.mercury.eos.entities.Branch;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

@GeneratorType(className = Branch.class, fieldType = FieldType.ALL_TYPES)
public final class BranchCollection extends BeanCollectionGenerator<Branch> {

	public BranchCollection() {
		super(Branch.class, 5);
	}

	public final static BranchCollection instance() {
		return new BranchCollection();
	}
}

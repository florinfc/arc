package arc.mercury.eos.generators;

import arc.mercury.eos.entities.Company;
import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.SingleBeanGenerator;

@GeneratorType(className = Company.class, fieldType = FieldType.ALL_TYPES)
public final class CompanyRecord extends SingleBeanGenerator<Company> {

    public CompanyRecord() {
        super(Company.class);
    }

    public final static CompanyRecord instance() {
        return new CompanyRecord();
    }
}

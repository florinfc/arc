package arc.mercury.eos.managers;

import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author florin
 * @version $Revision: 1.0 $
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({

		InvoiceItemManagerTest.class,

})
public class TestAll {

	public static void main(String[] args) {
		JUnitCore.runClasses(new Class[]{TestAll.class});
	}
}

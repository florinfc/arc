package arc.mercury.eos.generators;

import arc.mercury.eos.entities.Assignment;
import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.SingleBeanGenerator;

@GeneratorType(className = Assignment.class, fieldType = FieldType.ALL_TYPES)
public final class AssignmentRecord extends SingleBeanGenerator<Assignment> {

    public AssignmentRecord() {
        super(Assignment.class);
    }

    public final static AssignmentRecord instance() {
        return new AssignmentRecord();
    }
}

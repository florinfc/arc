package arc.mercury.eos.generators;

import arc.mercury.eos.entities.Company;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

@GeneratorType(className = Company.class, fieldType = FieldType.ALL_TYPES)
public final class CompanyCollection extends BeanCollectionGenerator<Company> {

	public CompanyCollection() {
		super(Company.class, 5);
	}

	public final static CompanyCollection instance() {
		return new CompanyCollection();
	}
}

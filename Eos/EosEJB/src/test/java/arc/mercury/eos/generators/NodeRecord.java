package arc.mercury.eos.generators;

import arc.mercury.eos.entities.Node;
import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.SingleBeanGenerator;

@GeneratorType(className = Node.class, fieldType = FieldType.ALL_TYPES)
public final class NodeRecord extends SingleBeanGenerator<Node> {

    public NodeRecord() {
        super(Node.class);
    }

    public final static MessageRecord instance() {
        return new MessageRecord();
    }
}

package arc.mercury.eos.generators;

import arc.mercury.eos.entities.Person;
import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.SingleBeanGenerator;

@GeneratorType(className = Person.class, fieldType = FieldType.ALL_TYPES)
public final class PersonRecord extends SingleBeanGenerator<Person> {

    public PersonRecord() {
        super(Person.class);
    }

    public final static PersonRecord instance() {
        return new PersonRecord();
    }
}

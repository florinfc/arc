package arc.mercury.eos.generators;

import arc.mercury.eos.entities.InvoiceItem;
import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.SingleBeanGenerator;

@GeneratorType(className = InvoiceItem.class, fieldType = FieldType.ALL_TYPES)
public final class InvoiceItemRecord extends SingleBeanGenerator<InvoiceItem> {

	public InvoiceItemRecord() {
		super(InvoiceItem.class);
	}

	public final static InvoiceItemRecord instance() {
		return new InvoiceItemRecord();
	}
}

package arc.mercury.eos.generators;

import arc.mercury.eos.entities.Person;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

@GeneratorType(className = Person.class, fieldType = FieldType.ALL_TYPES)
public final class PersonCollection extends BeanCollectionGenerator<Person> {

	public PersonCollection() {
		super(Person.class, 5);
	}

	public final static PersonCollection instance() {
		return new PersonCollection();
	}
}

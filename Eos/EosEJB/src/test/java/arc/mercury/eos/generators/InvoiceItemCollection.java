package arc.mercury.eos.generators;

import arc.mercury.eos.entities.InvoiceItem;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

@GeneratorType(className = InvoiceItem.class, fieldType = FieldType.ALL_TYPES)
public final class InvoiceItemCollection extends
		BeanCollectionGenerator<InvoiceItem> {

	public InvoiceItemCollection() {
		super(InvoiceItem.class, 5);
	}

	public final static InvoiceItemCollection instance() {
		return new InvoiceItemCollection();
	}
}

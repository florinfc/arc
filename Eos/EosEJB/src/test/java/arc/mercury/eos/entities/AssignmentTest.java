package arc.mercury.eos.entities;

import arc.mercury.com.generators.UserRecord;
import arc.mercury.eos.generators.NodeRecord;

import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;

public class AssignmentTest extends BaseEntityFixture<Assignment> {

	private static final Generator<?>[] SPECIAL_GENERATORS = {
			NodeRecord.instance(), UserRecord.instance()

	};

	public AssignmentTest() {
		super(Assignment.class, SPECIAL_GENERATORS);
	}
}

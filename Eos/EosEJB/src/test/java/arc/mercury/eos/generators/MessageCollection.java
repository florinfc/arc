package arc.mercury.eos.generators;

import arc.mercury.eos.entities.Message;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

@GeneratorType(className = Message.class, fieldType = FieldType.ALL_TYPES)
public final class MessageCollection extends BeanCollectionGenerator<Message> {

	public MessageCollection() {
		super(Message.class, 5);
	}

	public static final MessageCollection instance() {
		return new MessageCollection();
	}
}

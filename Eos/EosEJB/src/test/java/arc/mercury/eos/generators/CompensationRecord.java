package arc.mercury.eos.generators;

import arc.mercury.eos.entities.Compensation;
import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.SingleBeanGenerator;

@GeneratorType(className = Compensation.class, fieldType = FieldType.ALL_TYPES)
public final class CompensationRecord extends SingleBeanGenerator<Compensation> {

    public CompensationRecord() {
        super(Compensation.class);
    }

    public final static CompensationRecord instance() {
        return new CompensationRecord();
    }
}

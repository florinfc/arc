package arc.mercury.eos.generators;

import arc.mercury.eos.entities.Compensation;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

@GeneratorType(className = Compensation.class, fieldType = FieldType.ALL_TYPES)
public class CompensationCollection extends
		BeanCollectionGenerator<Compensation> {

	public CompensationCollection() {
		super(Compensation.class, 5);
	}

	public final static CompensationCollection instance() {
		return new CompensationCollection();
	}
}

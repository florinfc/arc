package arc.mercury.eos.entities;

import arc.mercury.com.generators.UserRecord;
import arc.mercury.eos.generators.NodeRecord;

import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;

/**
 * Class MessageTest User: florin Date: 28.08.2013
 */
public class MessageTest extends BaseEntityFixture<Message> {

	private static final Generator<?>[] SPECIAL_GENERATORS = {
			NodeRecord.instance(), UserRecord.instance() };

	public MessageTest() {
		super(Message.class, SPECIAL_GENERATORS);
	}
}

package arc.mercury.eos.generators;

import arc.mercury.eos.entities.Client;
import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.SingleBeanGenerator;

@GeneratorType(className = Client.class, fieldType = FieldType.ALL_TYPES)
public final class ClientRecord extends SingleBeanGenerator<Client> {

    public ClientRecord(){
        super(Client.class);
    }

    public final static ClientRecord instance() {
        return new ClientRecord();
    }
}

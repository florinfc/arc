CREATE TABLE tactivity(
id bigint AUTO_INCREMENT(1,1) NOT NULL,
userid bigint DEFAULT 0,
nodeid bigint DEFAULT 0,
startdate datetime,
enddate datetime,
notes character varying(4096),
activity character varying(4096),
[type] integer DEFAULT 0,
[value] float DEFAULT 0,
reason character varying(255),
billed integer DEFAULT 0
) COLLATE iso88591_bin ;

ALTER SERIAL tactivity_ai_id START WITH 1;
CREATE TABLE tanswer(
id numeric(19,0) NOT NULL,
[name] character varying(255),
userid bigint,
formid numeric(19,0),
maxpage integer,
javavalue character varying(1073741823),
pages character varying(32)
) COLLATE iso88591_bin ;

CREATE TABLE tassignment(
id bigint AUTO_INCREMENT(1,1) NOT NULL,
userid bigint DEFAULT 0,
nodeid bigint DEFAULT 0,
roleid bigint DEFAULT 0
) COLLATE iso88591_bin ;

ALTER SERIAL tassignment_ai_id START WITH 1;
CREATE TABLE tbranch(
id bigint AUTO_INCREMENT(1,1) NOT NULL,
[name] character varying(255),
companyid bigint DEFAULT 0,
[type] integer DEFAULT 0,
address1 character varying(255),
address2 character varying(255),
city character varying(255),
state character varying(255),
country character varying(255),
breakrequired integer DEFAULT 0,
breakperday integer DEFAULT 0,
breakduration float DEFAULT 0,
lunchrequired integer DEFAULT 0,
lunchduration integer DEFAULT 0
) COLLATE iso88591_bin ;

ALTER SERIAL tbranch_ai_id START WITH 1;
CREATE TABLE tclient(
id bigint AUTO_INCREMENT(1,1) NOT NULL,
[name] character varying(255),
branchid bigint DEFAULT 0,
address1 character varying(255),
address2 character varying(255),
city character varying(255),
zip character varying(255),
state character varying(255),
country character varying(255),
notifyneeded integer DEFAULT 0,
notifytype integer DEFAULT 0,
notifytolerance integer DEFAULT 0,
notifymode integer DEFAULT 0,
roundinc integer DEFAULT 0,
userid bigint DEFAULT 0,
approveid bigint DEFAULT 0,
accountid bigint DEFAULT 0,
adminid bigint DEFAULT 0,
mintime integer DEFAULT 0,
mintimeused integer DEFAULT 0,
dayinvoice date,
daysreview integer DEFAULT 0,
autoinvoice integer DEFAULT 0
) COLLATE iso88591_bin ;

ALTER SERIAL tclient_ai_id START WITH 1;
CREATE TABLE tcompany(
id bigint AUTO_INCREMENT(1,1) NOT NULL,
[name] character varying(255),
address1 character varying(255),
address2 character varying(255),
city character varying(255),
state character varying(255),
zip character varying(255),
country character varying(255),
idletime float DEFAULT 0,
idlemonitor integer DEFAULT 0,
rounded integer DEFAULT 0,
dayinvoice date,
needapprove integer DEFAULT 0,
needrounding integer DEFAULT 0,
autoinvoice integer DEFAULT 0
) COLLATE iso88591_bin ;

ALTER SERIAL tcompany_ai_id START WITH 1;
CREATE TABLE tcompensation(
id bigint AUTO_INCREMENT(1,1) NOT NULL,
[type] integer DEFAULT 0,
baserate float DEFAULT 0,
overtimerate float DEFAULT 0,
holidayrate float DEFAULT 0,
commissionrate float DEFAULT 0,
minwage float DEFAULT 0,
flatamount float DEFAULT 0,
commissiontype integer DEFAULT 0,
commissionvalue float DEFAULT 0,
salestype integer DEFAULT 0,
salesvalue float DEFAULT 0,
[shared] integer DEFAULT 0
) COLLATE iso88591_bin ;

ALTER SERIAL tcompensation_ai_id START WITH 1;
CREATE TABLE tform(
id numeric(19,0) NOT NULL,
[active] integer,
code character varying(255),
description character varying(255),
[name] character varying(255),
stateid numeric(19,0),
statusid numeric(19,0)
) COLLATE iso88591_bin ;

CREATE TABLE thelp(
id bigint NOT NULL,
[name] character varying(255),
adder character varying(255),
savings integer,
fee integer,
form character varying(255),
doc character varying(255),
total integer
) COLLATE iso88591_bin ;

CREATE TABLE tinvoice(
id bigint AUTO_INCREMENT(1,1) NOT NULL,
clientid bigint DEFAULT 0,
number character varying(255),
[date] date
) COLLATE iso88591_bin ;

ALTER SERIAL tinvoice_ai_id START WITH 1;
CREATE TABLE tinvoiceitem(
id bigint AUTO_INCREMENT(1,1) NOT NULL,
invoiceid bigint DEFAULT 0,
nodeid bigint DEFAULT 0,
description character varying(4096),
hours float DEFAULT 0,
hourrate float DEFAULT 0,
flatrate float DEFAULT 0,
approval integer DEFAULT 0,
[type] integer DEFAULT 0,
[value] float DEFAULT 0,
reason character varying(255),
total float DEFAULT 0,
[status] integer DEFAULT 0
) COLLATE iso88591_bin ;

ALTER SERIAL tinvoiceitem_ai_id START WITH 1;
CREATE TABLE tmessage(
id bigint AUTO_INCREMENT(1,1) NOT NULL,
userid bigint,
nodeid bigint,
text character varying(4096),
[date] datetime,
link character varying(255),
important integer
) COLLATE iso88591_bin ;

ALTER SERIAL tmessage_ai_id START WITH 1;
CREATE TABLE tnaics(
id numeric(19,0) NOT NULL,
[name] character varying(4096)
) COLLATE iso88591_bin ;

CREATE TABLE tnode(
id bigint AUTO_INCREMENT(1,1) NOT NULL,
[name] character varying(255),
description character varying(4096),
[type] integer DEFAULT 0,
parentid bigint DEFAULT 0,
ownerid bigint DEFAULT 0,
[status] integer DEFAULT 0,
budgeted float DEFAULT 0,
estimated float DEFAULT 0,
cycleperiod integer DEFAULT 0,
startdate date,
enddate date,
duedate date,
billable integer DEFAULT 0,
compensationid bigint DEFAULT 0,
hours integer DEFAULT 0,
hourly float DEFAULT 0,
flat float DEFAULT 0,
holiday float DEFAULT 0,
roundinc integer DEFAULT 0,
invoicemethod integer DEFAULT 0,
invoicerate float DEFAULT 0,
[priority] integer DEFAULT 0,
autoinvoice integer DEFAULT 0
) COLLATE iso88591_bin ;

ALTER SERIAL tnode_ai_id START WITH 1;
CREATE TABLE tpage(
id numeric(19,0) NOT NULL,
[name] character varying(255),
ord integer,
formid numeric(19,0)
) COLLATE iso88591_bin ;

CREATE TABLE tperson(
id bigint AUTO_INCREMENT(1,1) NOT NULL,
[name] character varying(255),
userid bigint DEFAULT 0,
branchid bigint DEFAULT 0,
contactid bigint DEFAULT 0,
birthdate date,
hiredate date,
releasedate date,
probationperiod integer DEFAULT 0,
holidays integer DEFAULT 0,
compensationid bigint DEFAULT 0,
[status] integer DEFAULT 0,
picture blob,
firstname character varying(255),
lastname character varying(255),
midname character varying(255),
address1 character varying(255),
address2 character varying(255),
city character varying(255),
state character varying(255),
zip character varying(255),
country character varying(255),
mail character varying(255),
phone character varying(255),
fax character varying(255),
mobile character varying(255),
ssn character varying(255),
intaccrual integer DEFAULT 0,
intpaid integer DEFAULT 0,
intpaidrate float DEFAULT 0,
intsick integer DEFAULT 0,
intsickrate float DEFAULT 0,
intvacation integer DEFAULT 0,
intvacationrate float DEFAULT 0,
extaccrual integer DEFAULT 0,
extpaid integer DEFAULT 0,
extpaidrate float DEFAULT 0,
extsick integer DEFAULT 0,
extsickrate float DEFAULT 0,
extvacation integer DEFAULT 0,
extvacationrate float DEFAULT 0,
ffiling integer,
fallowanceno integer,
fextrawh integer,
fexemptwh integer,
fexemptfuta integer,
sfiling integer,
sincometax integer,
sunemployment integer,
sexemptsui integer,
sexemptno integer,
sextrawh integer
) COLLATE iso88591_bin ;

ALTER SERIAL tperson_ai_id START WITH 1;
CREATE TABLE tquestion(
id numeric(19,0) NOT NULL,
[name] character varying(255),
pageid numeric(19,0),
ord integer,
label character varying(4096),
pattern character varying(255),
hint character varying(4096),
defaultvalue character varying(4096)
) COLLATE iso88591_bin ;

CREATE TABLE tquote(
id bigint NOT NULL,
[name] character varying(4096)
) COLLATE iso88591_bin ;

CREATE TABLE trestrict(
id numeric(19,0) NOT NULL,
[name] character varying(255),
jump character varying(1),
condition character varying(255),
[show] character varying(255),
questionid character varying(255)
) COLLATE iso88591_bin ;

CREATE TABLE trole(
id bigint AUTO_INCREMENT(1,1) NOT NULL,
[name] character varying(255) COLLATE utf8_bin ,
userid bigint
) COLLATE utf8_bin ;

ALTER SERIAL trole_ai_id START WITH 4;
CREATE TABLE trole_tuser(
trole_id bigint,
users_id bigint,
id integer NOT NULL
) COLLATE iso88591_bin ;

CREATE TABLE tss4(
id numeric(19,0) NOT NULL,
[name] character varying(255)
) COLLATE iso88591_bin ;

CREATE TABLE tstate(
id numeric(19,0) NOT NULL,
[active] integer,
code character varying(2),
[name] character varying(255),
userid numeric(19,0),
minwage float DEFAULT 0
) COLLATE iso88591_bin ;

CREATE TABLE tstatus(
id numeric(19,0) NOT NULL,
[name] character varying(255),
nextid numeric(19,0)
) COLLATE iso88591_bin ;

CREATE TABLE tteam(
id bigint AUTO_INCREMENT(1,1) NOT NULL,
[name] character varying(255),
userid bigint
) COLLATE iso88591_bin ;

ALTER SERIAL tteam_ai_id START WITH 1;
CREATE TABLE tteam_tuser(
id integer NOT NULL,
tteam_id bigint,
users_id bigint
) COLLATE iso88591_bin ;

CREATE TABLE tuser(
id numeric(19,0) NOT NULL,
[name] character varying(255) DEFAULT '''anonymous''',
[password] character varying(255),
mail character varying(255),
fname character varying(255) DEFAULT '''anonymous''',
lname character varying(255) DEFAULT '''anonymous''',
photo bit varying(1073741823),
roleid bigint DEFAULT 1,
teamid bigint DEFAULT 0
) COLLATE iso88591_bin ;

CREATE TABLE tuser_trole(
id integer NOT NULL,
tuser_id bigint,
roles_id bigint
) COLLATE iso88591_bin ;

CREATE TABLE tuser_tteam(
id integer NOT NULL,
tuser_id bigint,
teams_id bigint
) COLLATE iso88591_bin ;


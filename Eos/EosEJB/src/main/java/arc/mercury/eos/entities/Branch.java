package arc.mercury.eos.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import arc.mercury.com.base.BaseClass;
import arc.mercury.com.enums.BranchType;

/**
 * Class Branch User: florin Date: 02.09.2013
 */
@Entity
@Table(name = "TBranch")
@NamedQueries({
		@NamedQuery(name = "Branch.findAll", query = "select item from Branch item order by item.id"),
		@NamedQuery(name = "Branch.findById", query = "select item from Branch item where item.id = :param"),
		@NamedQuery(name = "Branch.findByCompany", query = "select item from Branch item where item.company.id = :param order by item.id")

})
@SuppressWarnings("serial")
public class Branch extends BaseClass<Branch> {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "name")
	private String name;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "companyId")
	private Company company;
	@Column(name = "type")
	private BranchType type;
	@Column(name = "address1")
	private String address1;
	@Column(name = "address2")
	private String address2;
	@Column(name = "city")
	private String city;
	@Column(name = "state")
	private String state;
	@Column(name = "zip")
	private String zip;
	@Column(name = "country")
	private String country;
	@Column(name = "breakRequired")
	private boolean breakRequired;
	@Column(name = "breakPerDay")
	private Integer breakPerDay;
	@Column(name = "breakDuration")
	private Float breakDuration;
	@Column(name = "lunchRequired")
	private boolean lunchRequired;
	@Column(name = "lunchDuration")
	private Float lunchDuration;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public BranchType getType() {
		return type;
	}

	public void setType(BranchType type) {
		this.type = type;
	}

	public boolean isBreakRequired() {
		return breakRequired;
	}

	public void setBreakRequired(boolean breakRequired) {
		this.breakRequired = breakRequired;
	}

	public Integer getBreakPerDay() {
		return breakPerDay;
	}

	public void setBreakPerDay(Integer breakPerDay) {
		this.breakPerDay = breakPerDay;
	}

	public Float getBreakDuration() {
		return breakDuration;
	}

	public void setBreakDuration(Float breakDuration) {
		this.breakDuration = breakDuration;
	}

	public boolean isLunchRequired() {
		return lunchRequired;
	}

	public void setLunchRequired(boolean lunchRequired) {
		this.lunchRequired = lunchRequired;
	}

	public Float getLunchDuration() {
		return lunchDuration;
	}

	public void setLunchDuration(Float lunchDuration) {
		this.lunchDuration = lunchDuration;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}

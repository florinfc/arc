package arc.mercury.eos.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import arc.mercury.com.base.BaseClass;

/**
 * Class AnswerValue User: florin Date: 02.09.2013
 */
@Entity
@Table(name = "TCompany")
@NamedQueries({
		@NamedQuery(name = "Company.findAll", query = "select item from Company item order by item.id"),
		@NamedQuery(name = "Company.findById", query = "select item from Company item where item.id = :param")

})
@SuppressWarnings("serial")
public class Company extends BaseClass<Company> {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "name")
	private String name;
	@Column(name = "address1")
	private String address1;
	@Column(name = "address2")
	private String address2;
	@Column(name = "city")
	private String city;
	@Column(name = "state")
	private String state;
	@Column(name = "zip")
	private String zip;
	@Column(name = "country")
	private String country;
	@Column(name = "idleTime")
	private Float idleTime;
	@Column(name = "idleMonitor")
	private Integer idleMonitor;
	@Column(name = "rounded")
	private Integer rounded;
	@Column(name = "dayInvoice")
	private Date dayInvoice;
	@Column(name = "autoInvoice")
	private boolean autoInvoice;
	@Column(name = "needApprove")
	private boolean needApprove;
	@Column(name = "needRounding")
	private boolean needRounding;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Float getIdleTime() {
		return idleTime;
	}

	public void setIdleTime(Float idleTime) {
		this.idleTime = idleTime;
	}

	public Integer getIdleMonitor() {
		return idleMonitor;
	}

	public void setIdleMonitor(Integer idleMonitor) {
		this.idleMonitor = idleMonitor;
	}

	public Integer getRounded() {
		return rounded;
	}

	public void setRounded(Integer rounded) {
		this.rounded = rounded;
	}

	public Date getDayInvoice() {
		return dayInvoice;
	}

	public void setDayInvoice(Date dayInvoice) {
		this.dayInvoice = dayInvoice;
	}

	public boolean isAutoInvoice() {
		return autoInvoice;
	}

	public void setAutoInvoice(boolean autoInvoice) {
		this.autoInvoice = autoInvoice;
	}

	public boolean isNeedApprove() {
		return needApprove;
	}

	public void setNeedApprove(boolean needApprove) {
		this.needApprove = needApprove;
	}

	public boolean isNeedRounding() {
		return needRounding;
	}

	public void setNeedRounding(boolean needRounding) {
		this.needRounding = needRounding;
	}
}

package arc.mercury.eos.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import arc.mercury.com.base.BaseClass;
import arc.mercury.com.entities.Role;
import arc.mercury.com.entities.User;

/**
 * Class Assignment User: florin Date: 02.09.2013
 */
@Entity
@Table(name = "TAssignment")
@NamedQueries({
		@NamedQuery(name = "Assignment.findAll", query = "select item from Assignment item order by item.id"),
		@NamedQuery(name = "Assignment.findById", query = "select item from Assignment item where item.id = :param"),
		@NamedQuery(name = "Assignment.findByNode", query = "select item from Assignment item where item.node.id = :param order by item.id"),
		@NamedQuery(name = "Assignment.findByUser", query = "select item from Assignment item where item.user.id = :param order by item.id")

})
@SuppressWarnings("serial")
public class Assignment extends BaseClass<Assignment> {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	private User user;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "nodeId")
	private Node node;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "roleId")
	private Role role;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Node getNode() {
		return node;
	}

	public void setNode(Node node) {
		this.node = node;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
}

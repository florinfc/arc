package arc.mercury.eos.managers;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import arc.mercury.com.base.BaseManager;
import arc.mercury.eos.entities.Activity;

/**
 * Class ActivityManager
 */
@Stateless
@SuppressWarnings("unchecked")
public class ActivityManager extends BaseManager<Activity> {

	public List<Activity> findByNode(Long nodeId) {
		List<Activity> result = null;
		if (nodeId != null) {
			Query query = getManager().createNamedQuery("Activity.findByNode");
			query.setParameter("param", nodeId);
			result = (List<Activity>) query.getResultList();
		}
		if (result == null) {
			result = new ArrayList<Activity>(0);
		}
		return result;
	}

	public List<Activity> findByUser(Long userId) {
		List<Activity> result = null;
		if (userId != null) {
			Query query = getManager().createNamedQuery("Activity.findByUser");
			query.setParameter("param", userId);
			result = (List<Activity>) query.getResultList();
		}
		if (result == null) {
			result = new ArrayList<Activity>(0);
		}
		return result;
	}

}

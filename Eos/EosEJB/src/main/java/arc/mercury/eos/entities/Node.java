package arc.mercury.eos.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import arc.mercury.com.base.BaseClass;
import arc.mercury.com.enums.CyclePeriod;
import arc.mercury.com.enums.InvoiceMethod;
import arc.mercury.com.enums.NodeStatus;
import arc.mercury.com.enums.NodeType;

/**
 * Class Node User: florin Date: 02.09.2013
 */
@Entity
@Table(name = "TNode")
@NamedQueries({
		@NamedQuery(name = "Node.findAll", query = "select item from Node item order by item.id"),
		@NamedQuery(name = "Node.findById", query = "select item from Node item where item.id = :param")

})
@SuppressWarnings("serial")
public class Node extends BaseClass<Node> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "name")
	private String name;
	@Column(name = "description")
	private String description;
	@Column(name = "type")
	private NodeType type;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parentId")
	private Node parent;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ownerId")
	private Person owner;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "compensationId")
	private Compensation compensation;
	@Column(name = "status")
	private NodeStatus status;
	@Column(name = "budgeted")
	private Float budgeted;
	@Column(name = "estimated")
	private Float estimated;
	@Column(name = "cyclePeriod")
	private CyclePeriod cyclePeriod;
	@Column(name = "startDate")
	private Date startDate;
	@Column(name = "endDate")
	private Date endDate;
	@Column(name = "dueDate")
	private Date dueDate;
	@Column(name = "hours")
	private Integer hours;
	@Column(name = "hourly")
	private Float hourly;
	@Column(name = "flat")
	private Float flat;
	@Column(name = "holiday")
	private Float holiday;
	@Column(name = "roundInc")
	private Integer roundInc;
	@Column(name = "invoiceMethod")
	private InvoiceMethod invoiceMethod;
	@Column(name = "invoiceRate")
	private Float invoiceRate;
	@Column(name = "priority")
	private Integer priority;
	@Column(name = "billable")
	private boolean billable;
	@Column(name = "autoInvoice")
	private boolean autoInvoice;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public NodeType getType() {
		return type;
	}

	public void setType(NodeType type) {
		this.type = type;
	}

	public Node getParent() {
		return parent;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}

	public void setParentId(Node parent) {
		this.parent = parent;
	}

	public Person getOwner() {
		return owner;
	}

	public void setOwner(Person owner) {
		this.owner = owner;
	}

	public Compensation getCompensation() {
		return compensation;
	}

	public void setCompensation(Compensation compensation) {
		this.compensation = compensation;
	}

	public NodeStatus getStatus() {
		return status;
	}

	public void setStatus(NodeStatus status) {
		this.status = status;
	}

	public Float getBudgeted() {
		return budgeted;
	}

	public void setBudgeted(Float budgeted) {
		this.budgeted = budgeted;
	}

	public Float getEstimated() {
		return estimated;
	}

	public void setEstimated(Float estimated) {
		this.estimated = estimated;
	}

	public CyclePeriod getCyclePeriod() {
		return cyclePeriod;
	}

	public void setCyclePeriod(CyclePeriod cyclePeriod) {
		this.cyclePeriod = cyclePeriod;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Integer getHours() {
		return hours;
	}

	public void setHours(Integer hours) {
		this.hours = hours;
	}

	public Float getHourly() {
		return hourly;
	}

	public void setHourly(Float hourly) {
		this.hourly = hourly;
	}

	public Float getFlat() {
		return flat;
	}

	public void setFlat(Float flat) {
		this.flat = flat;
	}

	public Float getHoliday() {
		return holiday;
	}

	public void setHoliday(Float holiday) {
		this.holiday = holiday;
	}

	public Integer getRoundInc() {
		return roundInc;
	}

	public void setRoundInc(Integer roundInc) {
		this.roundInc = roundInc;
	}

	public InvoiceMethod getInvoiceMethod() {
		return invoiceMethod;
	}

	public void setInvoiceMethod(InvoiceMethod invoiceMethod) {
		this.invoiceMethod = invoiceMethod;
	}

	public Float getInvoiceRate() {
		return invoiceRate;
	}

	public void setInvoiceRate(Float invoiceRate) {
		this.invoiceRate = invoiceRate;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public boolean isBillable() {
		return billable;
	}

	public void setBillable(boolean billable) {
		this.billable = billable;
	}

	public boolean isAutoInvoice() {
		return autoInvoice;
	}

	public void setAutoInvoice(boolean autoInvoice) {
		this.autoInvoice = autoInvoice;
	}
}

package arc.mercury.eos.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import arc.mercury.com.base.BaseClass;
import arc.mercury.com.enums.CompensationType;
import arc.mercury.com.enums.ValueType;

/**
 * Class Compensation User: florin Date: 02.09.2013
 */
@Entity
@Table(name = "TCompensation")
@NamedQueries({
		@NamedQuery(name = "Compensation.findAll", query = "select item from Compensation item order by item.id"),
		@NamedQuery(name = "Compensation.findById", query = "select item from Compensation item where item.id = :param")

})
@SuppressWarnings("serial")
public class Compensation extends BaseClass<Compensation> {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "type")
	private CompensationType type;
	@Column(name = "baseRate")
	private Float baseRate;
	@Column(name = "overtimeRate")
	private Float overtimeRate;
	@Column(name = "holidayRate")
	private Float holidayRate;
	@Column(name = "commissionRate")
	private Float commissionRate;
	@Column(name = "minWage")
	private Float minWage;
	@Column(name = "flatAmount")
	private Float flatAmount;
	@Column(name = "commissionType")
	private ValueType commissionType;
	@Column(name = "commissionValue")
	private Float commissionValue;
	@Column(name = "salesType")
	private ValueType salesType;
	@Column(name = "salesValue")
	private Float salesValue;
	@Column(name = "shared")
	private Integer shared;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CompensationType getType() {
		return type;
	}

	public void setType(CompensationType type) {
		this.type = type;
	}

	public Float getBaseRate() {
		return baseRate;
	}

	public void setBaseRate(Float baseRate) {
		this.baseRate = baseRate;
	}

	public Float getOvertimeRate() {
		return overtimeRate;
	}

	public void setOvertimeRate(Float overtimeRate) {
		this.overtimeRate = overtimeRate;
	}

	public Float getHolidayRate() {
		return holidayRate;
	}

	public void setHolidayRate(Float holidayRate) {
		this.holidayRate = holidayRate;
	}

	public Float getCommissionRate() {
		return commissionRate;
	}

	public void setCommissionRate(Float commissionRate) {
		this.commissionRate = commissionRate;
	}

	public Float getMinWage() {
		return minWage;
	}

	public void setMinWage(Float minWage) {
		this.minWage = minWage;
	}

	public Float getFlatAmount() {
		return flatAmount;
	}

	public void setFlatAmount(Float flatAmount) {
		this.flatAmount = flatAmount;
	}

	public ValueType getCommissionType() {
		return commissionType;
	}

	public void setCommissionType(ValueType commissionType) {
		this.commissionType = commissionType;
	}

	public Float getCommissionValue() {
		return commissionValue;
	}

	public void setCommissionValue(Float commissionValue) {
		this.commissionValue = commissionValue;
	}

	public ValueType getSalesType() {
		return salesType;
	}

	public void setSalesType(ValueType salesType) {
		this.salesType = salesType;
	}

	public Float getSalesValue() {
		return salesValue;
	}

	public void setSalesValue(Float salesValue) {
		this.salesValue = salesValue;
	}

	public Integer getShared() {
		return shared;
	}

	public void setShared(Integer shared) {
		this.shared = shared;
	}
}

package arc.mercury.eos.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import arc.mercury.com.base.BaseClass;
import arc.mercury.com.enums.ApprovalType;
import arc.mercury.com.enums.InvoiceStatus;
import arc.mercury.com.enums.ValueType;

/**
 * Class InvoiceItemItem User: florin Date: 02.09.2013
 */
@Entity
@Table(name = "TInvoiceItem")
@NamedQueries({
		@NamedQuery(name = "InvoiceItem.findAll", query = "select item from InvoiceItem item order by item.id"),
		@NamedQuery(name = "InvoiceItem.findById", query = "select item from InvoiceItem item where item.id = :param"),
		@NamedQuery(name = "InvoiceItem.findByInvoice", query = "select item from InvoiceItem item where item.invoice = :param order by item.id"),
		@NamedQuery(name = "InvoiceItem.findByNode", query = "select item from InvoiceItem item where item.node.id = :param order by item.id")

})
@SuppressWarnings("serial")
public class InvoiceItem extends BaseClass<InvoiceItem> {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "description")
	private String description;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "invoiceId")
	private Invoice invoice;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "nodeId")
	private Node node;
	@Column(name = "hours")
	private Float hours;
	@Column(name = "hourRate")
	private Float hourRate;
	@Column(name = "flatRate")
	private Float flatRate;
	@Column(name = "approval")
	private ApprovalType approval;
	@Column(name = "type")
	private ValueType type;
	@Column(name = "value")
	private Float value;
	@Column(name = "reason")
	private String reason;
	@Column(name = "total")
	private Float total;
	@Column(name = "status")
	private InvoiceStatus status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public Node getNode() {
		return node;
	}

	public void setNode(Node node) {
		this.node = node;
	}

	public Float getHours() {
		return hours;
	}

	public void setHours(Float hours) {
		this.hours = hours;
	}

	public Float getHourRate() {
		return hourRate;
	}

	public void setHourRate(Float hourRate) {
		this.hourRate = hourRate;
	}

	public Float getFlatRate() {
		return flatRate;
	}

	public void setFlatRate(Float flatRate) {
		this.flatRate = flatRate;
	}

	public ApprovalType getApproval() {
		return approval;
	}

	public void setApproval(ApprovalType approval) {
		this.approval = approval;
	}

	public ValueType getType() {
		return type;
	}

	public void setType(ValueType type) {
		this.type = type;
	}

	public Float getValue() {
		return value;
	}

	public void setValue(Float value) {
		this.value = value;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Float getTotal() {
		return total;
	}

	public void setTotal(Float total) {
		this.total = total;
	}

	public InvoiceStatus getStatus() {
		return status;
	}

	public void setStatus(InvoiceStatus status) {
		this.status = status;
	}
}

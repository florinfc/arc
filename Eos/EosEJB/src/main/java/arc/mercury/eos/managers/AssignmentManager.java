package arc.mercury.eos.managers;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import arc.mercury.com.base.BaseManager;
import arc.mercury.eos.entities.Assignment;

@Stateless
@SuppressWarnings("unchecked")
public class AssignmentManager extends BaseManager<Assignment> {

	public List<Assignment> findByNode(Long nodeId) {
		List<Assignment> result = null;
		if (nodeId != null) {
			Query query = getManager()
					.createNamedQuery("Assignment.findByNode");
			query.setParameter("param", nodeId);
			result = (List<Assignment>) query.getResultList();
		}
		if (result == null) {
			result = new ArrayList<Assignment>(0);
		}
		return result;
	}

	public List<Assignment> findByUser(Long userId) {
		List<Assignment> result = null;
		if (userId != null) {
			Query query = getManager()
					.createNamedQuery("Assignment.findByUser");
			query.setParameter("param", userId);
			result = (List<Assignment>) query.getResultList();
		}
		if (result == null) {
			result = new ArrayList<Assignment>(0);
		}
		return result;
	}

}

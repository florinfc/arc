package arc.mercury.eos.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import arc.mercury.com.base.BaseClass;
import arc.mercury.com.entities.User;

/**
 * Class Activity User: florin Date: 02.09.2013
 */
@Entity
@Table(name = "TActivity")
@NamedQueries({
		@NamedQuery(name = "Activity.findAll", query = "select item from Activity item order by item.id"),
		@NamedQuery(name = "Activity.findById", query = "select item from Activity item where item.id = :param"),
		@NamedQuery(name = "Activity.findByNode", query = "select item from Activity item where item.node.id = :param order by item.id"),
		@NamedQuery(name = "Activity.findByUser", query = "select item from Activity item where item.user.id = :param order by item.id")

})
@SuppressWarnings("serial")
public class Activity extends BaseClass<Activity> {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	private User user;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "nodeId")
	private Node node;
	@Column(name = "startDate")
	private Date startDate;
	@Column(name = "endDate")
	private Date endDate;
	@Column(name = "notes")
	private String notes;
	@Column(name = "activity")
	private String activity;
	@Column(name = "billed")
	private boolean billed;
	@Column(name = "discount")
	private Float discount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Node getNode() {
		return node;
	}

	public void setNode(Node node) {
		this.node = node;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public boolean isBilled() {
		return billed;
	}

	public void setBilled(boolean billed) {
		this.billed = billed;
	}

	public Float getDiscount() {
		return discount;
	}

	public void setDiscount(Float discount) {
		this.discount = discount;
	}
}

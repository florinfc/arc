package arc.mercury.eos.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import arc.mercury.com.base.BaseClass;
import arc.mercury.com.enums.NotifyMode;
import arc.mercury.com.enums.ValueType;

/**
 * Class Client User: florin Date: 02.09.2013
 */
@Entity
@Table(name = "TClient")
@NamedQueries({
		@NamedQuery(name = "Client.findAll", query = "select item from Client item order by item.id"),
		@NamedQuery(name = "Client.findById", query = "select item from Client item where item.id = :param"),
		@NamedQuery(name = "Client.findByBranch", query = "select item from Client item where item.branch.id = :param order by item.id"),
		@NamedQuery(name = "Client.findByPerson", query = "select item from Client item where item.person.id = :param order by item.id")

})
@SuppressWarnings("serial")
public class Client extends BaseClass<Client> {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "name")
	private String name;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branchId")
	private Branch branch;
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personId")
	private Person person;
	@Column(name = "address1")
	private String address1;
	@Column(name = "address2")
	private String address2;
	@Column(name = "city")
	private String city;
	@Column(name = "state")
	private String state;
	@Column(name = "zip")
	private String zip;
	@Column(name = "country")
	private String country;
	@Column(name = "notifyNeeded")
	private boolean notifyNeeded;
	@Column(name = "notifyType")
	private ValueType notifyType;
	@Column(name = "notifyTolerance")
	private Integer notifyTolerance;
	@Column(name = "notifyMode")
	private NotifyMode notifyMode;
	@Column(name = "roundInc")
	private Integer roundInc;
	@Column(name = "approveId")
	private Long approveId;
	@Column(name = "accountId")
	private Long accountId;
	@Column(name = "adminId")
	private Long adminId;
	@Column(name = "minTime")
	private Integer minTime;
	@Column(name = "minTimeUsed")
	private boolean minTimeUsed;
	@Column(name = "dayInvoice")
	private Date dayInvoice;
	@Column(name = "daysReview")
	private Integer daysReview;
	@Column(name = "autoInvoice")
	private boolean autoInvoice;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public boolean isNotifyNeeded() {
		return notifyNeeded;
	}

	public void setNotifyNeeded(boolean notifyNeeded) {
		this.notifyNeeded = notifyNeeded;
	}

	public ValueType getNotifyType() {
		return notifyType;
	}

	public void setNotifyType(ValueType notifyType) {
		this.notifyType = notifyType;
	}

	public Integer getNotifyTolerance() {
		return notifyTolerance;
	}

	public void setNotifyTolerance(Integer notifyTolerance) {
		this.notifyTolerance = notifyTolerance;
	}

	public NotifyMode getNotifyMode() {
		return notifyMode;
	}

	public void setNotifyMode(NotifyMode notifyMode) {
		this.notifyMode = notifyMode;
	}

	public Integer getRoundInc() {
		return roundInc;
	}

	public void setRoundInc(Integer roundInc) {
		this.roundInc = roundInc;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Long getApproveId() {
		return approveId;
	}

	public void setApproveId(Long approveId) {
		this.approveId = approveId;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Long getAdminId() {
		return adminId;
	}

	public void setAdminId(Long adminId) {
		this.adminId = adminId;
	}

	public Integer getMinTime() {
		return minTime;
	}

	public void setMinTime(Integer minTime) {
		this.minTime = minTime;
	}

	public boolean isMinTimeUsed() {
		return minTimeUsed;
	}

	public void setMinTimeUsed(boolean minTimeUsed) {
		this.minTimeUsed = minTimeUsed;
	}

	public Date getDayInvoice() {
		return dayInvoice;
	}

	public void setDayInvoice(Date dayInvoice) {
		this.dayInvoice = dayInvoice;
	}

	public Integer getDaysReview() {
		return daysReview;
	}

	public void setDaysReview(Integer daysReview) {
		this.daysReview = daysReview;
	}

	public boolean isAutoInvoice() {
		return autoInvoice;
	}

	public void setAutoInvoice(boolean autoInvoice) {
		this.autoInvoice = autoInvoice;
	}
}

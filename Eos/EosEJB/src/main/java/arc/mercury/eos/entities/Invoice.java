package arc.mercury.eos.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import arc.mercury.com.base.BaseClass;

/**
 * Class Invoice User: florin Date: 02.09.2013
 */
@Entity
@Table(name = "TInvoice")
@NamedQueries({
		@NamedQuery(name = "Invoice.findAll", query = "select item from Invoice item order by item.id"),
		@NamedQuery(name = "Invoice.findById", query = "select item from Invoice item where item.id = :param"),
		@NamedQuery(name = "Invoice.findByClient", query = "select item from Invoice item where item.client.id = :param order by item.id")

})
@SuppressWarnings("serial")
public class Invoice extends BaseClass<Invoice> {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "clientId")
	private Client client;
	@Column(name = "number")
	private String number;
	@Column(name = "date")
	private Date date;
	@OneToMany(fetch = FetchType.LAZY)
	private List<InvoiceItem> items;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<InvoiceItem> getItems() {
		return items;
	}

	public void setItems(List<InvoiceItem> items) {
		this.items = items;
	}
}

package arc.mercury.eos.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import arc.mercury.com.base.BaseClass;
import arc.mercury.com.entities.User;

/**
 * Class Message User: florin Date: 29.08.2013
 */
@Entity
@Table(name = "TMessage")
@NamedQueries({
		@NamedQuery(name = "Message.findAll", query = "select item from Message item order by item.id"),
		@NamedQuery(name = "Message.findById", query = "select item from Message item where item.id = :param"),
		@NamedQuery(name = "Message.findByNode", query = "select item from Message item where item.node.id = :param order by item.id"),
		@NamedQuery(name = "Message.findByUser", query = "select item from Message item where item.user.id = :param order by item.id")

})
@SuppressWarnings("serial")
public class Message extends BaseClass<Message> {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	private User user;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "nodeId")
	private Node node;
	@Column(name = "text")
	private String text;
	@Column(name = "date")
	private Date date;
	@Column(name = "link")
	private String link;
	@Column(name = "important")
	private boolean important;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Node getNode() {
		return node;
	}

	public void setNode(Node node) {
		this.node = node;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public boolean isImportant() {
		return important;
	}

	public void setImportant(boolean important) {
		this.important = important;
	}
}

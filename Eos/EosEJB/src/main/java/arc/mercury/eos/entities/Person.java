package arc.mercury.eos.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import arc.mercury.com.base.BaseClass;
import arc.mercury.com.entities.User;

/**
 * Class Person User: florin Date: 02.09.2013
 */
@Entity
@Table(name = "TPERSON")
@NamedQueries({
		@NamedQuery(name = "Person.findAll", query = "select item from Person item order by item.id"),
		@NamedQuery(name = "Person.findById", query = "select item from Person item where item.id = :param"),
		@NamedQuery(name = "Person.findByBranch", query = "select item from Person item where item.branch.id = :param order by item.id"),
		@NamedQuery(name = "Person.findByUser", query = "select item from Person item where item.user.id = :param order by item.id")

})
@SuppressWarnings("serial")
public class Person extends BaseClass<Person> {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "name")
	private String name;
	@OneToOne
	@JoinColumn(name = "userId")
	private User user;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branchId")
	private Branch branch;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contactId")
	private Person contact;
	@OneToOne
	@JoinColumn(name = "compensationId")
	private Compensation compensation;
	@Column(name = "firstName")
	private String firstName;
	@Column(name = "lastName")
	private String lastName;
	@Column(name = "midName")
	private String midName;
	@Column(name = "address1")
	private String address1;
	@Column(name = "address2")
	private String address2;
	@Column(name = "city")
	private String city;
	@Column(name = "state")
	private String state;
	@Column(name = "zip")
	private String zip;
	@Column(name = "country")
	private String country;
	@Column(name = "mail")
	private String mail;
	@Column(name = "phone")
	private String phone;
	@Column(name = "fax")
	private String fax;
	@Column(name = "mobile")
	private String mobile;
	@Column(name = "ssn")
	private String ssn;
	@Column(name = "birthDate")
	private Date birthDate;
	@Column(name = "hireDate")
	private Date hireDate;
	@Column(name = "releaseDate")
	private Date releaseDate;
	@Column(name = "probationPeriod")
	private Integer probationPeriod;
	@Column(name = "holidays")
	private Integer holidays;
	@Column(name = "intAccrual")
	private Integer intAccrual;
	@Column(name = "intPaid")
	private boolean intPaid;
	@Column(name = "intPaidRate")
	private Float intPaidRate;
	@Column(name = "intSick")
	private boolean intSick;
	@Column(name = "intSickRate")
	private Float intSickRate;
	@Column(name = "intVacation")
	private boolean intVacation;
	@Column(name = "intVacationRate")
	private Float intVacationRate;
	@Column(name = "extAccrual")
	private Integer extAccrual;
	@Column(name = "extPaid")
	private boolean extPaid;
	@Column(name = "extPaidRate")
	private Float extPaidRate;
	@Column(name = "extSick")
	private boolean extSick;
	@Column(name = "extSickRate")
	private Float extSickRate;
	@Column(name = "extVacation")
	private boolean extVacation;
	@Column(name = "extVacationRate")
	private Float extVacationRate;
	@Column(name = "fFiling")
	private Integer fFiling;
	@Column(name = "fAllowanceNo")
	private Integer fAllowanceNo;
	@Column(name = "fExtraWH")
	private Integer fExtraWH;
	@Column(name = "fExemptWH")
	private Integer fExemptWH;
	@Column(name = "fExemptFUTA")
	private Integer fExemptFUTA;
	@Column(name = "sFiling")
	private Integer sFiling;
	@Column(name = "sIncomeTax")
	private Integer sIncomeTax;
	@Column(name = "sUnemployment")
	private Integer sUnemployment;
	@Column(name = "sExemptSUI")
	private Integer sExemptSUI;
	@Column(name = "sExemptNo")
	private Integer sExemptNo;
	@Column(name = "sExtraWH")
	private Integer sExtraWH;
	@Column(name = "status")
	private Integer status;
	@Column(name = "picture")
	private byte[] picture;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public Person getContact() {
		return contact;
	}

	public void setContact(Person contact) {
		this.contact = contact;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMidName() {
		return midName;
	}

	public void setMidName(String midName) {
		this.midName = midName;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Date getHireDate() {
		return hireDate;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public Integer getProbationPeriod() {
		return probationPeriod;
	}

	public void setProbationPeriod(Integer probationPeriod) {
		this.probationPeriod = probationPeriod;
	}

	public Integer getHolidays() {
		return holidays;
	}

	public void setHolidays(Integer holidays) {
		this.holidays = holidays;
	}

	public Integer getIntAccrual() {
		return intAccrual;
	}

	public void setIntAccrual(Integer intAccrual) {
		this.intAccrual = intAccrual;
	}

	public boolean isIntPaid() {
		return intPaid;
	}

	public void setIntPaid(boolean intPaid) {
		this.intPaid = intPaid;
	}

	public Float getIntPaidRate() {
		return intPaidRate;
	}

	public void setIntPaidRate(Float intPaidRate) {
		this.intPaidRate = intPaidRate;
	}

	public boolean isIntSick() {
		return intSick;
	}

	public void setIntSick(boolean intSick) {
		this.intSick = intSick;
	}

	public Float getIntSickRate() {
		return intSickRate;
	}

	public void setIntSickRate(Float intSickRate) {
		this.intSickRate = intSickRate;
	}

	public boolean isIntVacation() {
		return intVacation;
	}

	public void setIntVacation(boolean intVacation) {
		this.intVacation = intVacation;
	}

	public Float getIntVacationRate() {
		return intVacationRate;
	}

	public void setIntVacationRate(Float intVacationRate) {
		this.intVacationRate = intVacationRate;
	}

	public Integer getExtAccrual() {
		return extAccrual;
	}

	public void setExtAccrual(Integer extAccrual) {
		this.extAccrual = extAccrual;
	}

	public boolean isExtPaid() {
		return extPaid;
	}

	public void setExtPaid(boolean extPaid) {
		this.extPaid = extPaid;
	}

	public Float getExtPaidRate() {
		return extPaidRate;
	}

	public void setExtPaidRate(Float extPaidRate) {
		this.extPaidRate = extPaidRate;
	}

	public boolean isExtSick() {
		return extSick;
	}

	public void setExtSick(boolean extSick) {
		this.extSick = extSick;
	}

	public Float getExtSickRate() {
		return extSickRate;
	}

	public void setExtSickRate(Float extSickRate) {
		this.extSickRate = extSickRate;
	}

	public boolean isExtVacation() {
		return extVacation;
	}

	public void setExtVacation(boolean extVacation) {
		this.extVacation = extVacation;
	}

	public Float getExtVacationRate() {
		return extVacationRate;
	}

	public void setExtVacationRate(Float extVacationRate) {
		this.extVacationRate = extVacationRate;
	}

	public Integer getfFiling() {
		return fFiling;
	}

	public void setfFiling(Integer fFiling) {
		this.fFiling = fFiling;
	}

	public Integer getfAllowanceNo() {
		return fAllowanceNo;
	}

	public void setfAllowanceNo(Integer fAllowanceNo) {
		this.fAllowanceNo = fAllowanceNo;
	}

	public Integer getfExtraWH() {
		return fExtraWH;
	}

	public void setfExtraWH(Integer fExtraWH) {
		this.fExtraWH = fExtraWH;
	}

	public Integer getfExemptWH() {
		return fExemptWH;
	}

	public void setfExemptWH(Integer fExemptWH) {
		this.fExemptWH = fExemptWH;
	}

	public Integer getfExemptFUTA() {
		return fExemptFUTA;
	}

	public void setfExemptFUTA(Integer fExemptFUTA) {
		this.fExemptFUTA = fExemptFUTA;
	}

	public Integer getsFiling() {
		return sFiling;
	}

	public void setsFiling(Integer sFiling) {
		this.sFiling = sFiling;
	}

	public Integer getsIncomeTax() {
		return sIncomeTax;
	}

	public void setsIncomeTax(Integer sIncomeTax) {
		this.sIncomeTax = sIncomeTax;
	}

	public Integer getsUnemployment() {
		return sUnemployment;
	}

	public void setsUnemployment(Integer sUnemployment) {
		this.sUnemployment = sUnemployment;
	}

	public Integer getsExemptSUI() {
		return sExemptSUI;
	}

	public void setsExemptSUI(Integer sExemptSUI) {
		this.sExemptSUI = sExemptSUI;
	}

	public Integer getsExemptNo() {
		return sExemptNo;
	}

	public void setsExemptNo(Integer sExemptNo) {
		this.sExemptNo = sExemptNo;
	}

	public Integer getsExtraWH() {
		return sExtraWH;
	}

	public void setsExtraWH(Integer sExtraWH) {
		this.sExtraWH = sExtraWH;
	}

	public Compensation getCompensation() {
		return compensation;
	}

	public void setCompensation(Compensation compensation) {
		this.compensation = compensation;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public byte[] getPicture() {
		return picture;
	}

	public void setPicture(byte[] picture) {
		this.picture = picture;
	}

}

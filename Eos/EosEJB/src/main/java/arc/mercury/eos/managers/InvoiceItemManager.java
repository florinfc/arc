package arc.mercury.eos.managers;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import arc.mercury.com.base.BaseManager;
import arc.mercury.eos.entities.InvoiceItem;

/**
 * Class InvoiceItemManager
 */
@Stateless
public class InvoiceItemManager extends BaseManager<InvoiceItem> {

	public InvoiceItem findByNode(Long nodeId) {
		if (nodeId == null) {
			return null;
		}
		InvoiceItem result = null;
		Query query = getManager().createNamedQuery("InvoiceItem.findByNode");
		query.setParameter("param", nodeId);
		try {
			result = (InvoiceItem) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	public InvoiceItem findByInvoice(Long invoiceId) {
		if (invoiceId == null) {
			return null;
		}
		InvoiceItem result = null;
		Query query = getManager()
				.createNamedQuery("InvoiceItem.findByInvoice");
		query.setParameter("param", invoiceId);
		try {
			result = (InvoiceItem) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

}

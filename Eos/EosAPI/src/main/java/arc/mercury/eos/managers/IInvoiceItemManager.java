package arc.mercury.eos.managers;

import arc.mercury.eos.entities.IInvoiceItem;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import java.util.List;

/**
 * Class IInvoiceItemManager
 * IInvoiceItem: florin
 * Date: 03.09.2013
 */
@Stateless
public interface IInvoiceItemManager {

	public EntityManager getManager();

	public IInvoiceItem insert(IInvoiceItem item);

	public IInvoiceItem update(IInvoiceItem item);

	public void delete(IInvoiceItem item);

	public void delete(Long id);

	public List<IInvoiceItem> findAll();

	public IInvoiceItem findById(Long id);

	public IInvoiceItem findByName(String name);

	public List<IInvoiceItem> findLikeName(String name);
}

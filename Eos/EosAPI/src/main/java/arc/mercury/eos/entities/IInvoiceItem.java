package arc.mercury.eos.entities;

import arc.mercury.base.BaseClass;
import arc.mercury.common.Discount;
import arc.mercury.enums.ApprovalType;
import arc.mercury.enums.InvoiceStatus;

/**
 * Class InvoiceItemItem
 * User: florin
 * Date: 02.09.2013
 */
public class IInvoiceItem extends BaseClass<IInvoiceItem> {
	private Long id;
	private String description;
	private Long invoiceId;
	private Long nodeId;
	private Float hours;
	private Float hourRate;
	private Float flatRate;
	private ApprovalType approval;
	private Discount discount;
	private Float total;
	private InvoiceStatus status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Long invoiceId) {
		this.invoiceId = invoiceId;
	}

	public Long getNodeId() {
		return nodeId;
	}

	public void setNodeId(Long nodeId) {
		this.nodeId = nodeId;
	}

	public Float getHours() {
		return hours;
	}

	public void setHours(Float hours) {
		this.hours = hours;
	}

	public Float getHourRate() {
		return hourRate;
	}

	public void setHourRate(Float hourRate) {
		this.hourRate = hourRate;
	}

	public Float getFlatRate() {
		return flatRate;
	}

	public void setFlatRate(Float flatRate) {
		this.flatRate = flatRate;
	}

	public ApprovalType getApproval() {
		return approval;
	}

	public void setApproval(ApprovalType approval) {
		this.approval = approval;
	}

	public Discount getDiscount() {
		return discount;
	}

	public void setDiscount(Discount discount) {
		this.discount = discount;
	}

	public Float getTotal() {
		return total;
	}

	public void setTotal(Float total) {
		this.total = total;
	}

	public InvoiceStatus getStatus() {
		return status;
	}

	public void setStatus(InvoiceStatus status) {
		this.status = status;
	}
}

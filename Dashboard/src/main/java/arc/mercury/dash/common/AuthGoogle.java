package arc.mercury.dash.common;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "google")
@SessionScoped
public class AuthGoogle extends AuthBase {

	public AuthGoogle() {
		super(AppType.Google);
	}

	public static void main(String[] args) {
		AuthGoogle auth = new AuthGoogle();
		String result = auth.getInfo();
		System.out.println(result);
	}
}

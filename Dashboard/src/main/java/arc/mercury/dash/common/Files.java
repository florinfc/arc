package arc.mercury.dash.common;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class Files {

	private String name;
	private Date modDate;
	private String modBy;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getModDate() {
		return modDate;
	}

	public void setModDate(Date modDate) {
		this.modDate = modDate;
	}

	public String getModBy() {
		return modBy;
	}

	public void setModBy(String modBy) {
		this.modBy = modBy;
	}

	public String getShortModDate() {
		DateFormat dsf = DateFormat
				.getDateInstance(DateFormat.SHORT, Locale.US);
		return dsf.format(modDate);
	}
}

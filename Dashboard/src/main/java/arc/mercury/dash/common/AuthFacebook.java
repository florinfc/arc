package arc.mercury.dash.common;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/*
 *//**
 * The Facebook User Filter ensures that a Facebook client that pertains to
 * the logged in user is available in the session object named
 * "facebook.user.client".
 * 
 * The session ID is stored as "facebook.user.session". It's important to get
 * the session ID only when the application actually needs it. The user has to
 * authorise to give the application a session key.
 * 
 */
/*
*/
@ManagedBean(name = "Facebook")
@SessionScoped
public class AuthFacebook extends AuthBase {

	public AuthFacebook() {
		super(AppType.Facebook);
	}

	public static void main(String[] args) {
		AuthFacebook auth = new AuthFacebook();
		String result = auth.getInfo();
		System.out.println(result);
	}
}

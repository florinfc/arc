package arc.mercury.dash.common;

public enum AppType {

	Own(0), Facebook(1), Google(2), Linkedin(3);

	private Integer value;

	AppType(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}

}

package arc.mercury.dash.common;

public class WorkLog {

	private Boolean selected;
	private String project;
	private String task;
	private String notes;
	private Double hours;

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Double getHours() {
		return hours;
	}

	public void setHours(Double d) {
		this.hours = d;
	}

}

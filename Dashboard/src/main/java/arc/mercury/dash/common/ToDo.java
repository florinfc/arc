package arc.mercury.dash.common;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class ToDo {

	private Boolean selected;
	private String name;
	private String description;
	private Date dueDate;
	private Date estTime;
	private Date wrkTime;
	private Boolean status;

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description.substring(0, 80);
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Date getEstTime() {
		return estTime;
	}

	public void setEstTime(Date estTime) {
		this.estTime = estTime;
	}

	public Date getWrkTime() {
		return wrkTime;
	}

	public void setWrkTime(Date wrkTime) {
		this.wrkTime = wrkTime;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getShortDueDate() {
		DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, Locale.US);
		return df.format(dueDate);
	}

	public String getShortWrkTime() {
		DateFormat df = DateFormat.getTimeInstance();
		return df.format(wrkTime);
	}

	public String getShortEstTime() {
		DateFormat df = DateFormat.getTimeInstance();
		return df.format(estTime);
	}

}

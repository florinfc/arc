package arc.mercury.dash.common;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Verb;

@ManagedBean(name = "callGoogle")
@SessionScoped
public class CallGoogle extends CallBase {

	public CallGoogle() {
		super(AppType.Google);
	}

	private static final String NETWORK_NAME = "Google";
	private static final String AUTHORIZE_URL = "https://www.google.com/accounts/OAuthAuthorizeToken?oauth_token=";
	private static final String PROTECTED_RESOURCE_URL = "https://docs.google.com/feeds/default/private/full/";
	private static final String SCOPE = "https://docs.google.com/feeds/";
	private static final String CODE = "ya29.AHES6ZS5gk-45i8Rqi4mJLERwtUD1Z6HPHib4wT5EoJyeiImKxi0TQ";

	public static void main(String[] args) {
		CallGoogle auth = new CallGoogle();
		// Token requestToken = new Token(CallBase.gKey, CallBase.gSecret);
		// Verifier verifier = new Verifier(CODE);
		// Token accessToken = auth.getService().getAccessToken(requestToken,
		// verifier);
		OAuthRequest request = new OAuthRequest(Verb.GET,
				PROTECTED_RESOURCE_URL);
		auth.getService().signRequest(auth.getAccessToken(), request);
		// request.addHeader("GData-Version", "3.0");
		Response response = request.send();
		System.out.println(response.getCode());
		System.out.println(response.getBody());
	}
}
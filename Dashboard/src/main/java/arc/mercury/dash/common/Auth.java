package arc.mercury.dash.common;

import java.security.Principal;
import java.security.acl.Group;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.jboss.security.SimpleGroup;
import org.jboss.security.SimplePrincipal;
import org.jboss.security.auth.spi.UsernamePasswordLoginModule;

import arc.mercury.com.entities.IUser;
import arc.mercury.dash.model.DashModel;

@ManagedBean(name = "auth", eager = true)
@SessionScoped
public class Auth extends UsernamePasswordLoginModule {

	@ManagedProperty(value = "#{dashModel}")
	private DashModel dashModel;
	private Principal userPrincipal = null;
	private Principal anonymous = null;
	private IUser login = new IUser();
	private IUser user = new IUser();

	@SuppressWarnings("rawtypes")
	public void initialize(Subject subject, CallbackHandler callbackHandler,
			Map sharedState, Map options) {
		super.initialize(subject, callbackHandler, sharedState, options);
		String anon = (String) options.get("unauthenticatedIdentity");
		if (anon != null) {
			anonymous = new SimplePrincipal(anon);
		}
	}

	@Override
	protected String getUsersPassword() {
		String uname = super.getUsername();
		if (user == null) {
			user = dashModel.getUserByMail(login.getName());
			if (user == null) {
				JsfMessage.format(FacesMessage.SEVERITY_ERROR,
						"Invalid ID or password.", null);
				return null;
			}
			login.setMail(login.getName());
			login.setName(null);
		}
		if (user.getPassword() == null) {
			user.setPassword("");
		}
		if (user.getName().isEmpty()) {
			if (!login.getMail().equals(user.getMail())) {
				JsfMessage.format(FacesMessage.SEVERITY_ERROR, "Invalid mail.",
						null);
				return null;
			}
		} else {
			if (!login.getName().equals(user.getName())
					|| !login.getPassword().equals(user.getPassword())) {
				JsfMessage.format(FacesMessage.SEVERITY_ERROR,
						"Invalid ID or password.", null);
				return null;
			}
		}
		return user.getPassword();
	}

	@Override
	protected Group[] getRoleSets() {
		Group[] groups = { new SimpleGroup("Roles") };
		SimplePrincipal role = new SimplePrincipal(user.getRole().getName());
		groups[0].addMember(role);
		return groups;
	}

	@Override
	public boolean login() {
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		try {
			userPrincipal = request.getUserPrincipal();
			if (request.getUserPrincipal() != null) {
				request.logout();
			}
			request.login(login.getName(), login.getPassword());
			userPrincipal = request.getUserPrincipal();
			user = dashModel.getUserByName(userPrincipal.getName());
		} catch (ServletException e) {
			JsfMessage
					.format(FacesMessage.SEVERITY_ERROR, e.getMessage(), null);
			return false;
		}
		return true;
	}

	public String clearUser() {
		user = new IUser();
		return null;
	}

	public DashModel getDashModel() {
		return dashModel;
	}

	public void setDashModel(DashModel dashModel) {
		this.dashModel = dashModel;
	}

	public Principal getUserPrincipal() {
		return userPrincipal;
	}

	public void setUserPrincipal(Principal userPrincipal) {
		this.userPrincipal = userPrincipal;
	}

	public Principal getAnonymous() {
		return anonymous;
	}

	public void setAnonymous(Principal anonymous) {
		this.anonymous = anonymous;
	}

	public IUser getLogin() {
		return login;
	}

	public void setLogin(IUser login) {
		this.login = login;
	}

	public IUser getUser() {
		return user;
	}

	public void setUser(IUser user) {
		this.user = user;
	}

}

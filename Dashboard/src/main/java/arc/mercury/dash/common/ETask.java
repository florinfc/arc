package arc.mercury.dash.common;

import arc.mercury.webservice.Task;

public class ETask extends Task {

	private Integer ownerId;
	private Integer repeatId;

	public ETask() {
		super();
	}

	public Integer getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}

	public Integer getRepeatId() {
		return repeatId;
	}

	public void setRepeatId(Integer repeatId) {
		this.repeatId = repeatId;
	}

}

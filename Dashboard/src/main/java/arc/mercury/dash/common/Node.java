package arc.mercury.dash.common;

import java.util.LinkedList;
import java.util.List;

public class Node<T> {
	  private T value;
	  private T status;
	  private T duedate;
	  private T estimate;
	  private T worktime;
	  private T id;
	  private T visible;
	  private Node<T> parent;
	  private LinkedList<Node<T>> kids = new LinkedList<Node<T>>();

	  public Node(Node<T> parent, T value,T status, T duedate, T estimate, T worktime, T id, T visible) {
		    this.parent = parent;
		    this.value = value;
		    this.status = status;
		    this.duedate = duedate;
		    this.estimate = estimate;
		    this.worktime = worktime;
		    this.setVisible(visible);
		    this.setId(id);
		    
		  }

	  public List<Node<T>> getKids() {return kids;}
	  public T getValue() { return value; }

	  public boolean getHasParent() { return parent != null; }

	  public boolean isFirstChild() {
	    return parent != null && parent.kids.peekFirst() == this;
	  }

	  public boolean isLastChild() {
	    return parent != null && parent.kids.peekLast() == this;
	  }

	public T getStatus() {
		return status;
	}
	public T getDuedate() {
		return duedate;
	}
	public void setDuedate(T duedate) {
		this.duedate = duedate;
	}
	public T getEstimate() {
		return estimate;
	}
	public void setEstimate(T estimate) {
		this.estimate = estimate;
	}
	public T getWorktime() {
		return worktime;
	}
	public void setWorktime(T worktime) {
		this.worktime = worktime;
	}

	public T getId() {
		return id;
	}

	public void setId(T id) {
		this.id = id;
	}

	public T getVisible() {
		return visible;
	}

	public void setVisible(T visible) {
		this.visible = visible;
	}


	}
package arc.mercury.dash.common;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.FacebookApi;
import org.scribe.builder.api.GoogleApi;
import org.scribe.builder.api.LinkedInApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

public class AuthBase {

	public static final String fScope = "https://graph.facebook.com/me";
	public static final String fKey = "5f06856d94024c1cad07731ddcb00d8c";
	public static final String fSecret = "60b04d1f66eb5722d7a5202c84324ce5";
	public static final String fAuthToken = "";
	public static final String fAuthSecret = "";
	public static final String fCallback = "";

	public static final String gScope = "https://www.googleapis.com/auth/plus.login";
	public static final String gKey = "AIzaSyAEIChmHZ6xj42Qykbt7p3qajYDCczXccQ";
	public static final String gSecret = "b4gd18mbKYy4f9LwtPs1jJLi";
	public static final String gAuthToken = "483790801663.apps.googleusercontent.com";
	public static final String gAuthSecret = "b4gd18mbKYy4f9LwtPs1jJLi";
	public static final String gCallback = "urn:ietf:wg:oauth:2.0:oob";

	public static final String lScope = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address)";
	public static final String lKey = "f26wwche5zko";
	public static final String lSecret = "kBaTXBifONSjP8vb";
	public static final String lAuthToken = "e1d4f80b-4f6d-4725-9ba0-153102878eed";
	public static final String lAuthSecret = "c9cf729c-a83a-4ff8-94da-775b1257a06f";
	public static final String lCallback = "";

	private OAuthService service;
	private Token accessToken;
	private String scope;

	public AuthBase(AppType app) {
		switch (app) {
		case Facebook:
			this.service = new ServiceBuilder().provider(FacebookApi.class)
					.apiKey(fKey).apiSecret(fSecret).callback(fCallback)
					.build();
			accessToken = new Token(fAuthToken, fAuthSecret);
			scope = fScope;
			break;
		case Google:
			this.service = new ServiceBuilder().provider(GoogleApi.class)
					.apiKey(gKey).apiSecret(gSecret).callback(gCallback)
					.build();
			accessToken = new Token(gAuthToken, gAuthSecret);
			scope = gScope;
			break;
		case Linkedin:
			this.service = new ServiceBuilder().provider(LinkedInApi.class)
					.apiKey(lKey).apiSecret(lSecret).callback(lCallback)
					.build();
			accessToken = new Token(lAuthToken, lAuthSecret);
			scope = lScope;
			break;
		default:
			break;
		}
	}

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws IOException {
		String authenticationUrl = service.getAuthorizationUrl(null);
		res.sendRedirect(authenticationUrl);
	}

	public String getInfo() {
		OAuthRequest request = new OAuthRequest(Verb.GET, scope);
		service.signRequest(accessToken, request);
		Response response = request.send();
		return response.getBody();
	}

}

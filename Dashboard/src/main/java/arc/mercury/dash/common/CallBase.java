package arc.mercury.dash.common;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.FacebookApi;
import org.scribe.builder.api.GoogleApi;
import org.scribe.builder.api.LinkedInApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

public class CallBase {

	public static final String fScope = "https://graph.facebook.com/me";
	public static final String fKey = "499130706828622";
	public static final String fSecret = "95b8519aee9f3aba6752ee1cfba2033d";
	public static final String fAuthToken = "";
	public static final String fAuthSecret = "";
	public static final String fCallback = "";
	public static final String appId = "499130706828622";

	public static final String gScope = "https://www.googleapis.com/auth/plus.login";
	public static final String gKey = "AIzaSyAEIChmHZ6xj42Qykbt7p3qajYDCczXccQ";
	public static final String gSecret = "b4gd18mbKYy4f9LwtPs1jJLi";
	public static final String gAuthToken = "483790801663.apps.googleusercontent.com";
	public static final String gAuthSecret = "b4gd18mbKYy4f9LwtPs1jJLi";
	public static final String gCallback = "urn:ietf:wg:oauth:2.0:oob";

	public static final String lScope = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address)";
	public static final String lKey = "f26wwche5zko";
	public static final String lSecret = "kBaTXBifONSjP8vb";
	public static final String lAuthToken = "f3e48801-c649-4573-baf9-83ee4a691e74";
	public static final String lAuthSecret = "de2c6e86-3143-49c9-acd2-7c5d5a7fde02";
	public static final String lCallback = "";

	private OAuthService service;
	private Token accessToken;

	public CallBase(AppType app) {
		switch (app) {
		case Facebook:
			this.service = new ServiceBuilder().provider(FacebookApi.class)
					.apiKey(fKey).apiSecret(fSecret).callback(fCallback)
					.build();
			accessToken = new Token(fAuthToken, fAuthSecret);
			break;
		case Google:
			this.service = new ServiceBuilder().provider(GoogleApi.class)
					.apiKey(gKey).apiSecret(gSecret).callback(gCallback)
					.build();
			accessToken = new Token(gAuthToken, gAuthSecret);
			break;
		case Linkedin:
			this.service = new ServiceBuilder().provider(LinkedInApi.class)
					.apiKey(lKey).apiSecret(lSecret).callback(lCallback)
					.build();
			accessToken = new Token(lAuthToken, lAuthSecret);
			break;
		default:
			break;
		}
	}

	public void doGet(HttpServletRequest req, HttpServletResponse res) {
		String code = "";
		Enumeration<String> paramEnum = req.getParameterNames();
		while (paramEnum.hasMoreElements()) {
			String name = (String) paramEnum.nextElement();
			if (name.equals("code")) {
				code = req.getParameter(name);
			}
			Verifier verifier = new Verifier(code);
			// Token requestToken = service.getRequestToken();
			// Verifier verifier = new Verifier(OAuthSecret);
			// accessToken = service.getAccessToken(requestToken, verifier);
		}
	}

	public String getInfo(String scope) {
		OAuthRequest request = new OAuthRequest(Verb.GET, scope);
		service.signRequest(accessToken, request);
		Response response = request.send();
		return response.getBody();
	}

	public Token getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(Token accessToken) {
		this.accessToken = accessToken;
	}

	public OAuthService getService() {
		return service;
	}

	public void setService(OAuthService service) {
		this.service = service;
	}

}

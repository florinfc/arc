package arc.mercury.dash.common;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "CbLinkedin")
@SessionScoped
public class CallLinkedin extends CallBase {

	public CallLinkedin() {
		super(AppType.Linkedin);
	}

}
package arc.mercury.dash.common;

import java.io.ByteArrayInputStream;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import arc.mercury.com.entities.IRole;
import arc.mercury.com.entities.IUser;

@ManagedBean(name = "linkedin")
@SessionScoped
public class AuthLinkedin extends AuthBase {

	private IUser user;

	public AuthLinkedin() {
		super(AppType.Linkedin);
	}

	public IUser getUser() {
		user = new IUser();
		String result = getInfo();
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc;
			doc = dBuilder.parse(new InputSource(new ByteArrayInputStream(
					result.getBytes())));
			NodeList nList = doc.getElementsByTagName("person");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					user.setName(eElement.getElementsByTagName("id").item(0)
							.getTextContent());
					user.setFname(eElement.getElementsByTagName("first-name")
							.item(0).getTextContent());
					user.setLname(eElement.getElementsByTagName("last-name")
							.item(0).getTextContent());
					user.setMail(eElement.getElementsByTagName("email-address")
							.item(0).getTextContent());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		user.setPassword("");
		IRole role = new IRole();
		role.setId(1L);
		user.setRole(role);
		return user;
	}

	public static void main(String[] args) {
		AuthLinkedin auth = new AuthLinkedin();
		System.out.println(auth.getUser());
	}

}

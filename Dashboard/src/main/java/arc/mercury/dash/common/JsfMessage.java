package arc.mercury.dash.common;

import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

public class JsfMessage {

	/**
	 * Formats and displays a message in the messages area of the screen
	 * 
	 * @param sev
	 *            severity level (on the form FacesMessage.SEVERITY_XXX)
	 * @param msg
	 *            multi-line message to be displayed
	 * @param log
	 *            optional (if not null) Logger to trace the message
	 */
	public static void format(Severity sev, String msg, Logger log) {
		FacesContext context = FacesContext.getCurrentInstance();
		for (String line : msg.split("\n")) {
			context.addMessage(null, new FacesMessage(sev, line, msg));
		}
		if (log != null) {
			log.info(msg);
		}
	}

}

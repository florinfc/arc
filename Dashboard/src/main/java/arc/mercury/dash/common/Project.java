package arc.mercury.dash.common;

import java.sql.Time;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import arc.mercury.webservice.BaseClassExt;

public class Project extends BaseClassExt {

	private Boolean selected;
	private Date dueDay;
	private Time estTime;
	private Time wrkTime;
	private Boolean status;

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public Date getDueDay() {
		return dueDay;
	}

	public void setDueDay(Date dueDay) {
		this.dueDay = dueDay;
	}

	public Time getEstTime() {
		return estTime;
	}

	public void setEstTime(Time estTime) {
		this.estTime = estTime;
	}

	public Time getWrkTime() {
		return wrkTime;
	}

	public void setWrkTime(Time wrkTime) {
		this.wrkTime = wrkTime;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getShortDueDate() {
		DateFormat dsf = DateFormat
				.getDateInstance(DateFormat.SHORT, Locale.US);
		return dsf.format(dueDate);
	}

	public String getShortWrkTime() {
		DateFormat df = DateFormat.getTimeInstance();
		return df.format(wrkTime);
	}

	public String getShortEstTime() {
		DateFormat df = DateFormat.getTimeInstance();
		return df.format(estTime);
	}

}

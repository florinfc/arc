package arc.mercury.dash.model;

import arc.mercury.com.common.AnswerValue;
import arc.mercury.com.entities.IMail;
import arc.mercury.com.entities.IState;
import arc.mercury.com.entities.IUser;
import arc.mercury.com.enums.MailName;
import arc.mercury.com.services.IDashServices;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@SuppressWarnings("serial")
@ManagedBean(name = "dashModel", eager = false)
@SessionScoped
public class DashModel extends Service implements Serializable {

	private static String nsURI = "http://dash.server:8081/";
	private static QName servicesName = new QName(nsURI, "DashServicesService");
	private static QName servicesPort = new QName(nsURI, "DashServicesPort");
	private static Logger log = Logger.getLogger("DashModel");
	private IDashServices dashService;

	public DashModel() throws Exception {
		super(new URL(nsURI + "common/services?wsdl"), servicesName);
		dashService = super.getPort(servicesPort, IDashServices.class);
	}

	public IDashServices getDashService() {
		return dashService;
	}

	public void setDashService(IDashServices dashService) {
		this.dashService = dashService;
	}

	public String getTitle() {
		return "Dashboard";
	}

	public List<IUser> getAllUsers() {
		return dashService.getAllUsers();
	}

	public List<IUser> getAnonymous() {
		return dashService.getAnonymous();
	}

	public IUser getUserById(Long id) {
		return dashService.getUserById(id);
	}

	public IUser getUserByName(String name) {
		return dashService.getUserByName(name);
	}

	public IUser getUserByMail(String mail) {
		return dashService.getUserByMail(mail);
	}

	public IUser saveUser(IUser user) {
		return dashService.saveUser(user);
	}

	public List<SelectItem> getStates() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(0, "State"));
		for (IState item : dashService.getStates()) {
			if (item.getActive() != 0)
				items.add(new SelectItem(item.getId(), item.getCode()));
		}
		return items;
	}

	public List<IState> getAllStates() {
		return dashService.getStates();
	}

	public IState getStateById(Long id) {
		return dashService.getStateById(id);
	}

	public void deleteUser(IUser user) {
		dashService.deleteUser(user);
	}

	public String sendMail(IUser user) {
		IUser[] toList = new IUser[1];
		toList[0] = user;

		IMail mail = dashService.getMailByName(MailName.Save.getValue());

		String subject = mail.getSubject();
		String url = "http://dash.server:8081/dashboard/incwiz/start.xhtml?id="
				+ user.getId().toString();
		String line = String.format(mail.getContent(), url);
		String[] bodyLines = line.split("\\n");

		return dashService.sendSimpleMail(user, subject, bodyLines, null);
	}

	public String sendMail(IUser user, String[] atts) {
		IUser[] toList = new IUser[1];
		toList[0] = user;

		IMail mail = dashService.getMailByName(MailName.Finish.getValue());

		String subject = mail.getSubject();
		String[] bodyLines = mail.getContent().split("\\n");
		return dashService.sendSimpleMail(user, subject, bodyLines, atts);
	}

	public String incPdf(String formName, IUser user, AnswerValue com) {
		try {
			String result = dashService.fillPdf(
					System.getProperty("file.separator") + "Forms", formName,
					com, user.getFullName());
			return result;
		} catch (Exception e) {
			log.severe(e.toString());
			return null;
		}
	}

}

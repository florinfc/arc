package arc.mercury.dash.model;

import arc.mercury.com.base.Constants;
import arc.mercury.webservice.*;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.logging.Logger;

@ManagedBean(name = "eosModel", eager = false)
@SessionScoped
@WebServiceClient(name = "EOS", targetNamespace = "http://arcmercury.com/websevices", wsdlLocation = "http://eos.server:8082/EOS.asmx?wsdl")
public class EosModel extends Service {

	private final static URL EOS_WSDL_LOCATION;
	private final static Logger logger = Logger
			.getLogger(arc.mercury.webservice.EOS.class.getName());

	static {
		URL url = null;
		try {
			URL baseUrl;
			baseUrl = arc.mercury.webservice.EOS.class.getResource(".");
			url = new URL(baseUrl, "http://eos.server:8082/EOS.asmx?wsdl");
		} catch (MalformedURLException e) {
			logger.warning("Failed to create URL for the wsdl Location: 'http://eos.server:8082/EOS.asmx?wsdl', retrying as a local file");
			logger.warning(e.getMessage());
		}
		EOS_WSDL_LOCATION = url;
	}

	public String UserID = "19";

	public EosModel(URL wsdlLocation, QName serviceName) {
		super(wsdlLocation, serviceName);
	}

	public EosModel() {
		super(EOS_WSDL_LOCATION, new QName("http://arcmercury.com/websevices",
				"EOS"));
	}

	/**
	 * @return returns EOSSoap
	 */
	@WebEndpoint(name = "EOSSoap")
	public EOSSoap getEOSSoap() {
		return super.getPort(new QName("http://arcmercury.com/websevices",
				"EOSSoap"), EOSSoap.class);
	}

	/**
	 * @param features A list of {@link javax.xml.ws.WebServiceFeature} to configure
	 *                 on the proxy. Supported features not in the
	 *                 <code>features</code> parameter will have their default
	 *                 values.
	 * @return returns EOSSoap
	 */
	// @WebEndpoint(name = "EOSSoap")
	// public EOSSoap getEOSSoap(WebServiceFeature... features) {
	// return super.getPort(new QName("http://arcmercury.com/websevices",
	// "EOSSoap"), EOSSoap.class, features);
	// }
	public List<AmObject> getWSObject(int ClassID, int ParentID, int ObjectID,
									  int UserID) {

		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getObject(Constants.KEY, ClassID, ParentID, ObjectID, 19)
				.getAmObject();

	}

	public void insertWSObject(int ClassID, int ParentID, int ObjectID,
							   String Attributes) {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);

		soap.insertObject(Constants.KEY, ClassID, ParentID, ObjectID,
				Attributes, UserID);
	}

	public Integer insertObject(int ClassID, int ParentID, int ObjectID,
								String Attributes) {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);

		return soap.insertObject(Constants.KEY, ClassID, ParentID, ObjectID,
				Attributes, UserID);

	}

	public void updateWSObject(int ClassID, int ParentID, int ObjectID,
							   String Attributes) {

		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		soap.updateObject(Constants.KEY, ClassID, ParentID, ObjectID,
				Attributes, Integer.parseInt(UserID));
	}

	public List<Client> getClients(int UserID) {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getAllClientsForUser(Constants.KEY, UserID).getClient();
	}

	public List<InvoiceMethod> getInvoiceMethods() {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getInvoiceMethods(Constants.KEY).getInvoiceMethod();
	}

	public List<BaseClass> getInvoiceType() {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getInvoiceType(Constants.KEY).getBaseClass();
	}

	public List<BaseClass> getTaskRepeat() {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getTaskRepeat(Constants.KEY).getBaseClass();
	}

	public List<BaseClassExt> getPrandPjforUser() {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getAllProgramsandProjectsForUser(Constants.KEY, 19)
				.getBaseClassExt();
	}

	public List<BaseClass> getTasksandTodoforUser() {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getAllTasksAndTodosForUser(Constants.KEY, 19)
				.getBaseClass();
	}

	public List<BaseClass> getTodoforUser() {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getAllToDosForUser(Constants.KEY, 19, 0).getBaseClass();
	}

	public List<Project> getProgramswithMessage(XMLGregorianCalendar lastDate) {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getAllProgramsandProjectsWithMessage(Constants.KEY, 19,
				lastDate).getProject();
	}

	public List<Project> getMessagesforProject(XMLGregorianCalendar lastDate,
											   String type, Integer projectID) {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getMessagesforProjects(Constants.KEY, 19, projectID,
				lastDate, type).getProject();
	}

	public List<BaseClassExt> getPrandPjforUserandClient(Integer ClientID) {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getAllProgramsandProjectsForUserandClient(Constants.KEY,
				19, ClientID).getBaseClassExt();
	}

	public List<BaseClassExt> getAllTasksForUserandClient(Integer ClientID,
														  Integer ProjectID, String Type, Integer Archive) {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getAllTasksForUserandClient(Constants.KEY, 19, ClientID,
				ProjectID, Type, Archive).getBaseClassExt();
	}

	public List<BaseClassExt> getTodoChangesforTask(Integer TaskID) {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getTodoChangesforTask(Constants.KEY, 19, TaskID)
				.getBaseClassExt();
	}

	public void updateToDoStatus(int ToDoID, int Status) {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		soap.updateToDoStatus(Constants.KEY, ToDoID, Status);
	}

	public void updateObjectAttribute(int ObjectID, String Attribute, String Value) {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		soap.updateObjectAttribute(Constants.KEY, ObjectID, Attribute, Value, 19);
	}

	public void deleteObject(int ObjectID) {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		soap.deleteObject(Constants.KEY, ObjectID, 19);
	}

	public List<BaseClassExt> getTodoforUserExt(Integer Archive) {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getAllToDosForUserExt(Constants.KEY, 19, Archive).getBaseClassExt();
	}

	public Integer getEmployeebyUser() {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getEmployeebyUser(Constants.KEY, 19);
	}

	public String getUserInitials() {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getInitials(Constants.KEY, 19);
	}

	public List<WorkLog> getWorkLog(XMLGregorianCalendar WorkDate) {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getWorkLog(Constants.KEY, WorkDate).getWorkLog();
	}

	public String getMonthTotal(Integer month) {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getMonthTotal(Constants.KEY, month);
	}

	public List<Task> gettaskListing(Integer classID, Integer parentID) {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getTaskListing(Constants.KEY, classID, parentID, Integer.parseInt(UserID)).getTask();
	}

	public void insertActivityLog(Integer userID, Integer taskID, XMLGregorianCalendar startDate, String note, Float hours) {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		soap.insertActivityLog(Constants.KEY, userID, taskID, startDate, note, hours);
	}

	public void deleteActivityLog(Integer aLogID) {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		soap.deleteActivityLog(Constants.KEY, aLogID);
	}

	public WorkLog getActivityLogbyID(Integer logID) {
		WorkLog wl = new WorkLog();
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		wl = soap.getWorkLogbyID(Constants.KEY, logID);
		return wl;
	}

	public void updateActivityLog(Integer userID, Integer taskID, XMLGregorianCalendar startDate, String note, Float hours, Integer alogID) {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		soap.updateActivityLog(Constants.KEY, userID, taskID, startDate, note, hours, alogID);
	}

	public List<BaseClass> getEmployeeListing(Integer classID, Integer parentID) {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getEmployeeListing(Constants.KEY, classID, parentID, Integer.parseInt(UserID)).getBaseClass();
	}

	public Project getProjectbyID(Integer projectID) {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getProjectbyID(projectID);
	}

	public List<BaseClassExt> getAssignbyProject(Integer projectID) {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getAssignbyProject(Constants.KEY, projectID).getBaseClassExt();
	}

	public List<ActivityLog> getinvoicingActivityLog(Integer taskid, Integer clientID) {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getALogbyTask(Constants.KEY, taskid, clientID).getActivityLog();
	}

	public List<Project> getinvoicingProject(Integer clientID) {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getProjectInvoicing(Constants.KEY, clientID).getProject();
	}

	public List<Task> getinvoicingTask(Integer projectID) {
		EosModel service = new EosModel();
		EOSSoap soap = service.getPort(EOSSoap.class);
		return soap.getTaskInvoicing(Constants.KEY, projectID).getTask();
	}
}

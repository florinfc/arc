package arc.mercury.dash.model;

import arc.mercury.com.base.BaseJson;
import arc.mercury.com.base.Constants;
import arc.mercury.com.common.AnswerValue;
import arc.mercury.com.entities.*;
import arc.mercury.com.enums.CeoType;
import arc.mercury.com.enums.CfoType;
import arc.mercury.com.enums.SecType;
import arc.mercury.com.services.IIncServices;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@ManagedBean(name = "incModel", eager = false)
@SessionScoped
public class IncModel extends Service {

	private static String nsURI = "http://dash.server:8081/";
	private static QName servicesName = new QName(nsURI, "IncServicesService");
	private static QName servicesPort = new QName(nsURI, "IncServicesPort");
	private IIncServices incService;
	private Integer otherReasonValue = 100;

	public IncModel() throws Exception {
		super(new URL(nsURI + "incwiz/services?wsdl"), servicesName);
		incService = super.getPort(servicesPort, IIncServices.class);
	}

	public String getTitle() {
		return "Incorporation Wizard";
	}

	public List<IForm> getForms(String code) {
		return incService.getForms(code);
	}

	public void submitForm(IForm form) {
		incService.submitForm(form);
	}

	public List<IHelp> getHelp() {
		return incService.getHelp();
	}

	public IForm getForm(Long formId) {
		IForm form = incService.getForm(formId);
		return form;
	}

	public IUser saveUser(IUser user) {
		return incService.saveUser(user);
	}

	public IPage savePage(IPage page) {
		IPage newpage = incService.savePage(page);
		return newpage;
	}

	public IAnswer newAnswer() {
		IAnswer newanswer = incService.newAnswer();
		return newanswer;
	}

	public IAnswer saveAnswer(IAnswer answer) {
		IAnswer newanswer = incService.saveAnswer(answer);
		newanswer.setCom(answer.getCom());
		return newanswer;
	}

	public List<IAnswer> getAnswers() {
		return incService.getAnswers();
	}

	public IAnswer getAnswer(Long uid, Long fid) {
		IAnswer answer = incService.getAnswer(uid, fid);
		if (answer == null) {
			answer = new IAnswer();
		}
		if (answer.getValue() == null) {
			answer.setCom(new AnswerValue());
		} else {
			Object obj = BaseJson
					.toObject(answer.getValue(), AnswerValue.class);
			answer.setCom((AnswerValue) obj);
		}
		return answer;
	}

	public List<SelectItem> getNaics() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		for (INaics item : incService.getNaics()) {
			items.add(new SelectItem(item.getId(), item.getName()));
		}
		return items;
	}

	public List<SelectItem> getNaics(String text) {
		if (text == null || text.trim().isEmpty()) {
			return getNaics();
		}
		List<SelectItem> items = new ArrayList<SelectItem>();
		List<INaics> naics = incService.getNaicsLike(text);
		if (naics != null) {
			for (INaics item : naics) {
				items.add(new SelectItem(item.getId(), item.getName()));
			}
		}
		return items;
	}

	public INaics findNaicsById(Long id) {
		return incService.getNaicsById(id);
	}

	public List<SelectItem> getActivities() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		for (ISS4 item : incService.getSS4()) {
			items.add(new SelectItem(item.getId(), item.getName()));
		}
		return items;
	}

	public List<SelectItem> getReason() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(0, "Started a new company"));
		items.add(new SelectItem(1, "Hired employees"));
		items.add(new SelectItem(2,
				"Compliance with IRS withholding regulations"));
		items.add(new SelectItem(3, "Banking Purpose"));
		items.add(new SelectItem(4, "Changed type of organization"));
		items.add(new SelectItem(5, "Purchased going business"));
		items.add(new SelectItem(6, "Created a trust"));
		items.add(new SelectItem(7, "Created a pension plan"));
		items.add(new SelectItem(otherReasonValue, "Other"));
		return items;
	}

	public List<SelectItem> getLLC() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(1, "LLC"));
		items.add(new SelectItem(2, "Corporation"));
		return items;
	}

	public List<SelectItem> getManagedLLC() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(0, "Select an option"));
		items.add(new SelectItem(1, "One manager"));
		items.add(new SelectItem(2, "More than one managers"));
		items.add(new SelectItem(3, "All LLC members"));
		return items;
	}

	public List<SelectItem> getCid() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(0, "Select an option"));
		items.add(new SelectItem(1, "Community appartment project"));
		items.add(new SelectItem(2, "Planned development"));
		items.add(new SelectItem(3, "Condominium project"));
		items.add(new SelectItem(4, "Stock cooperative"));
		items.add(new SelectItem(5, "Home owner's association"));
		return items;
	}

	public List<SelectItem> getDay() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		for (Integer x = 1; x < 32; ++x) {
			items.add(new SelectItem(x, x.toString()));
		}
		return items;
	}

	public List<SelectItem> getMonth() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(0, "Jan"));
		items.add(new SelectItem(1, "Feb"));
		items.add(new SelectItem(2, "Mar"));
		items.add(new SelectItem(3, "Apr"));
		items.add(new SelectItem(4, "May"));
		items.add(new SelectItem(5, "Jun"));
		items.add(new SelectItem(6, "Jul"));
		items.add(new SelectItem(7, "Aug"));
		items.add(new SelectItem(8, "Sep"));
		items.add(new SelectItem(9, "Oct"));
		items.add(new SelectItem(10, "Nov"));
		items.add(new SelectItem(11, "Dec"));
		return items;
	}

	public List<SelectItem> getMonthLong() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(0, "January"));
		items.add(new SelectItem(1, "February"));
		items.add(new SelectItem(2, "Mars"));
		items.add(new SelectItem(3, "April"));
		items.add(new SelectItem(4, "May"));
		items.add(new SelectItem(5, "June"));
		items.add(new SelectItem(6, "July"));
		items.add(new SelectItem(7, "August"));
		items.add(new SelectItem(8, "September"));
		items.add(new SelectItem(9, "October"));
		items.add(new SelectItem(10, "November"));
		items.add(new SelectItem(11, "December"));
		return items;
	}

	public List<SelectItem> getYears() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		Integer year = cal.get(Calendar.YEAR);
		for (Integer x = 0; x < 2; ++x) {
			Integer cy = year + x;
			items.add(new SelectItem(cy, cy.toString()));
		}
		return items;
	}

	public List<SelectItem> getQuarters() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(1, "Q1 Jan - Mar"));
		items.add(new SelectItem(2, "Q2 Apr - Jun"));
		items.add(new SelectItem(3, "Q3 Jul - Sep"));
		items.add(new SelectItem(4, "Q4 Oct - Dec"));
		return items;
	}

	public List<SelectItem> getSecondary() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(1, "Q1 Jan - Mar"));
		items.add(new SelectItem(2, "Q2 Apr - Jun"));
		items.add(new SelectItem(3, "Q3 Jul - Sep"));
		items.add(new SelectItem(4, "Q4 Oct - Dec"));
		return items;
	}

	public List<SelectItem> getPeople() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(1, "President"));
		items.add(new SelectItem(2, "CEO"));
		items.add(new SelectItem(3, "Director"));
		items.add(new SelectItem(4, "Manager"));
		return items;
	}

	public List<SelectItem> getCEO() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		for (CeoType type : CeoType.values()) {
			items.add(new SelectItem(type.getValue()));
		}
		return items;
	}

	public List<SelectItem> getCFO() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		for (CfoType type : CfoType.values()) {
			items.add(new SelectItem(type.getValue()));
		}
		return items;
	}

	public List<SelectItem> getSecretary() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		for (SecType type : SecType.values()) {
			items.add(new SelectItem(type.getValue()));
		}
		return items;
	}

	public List<SelectItem> getMembers() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(1, "Manager"));
		items.add(new SelectItem(2, "Member"));
		return items;
	}

	public List<SelectItem> getSell() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(1, "Alcohol"));
		items.add(new SelectItem(2, "Tires"));
		items.add(new SelectItem(3, "Tobacco"));
		items.add(new SelectItem(4, "Fuel"));
		items.add(new SelectItem(5, "Lumber"));
		items.add(new SelectItem(6, "Wood engineered products"));
		items.add(new SelectItem(7, "Firearms"));
		return items;
	}

	public List<SelectItem> getAmount() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(1, "Percentage"));
		items.add(new SelectItem(2, "Number of shares"));
		return items;
	}

	public List<SelectItem> getShare() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(1, "Cash"));
		items.add(new SelectItem(2, "Property"));
		items.add(new SelectItem(3, "Service value"));
		return items;
	}

	public Integer getOtherReasonValue() {
		return otherReasonValue;
	}

	public void setOtherReasonValue(Integer otherReasonValue) {
		this.otherReasonValue = otherReasonValue;
	}

	public List<SelectItem> getNonProfitTypes() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(1, "Select your non-profit type"));
		items.add(new SelectItem(Constants.Religious, "Religious"));
		items.add(new SelectItem(Constants.Public, "Public Benefit Cause"));
		items.add(new SelectItem(Constants.Mutual, "Mutual Benefit"));
		items.add(new SelectItem(Constants.CID, "Common Interest Development"));
		return items;
	}

	public List<IQuote> getQuotes() {
		return incService.getQuote();
	}
}

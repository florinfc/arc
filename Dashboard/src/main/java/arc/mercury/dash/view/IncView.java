package arc.mercury.dash.view;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import arc.mercury.com.base.Constants;
import arc.mercury.com.common.AnswerValue;
import arc.mercury.com.common.Board;
import arc.mercury.com.common.Identity;
import arc.mercury.com.entities.IAnswer;
import arc.mercury.com.entities.IForm;
import arc.mercury.com.entities.IHelp;
import arc.mercury.com.entities.INaics;
import arc.mercury.com.entities.IPage;
import arc.mercury.com.entities.IQuestion;
import arc.mercury.com.entities.IQuote;
import arc.mercury.com.entities.IRole;
import arc.mercury.com.entities.IState;
import arc.mercury.com.entities.IUser;
import arc.mercury.com.enums.CeoType;
import arc.mercury.com.enums.CfoType;
import arc.mercury.com.enums.SecType;
import arc.mercury.dash.common.JsfMessage;
import arc.mercury.dash.model.DashModel;
import arc.mercury.dash.model.IncModel;

@SuppressWarnings("unchecked")
@ManagedBean(name = "incView", eager = false)
@SessionScoped
public class IncView {

	Logger log = Logger.getLogger("IncView");
	@ManagedProperty(value = "#{dashView}")
	private DashView dashView;
	@ManagedProperty(value = "#{dashModel}")
	private DashModel dashModel;
	@ManagedProperty(value = "#{incModel}")
	private IncModel incModel;
	private IState state;
	private IUser user = null;
	private IForm form;
	private Long formId = 1L;
	private List<IPage> pages = null;
	private IPage page = null;
	private List<IQuestion> questions = null;
	private IQuestion question = null;
	private Integer currPage = 1;
	private Integer lastPage = 0;
	private Integer total = 0;
	private IAnswer answer = new IAnswer();
	private String naicsText;
	private Integer owners = 1;
	private List<SelectItem> naics = null;
	private List<SelectItem> ss4 = null;
	private List<IQuote> quotes;
	private List<IHelp> help;
	private Integer totalFee = 0;

	public IncView() {
	}

	public String getDefaultComName1() {
		return Constants.defName1;
	}

	public String getDefaultComName2() {
		return Constants.defName2;
	}

	public String getDefaultComName3() {
		return Constants.defName3;
	}

	public String getDefaultAgentName() {
		return Constants.defAgentName;
	}

	public String getDefaultTradeName() {
		return Constants.defTradeName;
	}

	public String getDefaultFullName() {
		return Constants.defFname + " " + Constants.defLname;
	}

	public String getDefaultAddress1() {
		return Constants.defAddress1;
	}

	public String getDefaultAddress2() {
		return Constants.defAddress2;
	}

	public Integer getAOF() {
		return Constants.AOF;
	}

	public Integer getEIN() {
		return Constants.EIN;
	}

	public Integer getSOI() {
		return Constants.SOI;
	}

	public Integer getEID() {
		return Constants.EID;
	}

	public Integer getRSN() {
		return Constants.RSN;
	}

	public Integer getSUB() {
		return Constants.SUB;
	}

	public Integer getASP() {
		return Constants.ASP;
	}

	public Integer getBMM() {
		return Constants.BMM;
	}

	public Integer getPCP() {
		return Constants.PCP;
	}

	public Integer getWSP() {
		return Constants.WSP;
	}

	public Integer getLCP() {
		return Constants.LCP;
	}

	public Integer getBCP() {
		return Constants.BCP;
	}

	public Integer getACC() {
		return Constants.ACC;
	}

	@PostConstruct
	public void start() {
		getNaics();
		getSs4();
		quotes = incModel.getQuotes();
		help = incModel.getHelp();
		getForm();
	}

	public void init() {
		user = null;
		FacesContext facesCtx = FacesContext.getCurrentInstance();
		ExternalContext ctx = facesCtx.getExternalContext();
		HttpServletRequest request = (HttpServletRequest) ctx.getRequest();
		String id = request.getParameter("id");
		if (id == null || id.isEmpty()) {
			if (dashView.isLoggedOn()) {
				user = dashView.getUser();
			}
			if (user == null) {
				user = new IUser();
			}
		} else {
			user = dashModel.getUserById(Long.valueOf(id));
			dashView.setUser(user);
		}
		answer = null;
		if (user != null && user.getId() != null) {
			answer = incModel.getAnswer(user.getId(), form.getId());
		}
		if (answer == null) {
			answer = new IAnswer();
		}
		answer.setFormId(formId);
		navigate(answer.getMaxPage());
		navigate(currPage);
	}

	private String pageLink(Integer p) {
		FacesContext facesCtx = FacesContext.getCurrentInstance();
		ExternalContext ctx = facesCtx.getExternalContext();
		String root = ctx.getRequestContextPath() + "/incwiz/";
		String page = String.format("page%02d", p);
		String url = root + page + ".xhtml";
		return url;
	}

	public String navigate(Integer p) {
		currPage = p;
		page = getPages().get(currPage - 1);
		questions = getQuestions();
		// answer.getPages()[currPage - 1] = '1';
		if (answer.getMaxPage() < currPage) {
			answer.setMaxPage(currPage);
		}
		try {
			FacesContext facesCtx = FacesContext.getCurrentInstance();
			ExternalContext ctx = facesCtx.getExternalContext();
			ctx.redirect(pageLink(p));
		} catch (Exception e) {
			log.info(e.toString());
		}
		return null;
	}

	public String nextPage() {
		if (currPage == lastPage) {
			return null;
		}
		adjustPage();
		return navigate(page.getNextPage());
	}

	public void nextPageAjax(AjaxBehaviorEvent e) {
		nextPage();
	}

	public String prevPage() {
		if (currPage == 1) {
			return null;
		}
		adjustPage();
		return navigate(page.getPrevPage());
	}

	public void prevPageAjax(AjaxBehaviorEvent e) {
		prevPage();
	}

	public void adjustPage() {
		int prev = page.getId().intValue() - 1;
		if (prev <= 0)
			prev = 1;
		int next = page.getId().intValue() + 1;
		if (next >= lastPage)
			next = lastPage;
		if (currPage == 2) {
			if (answer.getCom().getLLC() == 1) {
				next = 6;
			}
		}
		if (currPage == 5 || currPage == 7) {
			if (answer.getCom().getHelpEIN() == 0) {
				answer.getCom().setHelpEIN(1);
				next = 8;
			} else {
				next = 9;
			}
		}
		if (currPage == 3 || currPage == 6) {
			prev = 2;
		}
		if (currPage == 8) {
			if (answer.getCom().getLLC() == 1) {
				prev = 7;
			} else {
				prev = 5;
			}
		}
		if (currPage == 9) {
			if (answer.getCom().getHelpEIN() != 0) {
				if (answer.getCom().getLLC() == 1) {
					prev = 7;
				} else {
					prev = 5;
				}
			}
		}
		page.setPrevPage(prev);
		page.setNextPage(next);
	}

	public List<IAnswer> listAnswers() {
		List<IAnswer> answers = incModel.getAnswers();
		for (IAnswer a : answers) {
			a.setName(dashModel.getUserById(a.getUserId()).getFullName());
		}
		return answers;
	}

	public List<SelectItem> getItems(String method) {
		List<SelectItem> items = new ArrayList<SelectItem>();
		if (method != null) {
			try {
				Method m = incModel.getClass().getDeclaredMethod(method);
				m.setAccessible(true);
				items = (List<SelectItem>) m.invoke(incModel);
			} catch (Exception e) {
				JsfMessage.format(FacesMessage.SEVERITY_ERROR, e.toString(),
						log);
			}
		}
		return items;
	}

	public void getMethod(String method) {
		if (method != null) {
			try {
				Method m = incModel.getClass().getDeclaredMethod(method);
				m.setAccessible(true);
				m.invoke(incModel);
			} catch (Exception e) {
				JsfMessage.format(FacesMessage.SEVERITY_ERROR, e.toString(),
						log);
			}
		}
	}

	public void getForm() {
		form = incModel.getForm(formId);
		pages = form.getPages();
		Collections.sort(pages, new PageComparator());
		lastPage = form.getPages().size();
		for (IPage p : pages) {
			p.setPrevPage(p.getOrd() - 1);
			p.setNextPage(p.getOrd() + 1);
		}
		total = lastPage;
	}

	public void saveAnswer() {
		IAnswer old = incModel.getAnswer(user.getId(), formId);
		if (old != null && old.getId() != null) {
			answer.setId(old.getId());
		}
		String p = answer.getCom().getPurposeName();
		if (p == null) {
			INaics n = incModel.findNaicsById(answer.getCom().getPurpose()
					.longValue());
			if (n != null) {
				answer.getCom().setPurposeName(n.getName());
			}
		}
		if (currPage > answer.getMaxPage()) {
			answer.setMaxPage(currPage);
		}
		answer.setUserId(user.getId());
		answer.setFormId(formId);
		answer.setValue(answer.getCom().toJson());
		incModel.saveAnswer(answer);
	}

	public String saveForm() {
		user = dashView.getUser();
		if (user.getId() == null && user.getMail() != null) {
			IUser newUser = dashModel.getUserByMail(user.getMail());
			if (newUser == null) {
				newUser = new IUser();
				newUser.setMail(user.getMail());
				newUser.setRole(new IRole());
				user = dashModel.saveUser(newUser);
			} else {
				user = newUser;
			}
		}
		saveAnswer();
		String result = dashModel.sendMail(user);
		if (!result.startsWith("Sent")) {
			JsfMessage.format(FacesMessage.SEVERITY_ERROR, result, log);
		}
		return null;
	}

	public String submitForm() {
		if (!checkAnswer()) {
			return null;
		}
		saveAnswer();

		List<String> atts = new ArrayList<String>();
		if (answer.getCom().getNeedLicense() == 0) {
			atts.add(dashModel.incPdf("arts-pc.pdf", user, answer.getCom()));
		} else {
			if (answer.getCom().getLLC() == 1) {
				atts.add(dashModel.incPdf("arts-llc1.pdf", user,
						answer.getCom()));
			} else {
				if (answer.getCom().comIsNonProfit()) {
					if (answer.getCom().comIsReligious()) {
						atts.add(dashModel.incPdf("arts-re.pdf", user,
								answer.getCom()));
					}
					if (answer.getCom().comIsPublic()) {
						atts.add(dashModel.incPdf("arts-pb.pdf", user,
								answer.getCom()));
					}
					if (answer.getCom().comIsMutual()) {
						atts.add(dashModel.incPdf("arts-mu.pdf", user,
								answer.getCom()));
					}
					if (answer.getCom().comIsCID()) {
						atts.add(dashModel.incPdf("arts-cid.pdf", user,
								answer.getCom()));
					}
				} else {
					if (answer.getCom().getCloseCorp() == 1) {
						atts.add(dashModel.incPdf("arts-cl.pdf", user,
								answer.getCom()));
					} else {
						atts.add(dashModel.incPdf("arts-gs.pdf", user,
								answer.getCom()));
					}
				}
			}
		}
		String[] a = new String[atts.size()];
		for (int x = 0; x < atts.size(); ++x) {
			a[x] = atts.get(x);
		}
		dashModel.sendMail(user, a);
		try {
			FacesContext facesCtx = FacesContext.getCurrentInstance();
			ExternalContext ctx = facesCtx.getExternalContext();
			String root = ctx.getRequestContextPath();
			ctx.redirect(root);
		} catch (Exception e) {
			log.info(e.toString());
		}
		return null;
	}

	public Boolean checkAnswer() {
		Integer invalid = 0;
		StringBuffer msg = new StringBuffer();
		for (Board b : answer.getCom().getOwners()) {
			if (b.firstNameIsEmpty()) {
				invalid = errorAnswer(msg, "Owner's First Name is missing.", 10);
			}
			if (b.lastNameIsEmpty()) {
				invalid = errorAnswer(msg, "Owner's Last Name is missing.", 10);
			}
		}
		if (invalid == 0) {
			return true;
		}
		JsfMessage.format(FacesMessage.SEVERITY_ERROR, msg.toString(), log);
		return false;
	}

	private Integer errorAnswer(StringBuffer msg, String errText,
			Integer errPage) {
		msg.append("<a href='http://dash.server:8081/")
				.append(pageLink(errPage)).append("'>").append(errText)
				.append("</a>\n");
		return errPage;
	}

	public void setForm(IForm form) {
		this.form = form;
	}

	public void adjustNonProfit(AjaxBehaviorEvent event) {

		if (answer.getCom().getNonProfitType() == 2) {
			naicsText = "religious";
		} else if (answer.getCom().getNonProfitType() == 4) {
			naicsText = "real estate";
		} else {
			naicsText = "";
		}
		naics = incModel.getNaics(naicsText);

	}

	public boolean isOwner(String name) {
		if (name.equals(getDefaultFullName())) {
			return false;
		}
		for (Board b : answer.getCom().getOwners()) {
			if (name.equals(b.getFullName())) {
				return true;
			}
		}
		return false;
	}

	public List<IQuestion> getQuestions() {
		questions = page.getQuestions();
		Collections.sort(questions, new QuestionComparator());
		return questions;
	}

	public void setQuestions(List<IQuestion> questions) {
		this.questions = questions;
	}

	public IQuestion quest(Integer ord) {
		if (page == null) {
			return null;
		}
		if (questions == null) {
			questions = page.getQuestions();
		}
		IQuestion q = null;
		if (ord < questions.size()) {
			q = questions.get(ord);
		}
		return q;
	}

	public IQuestion quest(Integer page, Integer ord) {
		if (pages == null) {
			pages = form.getPages();
		}
		IPage p = null;
		if (page < pages.size()) {
			p = pages.get(page);
		}
		IQuestion q = null;
		if (ord < p.getQuestions().size()) {
			q = p.getQuestions().get(ord);
		}
		return q;
	}

	public List<SelectItem> getBoardNames() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		for (int x = 1; x <= answer.getCom().getBoard().size(); ++x) {
			items.add(new SelectItem(x, answer.getCom().getBoard().get(x - 1)
					.getFullName()));
		}
		return items;
	}

	public Map<String, Integer> getRadio(String label) {
		Map<String, Integer> items = new HashMap<String, Integer>();
		if (!label.isEmpty()) {
			String[] labels = label.split("/");
			for (int x = 0; x < labels.length; ++x) {
				items.put(labels[x], x + 1);
			}
		}
		return items;
	}

	public String getStateName() {
		if (state == null) {
			Long id = answer.getCom().getState().longValue();
			state = dashModel.getStateById(id);
		}
		return (state == null) ? null : state.getName();
	}

	public Long getProgress() {
		Double x = getAnswer().getMaxPage().doubleValue() - 2;
		if (x < 0)
			x = 0.0;
		Double t = total.doubleValue() - 3;
		if (t <= 0)
			t = 1.0;
		if (x > t)
			x = t;
		return Math.round(100.0 * x / t);
	}

	public List<SelectItem> getCEO() {
		return (answer.getCom().getLLC() == 2) ? incModel.getCEO() : incModel
				.getCEO();
	}

	public List<SelectItem> getCFO() {
		return (answer.getCom().getLLC() == 2) ? incModel.getCFO() : incModel
				.getMembers();
	}

	public List<SelectItem> getSecretary() {
		return (answer.getCom().getLLC() == 2) ? incModel.getSecretary()
				: incModel.getMembers();
	}

	public Identity getPrincipal() {
		return answer.getCom().getAddress().get(0);
	}

	public Identity getMailing() {
		return answer.getCom().getAddress().get(1);
	}

	public Identity getOffice() {
		return answer.getCom().getAddress().get(2);
	}

	public Identity getPhysical() {
		return answer.getCom().getAddress().get(3);
	}

	public String getLabelWithState(int ix) {
		return String.format(getQuestions().get(ix).getLabel(), getStateName());
	}

	public String getLabelWithType(int ix) {
		return String.format(getQuestions().get(ix).getLabel(), (answer
				.getCom().getLLC() == 1) ? "Organization" : "Incorporation");
	}

	public Boolean subS() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MONTH, answer.getCom().getCloseMonth());
		int d = cal.get(Calendar.DAY_OF_MONTH);
		int m = cal.get(Calendar.MONTH);
		int n = answer.getCom().getNbOwners();
		return (n < 100 && d == 31 && m == 11);
	}

	public String getQuote() {
		Double d = Math.random() * quotes.size();
		return quotes.get(d.intValue()).getName();
	}

	public IncModel getIncModel() {
		return incModel;
	}

	public void setIncModel(IncModel incModel) {
		this.incModel = incModel;
	}

	public String getTitle() {
		return incModel.getTitle();
	}

	public IUser getUser() {
		return user;
	}

	public void setUser(IUser user) {
		this.user = user;
	}

	public Long getFormId() {
		return formId;
	}

	public void setFormId(Long formId) {
		this.formId = formId;
	}

	public IState getState() {
		return state;
	}

	public void setState(IState state) {
		this.state = state;
	}

	public IQuestion getQuestion() {
		return question;
	}

	public void setQuestion(IQuestion question) {
		this.question = question;
	}

	public IPage getPage() {
		return page;
	}

	public void setPage(IPage page) {
		this.page = page;
	}

	public Integer getLastPage() {
		return lastPage;
	}

	public void setLastPage(Integer lastPage) {
		this.lastPage = lastPage;
	}

	public Integer getCurrPage() {
		return currPage;
	}

	public void setCurrPage(Integer currPage) {
		this.currPage = currPage;
	}

	public List<IPage> getPages() {
		if (pages == null) {
			pages = form.getPages();
		}
		return pages;
	}

	public void setPages(List<IPage> pages) {
		this.pages = pages;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public IAnswer getAnswer() {
		return answer;
	}

	public void setAnswer(IAnswer answer) {
		this.answer = answer;
	}

	public DashView getDashView() {
		return dashView;
	}

	public void setDashView(DashView dashView) {
		this.dashView = dashView;
	}

	public DashModel getDashModel() {
		return dashModel;
	}

	public void setDashModel(DashModel dashModel) {
		this.dashModel = dashModel;
	}

	public void naicsListener(AjaxBehaviorEvent e) {
		naics.clear();
		naics = incModel.getNaics(naicsText);
	}

	public List<SelectItem> getNaics() {
		if (naics == null) {
			naics = incModel.getNaics();
		}
		return naics;
	}

	public void setNaics(List<SelectItem> naics) {
		this.naics = naics;
	}

	public List<SelectItem> getNaicsList(String text) {
		naics = incModel.getNaics(text);
		return naics;
	}

	public List<SelectItem> getPurpose() {
		return getNaics();
	}

	public List<SelectItem> getBusiness() {
		return getNaics();
	}

	public List<SelectItem> getSs4() {
		if (ss4 == null) {
			ss4 = incModel.getActivities();
		}
		return ss4;
	}

	public void setSs4(List<SelectItem> ss4) {
		this.ss4 = ss4;
	}

	public String getNaicsText() {
		return naicsText;
	}

	public void setNaicsText(String naicsText) {
		this.naicsText = naicsText;
	}

	public List<IHelp> getHelp() {
		return help;
	}

	public void setHelp(List<IHelp> help) {
		this.help = help;
	}

	public Integer getOwners() {
		return owners;
	}

	public void setOwners(Integer owners) {
		this.owners = owners;
	}

	public List<IQuote> getQuotes() {
		return quotes;
	}

	public void setQuotes(List<IQuote> quotes) {
		this.quotes = quotes;
	}

	public void adjustNbOwners(AjaxBehaviorEvent e) {
		List<Board> board = answer.getCom().getBoard();
		List<Board> shares = answer.getCom().getOwners();
		Integer nbOwners = answer.getCom().getNbOwners();
		Integer nbShares = shares.size();
		int diff = nbOwners - nbShares;
		if (diff > 0) {
			for (int i = 0; i < diff; ++i) {
				shares.add(new Board());
			}
		} else {
			for (int i = nbShares; i > nbOwners; --i) {
				shares.remove(i - 1);
			}
			if (!answer.getCom().getOwnerCopied()) {
				if (!(board == null || board.isEmpty())) {
					if (!(board.get(0).firstNameIsEmpty() && board.get(0)
							.lastNameIsEmpty())) {
						shares.set(0, new Board(board.get(0)));
						answer.getCom().setOwnerCopied(true);
					}
				}
			}
		}
	}

	public void addShares(AjaxBehaviorEvent e) {
		answer.getCom().setNbOwners(answer.getCom().getNbOwners() + owners);
		adjustNbOwners(null);
		owners = 1;
	}

	public void delShares(AjaxBehaviorEvent e) {
		if (owners < answer.getCom().getNbOwners()) {
			answer.getCom().setNbOwners(answer.getCom().getNbOwners() - owners);
			adjustNbOwners(null);
		} else {
			JsfMessage.format(FacesMessage.SEVERITY_WARN,
					"You must have at least 1 owner!", log);
		}
		owners = 1;
	}

	public void addBoard(AjaxBehaviorEvent e) {
		answer.getCom().getBoard().add(new Board(0));
	}

	public void delBoard(AjaxBehaviorEvent e) {
		int nbBoard = answer.getCom().getBoard().size();
		if (nbBoard > 3) {
			answer.getCom().getBoard().remove(nbBoard - 1);
		} else {
			JsfMessage.format(FacesMessage.SEVERITY_WARN,
					"You must have at least 3 executive functions!", log);
		}
	}

	public void adjustOwners(AjaxBehaviorEvent e) {
		List<Board> board = answer.getCom().getBoard();
		List<Board> shares = answer.getCom().getOwners();
		Integer nbOwners = answer.getCom().getNbOwners();
		if (!(board == null || board.isEmpty())) {
			if (!(board.get(0).firstNameIsEmpty() && board.get(0)
					.lastNameIsEmpty())) {
				shares.set(0, new Board(board.get(0)));
				answer.getCom().setOwnerCopied(true);
			}
		}
	}

	public void adjustBoard() {
		List<Board> board = answer.getCom().getBoard();
		List<Board> owners = answer.getCom().getOwners();
		Integer nbOwners = answer.getCom().getNbOwners();
		if (nbOwners > 0) {
			board.set(0, new Board(owners.get(0)));
		}
		if (nbOwners > 1) {
			board.set(1, new Board(owners.get(1)));
		}
		if (nbOwners > 2) {
			board.set(2, new Board(owners.get(2)));
		}
		board.get(0).setTitle(CeoType.President.name());
		board.get(1).setTitle(CfoType.Treasurer.name());
		board.get(2).setTitle(SecType.Secretary.name());
		answer.getCom().setBoardCopied(true);
	}

	public Integer getTotalFee() {
		return totalFee;
	}

	public void adjustTotal(AjaxBehaviorEvent e) {
		totalFee = 0;
		if (answer.getCom().getHelpAOF() == 1) {
			totalFee += help.get(0).getFee();
		}
		if (answer.getCom().getHelpEIN() == 1) {
			totalFee += help.get(1).getFee();
		}
		if (answer.getCom().getHelpSOI() == 1) {
			totalFee += help.get(2).getFee();
		}
		if (answer.getCom().getHelpEID() == 1) {
			totalFee += help.get(3).getFee();
		}
		if (answer.getCom().getHelpRSN() == 1) {
			totalFee += help.get(4).getFee();
		}
		if (answer.getCom().getHelpSUB() == 1) {
			totalFee += help.get(5).getFee();
		}
	}

	public void applicationUri(ComponentSystemEvent e) {
		ExternalContext ext = FacesContext.getCurrentInstance()
				.getExternalContext();
		HttpServletRequest request = (HttpServletRequest) ext.getRequest();
		String url = request.getRequestURL().toString();
		int x = url.lastIndexOf("page") + 4;
		int p = Integer.valueOf(url.substring(x, x + 2));
		if (p == 10) {
			if (!answer.getCom().getOwnerCopied()) {
				adjustOwners(null);
			}
		}
		if (p == 12) {
			if (!answer.getCom().getBoardCopied()) {
				adjustBoard();
			}
		}
		if (p == 13) {
			adjustTotal(null);
		}
		if (p != currPage) {
			navigate(p);
		}
	}

	public void setAddressStates(AjaxBehaviorEvent e) {
		AnswerValue com = answer.getCom();
		String state = com.getState().toString();
		if (com.getBusinessAddress().getState().equals(Constants.defState))
			com.getBusinessAddress().setState(state);
		if (com.getBankAddress().getAddress().getState()
				.equals(Constants.defState))
			com.getBankAddress().getAddress().setState(state);
		if (com.getEidAddress().getState().equals(Constants.defState))
			com.getEidAddress().setState(state);
		for (Identity i : com.getAddress()) {
			if (i.getAddress().getState().equals(Constants.defState))
				i.getAddress().setState(state);
		}
		for (Board b : com.getOwners()) {
			if (b.getIdent().getAddress().getState().equals(Constants.defState))
				b.getIdent().getAddress().setState(state);
		}
		for (Board b : com.getBoard()) {
			if (b.getIdent().getAddress().getState().equals(Constants.defState))
				b.getIdent().getAddress().setState(state);
		}
		for (Board b : com.getPersonal()) {
			if (b.getIdent().getAddress().getState().equals(Constants.defState))
				b.getIdent().getAddress().setState(state);
		}
		for (Board b : com.getSuppliers()) {
			if (b.getIdent().getAddress().getState().equals(Constants.defState))
				b.getIdent().getAddress().setState(state);
		}

	}

	public String getAdder(Integer type) {
		Integer fee = 0;
		if (type == Constants.AOF) {
			fee = answer.getCom().getHelpAOF();
		}
		if (type == Constants.EIN) {
			fee = answer.getCom().getHelpEIN();
		}
		if (type == Constants.SOI) {
			fee = answer.getCom().getHelpSOI();
		}
		if (type == Constants.EID) {
			fee = answer.getCom().getHelpEID();
		}
		if (type == Constants.RSN) {
			fee = answer.getCom().getHelpRSN();
		}
		if (type == Constants.SUB) {
			fee = answer.getCom().getHelpSUB();
		}
		if (type == Constants.ASP) {
			fee = answer.getCom().getHelpASP();
		}
		if (type == Constants.BMM) {
			fee = answer.getCom().getHelpBMM();
		}
		if (type == Constants.PCP) {
			fee = answer.getCom().getHelpPCP();
		}
		if (type == Constants.WSP) {
			fee = answer.getCom().getHelpWSP();
		}
		if (type == Constants.LCP) {
			fee = answer.getCom().getHelpLCP();
		}
		if (type == Constants.BCP) {
			fee = answer.getCom().getHelpBCP();
		}
		if (type == Constants.ACC) {
			fee = answer.getCom().getHelpACC();
		}
		IHelp h = help.get(type - 1);
		if (fee == 0) {
			return "*adder: None";
		} else {
			return "*adder: "
					+ String.format(h.getAdder(), h.getFee().toString());
		}
	}

	public static class PageComparator implements Comparator<IPage> {

		@Override
		public int compare(IPage o1, IPage o2) {
			return (o1.getOrd() > o2.getOrd() ? 1
					: (o1.getOrd() == o2.getOrd() ? 0 : -1));
		}
	}

	public static class QuestionComparator implements Comparator<IQuestion> {

		@Override
		public int compare(IQuestion o1, IQuestion o2) {
			return (o1.getOrd() > o2.getOrd() ? 1
					: (o1.getOrd() == o2.getOrd() ? 0 : -1));
		}
	}
}

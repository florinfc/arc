package arc.mercury.dash.view;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.xml.datatype.XMLGregorianCalendar;

import arc.mercury.com.utils.Packer;
import arc.mercury.dash.common.JsfMessage;
import arc.mercury.dash.common.Node;
import arc.mercury.dash.model.EosModel;
import arc.mercury.webservice.AmObject;
import arc.mercury.webservice.BaseClass;
import arc.mercury.webservice.BaseClassExt;
import arc.mercury.webservice.Client;
import arc.mercury.webservice.InvoiceMethod;
import arc.mercury.webservice.Project;
import arc.mercury.webservice.Task;
import arc.mercury.webservice.WorkLog;

@ManagedBean(name = "eosView", eager = false)
@SessionScoped
public class EosView {

	@SuppressWarnings("deprecation")
	Date today = new Date(113, 05, 05);
	Logger log = Logger.getLogger("EosView");
	private String Taskname = "";
	private String duedate = "";
	private String description = "";
	private Integer hourly = 0;
	private Integer flatrate = 0;
	private Integer clientID = 0;
	private Integer invoicemethodID = 0;
	private Integer invoiceTypeId = 0;
	private Integer taskInvoiceMethodID = 0;
	private List<InvoiceMethod> list = null;
	private List<BaseClass> invoiceType = null;
	private Integer ParentID = 0;
	private Integer TaskID = 0;
	private List<BaseClassExt> projects;
	private List<BaseClassExt> todos;
	private List<Project> projectMessage;
	private List<Project> messagesforProject;
	private Integer ProjectID;
	private String ProjectType;
	private Integer ProjectProgress;
	private List<BaseClassExt> projectsListbyClient;
	private Map<String, Boolean> checkMap = new HashMap<String, Boolean>();
	private Map<Integer, Boolean> checkTdMap = new HashMap<Integer, Boolean>();
	private Map<Integer, Boolean> checkLMap = new HashMap<Integer, Boolean>();
	private Boolean taskCheck = false;
	private Boolean tdCheck = false;
	private Boolean logCheck = false;
	private Boolean showArchive = false;
	private Boolean showtdArchive = false;
	private List<BaseClassExt> todoslist;
	private String guestInitials = "";
	private String message = "";
	private Boolean markImportant = false;
	private Integer UserID = 19;
	private List<WorkLog> worklog;
	private WorkLog node;
	private Integer logID = 0;
	private String monthTotal = "";
	private String notes = "";
	private Integer taskLogID = 0;
	private String taskHours = "";
	private List<Task> standardTasks;
	private Integer standardTasksID = 0;
	private List<BaseClass> taskRepeatList;
	private List<Task> taskList = new ArrayList<Task>();
	private List<BaseClass> employees = new ArrayList<BaseClass>();
	private List<BaseClassExt> assignList = new ArrayList<BaseClassExt>();
	private String assignorder = "";
	private List<Project> invoicingProjects = new ArrayList<Project>();
	private Boolean invoiceall = false;
	private XMLGregorianCalendar lastDate = arc.mercury.com.utils.Convert
			.toXMLGregorianCalendar(today);
	private Date WorkDate = new Date();
	private Node<String> root;
	private List<Task> taskLogs;
	private Project project = new Project();
	private Task task = new Task();
	private Boolean hideDescProject = false;
	private Boolean hideDescTask = false;
	private Integer employeeID = 0;
	private Boolean newtask = false;
	private Integer budgetedHours = 0;
	@ManagedProperty(value = "#{eosModel}")
	private EosModel eosModel;
	private String wkdate = "";
	@SuppressWarnings("deprecation")
	private String currentMonth = Integer.toString(new Date().getMonth() + 1);

	public EosView() {

		if (eosModel == null) {
			try {
				eosModel = new EosModel();
			} catch (Exception e) {
				JsfMessage.format(FacesMessage.SEVERITY_ERROR,
						"Cannot inject model class :\n", log);
				JsfMessage.format(FacesMessage.SEVERITY_ERROR, e.toString(),
						log);
			}
		}
	}

	public List<BaseClass> getEmployees() {
		employees = eosModel.getEmployeeListing(1, 0);
		return employees;
	}

	public void setEmployees(List<BaseClass> employees) {
		this.employees = employees;
	}

	public Date getWorkDate() {
		return WorkDate;
	}

	public void setWorkDate(Date workDate) {
		WorkDate = workDate;
	}

	public EosModel getEosModel() {
		return eosModel;
	}

	public void setEosModel(EosModel eosModel) {
		this.eosModel = eosModel;
	}

	public List<Project> getInvoicingProjects() {
		return invoicingProjects;
	}

	public void setInvoicingProjects(List<Project> invoicingProjects) {
		this.invoicingProjects = invoicingProjects;
	}

	public String getTitle() {
		StringBuffer sb = new StringBuffer("Dashboard");
		return sb.toString();
	}

	public Boolean getInvoiceall() {
		return invoiceall;
	}

	public void setInvoiceall(Boolean invoiceall) {
		this.invoiceall = invoiceall;
	}

	public Integer getProjectID() {

		return ProjectID;
	}

	public void setProjectID(Integer projectID) {
		ProjectID = projectID;
	}

	public String getProjectType() {

		return ProjectType;
	}

	public void setProjectType(String projectType) {
		ProjectType = projectType;
	}

	public String getTaskname() {
		return Taskname;
	}

	public void setTaskname(String Taskname) {
		this.Taskname = Taskname;
	}

	public String getDuedate() {
		return duedate;
	}

	public void setDuedate(String duedate) {
		this.duedate = duedate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getHourly() {
		return hourly;
	}

	public void setHourly(Integer hourly) {
		this.hourly = hourly;
	}

	public Integer getFlatrate() {
		return flatrate;
	}

	public void setFlatrate(Integer flatrate) {
		this.flatrate = flatrate;
	}

	public Integer getClientID() {
		return clientID;
	}

	public void setClientID(Integer clientID) {
		this.clientID = clientID;
	}

	public Integer getInvoicemethodID() {
		return invoicemethodID;
	}

	public void setInvoicemethodID(Integer invoicemethodID) {
		this.invoicemethodID = invoicemethodID;
	}

	public String getIDfromvalue(String objvalue) {
		Integer method = 0;
		for (InvoiceMethod m : list) {
			if (m.equals(objvalue))
				method = m.getID();
		}

		return method.toString();
	}

	public void getToDoObject() {
		int ParentID = clientID;
		List<AmObject> alist = eosModel.getWSObject(8, ParentID, TaskID, 2);
		String objname = "";
		String objvalue = "";
		for (AmObject obj : alist) {
			objname = obj.getName();
			objvalue = obj.getValue();
			if (objname == "TaskName")
				Taskname = objvalue;
			else if (objname == "Description")
				description = objvalue;
			else if (objname == "DueDate")
				Taskname = duedate;
			else if (objname == "CompensationMethod")
				Taskname = getIDfromvalue(objvalue);
			else if (objname == "FlatFee")
				flatrate = Integer.parseInt(objvalue);
			else if (objname == "Hourly")
				hourly = Integer.parseInt(objvalue);

		}
	}

	public String insertToDo() {
		int ParentID = clientID;
		String Attributes = "";

		String Method = list.get(invoicemethodID - 1).getName();
		Packer Datapack = new Packer();
		Datapack.Append("TaskName", Taskname);
		Datapack.Append("Description", description);
		Datapack.Append("CompensationMethod", Method);
		Datapack.Append("Hourly", Integer.toString(hourly));
		Datapack.Append("FlatFee", Integer.toString(flatrate));
		Datapack.Append("DueDate", duedate);
		Datapack.Append("Status", "");
		Datapack.Append("BudgetedHours", "");
		Datapack.Append("EstimatedHours", "");
		Datapack.Append("EmpPrimary", eosModel.getEmployeebyUser().toString());
		Datapack.Append("EmpSecondary", "");
		Datapack.Append("EmpTertiary", "");
		Datapack.Append("isBillable", "");
		Datapack.Append("Archived", "");
		Attributes = Datapack.GetPack();

		if (TaskID == 0)
			eosModel.insertWSObject(8, ParentID, TaskID, Attributes);
		else
			eosModel.updateWSObject(8, ParentID, TaskID, Attributes);

		return null;
	}

	public List<BaseClassExt> getProjects() {
		projects = eosModel.getPrandPjforUser();
		return projects;
	}

	public void setProjects(List<BaseClassExt> projects) {
		this.projects = projects;
	}

	public Integer getProgressbyProject() {
		Integer progress = 0;
		if (ProjectID != null) {
			for (BaseClassExt p : projects) {
				if (p.getID() == ProjectID)
					progress = Integer.parseInt(p.getValue());
			}
		}

		return progress;
	}

	public String getTypebyProject() {
		String Type = "";

		for (BaseClassExt p : projects) {
			if (p.getID() == ProjectID)
				Type = p.getType();
		}

		return Type;
	}

	public List<SelectItem> getProjectsList() {
		List<BaseClassExt> list = eosModel.getPrandPjforUser();
		List<SelectItem> lst = new ArrayList<SelectItem>();
		lst.add(new SelectItem(0, ""));
		for (BaseClassExt bc : list) {
			lst.add(new SelectItem(bc.getID(), bc.getName()));
		}
		return lst;
	}

	public List<SelectItem> getEmployeeList() {
		List<BaseClass> list = getEmployees();
		List<SelectItem> lst = new ArrayList<SelectItem>();
		lst.add(new SelectItem(0, ""));
		for (BaseClass emp : list) {
			lst.add(new SelectItem(emp.getID(), emp.getName()));
		}
		return lst;
	}

	public List<SelectItem> getClients() {

		List<Client> list = eosModel.getClients(2);
		List<SelectItem> lst = new ArrayList<SelectItem>();
		lst.add(new SelectItem(0, ""));
		for (Client cl : list) {
			lst.add(new SelectItem(cl.getID(), cl.getName()));
		}

		return lst;

	}

	public List<SelectItem> getInvoiceMethods() {
		getList();
		List<SelectItem> lst = new ArrayList<SelectItem>();
		lst.add(new SelectItem(0, ""));
		for (InvoiceMethod im : list) {
			lst.add(new SelectItem(im.getID(), im.getName()));
		}

		return lst;

	}

	public List<SelectItem> getTaskInvoiceMethods() {
		getList();
		List<SelectItem> lst = new ArrayList<SelectItem>();
		lst.add(new SelectItem(0, ""));
		for (InvoiceMethod im : list) {
			lst.add(new SelectItem(im.getID(), im.getName()));
		}

		return lst;

	}

	public List<SelectItem> getTaskRepeat() {
		getTaskRepeatList();
		List<SelectItem> lst = new ArrayList<SelectItem>();
		lst.add(new SelectItem(0, ""));
		for (BaseClass b : taskRepeatList) {
			lst.add(new SelectItem(b.getID(), b.getName()));
		}

		return lst;

	}

	public List<SelectItem> getStandardTasksList() {
		standardTasks = eosModel.gettaskListing(17, 0);
		List<SelectItem> lst = new ArrayList<SelectItem>();
		lst.add(new SelectItem(0, ""));
		for (Task t : standardTasks) {
			lst.add(new SelectItem(t.getID(), t.getTaskName()));
		}
		return lst;

	}

	public List<SelectItem> getInvoiceTypeList() {
		invoiceType = eosModel.getInvoiceType();
		List<SelectItem> lst = new ArrayList<SelectItem>();
		lst.add(new SelectItem(0, ""));
		for (BaseClass it : invoiceType) {
			lst.add(new SelectItem(it.getID(), it.getName()));
		}

		return lst;

	}

	public List<SelectItem> getProjectsListbyClient() {
		List<BaseClassExt> list = eosModel.getPrandPjforUserandClient(clientID);
		List<SelectItem> lst = new ArrayList<SelectItem>();
		lst.add(new SelectItem(0, ""));
		for (BaseClassExt bc : list) {
			lst.add(new SelectItem(bc.getID(), bc.getName()));
		}
		return lst;
	}

	public List<Node<String>> getTreeNodes() {
		buildTreeNode();
		return walk(new ArrayList<Node<String>>(), root);
	}

	private List<Node<String>> walk(List<Node<String>> list, Node<String> node) {
		list.add(node);
		for (Node<String> kid : node.getKids()) {
			walk(list, kid);
		}
		return list;
	}

	public void buildTreeNode() {
		List<BaseClassExt> rootlist = new ArrayList<BaseClassExt>();
		if (ProjectID != null)
			rootlist = eosModel.getAllTasksForUserandClient(clientID,
					ProjectID, ProjectType, 0);
		List<BaseClassExt> nodeslist = new ArrayList<BaseClassExt>();

		root = new Node<String>(null, "Tasks", "", "", "", "", "", "");

		for (BaseClassExt r : rootlist) {

			root.getKids().add(
					new Node<String>(root, r.getName(), r.getValue(), "", "",
							"", "", ""));
			nodeslist = eosModel.getTodoChangesforTask(r.getID());
			for (BaseClassExt n : nodeslist) {
				if (r.getID() == Integer.parseInt(n.getType())) {
					if (n.getName() != "") {
						Node<String> child = root.getKids().get(
								rootlist.indexOf(r));
						child.getKids().add(
								new Node<String>(child, n.getName(), n
										.getValue(), "", "", "", "", ""));
					}
				}
			}
		}

	}

	public List<Node<String>> getTaskListNodes() {
		buildTaskListNode();
		return walk(new ArrayList<Node<String>>(), root);
	}

	public void buildTaskListNode() {
		int archived = 0;
		if (showArchive == true)
			archived = 1;
		else
			archived = 0;

		List<BaseClassExt> rootlist = new ArrayList<BaseClassExt>();
		if (ProjectID != null)
			rootlist = eosModel.getAllTasksForUserandClient(clientID,
					ProjectID, ProjectType, archived);
		List<BaseClassExt> nodeslist = new ArrayList<BaseClassExt>();

		root = new Node<String>(null, "", "", "", "", "", "", "false");
		Boolean status = false;
		for (BaseClassExt r : rootlist) {

			root.getKids().add(
					new Node<String>(root, r.getName(), r.getValue(), r
							.getDueDate().toString(), Integer.toString(r
							.getEstimateH()) + "h", r.getWorktime() + "h",
							Integer.toString(r.getToDoID()), "true"));
			if (r.getValue() != "100%")
				status = false;
			else
				status = true;
			checkMap.put(Integer.toString(r.getID()), status);

			nodeslist = eosModel.getTodoChangesforTask(r.getID());
			for (BaseClassExt n : nodeslist) {
				if (r.getID() == Integer.parseInt(n.getType())) {
					if (n.getName() != "") {
						Node<String> child = root.getKids().get(
								rootlist.indexOf(r));
						child.getKids().add(
								new Node<String>(child, "&nbsp;&nbsp;&nbsp;"
										+ n.getName(), n.getValue(), n
										.getDueDate().toString(), Integer
										.toString(n.getEstimateH()) + "h", n
										.getWorktime() + "h", "", "false"));
						if (n.getValue() != "Completed")
							status = false;
						else
							status = true;
						checkMap.put(Integer.toString(n.getID()), status);

					}
				}
			}
		}

	}

	public List<InvoiceMethod> getList() {
		if (list == null) {
			list = eosModel.getInvoiceMethods();
		}
		return list;
	}

	public void setList(List<InvoiceMethod> list) {
		this.list = list;
	}

	public Integer getTaskID() {
		return TaskID;
	}

	public void setTaskID(Integer taskID) {
		TaskID = taskID;
	}

	public Integer getParentID() {
		return ParentID;
	}

	public void setParentID(Integer parentID) {
		ParentID = parentID;
	}

	public List<BaseClassExt> getTodos() {
		todos = eosModel.getTodoforUserExt(0);
		return todos;
	}

	public void setTodos(List<BaseClassExt> todos) {
		this.todos = todos;
	}

	public List<Project> getProjectMessage() {

		projectMessage = eosModel.getProgramswithMessage(lastDate);
		return projectMessage;
	}

	public void setProjectMessage(List<Project> projectMessage) {
		this.projectMessage = projectMessage;
	}

	public List<Project> getMessagesforProject() {
		ProjectType = getTypebyProject();
		messagesforProject = eosModel.getMessagesforProject(lastDate,
				ProjectType, ProjectID);

		return messagesforProject;
	}

	public void setMessagesforProject(List<Project> messagesforProject) {
		this.messagesforProject = messagesforProject;
	}

	public String submitproject() {
		FacesContext fc = FacesContext.getCurrentInstance();
		Map<String, String> params = fc.getExternalContext()
				.getRequestParameterMap();
		ProjectID = Integer.parseInt(params.get("id"));
		ProjectType = params.get("type");
		clientID = Integer.parseInt(params.get("clientID"));
		String result = "employee/projectView.xhtml?faces-redirect=true";
		return result;

	}

	public Integer getProjectProgress() {
		ProjectProgress = getProgressbyProject();
		return ProjectProgress;
	}

	public void setProjectProgress(Integer projectProgress) {
		ProjectProgress = projectProgress;
	}

	public String viewTaskHistory() {

		getMessagesforProject();
		getTreeNodes();
		return "myProjects.xhtml?faces-redirect=true";
	}

	public Node<String> getRoot() {
		return root;
	}

	public void setRoot(Node<String> root) {
		this.root = root;
	}

	public void getSelected(AjaxBehaviorEvent event) {

		Iterator<String> keys = checkMap.keySet().iterator();
		while (keys.hasNext()) {
			String user = keys.next();
			checkMap.put(user, taskCheck);

		}
	}

	public void getLSelected(AjaxBehaviorEvent event) {

		Iterator<Integer> keys = checkLMap.keySet().iterator();
		while (keys.hasNext()) {
			Integer user = keys.next();
			checkLMap.put(user, logCheck);

		}
	}

	public void gettdSelected(AjaxBehaviorEvent event) {

		Iterator<Integer> keys = checkTdMap.keySet().iterator();
		while (keys.hasNext()) {
			Integer user = keys.next();
			checkTdMap.put(user, tdCheck);

		}
	}

	public Map<String, Boolean> getCheckMap() {

		return checkMap;

	}

	public void markCompleted() {
		Integer result = 0;

		for (Entry<String, Boolean> entry : checkMap.entrySet()) {

			if (entry.getValue()) {

				// result = result + ", " + entry.getKey();
				if (entry.getValue().toString() == "true") {
					result = 2;
					eosModel.updateToDoStatus(Integer.parseInt(entry.getKey()),
							result);
				}

			}

		}
	}

	public void marktdCompleted() {
		Integer result = 0;
		Integer ToDoID = 0;
		List<BaseClassExt> list = eosModel.getTodoforUserExt(0);

		for (Entry<Integer, Boolean> entry : checkTdMap.entrySet()) {

			if (entry.getValue()) {

				for (BaseClassExt t : list) {
					if (t.getID() == entry.getKey())
						ToDoID = t.getToDoID();

				}
				if (entry.getValue().toString() == "true") {
					result = 2;
					eosModel.updateToDoStatus(ToDoID, result);
				}

			}

		}
	}

	public void marktdnotCompleted() {
		Integer result = 0;
		Integer ToDoID = 0;
		List<BaseClassExt> list = eosModel.getTodoforUserExt(0);
		for (Entry<Integer, Boolean> entry : checkTdMap.entrySet()) {

			if (entry.getValue()) {
				for (BaseClassExt t : list) {
					if (t.getID() == entry.getKey())
						ToDoID = t.getToDoID();

				}
				// result = result + ", " + entry.getKey();
				if (entry.getValue().toString() == "true") {
					result = 0;
					eosModel.updateToDoStatus(ToDoID, result);
				}

			}

		}
	}

	public void deletetd() {

		for (Entry<Integer, Boolean> entry : checkTdMap.entrySet()) {

			if (entry.getValue()) {

				// result = result + ", " + entry.getKey();
				if (entry.getValue().toString() == "true") {

					eosModel.deleteObject(entry.getKey());
				}
			}
		}
	}

	public void deleteLogActivity() {

		for (Entry<Integer, Boolean> entry : checkLMap.entrySet()) {
			if (entry.getValue()) {
				if (entry.getValue().toString() == "true") {
					eosModel.deleteActivityLog(entry.getKey());
				}
			}
		}
	}

	public void sendmessage() {

		int ParentID = ProjectID;
		String Attributes = "";
		int important = 0;
		if (markImportant == true)
			important = 1;
		else
			important = 0;

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/dd/MM HH:mm");
		Packer Datapack = new Packer();
		Datapack.Append("Message", message);
		Datapack.Append("UserID", UserID.toString());
		Datapack.Append("MarkImportant", Integer.toString(important));
		Datapack.Append("MessageDate", sdf.format(date));
		Attributes = Datapack.GetPack();

		message = "";
		markImportant = false;
		eosModel.insertWSObject(19, ParentID, 0, Attributes);

	}

	public Boolean getTaskCheck() {
		return taskCheck;
	}

	public void setTaskCheck(Boolean taskCheck) {
		this.taskCheck = taskCheck;
	}

	public Boolean getShowArchive() {
		return showArchive;
	}

	public void setShowArchive(Boolean showArchive) {
		this.showArchive = showArchive;
	}

	public void markArchived() {
		int archived = 0;
		if (showArchive == true)
			archived = 1;
		else
			archived = 0;
		Integer TaskID = 0;
		List<BaseClassExt> list = eosModel.getAllTasksForUserandClient(
				clientID, ProjectID, ProjectType, archived);

		if (archived == 0) {
			for (Entry<String, Boolean> entry : checkMap.entrySet()) {

				if (entry.getValue()) {

					for (BaseClassExt t : list) {
						if (t.getToDoID() == Integer.parseInt(entry.getKey()))
							TaskID = t.getID();

					}

					if (entry.getValue().toString() == "true") {

						eosModel.updateObjectAttribute(TaskID, "Archived", "1");
					}

				}

			}
		} else {
			for (Entry<String, Boolean> entry : checkMap.entrySet()) {

				if (entry.getValue()) {

					for (BaseClassExt t : list) {
						if (t.getToDoID() == Integer.parseInt(entry.getKey()))
							TaskID = t.getID();

					}

					if (entry.getValue().toString() == "true") {

						eosModel.updateObjectAttribute(TaskID, "Archived", "0");
					}

				}

			}

		}

	}

	public void marktdArchived() {
		int archived = 0;
		if (showtdArchive == true)
			archived = 1;
		else
			archived = 0;
		Integer TaskID = 0;
		if (archived == 0) {
			for (Entry<Integer, Boolean> entry : checkTdMap.entrySet()) {

				if (entry.getValue()) {

					TaskID = entry.getKey();

					if (entry.getValue().toString() == "true") {

						eosModel.updateObjectAttribute(TaskID, "Archived", "1");
					}

				}

			}
		} else {
			for (Entry<Integer, Boolean> entry : checkTdMap.entrySet()) {

				if (entry.getValue()) {

					TaskID = entry.getKey();

					if (entry.getValue().toString() == "true") {

						eosModel.updateObjectAttribute(TaskID, "Archived", "0");
					}

				}

			}

		}

	}

	public Boolean getShowtdArchive() {
		return showtdArchive;
	}

	public void setShowtdArchive(Boolean showtdArchive) {
		this.showtdArchive = showtdArchive;
	}

	public Boolean getTdCheck() {
		return tdCheck;
	}

	public void setTdCheck(Boolean tdCheck) {
		this.tdCheck = tdCheck;
	}

	public List<BaseClassExt> getTodoslist() {

		return todoslist = returntodolist();
	}

	public void setTodoslist(List<BaseClassExt> todoslist) {
		this.todoslist = todoslist;
	}

	public Map<Integer, Boolean> getCheckTdMap() {
		return checkTdMap;
	}

	public List<BaseClassExt> returntodolist() {
		int archived = 0;
		if (showtdArchive == true)
			archived = 1;
		else
			archived = 0;
		return eosModel.getTodoforUserExt(archived);
	}

	public String getGuestInitials() {
		guestInitials = eosModel.getUserInitials();
		return guestInitials;
	}

	public void setGuestInitials(String guestInitials) {
		this.guestInitials = guestInitials;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean getMarkImportant() {
		return markImportant;
	}

	public void setMarkImportant(Boolean markImportant) {
		this.markImportant = markImportant;
	}

	public Integer getUserID() {
		return UserID;
	}

	public void setUserID(Integer userID) {
		UserID = userID;
	}

	public List<WorkLog> getWorklog() {

		worklog = eosModel.getWorkLog(arc.mercury.com.utils.Convert
				.toXMLGregorianCalendar(WorkDate));
		return worklog;
	}

	public void setWorklog(List<WorkLog> worklog) {
		this.worklog = worklog;
	}

	public void showWorkLog() {

		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		wkdate = request.getParameter("form:wkdate");
		currentMonth = Integer.toString((Integer.parseInt(request
				.getParameter("form:month")) + 1));
		monthTotal = eosModel.getMonthTotal(Integer.parseInt(currentMonth));
		DateFormat formatter;
		formatter = new SimpleDateFormat("d-MMM-yyyy,HH:mm:ss");
		try {
			WorkDate = formatter.parse(wkdate);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		worklog = eosModel.getWorkLog(arc.mercury.com.utils.Convert
				.toXMLGregorianCalendar(WorkDate));

	}

	public String getWkdate() {
		return wkdate;
	}

	public void setWkdate(String wkdate) {
		this.wkdate = wkdate;
	}

	public String getCurrentMonth() {
		return currentMonth;
	}

	public void setCurrentMonth(String currentMonth) {
		this.currentMonth = currentMonth;
	}

	public String getMonthTotal() {
		monthTotal = eosModel.getMonthTotal(Integer.parseInt(currentMonth));
		return monthTotal;
	}

	public void setMonthTotal(String monthTotal) {
		this.monthTotal = monthTotal;
	}

	public List<Task> getTaskLogs() {
		if (ProjectID != null)
			taskLogs = eosModel.gettaskListing(8, ProjectID);
		return taskLogs;
	}

	public void setTaskLogs(List<Task> taskLogs) {
		this.taskLogs = taskLogs;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Integer getTaskLogID() {
		return taskLogID;
	}

	public void setTaskLogID(Integer taskLogID) {
		this.taskLogID = taskLogID;
	}

	public String getTaskHours() {
		return taskHours;
	}

	public void setTaskHours(String taskHours) {
		this.taskHours = taskHours;
	}

	public void settaskid() {
		FacesContext fc = FacesContext.getCurrentInstance();
		Map<String, String> params = fc.getExternalContext()
				.getRequestParameterMap();
		taskLogID = Integer.parseInt(params.get("id"));

	}

	public Map<Integer, Boolean> getCheckLMap() {
		return checkLMap;
	}

	public Boolean getLogCheck() {
		return logCheck;
	}

	public void setLogCheck(Boolean logCheck) {
		this.logCheck = logCheck;
	}

	public void saveLog() {
		DateFormat formatter;
		formatter = new SimpleDateFormat("d-MMM-yyyy,HH:mm:ss");
		try {
			WorkDate = formatter.parse(wkdate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (logID == 0)
			eosModel.insertActivityLog(UserID, taskLogID,
					arc.mercury.com.utils.Convert
							.toXMLGregorianCalendar(WorkDate), notes, Float
							.parseFloat(taskHours));
		else
			eosModel.updateActivityLog(UserID, taskLogID,
					arc.mercury.com.utils.Convert
							.toXMLGregorianCalendar(WorkDate), notes, Float
							.parseFloat(taskHours), logID);
		notes = "";
		taskHours = "";
		logID = 0;

	}

	public void editActivityLog() {

		FacesContext fc = FacesContext.getCurrentInstance();
		Map<String, String> params = fc.getExternalContext()
				.getRequestParameterMap();
		logID = Integer.parseInt(params.get("logid"));
		WorkLog wl = eosModel.getActivityLogbyID(logID);
		clientID = Integer.parseInt(wl.getClientID());
		ProjectID = Integer.parseInt(wl.getProject());
		taskLogs = eosModel.gettaskListing(8, ProjectID);
		taskLogID = Integer.parseInt(wl.getTask());
		notes = wl.getNote();
		taskHours = wl.getWorkTime();

	}

	public Project getProject() {

		FacesContext fc = FacesContext.getCurrentInstance();
		Map<String, String> params = fc.getExternalContext()
				.getRequestParameterMap();
		try {
			ProjectID = Integer.parseInt(params.get("projectid"));
		} catch (Exception e) {
			JsfMessage.format(FacesMessage.SEVERITY_ERROR,
					"Cannot inject model class :\n", log);
			JsfMessage.format(FacesMessage.SEVERITY_ERROR, e.toString(), log);
		}

		if (ProjectID != null) {
			project = eosModel.getProjectbyID(ProjectID);
			clientID = project.getClientID();
			Integer method = 0;
			list = eosModel.getInvoiceMethods();
			for (InvoiceMethod m : list) {
				if (m.getName().equals(project.getInvoiceMethod()))
					method = m.getID();
			}
			invoicemethodID = method;
			flatrate = Integer.parseInt(project.getFlatFee());
			hourly = Integer.parseInt(project.getHourly());
			budgetedHours = Integer.parseInt(project.getBudgetedHours());
			invoiceType = eosModel.getInvoiceType();
			for (BaseClass m : invoiceType) {
				if (m.getName().equals(project.getAutomaticInvoice()))
					invoiceTypeId = m.getID();
			}

		}
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Boolean getHideDescProject() {
		return hideDescProject;
	}

	public void setHideDescProject(Boolean hideDescProject) {
		this.hideDescProject = hideDescProject;
	}

	public Boolean getHideDescTask() {
		return hideDescTask;
	}

	public void setHideDescTask(Boolean hideDescTask) {
		this.hideDescTask = hideDescTask;

	}

	public void showDescProj() {
		if (!hideDescProject)
			hideDescProject = true;
		else
			hideDescProject = false;
	}

	public void showDescTask() {
		if (!hideDescTask)
			hideDescTask = true;
		else
			hideDescTask = false;
	}

	public WorkLog getNode() {
		return node;
	}

	public void setNode(WorkLog node) {
		this.node = node;
	}

	public Integer getInvoiceTypeId() {
		return invoiceTypeId;
	}

	public void setInvoiceTypeId(Integer invoiceTypeId) {
		this.invoiceTypeId = invoiceTypeId;
	}

	public List<BaseClass> getInvoiceType() {
		if (invoiceType == null)
			eosModel.getInvoiceType();
		return invoiceType;
	}

	public void setInvoiceType(List<BaseClass> invoiceType) {
		this.invoiceType = invoiceType;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public List<Task> getStandardTasks() {
		if (standardTasks == null)
			eosModel.gettaskListing(17, 0);
		return standardTasks;
	}

	public void setStandardTasks(List<Task> standardTasks) {
		this.standardTasks = standardTasks;
	}

	public Integer getStandardTasksID() {
		return standardTasksID;
	}

	public void setStandardTasksID(Integer standardTasksID) {
		this.standardTasksID = standardTasksID;
	}

	public List<BaseClass> getTaskRepeatList() {
		if (taskRepeatList == null)
			taskRepeatList = eosModel.getTaskRepeat();
		return taskRepeatList;
	}

	public void setTaskRepeatList(List<BaseClass> taskRepeatList) {
		this.taskRepeatList = taskRepeatList;
	}

	public void assignEmployee() {

		String empname = "";
		BaseClassExt e = new BaseClassExt();
		for (BaseClass m : employees) {
			if (m.getID() == employeeID)
				empname = m.getName();
		}
		e.setID(employeeID);
		e.setName(empname);
		if (!empname.equals("") && empname != null)
			assignList.add(e);
	}

	public void addTask() {
		newtask = true;

	}

	public void addTasktoList() {
		String method = "";
		String stask = "";
		for (InvoiceMethod m : list) {
			if (m.getID() == taskInvoiceMethodID)
				method = m.getName();
		}

		for (Task m : standardTasks) {
			if (m.getID() == standardTasksID)
				stask = m.getTaskName();
		}
		Task t = new Task();
		if (task.getTaskName() == null)
			t.setTaskName(stask);
		else if (task.getTaskName().equals(""))
			t.setTaskName(stask);
		else
			t.setTaskName(task.getTaskName());
		t.setDescription(task.getDescription());
		t.setRepeat(task.isRepeat());
		t.setRepeatOption(task.getRepeatOption());
		t.setStartDate(task.getStartDate().toString());
		t.setEndDate(task.getEndDate().toString());
		t.setCompensationMethod(method);
		t.setHourly(task.getHourly());
		t.setFlatFee(task.getFlatFee());
		t.setEstimatedHours(task.getEstimatedHours());
		if (task.getTaskName() != null) {
			String Attributes = "";
			Packer Datapack = new Packer();
			Datapack.Append("TaskName", t.getTaskName());
			if (t.getDescription() == null)
				Datapack.Append("Description", "");
			else
				Datapack.Append("Description", t.getDescription());
			Attributes = Datapack.GetPack();
			standardTasksID = eosModel.insertObject(17, 0, 0, Attributes);
		}
		t.setID(standardTasksID);
		if (task.getTaskName() != null || !stask.equals(""))
			taskList.add(t);
	}

	public void removetask() {
		Integer taskID = 0;
		FacesContext fc = FacesContext.getCurrentInstance();
		Map<String, String> params = fc.getExternalContext()
				.getRequestParameterMap();
		taskID = Integer.parseInt(params.get("taskid"));

		for (int i = 0; i < taskList.size(); i++) {

			if (taskList.get(i).getID() == taskID) {
				taskList.remove(i);
			}
		}

	}

	public void removeassign() {
		Integer empID = 0;
		FacesContext fc = FacesContext.getCurrentInstance();
		Map<String, String> params = fc.getExternalContext()
				.getRequestParameterMap();
		empID = Integer.parseInt(params.get("empid"));

		for (int i = 0; i < assignList.size(); i++) {

			if (assignList.get(i).getID() == empID) {
				assignList.remove(i);
			}
		}

	}

	public void saveProject() {
		newtask = false;

		int ParentID = clientID;
		String Attributes = "";
		String automaticInvoice = "";
		for (BaseClass m : invoiceType) {
			if (m.getID() == invoiceTypeId)
				automaticInvoice = m.getName();
		}
		String invoiceMethod = "";
		for (InvoiceMethod m : list) {
			if (m.getID() == invoicemethodID)
				invoiceMethod = m.getName();
		}

		Packer Datapack = new Packer();
		Datapack.Append("ProgramName", project.getName());
		Datapack.Append("AutomaticInvoice", automaticInvoice);
		Datapack.Append("BudgetedHours", Integer.toString(budgetedHours));
		if (project.getDescription() != null)
			Datapack.Append("Description", project.getDescription());
		else
			Datapack.Append("Description", "");
		Datapack.Append("FlatFee", Integer.toString(flatrate));
		Datapack.Append("Hourly", Integer.toString(hourly));
		Datapack.Append("Occurrence", "Once");
		Datapack.Append("Status", "New");
		Datapack.Append("InvoiceMethod", invoiceMethod);
		Attributes = Datapack.GetPack();
		if (ProjectID != null) {
			eosModel.updateWSObject(10, ParentID, 0, Attributes);
			saveTask(ProjectID);
		} else {
			ProjectID = eosModel.insertObject(10, ParentID, 0, Attributes);
			saveTask(ProjectID);
		}
		for (int i = 0; i < assignList.size(); i++) {

			Packer assignDatapack = new Packer();
			String assignAttributes = "";
			assignDatapack.Append("Commission", "");
			assignDatapack.Append("CompensationMethod", "");
			assignDatapack.Append("Flatfee", "");
			assignDatapack.Append("HolidayAmount", "");
			assignDatapack.Append("HourAmount", "");
			assignDatapack.Append("Hourly", "");
			assignDatapack.Append("ProgramID", Integer.toString(ProjectID));
			assignDatapack.Append("ProjectID", "");
			assignDatapack.Append("Seat", Integer.toString(i) + 1);
			assignDatapack.Append("TaskID", "");
			assignDatapack.Append("TodoID", "");
			assignDatapack.Append("WorkType", "Program");
			assignAttributes = assignDatapack.GetPack();

			eosModel.insertObject(9, assignList.get(i).getID(), 0,
					assignAttributes);
		}
	}

	public void saveTask(int ProjectID) {
		for (Task t : taskList) {

			String taskinvoiceMethod = "";
			for (InvoiceMethod m : list) {
				if (m.getID() == taskInvoiceMethodID)
					taskinvoiceMethod = m.getName();
			}
			String taskRepeatName = "";
			for (BaseClass m : taskRepeatList) {
				if (m.getName() == t.getRepeatOption())
					taskRepeatName = m.getName();
			}

			Packer TaskDatapack = new Packer();
			String taskAttributes = "";
			TaskDatapack.Append("Archived", "0");
			TaskDatapack.Append("BudgetedHours", t.getEstimatedHours());
			TaskDatapack.Append("CompensationMethod", taskinvoiceMethod);
			TaskDatapack.Append("DueDate", "");
			TaskDatapack.Append("EmpPrimary", t.getOwner().toString());
			TaskDatapack.Append("EmpSecondary", "");
			TaskDatapack.Append("EmpTertiary", "");
			if (t.getEndDate() != null)
				TaskDatapack.Append("EndDate", t.getEndDate().toString());
			else
				TaskDatapack.Append("EndDate", "");
			TaskDatapack.Append("EstimatedHours", t.getEstimatedHours());
			if (t.getFlatFee() != null)
				TaskDatapack.Append("FlatFee", t.getFlatFee());
			else
				TaskDatapack.Append("FlatFee", "");
			if (t.getHourly() != null)
				TaskDatapack.Append("Hourly", t.getHourly());
			else
				TaskDatapack.Append("Hourly", "");
			TaskDatapack.Append("isBillable", "1");
			TaskDatapack.Append("Owner", t.getOwner().toString());
			TaskDatapack.Append("OwnerID", Integer.toString(t.getOwnerID()));
			TaskDatapack.Append("Repeat", Boolean.toString(t.isRepeat()));
			TaskDatapack.Append("RepeatOption", taskRepeatName);
			if (t.getStartDate() != null)
				TaskDatapack.Append("StartDate", t.getStartDate().toString());
			else
				TaskDatapack.Append("StartDate", "");
			TaskDatapack.Append("Status", "New");
			TaskDatapack.Append("TaskName", t.getTaskName());
			taskAttributes = TaskDatapack.GetPack();
			if (TaskID != null)
				eosModel.updateWSObject(8, ProjectID, 0, taskAttributes);
			else
				TaskID = eosModel.insertObject(8, ProjectID, 0, taskAttributes);

		}
	}

	public void refreshList() {
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		assignorder = request.getParameter("form:assignorder");
		assignorder.split(",");
		String[] assignorder1 = assignorder.split(",");
		String[] assignorder2;
		assignList.clear();

		for (int i = 0; i < assignorder1.length; i++) {
			BaseClassExt assignelement = new BaseClassExt();
			assignorder2 = assignorder1[i].split(":");
			String element = "";
			for (BaseClass m : employees) {
				if (m.getID() == Integer.parseInt(assignorder2[0]))
					element = m.getName();
			}
			assignelement.setID(Integer.parseInt(assignorder2[0]));
			assignelement.setName(element);
			assignList.add(assignelement);
		}

	}

	public List<Task> getTaskList() {
		if (ProjectID != null)
			taskList = eosModel.gettaskListing(8, ProjectID);
		return taskList;
	}

	public void setTaskList(List<Task> taskList) {
		this.taskList = taskList;
	}

	public Integer getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(Integer employeeID) {
		this.employeeID = employeeID;
	}

	public Boolean getNewtask() {
		return newtask;
	}

	public void setNewtask(Boolean newtask) {
		this.newtask = newtask;
	}

	public Integer getBudgetedHours() {
		return budgetedHours;
	}

	public void setBudgetedHours(Integer budgetedHours) {
		this.budgetedHours = budgetedHours;
	}

	public Integer getTaskInvoiceMethodID() {
		return taskInvoiceMethodID;
	}

	public void setTaskInvoiceMethodID(Integer taskInvoiceMethodID) {
		this.taskInvoiceMethodID = taskInvoiceMethodID;
	}

	public List<BaseClassExt> getAssignList() {
		if (ProjectID != null) {
			assignList = eosModel.getAssignbyProject(ProjectID);
		}
		return assignList;
	}

	public void setAssignList(List<BaseClassExt> assignList) {
		this.assignList = assignList;
	}

	public String getAssignorder() {
		return assignorder;
	}

	public void setAssignorder(String assignorder) {
		this.assignorder = assignorder;
	}

	public void billClient() {

	}

	public void createInvoice() {

	}

	public void reviewInvoice() {

	}
}

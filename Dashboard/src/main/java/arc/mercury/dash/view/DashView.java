package arc.mercury.dash.view;

import arc.mercury.com.entities.IRole;
import arc.mercury.com.entities.IUser;
import arc.mercury.dash.common.*;
import arc.mercury.dash.model.DashModel;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

@SuppressWarnings("serial")
@ManagedBean(name = "dashView", eager = true)
@SessionScoped
public class DashView extends HttpServlet {

	Logger log = Logger.getLogger("DashView");
	@ManagedProperty(value = "#{linkedin}")
	private AuthLinkedin linkedin;
	@ManagedProperty(value = "#{dashModel}")
	private DashModel dashModel;
	private FacesContext facesCtx;
	private ExternalContext ctx;
	private String root = "/dashboard";
	private String id;
	private String fName;
	private String lName;
	private String email;
	private IUser login = new IUser();
	private IUser user = new IUser();
	private IUser guest = new IUser();
	private List<SelectItem> states = null;
	private Integer uid = 0;
	private String msg;
	private String search;
	private Long clientId = 0L;
	private Long projectId = 0L;
	private Long invoiceMethodId = 0L;
	private Double flatRate;
	private Double hourRate;
	private String notes = "Notes:";
	private String todoDesc = "To do description";
	private ToDo todo;
	private boolean loggedOn = false;

	public DashView() throws Exception {
		super();
	}

	@PostConstruct
	public void init() {
		guest = new IUser();
		guest.setFname("Anonymous");
		guest.setLname("Anonymous");
		getStates();
	}

	@PreDestroy
	public void end() {
	}

	/**
	 * Processes requests for both HTTP GET and POST methods.
	 *
	 * @param request  servlet request
	 * @param response servlet response
	 */
	protected void processRequest(HttpServletRequest request,
								  HttpServletResponse response) throws ServletException, IOException {

		String userName = request.getParameter("UserName");
		String password = request.getParameter("Password");

		info("Before Login", request);
		try {
			request.login(userName, password);
		} catch (ServletException ex) {
			log.info("Login Failed with a ServletException.." + ex.getMessage());
			return;
		}
	}

	protected void info(String when, HttpServletRequest request) {
		log.info(when);
		log.info("IsUserInRole?.." + request.isUserInRole("client"));
		log.info("getRemoteUser?.." + request.getRemoteUser());
		log.info("getUserPrincipal?.." + request.getUserPrincipal());
		log.info("getAuthType?.." + request.getAuthType());
	}

	public DashModel getDashModel() {
		return dashModel;
	}

	public void setDashModel(DashModel dashModel) {
		this.dashModel = dashModel;
	}

	public String navigate(String page) {
		facesCtx = FacesContext.getCurrentInstance();
		ctx = facesCtx.getExternalContext();
		root = ctx.getRequestContextPath();
		String root = ctx.getRequestContextPath();
		String redirectURL = root + "/main/" + page + ".xhtml";
		try {
			ctx.redirect(redirectURL);
		} catch (Exception e) {
			log.info(e.toString());
		}
		return null;
	}

	public String checkUser() {
		if (login.getName() == null || login.getName().isEmpty()) {
			user = new IUser();
			return user.getRole().getName();
		}
		if (login.getName().equalsIgnoreCase("anonymous")) {
			cleanUser(null);
			return null;
		}
		user = dashModel.getUserByName(login.getName());
		if (user == null) {
			user = dashModel.getUserByMail(login.getName());
			if (user == null) {
				JsfMessage.format(FacesMessage.SEVERITY_ERROR,
						"Invalid ID or password.", log);
				user = new IUser();
				return null;
			}
			login.setMail(login.getName());
			login.setName(null);
		}
		if (user.getPassword() == null) {
			user.setPassword("");
		}
		if (user.getName().isEmpty()) {
			if (!login.getMail().equals(user.getMail())) {
				JsfMessage.format(FacesMessage.SEVERITY_ERROR, "Invalid mail.",
						log);
				user = new IUser();
				return null;
			}
		} else {
			if (!login.getName().equals(user.getName())
					|| !login.getPassword().equals(user.getPassword())) {
				JsfMessage.format(FacesMessage.SEVERITY_ERROR,
						"Invalid ID or password.", log);
				user = new IUser();
				return null;
			}
		}
		loggedOn = true;
		return navigate(user.getRole().getName());
	}

	public void cleanUser(AjaxBehaviorEvent event) {
		user = new IUser();
		loggedOn = false;
		String page = user.getRole().getName();
		navigate(page);
	}

	public IUser getUserById(Long id) {
		return dashModel.getUserById(id);
	}

	public IUser getUserByName(String name) {
		IUser user = dashModel.getUserByName(name);
		return user;
	}

	public IUser getUserByMail(String mail) {
		return dashModel.getUserByMail(mail);
	}

	public String getUserRole() {
		IRole role = user.getRole();
		if (role == null) {
			role = new IRole();
		}
		return role.getName();
	}

	public String saveUser() {
		boolean fail = false;
		if (user.getName().isEmpty()) {
			JsfMessage.format(FacesMessage.SEVERITY_ERROR,
					"User ID may not be empty!", log);
			fail = true;
		}
		if (user.getMail().isEmpty()) {
			JsfMessage.format(FacesMessage.SEVERITY_ERROR,
					"User E-mail may not be empty!", log);
			fail = true;
		}
		// if (user.getPassword().isEmpty()) {
		// JsfMessage.format(FacesMessage.SEVERITY_ERROR,
		// "User password may not be empty!", log);
		// fail = true;
		// }
		if (fail) {
			return "fail";
		}
		try {
			user = dashModel.saveUser(user);
		} catch (Exception e) {
			JsfMessage.format(FacesMessage.SEVERITY_ERROR,
					"User ID / e-mail already registered!", log);
			return "fail";
		}
		return user.getRole().getName();
	}

	public List<IUser> getAnonymous() {
		List<IUser> users = dashModel.getAnonymous();
		return users;
	}

	public IUser saveAnonymous(IUser user) {
		try {
			user.setRole(new IRole());
			user = dashModel.saveUser(user);
		} catch (Exception e) {
			JsfMessage.format(FacesMessage.SEVERITY_ERROR,
					"User ID / e-mail already registered!", log);
		}
		return user;
	}

	public IUser getLogin() {
		return login;
	}

	public void setLogin(IUser login) {
		this.login = login;
	}

	public IUser getUser() {
		if (user == null) {
			user = new IUser();
		}
		return user;
	}

	public void setUser(IUser user) {
		this.user = user;
	}

	public IUser getGuest() {
		return guest;
	}

	public void setGuest(IUser guest) {
		this.guest = guest;
	}

	public List<SelectItem> getStates() {
		if (states == null) {
			states = dashModel.getStates();
		}
		return states;
	}

	public void setStates(List<SelectItem> states) {
		this.states = states;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getTitle() {
		StringBuffer sb = new StringBuffer("Dashboard");
		return sb.toString();
	}

	public String getWelcome() {
		StringBuffer sb = new StringBuffer("Hi ");
		sb.append(user.getFname());
		sb.append("!");
		return sb.toString();
	}

	public String getSpecial() {
		StringBuffer sb = new StringBuffer(
				"Special offers for Business Plan generator until 15 april ");
		return sb.toString();
	}

	public String getMessageText() {
		StringBuffer sb = new StringBuffer();
		sb.append("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna. Aenean velit odio, elementum in tempus ut, vehicula eu diam. Pellentesque rhoncus aliquam mattis. Ut vulputate eros sed felis sodales nec vulputate justo hendrerit. Vivamus varius pretium ligula, a aliquam odio euismod sit amet. Quisque laoreet sem sit amet orci ullamcorper at ultricies metus viverra. Pellentesque arcu mauris, malesuada quis ornare accumsan, blandit sed diam.");
		return sb.toString();
	}

	public List<SelectItem> getClients() {
		List<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(1L, "Client name"));
		return list;
	}

	public List<SelectItem> getProjects() {
		List<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(1L, "Project name"));
		return list;
	}

	public List<SelectItem> getFileClients() {
		List<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(1L, "Clients listed below"));
		return list;
	}

	public List<SelectItem> getFilePPTD() {
		List<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(1L, "Project"));
		list.add(new SelectItem(2L, "Program"));
		list.add(new SelectItem(3L, "TO do list"));
		return list;
	}

	public List<SelectItem> getInvoiceMethod() {
		List<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(1L, "Hourly"));
		list.add(new SelectItem(2L, "Flat rate"));
		list.add(new SelectItem(3L, "Both"));
		list.add(new SelectItem(4L, "None"));
		return list;
	}

	public String getMessageDate() {
		DateFormat dsf = DateFormat.getDateInstance(DateFormat.FULL, Locale.US);
		return dsf.format(new Date());
	}

	public String getGuestInitials() {
		StringBuffer sb = new StringBuffer();
		sb.append(guest.getFname().substring(0, 1)).append(".");
		sb.append(guest.getLname().substring(0, 1)).append(".:");
		return sb.toString();
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getTodoDesc() {
		return todoDesc;
	}

	public void setTodoDesc(String todoDesc) {
		this.todoDesc = todoDesc;
	}

	public Double getFlatRate() {
		return flatRate;
	}

	public void setFlatRate(Double flatRate) {
		this.flatRate = flatRate;
	}

	public Double getHourRate() {
		return hourRate;
	}

	public void setHourRate(Double hourRate) {
		this.hourRate = hourRate;
	}

	public FacesContext getFacesCtx() {
		return facesCtx;
	}

	public void setFacesCtx(FacesContext facesCtx) {
		this.facesCtx = facesCtx;
	}

	public ExternalContext getCtx() {
		return ctx;
	}

	public void setCtx(ExternalContext ctx) {
		this.ctx = ctx;
	}

	public String getRoot() {
		return root;
	}

	public void setRoot(String root) {
		this.root = root;
	}

	public String getUid() {
		uid = (++uid % 7) + 1;
		return uid.toString();
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public List<Float[]> getWork() {
		List<Float[]> list = new ArrayList<Float[]>();
		for (int i = 0; i < 5; ++i) {
			Float[] td = new Float[8];
			td[0] = (float) 0.0;
			td[1] = (float) 3.7;
			td[2] = (float) 8.3;
			td[3] = (float) 0.0;
			td[4] = (float) 0.0;
			td[5] = (float) 0.0;
			td[6] = (float) 0.0;
			td[7] = (float) 12.0;
			list.add(td);
		}
		return list;
	}

	public List<WorkLog> getWorkLog() {
		List<WorkLog> list = new ArrayList<WorkLog>();
		for (int i = 0; i < 4; ++i) {
			WorkLog td = new WorkLog();
			td.setSelected(false);
			td.setProject("Project name");
			td.setTask("Task name here");
			td.setNotes("asd asd asd asd asd asd asd");
			td.setHours(20.0);
			list.add(td);
		}
		return list;
	}

	public List<Files> getFilesList() {
		List<Files> list = new ArrayList<Files>();
		for (int i = 0; i < 8; ++i) {
			Files td = new Files();
			td.setName("File name");
			td.setModDate(new Date());
			td.setModBy(guest.getFullName());
			list.add(td);
		}
		return list;
	}

	@SuppressWarnings("deprecation")
	public List<Project> getProjectList() {
		List<Project> list = new ArrayList<Project>();
		for (int i = 0; i < 8; ++i) {
			Project td = new Project();
			td.setName("Project name");
			td.setDueDay(new Date());
			td.setEstTime(new Time(((++uid % 8) * 10) + 1, 0, 0));
			td.setWrkTime(new Time(((++uid % 8) * 10) - 1, 30, 0));
			td.setStatus((uid % 3) > 0);
			list.add(td);
		}
		return list;
	}

	@SuppressWarnings("deprecation")
	public List<ToDo> getTodoList() {
		List<ToDo> list = new ArrayList<ToDo>();
		for (int i = 0; i < 8; ++i) {
			ToDo td = new ToDo();
			td.setName("To do name");
			td.setDescription(getMessageText());
			td.setDueDate(new Date());
			td.setEstTime(new Time(((++uid % 8) * 10) + 1, 0, 0));
			td.setWrkTime(new Time(((++uid % 8) * 10) - 1, 30, 0));
			td.setStatus((uid % 3) > 0);
			list.add(td);
		}
		return list;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public boolean isLoggedOn() {
		return loggedOn;
	}

	public void setLoggedOn(boolean loggedOn) {
		this.loggedOn = loggedOn;
	}

	public ToDo getTodo() {
		return todo;
	}

	public void setTodo(ToDo todo) {
		this.todo = todo;
	}

	public String addTodo() {
		todo = new ToDo();
		return "/dashboard/main/employee/editTodo.xhtml";
	}

	public Long getInvoiceMethodId() {
		return invoiceMethodId;
	}

	public void setInvoiceMethodId(Long invoiceMethodId) {
		this.invoiceMethodId = invoiceMethodId;
	}

	public AuthLinkedin getLinkedin() {
		return linkedin;
	}

	public void setLinkedin(AuthLinkedin linkedin) {
		this.linkedin = linkedin;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String addLinkedinUser() {
		user = linkedin.getUser();
		return saveUser();
	}
}

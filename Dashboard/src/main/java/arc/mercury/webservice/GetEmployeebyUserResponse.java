
package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getEmployeebyUserResult" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getEmployeebyUserResult"
})
@XmlRootElement(name = "getEmployeebyUserResponse")
public class GetEmployeebyUserResponse {

    protected int getEmployeebyUserResult;

    /**
     * Gets the value of the getEmployeebyUserResult property.
     * 
     */
    public int getGetEmployeebyUserResult() {
        return getEmployeebyUserResult;
    }

    /**
     * Sets the value of the getEmployeebyUserResult property.
     * 
     */
    public void setGetEmployeebyUserResult(int value) {
        this.getEmployeebyUserResult = value;
    }

}

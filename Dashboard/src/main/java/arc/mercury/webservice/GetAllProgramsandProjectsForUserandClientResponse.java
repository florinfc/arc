
package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetAllProgramsandProjectsForUserandClientResult" type="{http://arcmercury.com/websevices}ArrayOfBaseClassExt" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAllProgramsandProjectsForUserandClientResult"
})
@XmlRootElement(name = "GetAllProgramsandProjectsForUserandClientResponse")
public class GetAllProgramsandProjectsForUserandClientResponse {

    @XmlElement(name = "GetAllProgramsandProjectsForUserandClientResult")
    protected ArrayOfBaseClassExt getAllProgramsandProjectsForUserandClientResult;

    /**
     * Gets the value of the getAllProgramsandProjectsForUserandClientResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfBaseClassExt }
     *     
     */
    public ArrayOfBaseClassExt getGetAllProgramsandProjectsForUserandClientResult() {
        return getAllProgramsandProjectsForUserandClientResult;
    }

    /**
     * Sets the value of the getAllProgramsandProjectsForUserandClientResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfBaseClassExt }
     *     
     */
    public void setGetAllProgramsandProjectsForUserandClientResult(ArrayOfBaseClassExt value) {
        this.getAllProgramsandProjectsForUserandClientResult = value;
    }

}

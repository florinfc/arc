
package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getALogbyTaskResult" type="{http://arcmercury.com/websevices}ArrayOfActivityLog" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getALogbyTaskResult"
})
@XmlRootElement(name = "getALogbyTaskResponse")
public class GetALogbyTaskResponse {

    protected ArrayOfActivityLog getALogbyTaskResult;

    /**
     * Gets the value of the getALogbyTaskResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfActivityLog }
     *     
     */
    public ArrayOfActivityLog getGetALogbyTaskResult() {
        return getALogbyTaskResult;
    }

    /**
     * Sets the value of the getALogbyTaskResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfActivityLog }
     *     
     */
    public void setGetALogbyTaskResult(ArrayOfActivityLog value) {
        this.getALogbyTaskResult = value;
    }

}

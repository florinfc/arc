
package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the arc.mercury.webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: arc.mercury.webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetAllTasksAndTodosForUserResponse }
     * 
     */
    public GetAllTasksAndTodosForUserResponse createGetAllTasksAndTodosForUserResponse() {
        return new GetAllTasksAndTodosForUserResponse();
    }

    /**
     * Create an instance of {@link GetDayTotalResponse }
     * 
     */
    public GetDayTotalResponse createGetDayTotalResponse() {
        return new GetDayTotalResponse();
    }

    /**
     * Create an instance of {@link GetInitials }
     * 
     */
    public GetInitials createGetInitials() {
        return new GetInitials();
    }

    /**
     * Create an instance of {@link GetWorkLogbyID }
     * 
     */
    public GetWorkLogbyID createGetWorkLogbyID() {
        return new GetWorkLogbyID();
    }

    /**
     * Create an instance of {@link GetTaskListingResponse }
     * 
     */
    public GetTaskListingResponse createGetTaskListingResponse() {
        return new GetTaskListingResponse();
    }

    /**
     * Create an instance of {@link GetAllProgramsForUser }
     * 
     */
    public GetAllProgramsForUser createGetAllProgramsForUser() {
        return new GetAllProgramsForUser();
    }

    /**
     * Create an instance of {@link DeleteObject }
     * 
     */
    public DeleteObject createDeleteObject() {
        return new DeleteObject();
    }

    /**
     * Create an instance of {@link GetAllToDosForUserExt }
     * 
     */
    public GetAllToDosForUserExt createGetAllToDosForUserExt() {
        return new GetAllToDosForUserExt();
    }

    /**
     * Create an instance of {@link GetEmployeebyUser }
     * 
     */
    public GetEmployeebyUser createGetEmployeebyUser() {
        return new GetEmployeebyUser();
    }

    /**
     * Create an instance of {@link GetAllProgramsandProjectsWithMessage }
     * 
     */
    public GetAllProgramsandProjectsWithMessage createGetAllProgramsandProjectsWithMessage() {
        return new GetAllProgramsandProjectsWithMessage();
    }

    /**
     * Create an instance of {@link UpdateObject }
     * 
     */
    public UpdateObject createUpdateObject() {
        return new UpdateObject();
    }

    /**
     * Create an instance of {@link ArrayOfAmObject }
     * 
     */
    public ArrayOfAmObject createArrayOfAmObject() {
        return new ArrayOfAmObject();
    }

    /**
     * Create an instance of {@link UpdateToDoResponse }
     * 
     */
    public UpdateToDoResponse createUpdateToDoResponse() {
        return new UpdateToDoResponse();
    }

    /**
     * Create an instance of {@link GetAllToDosForUserExtResponse }
     * 
     */
    public GetAllToDosForUserExtResponse createGetAllToDosForUserExtResponse() {
        return new GetAllToDosForUserExtResponse();
    }

    /**
     * Create an instance of {@link GetWorkLog }
     * 
     */
    public GetWorkLog createGetWorkLog() {
        return new GetWorkLog();
    }

    /**
     * Create an instance of {@link InsertToDoResponse }
     * 
     */
    public InsertToDoResponse createInsertToDoResponse() {
        return new InsertToDoResponse();
    }

    /**
     * Create an instance of {@link GetAllClientsForUserResponse }
     * 
     */
    public GetAllClientsForUserResponse createGetAllClientsForUserResponse() {
        return new GetAllClientsForUserResponse();
    }

    /**
     * Create an instance of {@link DeleteActivityLogResponse }
     * 
     */
    public DeleteActivityLogResponse createDeleteActivityLogResponse() {
        return new DeleteActivityLogResponse();
    }

    /**
     * Create an instance of {@link GetObject }
     * 
     */
    public GetObject createGetObject() {
        return new GetObject();
    }

    /**
     * Create an instance of {@link ArrayOfInvoiceMethod }
     * 
     */
    public ArrayOfInvoiceMethod createArrayOfInvoiceMethod() {
        return new ArrayOfInvoiceMethod();
    }

    /**
     * Create an instance of {@link GetAllTasksForUserandClient }
     * 
     */
    public GetAllTasksForUserandClient createGetAllTasksForUserandClient() {
        return new GetAllTasksForUserandClient();
    }

    /**
     * Create an instance of {@link WorkLog }
     * 
     */
    public WorkLog createWorkLog() {
        return new WorkLog();
    }

    /**
     * Create an instance of {@link GetAssignbyProject }
     * 
     */
    public GetAssignbyProject createGetAssignbyProject() {
        return new GetAssignbyProject();
    }

    /**
     * Create an instance of {@link InsertActivityLog }
     * 
     */
    public InsertActivityLog createInsertActivityLog() {
        return new InsertActivityLog();
    }

    /**
     * Create an instance of {@link GetAllProgramsForUserResponse }
     * 
     */
    public GetAllProgramsForUserResponse createGetAllProgramsForUserResponse() {
        return new GetAllProgramsForUserResponse();
    }

    /**
     * Create an instance of {@link GetInvoiceMethods }
     * 
     */
    public GetInvoiceMethods createGetInvoiceMethods() {
        return new GetInvoiceMethods();
    }

    /**
     * Create an instance of {@link GetObjectResponse }
     * 
     */
    public GetObjectResponse createGetObjectResponse() {
        return new GetObjectResponse();
    }

    /**
     * Create an instance of {@link GetAssignbyProjectResponse }
     * 
     */
    public GetAssignbyProjectResponse createGetAssignbyProjectResponse() {
        return new GetAssignbyProjectResponse();
    }

    /**
     * Create an instance of {@link GetAllToDosForUserResponse }
     * 
     */
    public GetAllToDosForUserResponse createGetAllToDosForUserResponse() {
        return new GetAllToDosForUserResponse();
    }

    /**
     * Create an instance of {@link GetAllTasksAndTodosForUser }
     * 
     */
    public GetAllTasksAndTodosForUser createGetAllTasksAndTodosForUser() {
        return new GetAllTasksAndTodosForUser();
    }

    /**
     * Create an instance of {@link GetAllProgramsandProjectsForUserandClientResponse }
     * 
     */
    public GetAllProgramsandProjectsForUserandClientResponse createGetAllProgramsandProjectsForUserandClientResponse() {
        return new GetAllProgramsandProjectsForUserandClientResponse();
    }

    /**
     * Create an instance of {@link GetInvoiceTypeResponse }
     * 
     */
    public GetInvoiceTypeResponse createGetInvoiceTypeResponse() {
        return new GetInvoiceTypeResponse();
    }

    /**
     * Create an instance of {@link UpdateObjectAttributeResponse }
     * 
     */
    public UpdateObjectAttributeResponse createUpdateObjectAttributeResponse() {
        return new UpdateObjectAttributeResponse();
    }

    /**
     * Create an instance of {@link GetAllTasksForUserandClientResponse }
     * 
     */
    public GetAllTasksForUserandClientResponse createGetAllTasksForUserandClientResponse() {
        return new GetAllTasksForUserandClientResponse();
    }

    /**
     * Create an instance of {@link GetAllProjectsForUserResponse }
     * 
     */
    public GetAllProjectsForUserResponse createGetAllProjectsForUserResponse() {
        return new GetAllProjectsForUserResponse();
    }

    /**
     * Create an instance of {@link UpdateObjectResponse }
     * 
     */
    public UpdateObjectResponse createUpdateObjectResponse() {
        return new UpdateObjectResponse();
    }

    /**
     * Create an instance of {@link GetAllClientsForUser }
     * 
     */
    public GetAllClientsForUser createGetAllClientsForUser() {
        return new GetAllClientsForUser();
    }

    /**
     * Create an instance of {@link UpdateActivityLog }
     * 
     */
    public UpdateActivityLog createUpdateActivityLog() {
        return new UpdateActivityLog();
    }

    /**
     * Create an instance of {@link ArrayOfProject }
     * 
     */
    public ArrayOfProject createArrayOfProject() {
        return new ArrayOfProject();
    }

    /**
     * Create an instance of {@link GetTaskInvoicing }
     * 
     */
    public GetTaskInvoicing createGetTaskInvoicing() {
        return new GetTaskInvoicing();
    }

    /**
     * Create an instance of {@link ArrayOfBaseClass }
     * 
     */
    public ArrayOfBaseClass createArrayOfBaseClass() {
        return new ArrayOfBaseClass();
    }

    /**
     * Create an instance of {@link GetMessagesforProjectsResponse }
     * 
     */
    public GetMessagesforProjectsResponse createGetMessagesforProjectsResponse() {
        return new GetMessagesforProjectsResponse();
    }

    /**
     * Create an instance of {@link GetInvoiceMethodsResponse }
     * 
     */
    public GetInvoiceMethodsResponse createGetInvoiceMethodsResponse() {
        return new GetInvoiceMethodsResponse();
    }

    /**
     * Create an instance of {@link BaseClass }
     * 
     */
    public BaseClass createBaseClass() {
        return new BaseClass();
    }

    /**
     * Create an instance of {@link GetMessagesforProjects }
     * 
     */
    public GetMessagesforProjects createGetMessagesforProjects() {
        return new GetMessagesforProjects();
    }

    /**
     * Create an instance of {@link GetEmployeeListing }
     * 
     */
    public GetEmployeeListing createGetEmployeeListing() {
        return new GetEmployeeListing();
    }

    /**
     * Create an instance of {@link InsertObjectResponse }
     * 
     */
    public InsertObjectResponse createInsertObjectResponse() {
        return new InsertObjectResponse();
    }

    /**
     * Create an instance of {@link InsertToDo }
     * 
     */
    public InsertToDo createInsertToDo() {
        return new InsertToDo();
    }

    /**
     * Create an instance of {@link UpdateToDoStatusResponse }
     * 
     */
    public UpdateToDoStatusResponse createUpdateToDoStatusResponse() {
        return new UpdateToDoStatusResponse();
    }

    /**
     * Create an instance of {@link UpdateObjectAttribute }
     * 
     */
    public UpdateObjectAttribute createUpdateObjectAttribute() {
        return new UpdateObjectAttribute();
    }

    /**
     * Create an instance of {@link GetAllProjectsForUserWithMessage }
     * 
     */
    public GetAllProjectsForUserWithMessage createGetAllProjectsForUserWithMessage() {
        return new GetAllProjectsForUserWithMessage();
    }

    /**
     * Create an instance of {@link GetALogbyTaskResponse }
     * 
     */
    public GetALogbyTaskResponse createGetALogbyTaskResponse() {
        return new GetALogbyTaskResponse();
    }

    /**
     * Create an instance of {@link GetAllTasksForUserResponse }
     * 
     */
    public GetAllTasksForUserResponse createGetAllTasksForUserResponse() {
        return new GetAllTasksForUserResponse();
    }

    /**
     * Create an instance of {@link GetAllProjectsForUserandClientResponse }
     * 
     */
    public GetAllProjectsForUserandClientResponse createGetAllProjectsForUserandClientResponse() {
        return new GetAllProjectsForUserandClientResponse();
    }

    /**
     * Create an instance of {@link GetProjectInvoicingResponse }
     * 
     */
    public GetProjectInvoicingResponse createGetProjectInvoicingResponse() {
        return new GetProjectInvoicingResponse();
    }

    /**
     * Create an instance of {@link GetProjectbyIDResponse }
     * 
     */
    public GetProjectbyIDResponse createGetProjectbyIDResponse() {
        return new GetProjectbyIDResponse();
    }

    /**
     * Create an instance of {@link InsertObject }
     * 
     */
    public InsertObject createInsertObject() {
        return new InsertObject();
    }

    /**
     * Create an instance of {@link UpdateActivityLogResponse }
     * 
     */
    public UpdateActivityLogResponse createUpdateActivityLogResponse() {
        return new UpdateActivityLogResponse();
    }

    /**
     * Create an instance of {@link GetAllProjectsForUserWithMessageResponse }
     * 
     */
    public GetAllProjectsForUserWithMessageResponse createGetAllProjectsForUserWithMessageResponse() {
        return new GetAllProjectsForUserWithMessageResponse();
    }

    /**
     * Create an instance of {@link GetEmployeeListingResponse }
     * 
     */
    public GetEmployeeListingResponse createGetEmployeeListingResponse() {
        return new GetEmployeeListingResponse();
    }

    /**
     * Create an instance of {@link GetAllProgramsandProjectsForUser }
     * 
     */
    public GetAllProgramsandProjectsForUser createGetAllProgramsandProjectsForUser() {
        return new GetAllProgramsandProjectsForUser();
    }

    /**
     * Create an instance of {@link GetTaskRepeatResponse }
     * 
     */
    public GetTaskRepeatResponse createGetTaskRepeatResponse() {
        return new GetTaskRepeatResponse();
    }

    /**
     * Create an instance of {@link ArrayOfWorkLog }
     * 
     */
    public ArrayOfWorkLog createArrayOfWorkLog() {
        return new ArrayOfWorkLog();
    }

    /**
     * Create an instance of {@link GetTodoChangesforTaskResponse }
     * 
     */
    public GetTodoChangesforTaskResponse createGetTodoChangesforTaskResponse() {
        return new GetTodoChangesforTaskResponse();
    }

    /**
     * Create an instance of {@link GetAllProgramsandProjectsForUserResponse }
     * 
     */
    public GetAllProgramsandProjectsForUserResponse createGetAllProgramsandProjectsForUserResponse() {
        return new GetAllProgramsandProjectsForUserResponse();
    }

    /**
     * Create an instance of {@link GetAllToDoforClientResponse }
     * 
     */
    public GetAllToDoforClientResponse createGetAllToDoforClientResponse() {
        return new GetAllToDoforClientResponse();
    }

    /**
     * Create an instance of {@link GetWorkLogbyIDResponse }
     * 
     */
    public GetWorkLogbyIDResponse createGetWorkLogbyIDResponse() {
        return new GetWorkLogbyIDResponse();
    }

    /**
     * Create an instance of {@link UpdateToDoStatus }
     * 
     */
    public UpdateToDoStatus createUpdateToDoStatus() {
        return new UpdateToDoStatus();
    }

    /**
     * Create an instance of {@link Client }
     * 
     */
    public Client createClient() {
        return new Client();
    }

    /**
     * Create an instance of {@link ArrayOfActivityLog }
     * 
     */
    public ArrayOfActivityLog createArrayOfActivityLog() {
        return new ArrayOfActivityLog();
    }

    /**
     * Create an instance of {@link GetDayTotal }
     * 
     */
    public GetDayTotal createGetDayTotal() {
        return new GetDayTotal();
    }

    /**
     * Create an instance of {@link GetAllProgramsandProjectsForUserandClient }
     * 
     */
    public GetAllProgramsandProjectsForUserandClient createGetAllProgramsandProjectsForUserandClient() {
        return new GetAllProgramsandProjectsForUserandClient();
    }

    /**
     * Create an instance of {@link GetClassID }
     * 
     */
    public GetClassID createGetClassID() {
        return new GetClassID();
    }

    /**
     * Create an instance of {@link GetAllProjectsForUser }
     * 
     */
    public GetAllProjectsForUser createGetAllProjectsForUser() {
        return new GetAllProjectsForUser();
    }

    /**
     * Create an instance of {@link InvoiceMethod }
     * 
     */
    public InvoiceMethod createInvoiceMethod() {
        return new InvoiceMethod();
    }

    /**
     * Create an instance of {@link GetALogbyTask }
     * 
     */
    public GetALogbyTask createGetALogbyTask() {
        return new GetALogbyTask();
    }

    /**
     * Create an instance of {@link GetProjectbyID }
     * 
     */
    public GetProjectbyID createGetProjectbyID() {
        return new GetProjectbyID();
    }

    /**
     * Create an instance of {@link GetProjectInvoicing }
     * 
     */
    public GetProjectInvoicing createGetProjectInvoicing() {
        return new GetProjectInvoicing();
    }

    /**
     * Create an instance of {@link GetAllTasksForUser }
     * 
     */
    public GetAllTasksForUser createGetAllTasksForUser() {
        return new GetAllTasksForUser();
    }

    /**
     * Create an instance of {@link AmObject }
     * 
     */
    public AmObject createAmObject() {
        return new AmObject();
    }

    /**
     * Create an instance of {@link GetTaskListing }
     * 
     */
    public GetTaskListing createGetTaskListing() {
        return new GetTaskListing();
    }

    /**
     * Create an instance of {@link GetInvoiceType }
     * 
     */
    public GetInvoiceType createGetInvoiceType() {
        return new GetInvoiceType();
    }

    /**
     * Create an instance of {@link GetAllProjectsForUserandClient }
     * 
     */
    public GetAllProjectsForUserandClient createGetAllProjectsForUserandClient() {
        return new GetAllProjectsForUserandClient();
    }

    /**
     * Create an instance of {@link ArrayOfTask }
     * 
     */
    public ArrayOfTask createArrayOfTask() {
        return new ArrayOfTask();
    }

    /**
     * Create an instance of {@link GetAllProgramsandProjectsWithMessageResponse }
     * 
     */
    public GetAllProgramsandProjectsWithMessageResponse createGetAllProgramsandProjectsWithMessageResponse() {
        return new GetAllProgramsandProjectsWithMessageResponse();
    }

    /**
     * Create an instance of {@link BaseClassExt }
     * 
     */
    public BaseClassExt createBaseClassExt() {
        return new BaseClassExt();
    }

    /**
     * Create an instance of {@link GetAllToDoforClient }
     * 
     */
    public GetAllToDoforClient createGetAllToDoforClient() {
        return new GetAllToDoforClient();
    }

    /**
     * Create an instance of {@link GetClassIDResponse }
     * 
     */
    public GetClassIDResponse createGetClassIDResponse() {
        return new GetClassIDResponse();
    }

    /**
     * Create an instance of {@link GetWorkLogResponse }
     * 
     */
    public GetWorkLogResponse createGetWorkLogResponse() {
        return new GetWorkLogResponse();
    }

    /**
     * Create an instance of {@link DeleteObjectResponse }
     * 
     */
    public DeleteObjectResponse createDeleteObjectResponse() {
        return new DeleteObjectResponse();
    }

    /**
     * Create an instance of {@link GetTaskRepeat }
     * 
     */
    public GetTaskRepeat createGetTaskRepeat() {
        return new GetTaskRepeat();
    }

    /**
     * Create an instance of {@link DeleteActivityLog }
     * 
     */
    public DeleteActivityLog createDeleteActivityLog() {
        return new DeleteActivityLog();
    }

    /**
     * Create an instance of {@link ArrayOfHours }
     * 
     */
    public ArrayOfHours createArrayOfHours() {
        return new ArrayOfHours();
    }

    /**
     * Create an instance of {@link GetTaskInvoicingResponse }
     * 
     */
    public GetTaskInvoicingResponse createGetTaskInvoicingResponse() {
        return new GetTaskInvoicingResponse();
    }

    /**
     * Create an instance of {@link Hours }
     * 
     */
    public Hours createHours() {
        return new Hours();
    }

    /**
     * Create an instance of {@link GetAllPrgPrjandTD }
     * 
     */
    public GetAllPrgPrjandTD createGetAllPrgPrjandTD() {
        return new GetAllPrgPrjandTD();
    }

    /**
     * Create an instance of {@link UpdateToDo }
     * 
     */
    public UpdateToDo createUpdateToDo() {
        return new UpdateToDo();
    }

    /**
     * Create an instance of {@link Project }
     * 
     */
    public Project createProject() {
        return new Project();
    }

    /**
     * Create an instance of {@link SetStatusToDo }
     * 
     */
    public SetStatusToDo createSetStatusToDo() {
        return new SetStatusToDo();
    }

    /**
     * Create an instance of {@link GetInitialsResponse }
     * 
     */
    public GetInitialsResponse createGetInitialsResponse() {
        return new GetInitialsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfClient }
     * 
     */
    public ArrayOfClient createArrayOfClient() {
        return new ArrayOfClient();
    }

    /**
     * Create an instance of {@link Task }
     * 
     */
    public Task createTask() {
        return new Task();
    }

    /**
     * Create an instance of {@link GetMonthTotalResponse }
     * 
     */
    public GetMonthTotalResponse createGetMonthTotalResponse() {
        return new GetMonthTotalResponse();
    }

    /**
     * Create an instance of {@link GetAllPrgPrjandTDResponse }
     * 
     */
    public GetAllPrgPrjandTDResponse createGetAllPrgPrjandTDResponse() {
        return new GetAllPrgPrjandTDResponse();
    }

    /**
     * Create an instance of {@link GetAllToDosForUser }
     * 
     */
    public GetAllToDosForUser createGetAllToDosForUser() {
        return new GetAllToDosForUser();
    }

    /**
     * Create an instance of {@link ActivityLog }
     * 
     */
    public ActivityLog createActivityLog() {
        return new ActivityLog();
    }

    /**
     * Create an instance of {@link GetTodoChangesforTask }
     * 
     */
    public GetTodoChangesforTask createGetTodoChangesforTask() {
        return new GetTodoChangesforTask();
    }

    /**
     * Create an instance of {@link InsertActivityLogResponse }
     * 
     */
    public InsertActivityLogResponse createInsertActivityLogResponse() {
        return new InsertActivityLogResponse();
    }

    /**
     * Create an instance of {@link GetMonthTotal }
     * 
     */
    public GetMonthTotal createGetMonthTotal() {
        return new GetMonthTotal();
    }

    /**
     * Create an instance of {@link ArrayOfBaseClassExt }
     * 
     */
    public ArrayOfBaseClassExt createArrayOfBaseClassExt() {
        return new ArrayOfBaseClassExt();
    }

    /**
     * Create an instance of {@link SetStatusToDoResponse }
     * 
     */
    public SetStatusToDoResponse createSetStatusToDoResponse() {
        return new SetStatusToDoResponse();
    }

    /**
     * Create an instance of {@link GetEmployeebyUserResponse }
     * 
     */
    public GetEmployeebyUserResponse createGetEmployeebyUserResponse() {
        return new GetEmployeebyUserResponse();
    }

}

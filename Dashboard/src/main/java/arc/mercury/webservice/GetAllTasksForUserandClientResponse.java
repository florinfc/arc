
package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetAllTasksForUserandClientResult" type="{http://arcmercury.com/websevices}ArrayOfBaseClassExt" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAllTasksForUserandClientResult"
})
@XmlRootElement(name = "GetAllTasksForUserandClientResponse")
public class GetAllTasksForUserandClientResponse {

    @XmlElement(name = "GetAllTasksForUserandClientResult")
    protected ArrayOfBaseClassExt getAllTasksForUserandClientResult;

    /**
     * Gets the value of the getAllTasksForUserandClientResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfBaseClassExt }
     *     
     */
    public ArrayOfBaseClassExt getGetAllTasksForUserandClientResult() {
        return getAllTasksForUserandClientResult;
    }

    /**
     * Sets the value of the getAllTasksForUserandClientResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfBaseClassExt }
     *     
     */
    public void setGetAllTasksForUserandClientResult(ArrayOfBaseClassExt value) {
        this.getAllTasksForUserandClientResult = value;
    }

}

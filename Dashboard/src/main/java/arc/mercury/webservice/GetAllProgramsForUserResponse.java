
package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetAllProgramsForUserResult" type="{http://arcmercury.com/websevices}ArrayOfBaseClass" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAllProgramsForUserResult"
})
@XmlRootElement(name = "GetAllProgramsForUserResponse")
public class GetAllProgramsForUserResponse {

    @XmlElement(name = "GetAllProgramsForUserResult")
    protected ArrayOfBaseClass getAllProgramsForUserResult;

    /**
     * Gets the value of the getAllProgramsForUserResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfBaseClass }
     *     
     */
    public ArrayOfBaseClass getGetAllProgramsForUserResult() {
        return getAllProgramsForUserResult;
    }

    /**
     * Sets the value of the getAllProgramsForUserResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfBaseClass }
     *     
     */
    public void setGetAllProgramsForUserResult(ArrayOfBaseClass value) {
        this.getAllProgramsForUserResult = value;
    }

}


package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetAllToDosForUserResult" type="{http://arcmercury.com/websevices}ArrayOfBaseClass" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAllToDosForUserResult"
})
@XmlRootElement(name = "GetAllToDosForUserResponse")
public class GetAllToDosForUserResponse {

    @XmlElement(name = "GetAllToDosForUserResult")
    protected ArrayOfBaseClass getAllToDosForUserResult;

    /**
     * Gets the value of the getAllToDosForUserResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfBaseClass }
     *     
     */
    public ArrayOfBaseClass getGetAllToDosForUserResult() {
        return getAllToDosForUserResult;
    }

    /**
     * Sets the value of the getAllToDosForUserResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfBaseClass }
     *     
     */
    public void setGetAllToDosForUserResult(ArrayOfBaseClass value) {
        this.getAllToDosForUserResult = value;
    }

}

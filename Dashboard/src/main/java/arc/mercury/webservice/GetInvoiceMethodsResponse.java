
package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetInvoiceMethodsResult" type="{http://arcmercury.com/websevices}ArrayOfInvoiceMethod" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getInvoiceMethodsResult"
})
@XmlRootElement(name = "GetInvoiceMethodsResponse")
public class GetInvoiceMethodsResponse {

    @XmlElement(name = "GetInvoiceMethodsResult")
    protected ArrayOfInvoiceMethod getInvoiceMethodsResult;

    /**
     * Gets the value of the getInvoiceMethodsResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfInvoiceMethod }
     *     
     */
    public ArrayOfInvoiceMethod getGetInvoiceMethodsResult() {
        return getInvoiceMethodsResult;
    }

    /**
     * Sets the value of the getInvoiceMethodsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfInvoiceMethod }
     *     
     */
    public void setGetInvoiceMethodsResult(ArrayOfInvoiceMethod value) {
        this.getInvoiceMethodsResult = value;
    }

}

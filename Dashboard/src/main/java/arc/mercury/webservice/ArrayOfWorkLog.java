
package arc.mercury.webservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWorkLog complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWorkLog">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WorkLog" type="{http://arcmercury.com/websevices}WorkLog" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWorkLog", propOrder = {
    "workLog"
})
public class ArrayOfWorkLog {

    @XmlElement(name = "WorkLog", nillable = true)
    protected List<WorkLog> workLog;

    /**
     * Gets the value of the workLog property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the workLog property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWorkLog().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WorkLog }
     * 
     * 
     */
    public List<WorkLog> getWorkLog() {
        if (workLog == null) {
            workLog = new ArrayList<WorkLog>();
        }
        return this.workLog;
    }

}


package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getTaskInvoicingResult" type="{http://arcmercury.com/websevices}ArrayOfTask" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTaskInvoicingResult"
})
@XmlRootElement(name = "getTaskInvoicingResponse")
public class GetTaskInvoicingResponse {

    protected ArrayOfTask getTaskInvoicingResult;

    /**
     * Gets the value of the getTaskInvoicingResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTask }
     *     
     */
    public ArrayOfTask getGetTaskInvoicingResult() {
        return getTaskInvoicingResult;
    }

    /**
     * Sets the value of the getTaskInvoicingResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTask }
     *     
     */
    public void setGetTaskInvoicingResult(ArrayOfTask value) {
        this.getTaskInvoicingResult = value;
    }

}


package arc.mercury.webservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfBaseClassExt complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfBaseClassExt">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BaseClassExt" type="{http://arcmercury.com/websevices}BaseClassExt" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfBaseClassExt", propOrder = {
    "baseClassExt"
})
public class ArrayOfBaseClassExt {

    @XmlElement(name = "BaseClassExt", nillable = true)
    protected List<BaseClassExt> baseClassExt;

    /**
     * Gets the value of the baseClassExt property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the baseClassExt property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBaseClassExt().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BaseClassExt }
     * 
     * 
     */
    public List<BaseClassExt> getBaseClassExt() {
        if (baseClassExt == null) {
            baseClassExt = new ArrayList<BaseClassExt>();
        }
        return this.baseClassExt;
    }

}

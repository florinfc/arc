
package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getClassIDResult" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getClassIDResult"
})
@XmlRootElement(name = "getClassIDResponse")
public class GetClassIDResponse {

    protected int getClassIDResult;

    /**
     * Gets the value of the getClassIDResult property.
     * 
     */
    public int getGetClassIDResult() {
        return getClassIDResult;
    }

    /**
     * Sets the value of the getClassIDResult property.
     * 
     */
    public void setGetClassIDResult(int value) {
        this.getClassIDResult = value;
    }

}

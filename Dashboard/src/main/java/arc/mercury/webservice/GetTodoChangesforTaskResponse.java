
package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetTodoChangesforTaskResult" type="{http://arcmercury.com/websevices}ArrayOfBaseClassExt" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTodoChangesforTaskResult"
})
@XmlRootElement(name = "GetTodoChangesforTaskResponse")
public class GetTodoChangesforTaskResponse {

    @XmlElement(name = "GetTodoChangesforTaskResult")
    protected ArrayOfBaseClassExt getTodoChangesforTaskResult;

    /**
     * Gets the value of the getTodoChangesforTaskResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfBaseClassExt }
     *     
     */
    public ArrayOfBaseClassExt getGetTodoChangesforTaskResult() {
        return getTodoChangesforTaskResult;
    }

    /**
     * Sets the value of the getTodoChangesforTaskResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfBaseClassExt }
     *     
     */
    public void setGetTodoChangesforTaskResult(ArrayOfBaseClassExt value) {
        this.getTodoChangesforTaskResult = value;
    }

}

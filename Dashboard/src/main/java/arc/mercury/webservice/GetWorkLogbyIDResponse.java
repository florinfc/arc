
package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getWorkLogbyIDResult" type="{http://arcmercury.com/websevices}WorkLog" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getWorkLogbyIDResult"
})
@XmlRootElement(name = "getWorkLogbyIDResponse")
public class GetWorkLogbyIDResponse {

    protected WorkLog getWorkLogbyIDResult;

    /**
     * Gets the value of the getWorkLogbyIDResult property.
     * 
     * @return
     *     possible object is
     *     {@link WorkLog }
     *     
     */
    public WorkLog getGetWorkLogbyIDResult() {
        return getWorkLogbyIDResult;
    }

    /**
     * Sets the value of the getWorkLogbyIDResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkLog }
     *     
     */
    public void setGetWorkLogbyIDResult(WorkLog value) {
        this.getWorkLogbyIDResult = value;
    }

}

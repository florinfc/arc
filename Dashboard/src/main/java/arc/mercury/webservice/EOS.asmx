<?xml version="1.0" encoding="utf-8"?>
<wsdl:definitions xmlns:tm="http://microsoft.com/wsdl/mime/textMatching/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:tns="http://arcmercury.com/websevices" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" targetNamespace="http://arcmercury.com/websevices" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/">
  <wsdl:types>
    <s:schema elementFormDefault="qualified" targetNamespace="http://arcmercury.com/websevices">
      <s:element name="getClassID">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="ClassName" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getClassIDResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="getClassIDResult" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getObject">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="ClassID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="ParentID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="ObjectID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getObjectResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="getObjectResult" type="tns:ArrayOfAmObject" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="ArrayOfAmObject">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="AmObject" nillable="true" type="tns:AmObject" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="AmObject">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="ClassAttributeID" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="Name" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Value" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Access" type="s:string" />
        </s:sequence>
      </s:complexType>
      <s:element name="insertObject">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="ClassID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="ParentID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="ObjectID" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="Attributes" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="UserID" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="insertObjectResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="insertObjectResult" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getEmployeebyUser">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getEmployeebyUserResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="getEmployeebyUserResult" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updateObject">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="ClassID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="ParentID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="ObjectID" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="Attributes" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updateObjectResponse">
        <s:complexType />
      </s:element>
      <s:element name="deleteObject">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="ObjectID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="deleteObjectResponse">
        <s:complexType />
      </s:element>
      <s:element name="deleteActivityLog">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="ALogID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="deleteActivityLogResponse">
        <s:complexType />
      </s:element>
      <s:element name="insertToDo">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="Status" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="DueDate" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="Link" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="Subject" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="Note" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="insertToDoResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="insertToDoResult" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="insertActivityLog">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="TaskID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="StartDate" type="s:dateTime" />
            <s:element minOccurs="0" maxOccurs="1" name="Note" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="hours" type="s:float" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="insertActivityLogResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="insertActivityLogResult" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updateActivityLog">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="TaskID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="StartDate" type="s:dateTime" />
            <s:element minOccurs="0" maxOccurs="1" name="Note" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="hours" type="s:float" />
            <s:element minOccurs="1" maxOccurs="1" name="AlogID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updateActivityLogResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="updateActivityLogResult" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updateToDo">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="ID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="TimeSpent" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="DueDate" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="Subject" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="Note" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updateToDoResponse">
        <s:complexType />
      </s:element>
      <s:element name="setStatusToDo">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="ID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="Status" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="setStatusToDoResponse">
        <s:complexType />
      </s:element>
      <s:element name="GetAllClientsForUser">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllClientsForUserResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetAllClientsForUserResult" type="tns:ArrayOfClient" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="ArrayOfClient">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="Client" nillable="true" type="tns:Client" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="Client">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="ID" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="Name" type="s:string" />
        </s:sequence>
      </s:complexType>
      <s:element name="GetInvoiceMethods">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetInvoiceMethodsResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetInvoiceMethodsResult" type="tns:ArrayOfInvoiceMethod" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="ArrayOfInvoiceMethod">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="InvoiceMethod" nillable="true" type="tns:InvoiceMethod" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="InvoiceMethod">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="ID" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="Name" type="s:string" />
        </s:sequence>
      </s:complexType>
      <s:element name="GetProjectbyID">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="ProjectID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetProjectbyIDResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetProjectbyIDResult" type="tns:Project" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="Project">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="ID" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="Name" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Message" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Date" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="MessageCount" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="Initials" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Type" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="ClientID" type="s:int" />
          <s:element minOccurs="1" maxOccurs="1" name="Important" type="s:boolean" />
          <s:element minOccurs="0" maxOccurs="1" name="Description" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Status" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="InvoiceMethod" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="FlatFee" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Hourly" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="BudgetedHours" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Occurence" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="AutomaticInvoice" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="WorkTime" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="BilledTime" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Budget" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="BilledAmount" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="Billed" type="s:boolean" />
        </s:sequence>
      </s:complexType>
      <s:element name="GetInvoiceType">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetInvoiceTypeResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetInvoiceTypeResult" type="tns:ArrayOfBaseClass" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="ArrayOfBaseClass">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="BaseClass" nillable="true" type="tns:BaseClass" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="BaseClass">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="ID" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="Name" type="s:string" />
        </s:sequence>
      </s:complexType>
      <s:element name="GetTaskRepeat">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetTaskRepeatResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetTaskRepeatResult" type="tns:ArrayOfBaseClass" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllProgramsandProjectsForUser">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllProgramsandProjectsForUserResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetAllProgramsandProjectsForUserResult" type="tns:ArrayOfBaseClassExt" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="ArrayOfBaseClassExt">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="BaseClassExt" nillable="true" type="tns:BaseClassExt" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="BaseClassExt">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="ID" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="Name" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Value" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Type" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="ParentID" type="s:int" />
          <s:element minOccurs="1" maxOccurs="1" name="EstimateH" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="DueDate" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Worktime" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="ToDoID" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="Description" type="s:string" />
        </s:sequence>
      </s:complexType>
      <s:element name="GetAllProgramsForUser">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllProgramsForUserResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetAllProgramsForUserResult" type="tns:ArrayOfBaseClass" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllProjectsForUser">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllProjectsForUserResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetAllProjectsForUserResult" type="tns:ArrayOfBaseClassExt" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllTasksForUser">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllTasksForUserResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetAllTasksForUserResult" type="tns:ArrayOfBaseClass" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllToDosForUser">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="Archived" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllToDosForUserResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetAllToDosForUserResult" type="tns:ArrayOfBaseClass" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllToDosForUserExt">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="Archived" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllToDosForUserExtResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetAllToDosForUserExtResult" type="tns:ArrayOfBaseClassExt" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllTasksAndTodosForUser">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllTasksAndTodosForUserResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetAllTasksAndTodosForUserResult" type="tns:ArrayOfBaseClass" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllProgramsandProjectsWithMessage">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="date" type="s:dateTime" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllProgramsandProjectsWithMessageResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetAllProgramsandProjectsWithMessageResult" type="tns:ArrayOfProject" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="ArrayOfProject">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="Project" nillable="true" type="tns:Project" />
        </s:sequence>
      </s:complexType>
      <s:element name="GetAllProjectsForUserWithMessage">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="date" type="s:dateTime" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllProjectsForUserWithMessageResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetAllProjectsForUserWithMessageResult" type="tns:ArrayOfProject" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetMessagesforProjects">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="ProjectID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="date" type="s:dateTime" />
            <s:element minOccurs="0" maxOccurs="1" name="Type" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetMessagesforProjectsResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetMessagesforProjectsResult" type="tns:ArrayOfProject" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllProgramsandProjectsForUserandClient">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="ClientID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllProgramsandProjectsForUserandClientResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetAllProgramsandProjectsForUserandClientResult" type="tns:ArrayOfBaseClassExt" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllPrgPrjandTD">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="ClientID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllPrgPrjandTDResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetAllPrgPrjandTDResult" type="tns:ArrayOfBaseClassExt" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllProjectsForUserandClient">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="ClientID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllProjectsForUserandClientResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetAllProjectsForUserandClientResult" type="tns:ArrayOfBaseClassExt" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllToDoforClient">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="ClientID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllToDoforClientResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetAllToDoforClientResult" type="tns:ArrayOfBaseClassExt" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllTasksForUserandClient">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="ClientID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="ProjectID" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="Type" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="Archived" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllTasksForUserandClientResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetAllTasksForUserandClientResult" type="tns:ArrayOfBaseClassExt" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetTodoChangesforTask">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="TaskID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetTodoChangesforTaskResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetTodoChangesforTaskResult" type="tns:ArrayOfBaseClassExt" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updateToDoStatus">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="ID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="Status" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updateToDoStatusResponse">
        <s:complexType />
      </s:element>
      <s:element name="updateObjectAttribute">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="ObjectID" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="Attribute" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="Value" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updateObjectAttributeResponse">
        <s:complexType />
      </s:element>
      <s:element name="getInitials">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getInitialsResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="getInitialsResult" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getTaskListing">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="ClassID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="ParentID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getTaskListingResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="getTaskListingResult" type="tns:ArrayOfTask" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="ArrayOfTask">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="Task" nillable="true" type="tns:Task" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="Task">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="ID" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="TaskName" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Description" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Status" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="CompensationMethod" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="FlatFee" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Hourly" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="BudgetedHours" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="EstimatedHours" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="EmpPrimary" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="EmpSecondary" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="EmpTertiary" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="isBillable" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="DueDate" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="Archived" type="s:boolean" />
          <s:element minOccurs="1" maxOccurs="1" name="Repeat" type="s:boolean" />
          <s:element minOccurs="0" maxOccurs="1" name="StartDate" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="EndDate" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="RepeatOption" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Owner" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="RepeatID" type="s:int" />
          <s:element minOccurs="1" maxOccurs="1" name="ownerID" type="s:int" />
        </s:sequence>
      </s:complexType>
      <s:element name="getEmployeeListing">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="ClassID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="ParentID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="UserID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getEmployeeListingResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="getEmployeeListingResult" type="tns:ArrayOfBaseClass" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getAssignbyProject">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="ProjectID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getAssignbyProjectResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="getAssignbyProjectResult" type="tns:ArrayOfBaseClassExt" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getWorkLog">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="WorkDate" type="s:dateTime" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getWorkLogResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="getWorkLogResult" type="tns:ArrayOfWorkLog" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="ArrayOfWorkLog">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="WorkLog" nillable="true" type="tns:WorkLog" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="WorkLog">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="1" name="Project" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="WorkLogID" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="ProjectType" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Task" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Note" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="WorkTime" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="ClientID" type="s:string" />
        </s:sequence>
      </s:complexType>
      <s:element name="getWorkLogbyID">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="LogID" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getWorkLogbyIDResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="getWorkLogbyIDResult" type="tns:WorkLog" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getMonthTotal">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="Month" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getMonthTotalResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="getMonthTotalResult" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getDayTotal">
        <s:complexType />
      </s:element>
      <s:element name="getDayTotalResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="getDayTotalResult" type="tns:ArrayOfHours" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="ArrayOfHours">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="Hours" nillable="true" type="tns:Hours" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="Hours">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="EventID" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="EventName" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="StartDate" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="EndDate" type="s:string" />
        </s:sequence>
      </s:complexType>
    </s:schema>
  </wsdl:types>
  <wsdl:message name="getClassIDSoapIn">
    <wsdl:part name="parameters" element="tns:getClassID" />
  </wsdl:message>
  <wsdl:message name="getClassIDSoapOut">
    <wsdl:part name="parameters" element="tns:getClassIDResponse" />
  </wsdl:message>
  <wsdl:message name="getObjectSoapIn">
    <wsdl:part name="parameters" element="tns:getObject" />
  </wsdl:message>
  <wsdl:message name="getObjectSoapOut">
    <wsdl:part name="parameters" element="tns:getObjectResponse" />
  </wsdl:message>
  <wsdl:message name="insertObjectSoapIn">
    <wsdl:part name="parameters" element="tns:insertObject" />
  </wsdl:message>
  <wsdl:message name="insertObjectSoapOut">
    <wsdl:part name="parameters" element="tns:insertObjectResponse" />
  </wsdl:message>
  <wsdl:message name="getEmployeebyUserSoapIn">
    <wsdl:part name="parameters" element="tns:getEmployeebyUser" />
  </wsdl:message>
  <wsdl:message name="getEmployeebyUserSoapOut">
    <wsdl:part name="parameters" element="tns:getEmployeebyUserResponse" />
  </wsdl:message>
  <wsdl:message name="updateObjectSoapIn">
    <wsdl:part name="parameters" element="tns:updateObject" />
  </wsdl:message>
  <wsdl:message name="updateObjectSoapOut">
    <wsdl:part name="parameters" element="tns:updateObjectResponse" />
  </wsdl:message>
  <wsdl:message name="deleteObjectSoapIn">
    <wsdl:part name="parameters" element="tns:deleteObject" />
  </wsdl:message>
  <wsdl:message name="deleteObjectSoapOut">
    <wsdl:part name="parameters" element="tns:deleteObjectResponse" />
  </wsdl:message>
  <wsdl:message name="deleteActivityLogSoapIn">
    <wsdl:part name="parameters" element="tns:deleteActivityLog" />
  </wsdl:message>
  <wsdl:message name="deleteActivityLogSoapOut">
    <wsdl:part name="parameters" element="tns:deleteActivityLogResponse" />
  </wsdl:message>
  <wsdl:message name="insertToDoSoapIn">
    <wsdl:part name="parameters" element="tns:insertToDo" />
  </wsdl:message>
  <wsdl:message name="insertToDoSoapOut">
    <wsdl:part name="parameters" element="tns:insertToDoResponse" />
  </wsdl:message>
  <wsdl:message name="insertActivityLogSoapIn">
    <wsdl:part name="parameters" element="tns:insertActivityLog" />
  </wsdl:message>
  <wsdl:message name="insertActivityLogSoapOut">
    <wsdl:part name="parameters" element="tns:insertActivityLogResponse" />
  </wsdl:message>
  <wsdl:message name="updateActivityLogSoapIn">
    <wsdl:part name="parameters" element="tns:updateActivityLog" />
  </wsdl:message>
  <wsdl:message name="updateActivityLogSoapOut">
    <wsdl:part name="parameters" element="tns:updateActivityLogResponse" />
  </wsdl:message>
  <wsdl:message name="updateToDoSoapIn">
    <wsdl:part name="parameters" element="tns:updateToDo" />
  </wsdl:message>
  <wsdl:message name="updateToDoSoapOut">
    <wsdl:part name="parameters" element="tns:updateToDoResponse" />
  </wsdl:message>
  <wsdl:message name="setStatusToDoSoapIn">
    <wsdl:part name="parameters" element="tns:setStatusToDo" />
  </wsdl:message>
  <wsdl:message name="setStatusToDoSoapOut">
    <wsdl:part name="parameters" element="tns:setStatusToDoResponse" />
  </wsdl:message>
  <wsdl:message name="GetAllClientsForUserSoapIn">
    <wsdl:part name="parameters" element="tns:GetAllClientsForUser" />
  </wsdl:message>
  <wsdl:message name="GetAllClientsForUserSoapOut">
    <wsdl:part name="parameters" element="tns:GetAllClientsForUserResponse" />
  </wsdl:message>
  <wsdl:message name="GetInvoiceMethodsSoapIn">
    <wsdl:part name="parameters" element="tns:GetInvoiceMethods" />
  </wsdl:message>
  <wsdl:message name="GetInvoiceMethodsSoapOut">
    <wsdl:part name="parameters" element="tns:GetInvoiceMethodsResponse" />
  </wsdl:message>
  <wsdl:message name="GetProjectbyIDSoapIn">
    <wsdl:part name="parameters" element="tns:GetProjectbyID" />
  </wsdl:message>
  <wsdl:message name="GetProjectbyIDSoapOut">
    <wsdl:part name="parameters" element="tns:GetProjectbyIDResponse" />
  </wsdl:message>
  <wsdl:message name="GetInvoiceTypeSoapIn">
    <wsdl:part name="parameters" element="tns:GetInvoiceType" />
  </wsdl:message>
  <wsdl:message name="GetInvoiceTypeSoapOut">
    <wsdl:part name="parameters" element="tns:GetInvoiceTypeResponse" />
  </wsdl:message>
  <wsdl:message name="GetTaskRepeatSoapIn">
    <wsdl:part name="parameters" element="tns:GetTaskRepeat" />
  </wsdl:message>
  <wsdl:message name="GetTaskRepeatSoapOut">
    <wsdl:part name="parameters" element="tns:GetTaskRepeatResponse" />
  </wsdl:message>
  <wsdl:message name="GetAllProgramsandProjectsForUserSoapIn">
    <wsdl:part name="parameters" element="tns:GetAllProgramsandProjectsForUser" />
  </wsdl:message>
  <wsdl:message name="GetAllProgramsandProjectsForUserSoapOut">
    <wsdl:part name="parameters" element="tns:GetAllProgramsandProjectsForUserResponse" />
  </wsdl:message>
  <wsdl:message name="GetAllProgramsForUserSoapIn">
    <wsdl:part name="parameters" element="tns:GetAllProgramsForUser" />
  </wsdl:message>
  <wsdl:message name="GetAllProgramsForUserSoapOut">
    <wsdl:part name="parameters" element="tns:GetAllProgramsForUserResponse" />
  </wsdl:message>
  <wsdl:message name="GetAllProjectsForUserSoapIn">
    <wsdl:part name="parameters" element="tns:GetAllProjectsForUser" />
  </wsdl:message>
  <wsdl:message name="GetAllProjectsForUserSoapOut">
    <wsdl:part name="parameters" element="tns:GetAllProjectsForUserResponse" />
  </wsdl:message>
  <wsdl:message name="GetAllTasksForUserSoapIn">
    <wsdl:part name="parameters" element="tns:GetAllTasksForUser" />
  </wsdl:message>
  <wsdl:message name="GetAllTasksForUserSoapOut">
    <wsdl:part name="parameters" element="tns:GetAllTasksForUserResponse" />
  </wsdl:message>
  <wsdl:message name="GetAllToDosForUserSoapIn">
    <wsdl:part name="parameters" element="tns:GetAllToDosForUser" />
  </wsdl:message>
  <wsdl:message name="GetAllToDosForUserSoapOut">
    <wsdl:part name="parameters" element="tns:GetAllToDosForUserResponse" />
  </wsdl:message>
  <wsdl:message name="GetAllToDosForUserExtSoapIn">
    <wsdl:part name="parameters" element="tns:GetAllToDosForUserExt" />
  </wsdl:message>
  <wsdl:message name="GetAllToDosForUserExtSoapOut">
    <wsdl:part name="parameters" element="tns:GetAllToDosForUserExtResponse" />
  </wsdl:message>
  <wsdl:message name="GetAllTasksAndTodosForUserSoapIn">
    <wsdl:part name="parameters" element="tns:GetAllTasksAndTodosForUser" />
  </wsdl:message>
  <wsdl:message name="GetAllTasksAndTodosForUserSoapOut">
    <wsdl:part name="parameters" element="tns:GetAllTasksAndTodosForUserResponse" />
  </wsdl:message>
  <wsdl:message name="GetAllProgramsandProjectsWithMessageSoapIn">
    <wsdl:part name="parameters" element="tns:GetAllProgramsandProjectsWithMessage" />
  </wsdl:message>
  <wsdl:message name="GetAllProgramsandProjectsWithMessageSoapOut">
    <wsdl:part name="parameters" element="tns:GetAllProgramsandProjectsWithMessageResponse" />
  </wsdl:message>
  <wsdl:message name="GetAllProjectsForUserWithMessageSoapIn">
    <wsdl:part name="parameters" element="tns:GetAllProjectsForUserWithMessage" />
  </wsdl:message>
  <wsdl:message name="GetAllProjectsForUserWithMessageSoapOut">
    <wsdl:part name="parameters" element="tns:GetAllProjectsForUserWithMessageResponse" />
  </wsdl:message>
  <wsdl:message name="GetMessagesforProjectsSoapIn">
    <wsdl:part name="parameters" element="tns:GetMessagesforProjects" />
  </wsdl:message>
  <wsdl:message name="GetMessagesforProjectsSoapOut">
    <wsdl:part name="parameters" element="tns:GetMessagesforProjectsResponse" />
  </wsdl:message>
  <wsdl:message name="GetAllProgramsandProjectsForUserandClientSoapIn">
    <wsdl:part name="parameters" element="tns:GetAllProgramsandProjectsForUserandClient" />
  </wsdl:message>
  <wsdl:message name="GetAllProgramsandProjectsForUserandClientSoapOut">
    <wsdl:part name="parameters" element="tns:GetAllProgramsandProjectsForUserandClientResponse" />
  </wsdl:message>
  <wsdl:message name="GetAllPrgPrjandTDSoapIn">
    <wsdl:part name="parameters" element="tns:GetAllPrgPrjandTD" />
  </wsdl:message>
  <wsdl:message name="GetAllPrgPrjandTDSoapOut">
    <wsdl:part name="parameters" element="tns:GetAllPrgPrjandTDResponse" />
  </wsdl:message>
  <wsdl:message name="GetAllProjectsForUserandClientSoapIn">
    <wsdl:part name="parameters" element="tns:GetAllProjectsForUserandClient" />
  </wsdl:message>
  <wsdl:message name="GetAllProjectsForUserandClientSoapOut">
    <wsdl:part name="parameters" element="tns:GetAllProjectsForUserandClientResponse" />
  </wsdl:message>
  <wsdl:message name="GetAllToDoforClientSoapIn">
    <wsdl:part name="parameters" element="tns:GetAllToDoforClient" />
  </wsdl:message>
  <wsdl:message name="GetAllToDoforClientSoapOut">
    <wsdl:part name="parameters" element="tns:GetAllToDoforClientResponse" />
  </wsdl:message>
  <wsdl:message name="GetAllTasksForUserandClientSoapIn">
    <wsdl:part name="parameters" element="tns:GetAllTasksForUserandClient" />
  </wsdl:message>
  <wsdl:message name="GetAllTasksForUserandClientSoapOut">
    <wsdl:part name="parameters" element="tns:GetAllTasksForUserandClientResponse" />
  </wsdl:message>
  <wsdl:message name="GetTodoChangesforTaskSoapIn">
    <wsdl:part name="parameters" element="tns:GetTodoChangesforTask" />
  </wsdl:message>
  <wsdl:message name="GetTodoChangesforTaskSoapOut">
    <wsdl:part name="parameters" element="tns:GetTodoChangesforTaskResponse" />
  </wsdl:message>
  <wsdl:message name="updateToDoStatusSoapIn">
    <wsdl:part name="parameters" element="tns:updateToDoStatus" />
  </wsdl:message>
  <wsdl:message name="updateToDoStatusSoapOut">
    <wsdl:part name="parameters" element="tns:updateToDoStatusResponse" />
  </wsdl:message>
  <wsdl:message name="updateObjectAttributeSoapIn">
    <wsdl:part name="parameters" element="tns:updateObjectAttribute" />
  </wsdl:message>
  <wsdl:message name="updateObjectAttributeSoapOut">
    <wsdl:part name="parameters" element="tns:updateObjectAttributeResponse" />
  </wsdl:message>
  <wsdl:message name="getInitialsSoapIn">
    <wsdl:part name="parameters" element="tns:getInitials" />
  </wsdl:message>
  <wsdl:message name="getInitialsSoapOut">
    <wsdl:part name="parameters" element="tns:getInitialsResponse" />
  </wsdl:message>
  <wsdl:message name="getTaskListingSoapIn">
    <wsdl:part name="parameters" element="tns:getTaskListing" />
  </wsdl:message>
  <wsdl:message name="getTaskListingSoapOut">
    <wsdl:part name="parameters" element="tns:getTaskListingResponse" />
  </wsdl:message>
  <wsdl:message name="getEmployeeListingSoapIn">
    <wsdl:part name="parameters" element="tns:getEmployeeListing" />
  </wsdl:message>
  <wsdl:message name="getEmployeeListingSoapOut">
    <wsdl:part name="parameters" element="tns:getEmployeeListingResponse" />
  </wsdl:message>
  <wsdl:message name="getAssignbyProjectSoapIn">
    <wsdl:part name="parameters" element="tns:getAssignbyProject" />
  </wsdl:message>
  <wsdl:message name="getAssignbyProjectSoapOut">
    <wsdl:part name="parameters" element="tns:getAssignbyProjectResponse" />
  </wsdl:message>
  <wsdl:message name="getWorkLogSoapIn">
    <wsdl:part name="parameters" element="tns:getWorkLog" />
  </wsdl:message>
  <wsdl:message name="getWorkLogSoapOut">
    <wsdl:part name="parameters" element="tns:getWorkLogResponse" />
  </wsdl:message>
  <wsdl:message name="getWorkLogbyIDSoapIn">
    <wsdl:part name="parameters" element="tns:getWorkLogbyID" />
  </wsdl:message>
  <wsdl:message name="getWorkLogbyIDSoapOut">
    <wsdl:part name="parameters" element="tns:getWorkLogbyIDResponse" />
  </wsdl:message>
  <wsdl:message name="getMonthTotalSoapIn">
    <wsdl:part name="parameters" element="tns:getMonthTotal" />
  </wsdl:message>
  <wsdl:message name="getMonthTotalSoapOut">
    <wsdl:part name="parameters" element="tns:getMonthTotalResponse" />
  </wsdl:message>
  <wsdl:message name="getDayTotalSoapIn">
    <wsdl:part name="parameters" element="tns:getDayTotal" />
  </wsdl:message>
  <wsdl:message name="getDayTotalSoapOut">
    <wsdl:part name="parameters" element="tns:getDayTotalResponse" />
  </wsdl:message>
  <wsdl:portType name="EOSSoap">
    <wsdl:operation name="getClassID">
      <wsdl:input message="tns:getClassIDSoapIn" />
      <wsdl:output message="tns:getClassIDSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="getObject">
      <wsdl:input message="tns:getObjectSoapIn" />
      <wsdl:output message="tns:getObjectSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="insertObject">
      <wsdl:input message="tns:insertObjectSoapIn" />
      <wsdl:output message="tns:insertObjectSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="getEmployeebyUser">
      <wsdl:input message="tns:getEmployeebyUserSoapIn" />
      <wsdl:output message="tns:getEmployeebyUserSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="updateObject">
      <wsdl:input message="tns:updateObjectSoapIn" />
      <wsdl:output message="tns:updateObjectSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="deleteObject">
      <wsdl:input message="tns:deleteObjectSoapIn" />
      <wsdl:output message="tns:deleteObjectSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="deleteActivityLog">
      <wsdl:input message="tns:deleteActivityLogSoapIn" />
      <wsdl:output message="tns:deleteActivityLogSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="insertToDo">
      <wsdl:input message="tns:insertToDoSoapIn" />
      <wsdl:output message="tns:insertToDoSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="insertActivityLog">
      <wsdl:input message="tns:insertActivityLogSoapIn" />
      <wsdl:output message="tns:insertActivityLogSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="updateActivityLog">
      <wsdl:input message="tns:updateActivityLogSoapIn" />
      <wsdl:output message="tns:updateActivityLogSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="updateToDo">
      <wsdl:input message="tns:updateToDoSoapIn" />
      <wsdl:output message="tns:updateToDoSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="setStatusToDo">
      <wsdl:input message="tns:setStatusToDoSoapIn" />
      <wsdl:output message="tns:setStatusToDoSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetAllClientsForUser">
      <wsdl:input message="tns:GetAllClientsForUserSoapIn" />
      <wsdl:output message="tns:GetAllClientsForUserSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetInvoiceMethods">
      <wsdl:input message="tns:GetInvoiceMethodsSoapIn" />
      <wsdl:output message="tns:GetInvoiceMethodsSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetProjectbyID">
      <wsdl:input message="tns:GetProjectbyIDSoapIn" />
      <wsdl:output message="tns:GetProjectbyIDSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetInvoiceType">
      <wsdl:input message="tns:GetInvoiceTypeSoapIn" />
      <wsdl:output message="tns:GetInvoiceTypeSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetTaskRepeat">
      <wsdl:input message="tns:GetTaskRepeatSoapIn" />
      <wsdl:output message="tns:GetTaskRepeatSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetAllProgramsandProjectsForUser">
      <wsdl:input message="tns:GetAllProgramsandProjectsForUserSoapIn" />
      <wsdl:output message="tns:GetAllProgramsandProjectsForUserSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetAllProgramsForUser">
      <wsdl:input message="tns:GetAllProgramsForUserSoapIn" />
      <wsdl:output message="tns:GetAllProgramsForUserSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetAllProjectsForUser">
      <wsdl:input message="tns:GetAllProjectsForUserSoapIn" />
      <wsdl:output message="tns:GetAllProjectsForUserSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetAllTasksForUser">
      <wsdl:input message="tns:GetAllTasksForUserSoapIn" />
      <wsdl:output message="tns:GetAllTasksForUserSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetAllToDosForUser">
      <wsdl:input message="tns:GetAllToDosForUserSoapIn" />
      <wsdl:output message="tns:GetAllToDosForUserSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetAllToDosForUserExt">
      <wsdl:input message="tns:GetAllToDosForUserExtSoapIn" />
      <wsdl:output message="tns:GetAllToDosForUserExtSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetAllTasksAndTodosForUser">
      <wsdl:input message="tns:GetAllTasksAndTodosForUserSoapIn" />
      <wsdl:output message="tns:GetAllTasksAndTodosForUserSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetAllProgramsandProjectsWithMessage">
      <wsdl:input message="tns:GetAllProgramsandProjectsWithMessageSoapIn" />
      <wsdl:output message="tns:GetAllProgramsandProjectsWithMessageSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetAllProjectsForUserWithMessage">
      <wsdl:input message="tns:GetAllProjectsForUserWithMessageSoapIn" />
      <wsdl:output message="tns:GetAllProjectsForUserWithMessageSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetMessagesforProjects">
      <wsdl:input message="tns:GetMessagesforProjectsSoapIn" />
      <wsdl:output message="tns:GetMessagesforProjectsSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetAllProgramsandProjectsForUserandClient">
      <wsdl:input message="tns:GetAllProgramsandProjectsForUserandClientSoapIn" />
      <wsdl:output message="tns:GetAllProgramsandProjectsForUserandClientSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetAllPrgPrjandTD">
      <wsdl:input message="tns:GetAllPrgPrjandTDSoapIn" />
      <wsdl:output message="tns:GetAllPrgPrjandTDSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetAllProjectsForUserandClient">
      <wsdl:input message="tns:GetAllProjectsForUserandClientSoapIn" />
      <wsdl:output message="tns:GetAllProjectsForUserandClientSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetAllToDoforClient">
      <wsdl:input message="tns:GetAllToDoforClientSoapIn" />
      <wsdl:output message="tns:GetAllToDoforClientSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetAllTasksForUserandClient">
      <wsdl:input message="tns:GetAllTasksForUserandClientSoapIn" />
      <wsdl:output message="tns:GetAllTasksForUserandClientSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetTodoChangesforTask">
      <wsdl:input message="tns:GetTodoChangesforTaskSoapIn" />
      <wsdl:output message="tns:GetTodoChangesforTaskSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="updateToDoStatus">
      <wsdl:input message="tns:updateToDoStatusSoapIn" />
      <wsdl:output message="tns:updateToDoStatusSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="updateObjectAttribute">
      <wsdl:input message="tns:updateObjectAttributeSoapIn" />
      <wsdl:output message="tns:updateObjectAttributeSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="getInitials">
      <wsdl:input message="tns:getInitialsSoapIn" />
      <wsdl:output message="tns:getInitialsSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="getTaskListing">
      <wsdl:input message="tns:getTaskListingSoapIn" />
      <wsdl:output message="tns:getTaskListingSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="getEmployeeListing">
      <wsdl:input message="tns:getEmployeeListingSoapIn" />
      <wsdl:output message="tns:getEmployeeListingSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="getAssignbyProject">
      <wsdl:input message="tns:getAssignbyProjectSoapIn" />
      <wsdl:output message="tns:getAssignbyProjectSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="getWorkLog">
      <wsdl:input message="tns:getWorkLogSoapIn" />
      <wsdl:output message="tns:getWorkLogSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="getWorkLogbyID">
      <wsdl:input message="tns:getWorkLogbyIDSoapIn" />
      <wsdl:output message="tns:getWorkLogbyIDSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="getMonthTotal">
      <wsdl:input message="tns:getMonthTotalSoapIn" />
      <wsdl:output message="tns:getMonthTotalSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="getDayTotal">
      <wsdl:input message="tns:getDayTotalSoapIn" />
      <wsdl:output message="tns:getDayTotalSoapOut" />
    </wsdl:operation>
  </wsdl:portType>
  <wsdl:binding name="EOSSoap" type="tns:EOSSoap">
    <soap:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="getClassID">
      <soap:operation soapAction="http://arcmercury.com/websevices/getClassID" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getObject">
      <soap:operation soapAction="http://arcmercury.com/websevices/getObject" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="insertObject">
      <soap:operation soapAction="http://arcmercury.com/websevices/insertObject" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getEmployeebyUser">
      <soap:operation soapAction="http://arcmercury.com/websevices/getEmployeebyUser" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateObject">
      <soap:operation soapAction="http://arcmercury.com/websevices/updateObject" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="deleteObject">
      <soap:operation soapAction="http://arcmercury.com/websevices/deleteObject" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="deleteActivityLog">
      <soap:operation soapAction="http://arcmercury.com/websevices/deleteActivityLog" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="insertToDo">
      <soap:operation soapAction="http://arcmercury.com/websevices/insertToDo" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="insertActivityLog">
      <soap:operation soapAction="http://arcmercury.com/websevices/insertActivityLog" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateActivityLog">
      <soap:operation soapAction="http://arcmercury.com/websevices/updateActivityLog" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateToDo">
      <soap:operation soapAction="http://arcmercury.com/websevices/updateToDo" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="setStatusToDo">
      <soap:operation soapAction="http://arcmercury.com/websevices/setStatusToDo" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllClientsForUser">
      <soap:operation soapAction="http://arcmercury.com/websevices/GetAllClientsForUser" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetInvoiceMethods">
      <soap:operation soapAction="http://arcmercury.com/websevices/GetInvoiceMethods" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetProjectbyID">
      <soap:operation soapAction="http://arcmercury.com/websevices/GetProjectbyID" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetInvoiceType">
      <soap:operation soapAction="http://arcmercury.com/websevices/GetInvoiceType" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetTaskRepeat">
      <soap:operation soapAction="http://arcmercury.com/websevices/GetTaskRepeat" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllProgramsandProjectsForUser">
      <soap:operation soapAction="http://arcmercury.com/websevices/GetAllProgramsandProjectsForUser" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllProgramsForUser">
      <soap:operation soapAction="http://arcmercury.com/websevices/GetAllProgramsForUser" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllProjectsForUser">
      <soap:operation soapAction="http://arcmercury.com/websevices/GetAllProjectsForUser" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllTasksForUser">
      <soap:operation soapAction="http://arcmercury.com/websevices/GetAllTasksForUser" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllToDosForUser">
      <soap:operation soapAction="http://arcmercury.com/websevices/GetAllToDosForUser" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllToDosForUserExt">
      <soap:operation soapAction="http://arcmercury.com/websevices/GetAllToDosForUserExt" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllTasksAndTodosForUser">
      <soap:operation soapAction="http://arcmercury.com/websevices/GetAllTasksAndTodosForUser" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllProgramsandProjectsWithMessage">
      <soap:operation soapAction="http://arcmercury.com/websevices/GetAllProgramsandProjectsWithMessage" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllProjectsForUserWithMessage">
      <soap:operation soapAction="http://arcmercury.com/websevices/GetAllProjectsForUserWithMessage" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetMessagesforProjects">
      <soap:operation soapAction="http://arcmercury.com/websevices/GetMessagesforProjects" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllProgramsandProjectsForUserandClient">
      <soap:operation soapAction="http://arcmercury.com/websevices/GetAllProgramsandProjectsForUserandClient" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllPrgPrjandTD">
      <soap:operation soapAction="http://arcmercury.com/websevices/GetAllPrgPrjandTD" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllProjectsForUserandClient">
      <soap:operation soapAction="http://arcmercury.com/websevices/GetAllProjectsForUserandClient" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllToDoforClient">
      <soap:operation soapAction="http://arcmercury.com/websevices/GetAllToDoforClient" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllTasksForUserandClient">
      <soap:operation soapAction="http://arcmercury.com/websevices/GetAllTasksForUserandClient" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetTodoChangesforTask">
      <soap:operation soapAction="http://arcmercury.com/websevices/GetTodoChangesforTask" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateToDoStatus">
      <soap:operation soapAction="http://arcmercury.com/websevices/updateToDoStatus" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateObjectAttribute">
      <soap:operation soapAction="http://arcmercury.com/websevices/updateObjectAttribute" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getInitials">
      <soap:operation soapAction="http://arcmercury.com/websevices/getInitials" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getTaskListing">
      <soap:operation soapAction="http://arcmercury.com/websevices/getTaskListing" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getEmployeeListing">
      <soap:operation soapAction="http://arcmercury.com/websevices/getEmployeeListing" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getAssignbyProject">
      <soap:operation soapAction="http://arcmercury.com/websevices/getAssignbyProject" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getWorkLog">
      <soap:operation soapAction="http://arcmercury.com/websevices/getWorkLog" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getWorkLogbyID">
      <soap:operation soapAction="http://arcmercury.com/websevices/getWorkLogbyID" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getMonthTotal">
      <soap:operation soapAction="http://arcmercury.com/websevices/getMonthTotal" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getDayTotal">
      <soap:operation soapAction="http://arcmercury.com/websevices/getDayTotal" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:binding name="EOSSoap12" type="tns:EOSSoap">
    <soap12:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="getClassID">
      <soap12:operation soapAction="http://arcmercury.com/websevices/getClassID" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getObject">
      <soap12:operation soapAction="http://arcmercury.com/websevices/getObject" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="insertObject">
      <soap12:operation soapAction="http://arcmercury.com/websevices/insertObject" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getEmployeebyUser">
      <soap12:operation soapAction="http://arcmercury.com/websevices/getEmployeebyUser" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateObject">
      <soap12:operation soapAction="http://arcmercury.com/websevices/updateObject" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="deleteObject">
      <soap12:operation soapAction="http://arcmercury.com/websevices/deleteObject" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="deleteActivityLog">
      <soap12:operation soapAction="http://arcmercury.com/websevices/deleteActivityLog" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="insertToDo">
      <soap12:operation soapAction="http://arcmercury.com/websevices/insertToDo" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="insertActivityLog">
      <soap12:operation soapAction="http://arcmercury.com/websevices/insertActivityLog" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateActivityLog">
      <soap12:operation soapAction="http://arcmercury.com/websevices/updateActivityLog" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateToDo">
      <soap12:operation soapAction="http://arcmercury.com/websevices/updateToDo" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="setStatusToDo">
      <soap12:operation soapAction="http://arcmercury.com/websevices/setStatusToDo" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllClientsForUser">
      <soap12:operation soapAction="http://arcmercury.com/websevices/GetAllClientsForUser" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetInvoiceMethods">
      <soap12:operation soapAction="http://arcmercury.com/websevices/GetInvoiceMethods" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetProjectbyID">
      <soap12:operation soapAction="http://arcmercury.com/websevices/GetProjectbyID" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetInvoiceType">
      <soap12:operation soapAction="http://arcmercury.com/websevices/GetInvoiceType" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetTaskRepeat">
      <soap12:operation soapAction="http://arcmercury.com/websevices/GetTaskRepeat" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllProgramsandProjectsForUser">
      <soap12:operation soapAction="http://arcmercury.com/websevices/GetAllProgramsandProjectsForUser" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllProgramsForUser">
      <soap12:operation soapAction="http://arcmercury.com/websevices/GetAllProgramsForUser" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllProjectsForUser">
      <soap12:operation soapAction="http://arcmercury.com/websevices/GetAllProjectsForUser" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllTasksForUser">
      <soap12:operation soapAction="http://arcmercury.com/websevices/GetAllTasksForUser" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllToDosForUser">
      <soap12:operation soapAction="http://arcmercury.com/websevices/GetAllToDosForUser" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllToDosForUserExt">
      <soap12:operation soapAction="http://arcmercury.com/websevices/GetAllToDosForUserExt" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllTasksAndTodosForUser">
      <soap12:operation soapAction="http://arcmercury.com/websevices/GetAllTasksAndTodosForUser" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllProgramsandProjectsWithMessage">
      <soap12:operation soapAction="http://arcmercury.com/websevices/GetAllProgramsandProjectsWithMessage" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllProjectsForUserWithMessage">
      <soap12:operation soapAction="http://arcmercury.com/websevices/GetAllProjectsForUserWithMessage" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetMessagesforProjects">
      <soap12:operation soapAction="http://arcmercury.com/websevices/GetMessagesforProjects" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllProgramsandProjectsForUserandClient">
      <soap12:operation soapAction="http://arcmercury.com/websevices/GetAllProgramsandProjectsForUserandClient" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllPrgPrjandTD">
      <soap12:operation soapAction="http://arcmercury.com/websevices/GetAllPrgPrjandTD" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllProjectsForUserandClient">
      <soap12:operation soapAction="http://arcmercury.com/websevices/GetAllProjectsForUserandClient" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllToDoforClient">
      <soap12:operation soapAction="http://arcmercury.com/websevices/GetAllToDoforClient" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllTasksForUserandClient">
      <soap12:operation soapAction="http://arcmercury.com/websevices/GetAllTasksForUserandClient" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetTodoChangesforTask">
      <soap12:operation soapAction="http://arcmercury.com/websevices/GetTodoChangesforTask" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateToDoStatus">
      <soap12:operation soapAction="http://arcmercury.com/websevices/updateToDoStatus" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateObjectAttribute">
      <soap12:operation soapAction="http://arcmercury.com/websevices/updateObjectAttribute" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getInitials">
      <soap12:operation soapAction="http://arcmercury.com/websevices/getInitials" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getTaskListing">
      <soap12:operation soapAction="http://arcmercury.com/websevices/getTaskListing" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getEmployeeListing">
      <soap12:operation soapAction="http://arcmercury.com/websevices/getEmployeeListing" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getAssignbyProject">
      <soap12:operation soapAction="http://arcmercury.com/websevices/getAssignbyProject" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getWorkLog">
      <soap12:operation soapAction="http://arcmercury.com/websevices/getWorkLog" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getWorkLogbyID">
      <soap12:operation soapAction="http://arcmercury.com/websevices/getWorkLogbyID" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getMonthTotal">
      <soap12:operation soapAction="http://arcmercury.com/websevices/getMonthTotal" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getDayTotal">
      <soap12:operation soapAction="http://arcmercury.com/websevices/getDayTotal" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:service name="EOS">
    <wsdl:port name="EOSSoap" binding="tns:EOSSoap">
      <soap:address location="http://localhost:7826/EOS.asmx" />
    </wsdl:port>
    <wsdl:port name="EOSSoap12" binding="tns:EOSSoap12">
      <soap12:address location="http://localhost:7826/EOS.asmx" />
    </wsdl:port>
  </wsdl:service>
</wsdl:definitions>

package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetAllProjectsForUserandClientResult" type="{http://arcmercury.com/websevices}ArrayOfBaseClassExt" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAllProjectsForUserandClientResult"
})
@XmlRootElement(name = "GetAllProjectsForUserandClientResponse")
public class GetAllProjectsForUserandClientResponse {

    @XmlElement(name = "GetAllProjectsForUserandClientResult")
    protected ArrayOfBaseClassExt getAllProjectsForUserandClientResult;

    /**
     * Gets the value of the getAllProjectsForUserandClientResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfBaseClassExt }
     *     
     */
    public ArrayOfBaseClassExt getGetAllProjectsForUserandClientResult() {
        return getAllProjectsForUserandClientResult;
    }

    /**
     * Sets the value of the getAllProjectsForUserandClientResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfBaseClassExt }
     *     
     */
    public void setGetAllProjectsForUserandClientResult(ArrayOfBaseClassExt value) {
        this.getAllProjectsForUserandClientResult = value;
    }

}


package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetProjectbyIDResult" type="{http://arcmercury.com/websevices}Project" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getProjectbyIDResult"
})
@XmlRootElement(name = "GetProjectbyIDResponse")
public class GetProjectbyIDResponse {

    @XmlElement(name = "GetProjectbyIDResult")
    protected Project getProjectbyIDResult;

    /**
     * Gets the value of the getProjectbyIDResult property.
     * 
     * @return
     *     possible object is
     *     {@link Project }
     *     
     */
    public Project getGetProjectbyIDResult() {
        return getProjectbyIDResult;
    }

    /**
     * Sets the value of the getProjectbyIDResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Project }
     *     
     */
    public void setGetProjectbyIDResult(Project value) {
        this.getProjectbyIDResult = value;
    }

}


package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getWorkLogResult" type="{http://arcmercury.com/websevices}ArrayOfWorkLog" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getWorkLogResult"
})
@XmlRootElement(name = "getWorkLogResponse")
public class GetWorkLogResponse {

    protected ArrayOfWorkLog getWorkLogResult;

    /**
     * Gets the value of the getWorkLogResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWorkLog }
     *     
     */
    public ArrayOfWorkLog getGetWorkLogResult() {
        return getWorkLogResult;
    }

    /**
     * Sets the value of the getWorkLogResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWorkLog }
     *     
     */
    public void setGetWorkLogResult(ArrayOfWorkLog value) {
        this.getWorkLogResult = value;
    }

}


package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for Project complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Project">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Message" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Date" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MessageCount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Initials" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClientID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Important" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoiceMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlatFee" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Hourly" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BudgetedHours" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Occurence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AutomaticInvoice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WorkTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BilledTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Budget" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BilledAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Billed" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Project", propOrder = {
    "id",
    "name",
    "message",
    "date",
    "messageCount",
    "initials",
    "type",
    "clientID",
    "important",
    "description",
    "status",
    "invoiceMethod",
    "flatFee",
    "hourly",
    "budgetedHours",
    "occurence",
    "automaticInvoice",
    "workTime",
    "billedTime",
    "budget",
    "billedAmount",
    "billed"
})
public class Project {

    @XmlElement(name = "ID")
    protected int id;
    @XmlElement(name = "Name")
    protected String name;
    @XmlElement(name = "Message")
    protected String message;
    @XmlElement(name = "Date")
    protected String date;
    @XmlElement(name = "MessageCount")
    protected int messageCount;
    @XmlElement(name = "Initials")
    protected String initials;
    @XmlElement(name = "Type")
    protected String type;
    @XmlElement(name = "ClientID")
    protected int clientID;
    @XmlElement(name = "Important")
    protected boolean important;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "InvoiceMethod")
    protected String invoiceMethod;
    @XmlElement(name = "FlatFee")
    protected String flatFee;
    @XmlElement(name = "Hourly")
    protected String hourly;
    @XmlElement(name = "BudgetedHours")
    protected String budgetedHours;
    @XmlElement(name = "Occurence")
    protected String occurence;
    @XmlElement(name = "AutomaticInvoice")
    protected String automaticInvoice;
    @XmlElement(name = "WorkTime")
    protected String workTime;
    @XmlElement(name = "BilledTime")
    protected String billedTime;
    @XmlElement(name = "Budget")
    protected String budget;
    @XmlElement(name = "BilledAmount")
    protected String billedAmount;
    @XmlElement(name = "Billed")
    protected boolean billed;
    protected List<Task> invoicingTasks = new ArrayList<Task>();
    public List<Task> getInvoicingTasks() {
        return invoicingTasks;
    }

    public void setInvoicingTasks(List<Task> invoicingTasks) {
        this.invoicingTasks = invoicingTasks;
    }
    /**
     * Gets the value of the id property.
     * 
     */
    public int getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setID(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Gets the value of the date property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDate() {
        return date;
    }

    /**
     * Sets the value of the date property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDate(String value) {
        this.date = value;
    }

    /**
     * Gets the value of the messageCount property.
     * 
     */
    public int getMessageCount() {
        return messageCount;
    }

    /**
     * Sets the value of the messageCount property.
     * 
     */
    public void setMessageCount(int value) {
        this.messageCount = value;
    }

    /**
     * Gets the value of the initials property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInitials() {
        return initials;
    }

    /**
     * Sets the value of the initials property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInitials(String value) {
        this.initials = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the clientID property.
     * 
     */
    public int getClientID() {
        return clientID;
    }

    /**
     * Sets the value of the clientID property.
     * 
     */
    public void setClientID(int value) {
        this.clientID = value;
    }

    /**
     * Gets the value of the important property.
     * 
     */
    public boolean isImportant() {
        return important;
    }

    /**
     * Sets the value of the important property.
     * 
     */
    public void setImportant(boolean value) {
        this.important = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the invoiceMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceMethod() {
        return invoiceMethod;
    }

    /**
     * Sets the value of the invoiceMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceMethod(String value) {
        this.invoiceMethod = value;
    }

    /**
     * Gets the value of the flatFee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlatFee() {
        return flatFee;
    }

    /**
     * Sets the value of the flatFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlatFee(String value) {
        this.flatFee = value;
    }

    /**
     * Gets the value of the hourly property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHourly() {
        return hourly;
    }

    /**
     * Sets the value of the hourly property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHourly(String value) {
        this.hourly = value;
    }

    /**
     * Gets the value of the budgetedHours property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBudgetedHours() {
        return budgetedHours;
    }

    /**
     * Sets the value of the budgetedHours property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBudgetedHours(String value) {
        this.budgetedHours = value;
    }

    /**
     * Gets the value of the occurence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOccurence() {
        return occurence;
    }

    /**
     * Sets the value of the occurence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOccurence(String value) {
        this.occurence = value;
    }

    /**
     * Gets the value of the automaticInvoice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutomaticInvoice() {
        return automaticInvoice;
    }

    /**
     * Sets the value of the automaticInvoice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutomaticInvoice(String value) {
        this.automaticInvoice = value;
    }

    /**
     * Gets the value of the workTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkTime() {
        return workTime;
    }

    /**
     * Sets the value of the workTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkTime(String value) {
        this.workTime = value;
    }

    /**
     * Gets the value of the billedTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBilledTime() {
        return billedTime;
    }

    /**
     * Sets the value of the billedTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBilledTime(String value) {
        this.billedTime = value;
    }

    /**
     * Gets the value of the budget property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBudget() {
        return budget;
    }

    /**
     * Sets the value of the budget property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBudget(String value) {
        this.budget = value;
    }

    /**
     * Gets the value of the billedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBilledAmount() {
        return billedAmount;
    }

    /**
     * Sets the value of the billedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBilledAmount(String value) {
        this.billedAmount = value;
    }

    /**
     * Gets the value of the billed property.
     * 
     */
    public boolean isBilled() {
        return billed;
    }

    /**
     * Sets the value of the billed property.
     * 
     */
    public void setBilled(boolean value) {
        this.billed = value;
    }

}

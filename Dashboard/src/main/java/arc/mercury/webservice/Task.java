
package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for Task complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Task">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TaskName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompensationMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlatFee" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Hourly" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BudgetedHours" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EstimatedHours" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmpPrimary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmpSecondary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmpTertiary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="isBillable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DueDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Archived" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Repeat" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RepeatOption" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Owner" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RepeatID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ownerID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="WorkTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoiceAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BudgetAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Task", propOrder = {
    "id",
    "taskName",
    "description",
    "status",
    "compensationMethod",
    "flatFee",
    "hourly",
    "budgetedHours",
    "estimatedHours",
    "empPrimary",
    "empSecondary",
    "empTertiary",
    "isBillable",
    "dueDate",
    "archived",
    "repeat",
    "startDate",
    "endDate",
    "repeatOption",
    "owner",
    "repeatID",
    "ownerID",
    "workTime",
    "invoiceAmount",
    "budgetAmount"
})
public class Task {

    @XmlElement(name = "ID")
    protected int id;
    @XmlElement(name = "TaskName")
    protected String taskName;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "CompensationMethod")
    protected String compensationMethod;
    @XmlElement(name = "FlatFee")
    protected String flatFee;
    @XmlElement(name = "Hourly")
    protected String hourly;
    @XmlElement(name = "BudgetedHours")
    protected String budgetedHours;
    @XmlElement(name = "EstimatedHours")
    protected String estimatedHours;
    @XmlElement(name = "EmpPrimary")
    protected String empPrimary;
    @XmlElement(name = "EmpSecondary")
    protected String empSecondary;
    @XmlElement(name = "EmpTertiary")
    protected String empTertiary;
    protected String isBillable;
    @XmlElement(name = "DueDate")
    protected String dueDate;
    @XmlElement(name = "Archived")
    protected boolean archived;
    @XmlElement(name = "Repeat")
    protected boolean repeat;
    @XmlElement(name = "StartDate")
    protected String startDate;
    @XmlElement(name = "EndDate")
    protected String endDate;
    @XmlElement(name = "RepeatOption")
    protected String repeatOption;
    @XmlElement(name = "Owner")
    protected String owner;
    @XmlElement(name = "RepeatID")
    protected int repeatID;
    protected int ownerID;
    @XmlElement(name = "WorkTime")
    protected String workTime;
    @XmlElement(name = "InvoiceAmount")
    protected String invoiceAmount;
    @XmlElement(name = "BudgetAmount")
    protected String budgetAmount;

    protected List<ActivityLog> invoicingActivityLog = new ArrayList<ActivityLog>();

    public List<ActivityLog> getInvoicingActivityLog() {
        return invoicingActivityLog;
    }

    public void setInvoicingActivityLog(List<ActivityLog> invoicingActivityLog) {
        this.invoicingActivityLog = invoicingActivityLog;
    }

    /**
     * Gets the value of the id property.
     * 
     */
    public int getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setID(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the taskName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaskName() {
        return taskName;
    }

    /**
     * Sets the value of the taskName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaskName(String value) {
        this.taskName = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the compensationMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompensationMethod() {
        return compensationMethod;
    }

    /**
     * Sets the value of the compensationMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompensationMethod(String value) {
        this.compensationMethod = value;
    }

    /**
     * Gets the value of the flatFee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlatFee() {
        return flatFee;
    }

    /**
     * Sets the value of the flatFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlatFee(String value) {
        this.flatFee = value;
    }

    /**
     * Gets the value of the hourly property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHourly() {
        return hourly;
    }

    /**
     * Sets the value of the hourly property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHourly(String value) {
        this.hourly = value;
    }

    /**
     * Gets the value of the budgetedHours property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBudgetedHours() {
        return budgetedHours;
    }

    /**
     * Sets the value of the budgetedHours property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBudgetedHours(String value) {
        this.budgetedHours = value;
    }

    /**
     * Gets the value of the estimatedHours property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstimatedHours() {
        return estimatedHours;
    }

    /**
     * Sets the value of the estimatedHours property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstimatedHours(String value) {
        this.estimatedHours = value;
    }

    /**
     * Gets the value of the empPrimary property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmpPrimary() {
        return empPrimary;
    }

    /**
     * Sets the value of the empPrimary property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmpPrimary(String value) {
        this.empPrimary = value;
    }

    /**
     * Gets the value of the empSecondary property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmpSecondary() {
        return empSecondary;
    }

    /**
     * Sets the value of the empSecondary property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmpSecondary(String value) {
        this.empSecondary = value;
    }

    /**
     * Gets the value of the empTertiary property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmpTertiary() {
        return empTertiary;
    }

    /**
     * Sets the value of the empTertiary property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmpTertiary(String value) {
        this.empTertiary = value;
    }

    /**
     * Gets the value of the isBillable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsBillable() {
        return isBillable;
    }

    /**
     * Sets the value of the isBillable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsBillable(String value) {
        this.isBillable = value;
    }

    /**
     * Gets the value of the dueDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDueDate() {
        return dueDate;
    }

    /**
     * Sets the value of the dueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDueDate(String value) {
        this.dueDate = value;
    }

    /**
     * Gets the value of the archived property.
     * 
     */
    public boolean isArchived() {
        return archived;
    }

    /**
     * Sets the value of the archived property.
     * 
     */
    public void setArchived(boolean value) {
        this.archived = value;
    }

    /**
     * Gets the value of the repeat property.
     * 
     */
    public boolean isRepeat() {
        return repeat;
    }

    /**
     * Sets the value of the repeat property.
     * 
     */
    public void setRepeat(boolean value) {
        this.repeat = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartDate(String value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndDate(String value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the repeatOption property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepeatOption() {
        return repeatOption;
    }

    /**
     * Sets the value of the repeatOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepeatOption(String value) {
        this.repeatOption = value;
    }

    /**
     * Gets the value of the owner property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Sets the value of the owner property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwner(String value) {
        this.owner = value;
    }

    /**
     * Gets the value of the repeatID property.
     * 
     */
    public int getRepeatID() {
        return repeatID;
    }

    /**
     * Sets the value of the repeatID property.
     * 
     */
    public void setRepeatID(int value) {
        this.repeatID = value;
    }

    /**
     * Gets the value of the ownerID property.
     * 
     */
    public int getOwnerID() {
        return ownerID;
    }

    /**
     * Sets the value of the ownerID property.
     * 
     */
    public void setOwnerID(int value) {
        this.ownerID = value;
    }

    /**
     * Gets the value of the workTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkTime() {
        return workTime;
    }

    /**
     * Sets the value of the workTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkTime(String value) {
        this.workTime = value;
    }

    /**
     * Gets the value of the invoiceAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceAmount() {
        return invoiceAmount;
    }

    /**
     * Sets the value of the invoiceAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceAmount(String value) {
        this.invoiceAmount = value;
    }

    /**
     * Gets the value of the budgetAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBudgetAmount() {
        return budgetAmount;
    }

    /**
     * Sets the value of the budgetAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBudgetAmount(String value) {
        this.budgetAmount = value;
    }

}

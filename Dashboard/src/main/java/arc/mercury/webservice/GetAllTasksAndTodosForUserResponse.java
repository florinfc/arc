
package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetAllTasksAndTodosForUserResult" type="{http://arcmercury.com/websevices}ArrayOfBaseClass" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAllTasksAndTodosForUserResult"
})
@XmlRootElement(name = "GetAllTasksAndTodosForUserResponse")
public class GetAllTasksAndTodosForUserResponse {

    @XmlElement(name = "GetAllTasksAndTodosForUserResult")
    protected ArrayOfBaseClass getAllTasksAndTodosForUserResult;

    /**
     * Gets the value of the getAllTasksAndTodosForUserResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfBaseClass }
     *     
     */
    public ArrayOfBaseClass getGetAllTasksAndTodosForUserResult() {
        return getAllTasksAndTodosForUserResult;
    }

    /**
     * Sets the value of the getAllTasksAndTodosForUserResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfBaseClass }
     *     
     */
    public void setGetAllTasksAndTodosForUserResult(ArrayOfBaseClass value) {
        this.getAllTasksAndTodosForUserResult = value;
    }

}

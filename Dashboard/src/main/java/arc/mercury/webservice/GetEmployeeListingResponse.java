
package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getEmployeeListingResult" type="{http://arcmercury.com/websevices}ArrayOfBaseClass" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getEmployeeListingResult"
})
@XmlRootElement(name = "getEmployeeListingResponse")
public class GetEmployeeListingResponse {

    protected ArrayOfBaseClass getEmployeeListingResult;

    /**
     * Gets the value of the getEmployeeListingResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfBaseClass }
     *     
     */
    public ArrayOfBaseClass getGetEmployeeListingResult() {
        return getEmployeeListingResult;
    }

    /**
     * Sets the value of the getEmployeeListingResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfBaseClass }
     *     
     */
    public void setGetEmployeeListingResult(ArrayOfBaseClass value) {
        this.getEmployeeListingResult = value;
    }

}

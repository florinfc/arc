
package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="updateActivityLogResult" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updateActivityLogResult"
})
@XmlRootElement(name = "updateActivityLogResponse")
public class UpdateActivityLogResponse {

    protected int updateActivityLogResult;

    /**
     * Gets the value of the updateActivityLogResult property.
     * 
     */
    public int getUpdateActivityLogResult() {
        return updateActivityLogResult;
    }

    /**
     * Sets the value of the updateActivityLogResult property.
     * 
     */
    public void setUpdateActivityLogResult(int value) {
        this.updateActivityLogResult = value;
    }

}

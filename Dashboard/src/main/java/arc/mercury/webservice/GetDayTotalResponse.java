
package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getDayTotalResult" type="{http://arcmercury.com/websevices}ArrayOfHours" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getDayTotalResult"
})
@XmlRootElement(name = "getDayTotalResponse")
public class GetDayTotalResponse {

    protected ArrayOfHours getDayTotalResult;

    /**
     * Gets the value of the getDayTotalResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfHours }
     *     
     */
    public ArrayOfHours getGetDayTotalResult() {
        return getDayTotalResult;
    }

    /**
     * Sets the value of the getDayTotalResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfHours }
     *     
     */
    public void setGetDayTotalResult(ArrayOfHours value) {
        this.getDayTotalResult = value;
    }

}

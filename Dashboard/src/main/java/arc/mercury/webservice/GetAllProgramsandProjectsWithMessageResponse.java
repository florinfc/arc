
package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetAllProgramsandProjectsWithMessageResult" type="{http://arcmercury.com/websevices}ArrayOfProject" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAllProgramsandProjectsWithMessageResult"
})
@XmlRootElement(name = "GetAllProgramsandProjectsWithMessageResponse")
public class GetAllProgramsandProjectsWithMessageResponse {

    @XmlElement(name = "GetAllProgramsandProjectsWithMessageResult")
    protected ArrayOfProject getAllProgramsandProjectsWithMessageResult;

    /**
     * Gets the value of the getAllProgramsandProjectsWithMessageResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfProject }
     *     
     */
    public ArrayOfProject getGetAllProgramsandProjectsWithMessageResult() {
        return getAllProgramsandProjectsWithMessageResult;
    }

    /**
     * Sets the value of the getAllProgramsandProjectsWithMessageResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfProject }
     *     
     */
    public void setGetAllProgramsandProjectsWithMessageResult(ArrayOfProject value) {
        this.getAllProgramsandProjectsWithMessageResult = value;
    }

}

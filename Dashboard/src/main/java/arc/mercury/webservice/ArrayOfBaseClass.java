
package arc.mercury.webservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfBaseClass complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfBaseClass">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BaseClass" type="{http://arcmercury.com/websevices}BaseClass" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfBaseClass", propOrder = {
    "baseClass"
})
public class ArrayOfBaseClass {

    @XmlElement(name = "BaseClass", nillable = true)
    protected List<BaseClass> baseClass;

    /**
     * Gets the value of the baseClass property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the baseClass property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBaseClass().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BaseClass }
     * 
     * 
     */
    public List<BaseClass> getBaseClass() {
        if (baseClass == null) {
            baseClass = new ArrayList<BaseClass>();
        }
        return this.baseClass;
    }

}

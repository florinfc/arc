
package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getProjectInvoicingResult" type="{http://arcmercury.com/websevices}ArrayOfProject" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getProjectInvoicingResult"
})
@XmlRootElement(name = "getProjectInvoicingResponse")
public class GetProjectInvoicingResponse {

    protected ArrayOfProject getProjectInvoicingResult;

    /**
     * Gets the value of the getProjectInvoicingResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfProject }
     *     
     */
    public ArrayOfProject getGetProjectInvoicingResult() {
        return getProjectInvoicingResult;
    }

    /**
     * Sets the value of the getProjectInvoicingResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfProject }
     *     
     */
    public void setGetProjectInvoicingResult(ArrayOfProject value) {
        this.getProjectInvoicingResult = value;
    }

}


package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetAllClientsForUserResult" type="{http://arcmercury.com/websevices}ArrayOfClient" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAllClientsForUserResult"
})
@XmlRootElement(name = "GetAllClientsForUserResponse")
public class GetAllClientsForUserResponse {

    @XmlElement(name = "GetAllClientsForUserResult")
    protected ArrayOfClient getAllClientsForUserResult;

    /**
     * Gets the value of the getAllClientsForUserResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfClient }
     *     
     */
    public ArrayOfClient getGetAllClientsForUserResult() {
        return getAllClientsForUserResult;
    }

    /**
     * Sets the value of the getAllClientsForUserResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfClient }
     *     
     */
    public void setGetAllClientsForUserResult(ArrayOfClient value) {
        this.getAllClientsForUserResult = value;
    }

}

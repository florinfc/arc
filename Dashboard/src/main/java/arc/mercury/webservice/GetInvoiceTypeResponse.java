
package arc.mercury.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetInvoiceTypeResult" type="{http://arcmercury.com/websevices}ArrayOfBaseClass" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getInvoiceTypeResult"
})
@XmlRootElement(name = "GetInvoiceTypeResponse")
public class GetInvoiceTypeResponse {

    @XmlElement(name = "GetInvoiceTypeResult")
    protected ArrayOfBaseClass getInvoiceTypeResult;

    /**
     * Gets the value of the getInvoiceTypeResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfBaseClass }
     *     
     */
    public ArrayOfBaseClass getGetInvoiceTypeResult() {
        return getInvoiceTypeResult;
    }

    /**
     * Sets the value of the getInvoiceTypeResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfBaseClass }
     *     
     */
    public void setGetInvoiceTypeResult(ArrayOfBaseClass value) {
        this.getInvoiceTypeResult = value;
    }

}

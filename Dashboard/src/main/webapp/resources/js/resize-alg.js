function showHint(obj) {
	obj.parent().children('.hint').toggle();
}

function getWidth(percentage) {
	return Math.floor(($(window).width() - 1) * percentage/100);
}

function getHeight(percentage) {
	return Math.floor(($(window).height() - 1) * percentage/100);
}

function rescaleBody() {
	
	$('body').css({
		height: getHeight(100)
	});
	
	$('#content-nomenu').css({
		height: getHeight(66)
	});

    $('#content-nomenu-noheader').css({
        height: getHeight(73)
    });

    $('#content-nomenu-noheader .question-table-p1').css({
        height: getHeight(69)
    });

	$('#header-left').css({
		width: getWidth(80)
	});
	
	$('#header-right').css({
		width: getWidth(20)
	});
	
	$('#header-right a').vAlign();
	
	$('#menu').css({
		'line-height': getHeight(6)+'px'
	});
	
	$('#special-offer-bar').css({
		height: getHeight(5),
		'line-height': getHeight(5)+'px'
	});
	
	$('#content').css({
		height: getHeight(72)
	});
	
	$('#content-full, .mainbar, .sidebar').css({
		height: getHeight(87)
	});
	
	$('#incwiz-progress-bar').css({
		width: getWidth(30)
	});
	
}

function initScroll() {
    $(".tse-scrollable").each(function(){
        $(this).TrackpadScrollEmulator();	
    });
}

function updateScroll() {
    $(".tse-scrollable").each(function(){
    	$(this).TrackpadScrollEmulator('recalculate');
	});
}

function rescaleText() {
	defaultWindowRatio = 1.41;
	windowRatio = $(window).width()/$(window).height();
	if (windowRatio < defaultWindowRatio) {
		value = 1 + ($(window).height() - 800)/2000;
		$('body, html').css('font-size',value + 'em');
	}
	if (windowRatio < 2) {
		value = 1 + ($(window).width() - 1200)/2000;
		$('body, html').css('font-size',value + 'em');
	}

}

jQuery(document).ready(function($) {
	
	$ = jQuery;
	
	initScroll();
	
	// MAKE ALL TEXT INPUT FIELDS AUTOSELECT TEXT  
	$("input[type=text]").click(function() {
		$(this).select();
	});
	
	// ACTIVATE ANY TOOLTIPS WE MIGHT HAVE
    $('body').tooltip({
        selector: "*[data-toggle=tooltip]"
    });
    
    // ANIMATE LOGIN BUTTONS
    $('.login-button').each (function(){
    	
    	$(this).on('mouseenter', function() {
    		
    		$(this).stop();
    		$(this).animate({
    			"width": "11em"
    		});
    		
    	}).on('mouseleave', function() {
    		
    		$(this).stop();
    		$(this).animate({
    			"width": "2.5em"
    		});
    		
    	});
    	
    });
	
	$(window).trigger('resize');
	$(window).load(function(){
		rescaleBody();
		rescaleText();
		setUpProgressBars();
		updateScroll();		
	});
  
});

jQuery(window).bind('resize', function(){
	
	/* THIS GETS CALLED ON EVERY WINDOW RESIZE */
	rescaleBody();
	rescaleText();
	setUpProgressBars();
	updateScroll();

});

// VERTICAL ALIGN FUNCTION
(function ($) {
    $.fn.vAlign = function() {
        return this.each(function(i){
            var ah = $(this).height();
            var ph = $(this).parent().height();
            var mh = Math.ceil((ph-ah) / 2);
            $(this).css('margin-top', mh);
        });
    };
})(jQuery);
var modalWindow;
var modalBackground;
var modalStatus = false;

function showModal(html,height,callback) {
    modalBackground.css({
        position: 'absolute',
        height: $(window).height(),
        width: $(window).width(),
        'z-index': 10000
    });
    modalBackground.fadeIn(
        function() {
            modalBackground.click( function() {
                hideModal();
            });
        }
    );
    modalWindow.append('<img id="modalClose" src="/dashboard/resources/images/modal_close_icon.png" alt="close" title="close" />');
    modalWindow.append(html);
    modalWindow.find('#modalClose').css({
        position: 'absolute',
        right: '-10px',
        top: '-10px'
    }).click(function(){
        hideModal();
    }).hover(function(){
        $(this).css({
            cursor: 'pointer'
        });
    });
    modalWindow.css({
        position: 'absolute',
        top: height ? $(window).height()/4 : ($(window).height() - modalWindow.outerHeight())/2 - 50,
        left: $(window).width()/4,
        width: $(window).width()/2,
        'z-index': 10001
    });
    modalWindow.show();
    if (callback) {
        callback();
    }
    modalWindow.fadeIn();
    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            hideModal();
        }
    });
    $('#you-dont-need-to-see-my-identification').click(function(e){
    	hideModal();
    });
    modalStatus = true;
}

function repositionModal() {
    modalBackground.css({
        position: 'absolute',
        height: $(window).height(),
        width: $(window).width(),
        'z-index': 10000
    });
    modalWindow.css({
        position: 'absolute',
        top: $(window).height()/4,
        left: $(window).width()/4,
        width: $(window).width()/2,
        'z-index': 10001
    });
}

function hideModal(timer) {
    if (timer) {
        modalInterval = setTimeout(hideModal, timer);
    } else {
        modalBackground.fadeOut();
        modalWindow.fadeOut(function(){
            modalWindow.html('');
            modalStatus = false;
        });
        $(document).ready(function(){
            $("body").removeAttr("keyup");
        });
    }
}

$(window).on('resize', function(){
    if (modalStatus) {
        repositionModal();
    }
});
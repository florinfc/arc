var patchJSF = function () {
  jsf.ajax.addOnEvent(function (e) {
    if (e.status === 'success') {
      $("partial-response:first changes:first update[id='javax.faces.ViewState']", e.responseXML).each(function (i, u) {
        // update all forms
        $(document.forms).each(function (i, f) {
          var field = $("input[name='javax.faces.ViewState']", f);
          if (field.length == 0) {
            field = $("<input type=\"hidden\" name=\"javax.faces.ViewState\" />").appendTo(f);
          }
          field.val(u.firstChild.data);
        });
      });
    }
  });
}
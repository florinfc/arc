function setUpProgressBars() {
	
	$('.progress-bar').each( function() {
		
		$(this).children('div').width( $(this).attr('data-percentage')/100 * $(this).width() - 2);
		$(this).children('div').height( $(this).height() );
		
	});
	
}
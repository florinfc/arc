package arc.mercury.webservice;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebServiceException;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>EOSTest</code> contains tests for the class <code>{@link EOS}</code>.
 *
 * @generatedBy CodePro at 6/30/13 1:44 PM
 * @author oiu
 * @version $Revision: 1.0 $
 */
public class EOSTest {
	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 6/30/13 1:44 PM
	 */
	@Before
	public void setUp()
		throws Exception {
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 6/30/13 1:44 PM
	 */
	@After
	public void tearDown()
		throws Exception {
	}
}
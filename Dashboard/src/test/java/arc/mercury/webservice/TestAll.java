package arc.mercury.webservice;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The class <code>TestAll</code> builds a suite that can be used to run all of
 * the tests within its package as well as within any subpackages of its
 * package.
 * 
 * @generatedBy CodePro at 6/30/13 1:45 PM
 * @author oiu
 * @version $Revision: 1.0 $
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ SetUserIDResponseTest.class, InsertToDoTest.class,
		GetAllTasksForUserTest.class, InsertObjectTest.class,
		GetAllToDosForUserResponseTest.class,
		GetAllTasksAndTodosForUserResponseTest.class, AmObjectTest.class,
		UpdateObjectResponseTest.class, InsertToDoResponseTest.class,
		GetAllProjectsForUserResponseTest.class, ArrayOfAmObjectTest.class,
		ObjectFactoryTest.class, SetUserIDTest.class, DeleteObjectTest.class,
		EOSSoapTest.class, GetClassIDResponseTest.class,
		BaseClassExtTest.class, UpdateToDoResponseTest.class,
		GetInvoiceMethodsTest.class, GetAllProjectsForUserTest.class,
		InvoiceMethodTest.class, GetInvoiceMethodsResponseTest.class,
		GetAllProgramsandProjectsForUserTest.class,
		GetAllProgramsForUserTest.class, EOSTest.class,
		GetAllTasksForUserResponseTest.class, ArrayOfBaseClassTest.class,
		GetObjectResponseTest.class, SetStatusToDoTest.class,
		GetAllProgramsandProjectsForUserResponseTest.class,
		SetStatusToDoResponseTest.class, ArrayOfBaseClassExtTest.class,
		BaseClassTest.class, GetAllProgramsForUserResponseTest.class,
		ClientTest.class, GetObjectTest.class, InsertObjectResponseTest.class,
		GetAllTasksAndTodosForUserTest.class,
		GetAllClientsForUserResponseTest.class, ArrayOfClientTest.class,
		GetAllClientsForUserTest.class, GetAllToDosForUserTest.class,
		ArrayOfInvoiceMethodTest.class, GetClassIDTest.class,
		DeleteObjectResponseTest.class, UpdateToDoTest.class,
		UpdateObjectTest.class })
public class TestAll {
}

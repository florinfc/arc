package arc.mercury.webservice;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>GetInvoiceMethodsTest</code> contains tests for the class <code>{@link GetInvoiceMethods}</code>.
 *
 * @generatedBy CodePro at 6/30/13 1:45 PM
 * @author oiu
 * @version $Revision: 1.0 $
 */
public class GetInvoiceMethodsTest {
	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 6/30/13 1:45 PM
	 */
	@Before
	public void setUp()
		throws Exception {
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 6/30/13 1:45 PM
	 */
	@After
	public void tearDown()
		throws Exception {
	}
}
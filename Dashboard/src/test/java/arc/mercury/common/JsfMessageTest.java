package arc.mercury.common;

import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>JsfMessageTest</code> contains tests for the class <code>{@link JsfMessage}</code>.
 *
 * @generatedBy CodePro at 6/30/13 1:42 PM, using the JSF generator
 * @author oiu
 * @version $Revision: 1.0 $
 */
public class JsfMessageTest {
	/**
	 * Initialize a newly create test instance to have the given name.
	 *
	 * @param name the name of the test
	 *
	 * @generatedBy CodePro at 6/30/13 1:42 PM
	 */
	public JsfMessageTest(String name) {
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 6/30/13 1:42 PM
	 */
	@Before
	public void setUp()
		throws Exception {
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 6/30/13 1:42 PM
	 */
	@After
	public void tearDown()
		throws Exception {
	}
}
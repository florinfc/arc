package arc.mercury.common;

import java.text.DateFormat;
import java.util.Date;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>FilesTest</code> contains tests for the class <code>{@link Files}</code>.
 *
 * @generatedBy CodePro at 6/30/13 1:42 PM
 * @author oiu
 * @version $Revision: 1.0 $
 */
public class FilesTest {
	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 6/30/13 1:42 PM
	 */
	@Before
	public void setUp()
		throws Exception {
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 6/30/13 1:42 PM
	 */
	@After
	public void tearDown()
		throws Exception {
	}
}
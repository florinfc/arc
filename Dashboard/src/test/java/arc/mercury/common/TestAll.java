package arc.mercury.common;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The class <code>TestAll</code> builds a suite that can be used to run all of
 * the tests within its package as well as within any subpackages of its
 * package.
 * 
 * @generatedBy CodePro at 6/30/13 1:42 PM
 * @author oiu
 * @version $Revision: 1.0 $
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ FilesTest.class, WorkLogTest.class, ToDoTest.class,
		JsfMessageTest.class, ProjectTest.class })
public class TestAll {
}

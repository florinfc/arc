package arc.mercury;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The class <code>TestAll</code> builds a suite that can be used to run all of
 * the tests within its package as well as within any subpackages of its
 * package.
 * 
 * @generatedBy CodePro at 6/30/13 3:18 PM
 * @author oiu
 * @version $Revision: 1.0 $
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ arc.mercury.common.TestAll.class,
		arc.mercury.model.TestAll.class, arc.mercury.view.TestAll.class,
		arc.mercury.webservice.TestAll.class })
public class TestAll {
}

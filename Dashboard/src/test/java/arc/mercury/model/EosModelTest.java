package arc.mercury.model;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.ws.WebServiceException;
import arc.mercury.webservice.Client;
import org.junit.*;
import arc.mercury.webservice.AmObject;
import arc.mercury.webservice.EOSSoap;
import arc.mercury.webservice.InvoiceMethod;
import static org.junit.Assert.*;

/**
 * The class <code>EosModelTest</code> contains tests for the class <code>{@link EosModel}</code>.
 *
 * @generatedBy CodePro at 6/30/13 1:42 PM, using the JSF generator
 * @author oiu
 * @version $Revision: 1.0 $
 */
public class EosModelTest {
	/**
	 * Initialize a newly create test instance to have the given name.
	 *
	 * @param name the name of the test
	 *
	 * @generatedBy CodePro at 6/30/13 1:42 PM
	 */
	public EosModelTest(String name) {
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 6/30/13 1:42 PM
	 */
	@Before
	public void setUp()
		throws Exception {
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 6/30/13 1:42 PM
	 */
	@After
	public void tearDown()
		throws Exception {
	}
}
package arc.mercury.view;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import arc.mercury.webservice.InvoiceMethod;
import javax.faces.model.SelectItem;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>EosViewTest</code> contains tests for the class <code>{@link EosView}</code>.
 *
 * @generatedBy CodePro at 6/30/13 1:43 PM, using the JSF generator
 * @author oiu
 * @version $Revision: 1.0 $
 */
public class EosViewTest {
	/**
	 * Initialize a newly create test instance to have the given name.
	 *
	 * @param name the name of the test
	 *
	 * @generatedBy CodePro at 6/30/13 1:43 PM
	 */
	public EosViewTest(String name) {
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 6/30/13 3:18 PM
	 */
	@Before
	public void setUp()
		throws Exception {
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 6/30/13 3:18 PM
	 */
	@After
	public void tearDown()
		throws Exception {
	}
}
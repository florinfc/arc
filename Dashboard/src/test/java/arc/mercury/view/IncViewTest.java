package arc.mercury.view;

import org.junit.After;
import org.junit.Before;

import arc.mercury.dash.view.IncView;

/**
 * The class <code>IncViewTest</code> contains tests for the class
 * <code>{@link IncView}</code>.
 * 
 * @generatedBy CodePro at 6/30/13 1:44 PM, using the JSF generator
 * @author oiu
 * @version $Revision: 1.0 $
 */
public class IncViewTest {
	/**
	 * Initialize a newly create test instance to have the given name.
	 * 
	 * @param name
	 *            the name of the test
	 * 
	 * @generatedBy CodePro at 6/30/13 1:44 PM
	 */
	public IncViewTest(String name) {
	}

	/**
	 * Perform pre-test initialization.
	 * 
	 * @throws Exception
	 *             if the initialization fails for some reason
	 * 
	 * @generatedBy CodePro at 6/30/13 1:44 PM
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Perform post-test clean-up.
	 * 
	 * @throws Exception
	 *             if the clean-up fails for some reason
	 * 
	 * @generatedBy CodePro at 6/30/13 1:44 PM
	 */
	@After
	public void tearDown() throws Exception {
	}
}
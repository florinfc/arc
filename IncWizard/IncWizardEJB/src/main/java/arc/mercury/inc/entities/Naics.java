package arc.mercury.inc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import arc.mercury.com.base.BaseClass;
import arc.mercury.com.base.Constants;

@Entity
@Table(name = "TNaics")
@NamedQueries({

		@NamedQuery(name = "Naics.findAll", query = "select item from Naics item order by item.id"),
		@NamedQuery(name = "Naics.findById", query = "select item from Naics item where item.id = :param"),
		@NamedQuery(name = "Naics.findByName", query = "select item from Naics item where item.name = :param"),
		@NamedQuery(name = "Naics.findLikeName", query = "select item from Naics item where lower(item.name) like :param order by item.name")

})
@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class Naics extends BaseClass<Naics> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "name", length = Constants.MAXLEN)
	private String name;

	public Naics() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

package arc.mercury.inc.managers;

import arc.mercury.inc.entities.Restrict;

import java.util.List;

import javax.ejb.Local;
import javax.persistence.EntityManager;

@Local
public interface IRestrictManager {

	public EntityManager getManager();

	public List<Restrict> findAll();

	public Restrict findById(Long id);

	public Restrict findByName(String name);

	public List<Restrict> findLikeName(String name);

	public List<Restrict> findByQuestion(Long id);

}

package arc.mercury.inc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import arc.mercury.com.base.BaseClass;
import arc.mercury.com.base.Constants;

@Entity
@Table(name = "TSS4")
@NamedQueries({

		@NamedQuery(name = "SS4.findAll", query = "select item from SS4 item order by item.id"),
		@NamedQuery(name = "SS4.findById", query = "select item from SS4 item where item.id = :param"),
		@NamedQuery(name = "SS4.findByName", query = "select item from SS4 item where item.name = :param"),
		@NamedQuery(name = "SS4.findLikeName", query = "select item from SS4 item where item.name like :param order by item.name")

})
@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class SS4 extends BaseClass<SS4> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "name", length = Constants.MAXLEN)
	private String name;

	public SS4() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

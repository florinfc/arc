package arc.mercury.inc.managers;

import arc.mercury.inc.entities.Page;

import java.util.List;

import javax.ejb.Local;
import javax.persistence.EntityManager;

@Local
public interface IPageManager {

	public EntityManager getManager();

	public List<Page> findAll();

	public Page findById(Long id);

	public Page findByName(String name);

	public List<Page> findLikeName(String name);

	public List<Page> findByForm(Long id);

}

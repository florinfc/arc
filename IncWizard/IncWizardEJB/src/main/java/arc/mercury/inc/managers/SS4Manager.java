package arc.mercury.inc.managers;

import arc.mercury.inc.entities.SS4;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@SuppressWarnings("unchecked")
public class SS4Manager implements ISS4Manager {

	public SS4Manager() {
	}

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;

	/**
	 * {@inheritDoc}
	 */
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<SS4> findAll() {
		List<SS4> result = null;
		Query query = getManager().createNamedQuery("SS4.findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<SS4>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public SS4 findById(Long id) {
		if (id == null) {
			return null;
		}
		return (SS4) getManager().find(SS4.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	public SS4 findByName(String name) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		SS4 result = null;
		Query query = getManager().createNamedQuery("SS4.findByName");
		query.setParameter("param", name);
		try {
			result = (SS4) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<SS4> findLikeName(String name) {
		if (name == null || name.isEmpty()) {
			return new ArrayList<SS4>(0);
		}
		List<SS4> result = null;
		Query query = getManager().createNamedQuery("SS4.findLikeName");
		query.setParameter("param", "%" + name + "%");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<SS4>(0);
		}
		return result;
	}

}

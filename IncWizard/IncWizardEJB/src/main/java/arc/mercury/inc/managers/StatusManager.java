package arc.mercury.inc.managers;

import arc.mercury.inc.entities.Status;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@SuppressWarnings("unchecked")
public class StatusManager implements IStatusManager {

	public StatusManager() {
	}

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;

	/**
	 * {@inheritDoc}
	 */
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Status> findAll() {
		List<Status> result = null;
		Query query = getManager().createNamedQuery("Status.findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Status>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public Status findById(Long id) {
		if (id == null) {
			return null;
		}
		return (Status) getManager().find(Status.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	public Status findByName(String name) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		Status result = null;
		Query query = getManager().createNamedQuery("Status.findByName");
		query.setParameter("param", name);
		try {
			result = (Status) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Status> findLikeName(String name) {
		if (name == null || name.isEmpty()) {
			return new ArrayList<Status>(0);
		}
		List<Status> result = null;
		Query query = getManager().createNamedQuery("Status.findLikeName");
		query.setParameter("param", "%" + name + "%");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Status>(0);
		}
		return result;
	}

}

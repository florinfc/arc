package arc.mercury.inc.managers;

import arc.mercury.inc.entities.Naics;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@SuppressWarnings("unchecked")
public class NaicsManager implements INaicsManager {

	public NaicsManager() {
	}

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;

	/**
	 * {@inheritDoc}
	 */
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Naics> findAll() {
		List<Naics> result = null;
		Query query = getManager().createNamedQuery("Naics.findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Naics>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public Naics findById(Long id) {
		if (id == null) {
			return null;
		}
		return (Naics) getManager().find(Naics.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	public Naics findByName(String name) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		Naics result = null;
		Query query = getManager().createNamedQuery("Naics.findByName");
		query.setParameter("param", name);
		try {
			result = (Naics) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Naics> findLikeName(String name) {
		if (name == null) {
			return new ArrayList<Naics>(0);
		}
		List<Naics> result = null;
		Query query = getManager().createNamedQuery("Naics.findLikeName");
		query.setParameter("param", "%" + name.toLowerCase() + "%");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Naics>(0);
		}
		return result;
	}

}

package arc.mercury.inc.managers;

import arc.mercury.inc.entities.Form;

import java.util.List;

import javax.ejb.Local;
import javax.persistence.EntityManager;

@Local
public interface IFormManager {

	public EntityManager getManager();

	public List<Form> findAll();

	public Form findById(Long id);

	public Form findByName(String name);

	public List<Form> findLikeName(String name);

	public List<Form> findByState(String code);

	public List<Form> findByStatus(Long id);

}

package arc.mercury.inc.managers;

import arc.mercury.inc.entities.Page;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@SuppressWarnings("unchecked")
public class PageManager implements IPageManager {

	public PageManager() {
	}

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;

	/**
	 * {@inheritDoc}
	 */
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Page> findAll() {
		List<Page> result = null;
		Query query = getManager().createNamedQuery("Page.findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Page>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public Page findById(Long id) {
		if (id == null) {
			return null;
		}
		return (Page) getManager().find(Page.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	public Page findByName(String name) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		Page result = null;
		Query query = getManager().createNamedQuery("Page.findByName");
		query.setParameter("param", name);
		try {
			result = (Page) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Page> findLikeName(String name) {
		if (name == null || name.isEmpty()) {
			return new ArrayList<Page>(0);
		}
		List<Page> result = null;
		Query query = getManager().createNamedQuery("Page.findLikeName");
		query.setParameter("param", "%" + name + "%");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Page>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Page> findByForm(Long id) {
		List<Page> result = null;
		Query query = getManager().createNamedQuery("Page.findByForm");
		query.setParameter("param", id);
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Page>(0);
		}
		return result;
	}

}

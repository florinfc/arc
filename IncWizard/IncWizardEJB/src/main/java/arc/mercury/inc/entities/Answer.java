package arc.mercury.inc.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import arc.mercury.com.base.BaseClass;
import arc.mercury.com.common.AnswerValue;

@Entity
@Table(name = "TAnswer")
@NamedQueries({
		@NamedQuery(name = "Answer.findAll", query = "select item from Answer item order by item.id"),
		@NamedQuery(name = "Answer.findById", query = "select item from Answer item where item.id = :param"),
		@NamedQuery(name = "Answer.findByName", query = "select item from Answer item where item.name = :param"),
		@NamedQuery(name = "Answer.findLikeName", query = "select item from Answer item where item.name like :param order by item.name"),
		@NamedQuery(name = "Answer.findByForm", query = "select item from Answer item where item.userId = :uparam and item.formId = :fparam"),
		@NamedQuery(name = "Answer.findByUser", query = "select item from Answer item where item.userId = :param order by item.id")

})
@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class Answer extends BaseClass<Answer> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	protected Long id;
	@Column(name = "name")
	protected String name;
	@Column(name = "userId")
	private Long userId;
	@Column(name = "formId")
	private Long formId;
	@Column(name = "maxPage")
	private Integer maxPage;
	@Column(name = "pages")
	private char[] pages;
	@Column(name = "javaValue")
	private String value;
	@Transient
	private AnswerValue com;
	@Transient
	private List<Help> help;

	public Answer() {
		super();
	}

	public Answer(Long id, String name, Long uid, AnswerValue value) {
		super();
		this.id = id;
		this.name = name;
		this.userId = uid;
		this.value = value.toJson();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getFormId() {
		return formId;
	}

	public void setFormId(Long formId) {
		this.formId = formId;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}

	public AnswerValue getCom() {
		return com;
	}

	public void setCom(AnswerValue com) {
		this.com = com;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public char[] getPages() {
		return pages;
	}

	public void setPages(char[] pages) {
		this.pages = pages;
	}

	public List<Help> getHelp() {
		return help;
	}

	public void setHelp(List<Help> help) {
		this.help = help;
	}

}

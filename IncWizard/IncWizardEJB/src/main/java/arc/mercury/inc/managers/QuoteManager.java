package arc.mercury.inc.managers;

import arc.mercury.inc.entities.Quote;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@SuppressWarnings("unchecked")
public class QuoteManager implements IQuoteManager {

	public QuoteManager() {
	}

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;

	/**
	 * {@inheritDoc}
	 */
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Quote> findAll() {
		List<Quote> result = null;
		Query query = getManager().createNamedQuery("Quote.findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Quote>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public Quote findById(Long id) {
		if (id == null) {
			return null;
		}
		return (Quote) getManager().find(Quote.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	public Quote findByName(String name) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		Quote result = null;
		Query query = getManager().createNamedQuery("Quote.findByName");
		query.setParameter("param", name);
		try {
			result = (Quote) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Quote> findLikeName(String name) {
		if (name == null || name.isEmpty()) {
			return new ArrayList<Quote>(0);
		}
		List<Quote> result = null;
		Query query = getManager().createNamedQuery("Quote.findLikeName");
		query.setParameter("param", "%" + name + "%");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Quote>(0);
		}
		return result;
	}

}

package arc.mercury.inc.managers;

import arc.mercury.inc.entities.Answer;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@SuppressWarnings("unchecked")
public class AnswerManager implements IAnswerManager {

	public AnswerManager() {
	}

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;

	/**
	 * {@inheritDoc}
	 */
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	public Answer insert(Answer item) {
		if (item == null) {
			return null;
		}
		getManager().persist(item);
		return item;
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Answer item) {
		if (item == null) {
			return;
		}
		item = getManager().find(Answer.class, item.getId());
		if (item != null) {
			getManager().remove(item);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Long id) {
		if (id == null) {
			return;
		}
		Answer item = getManager().getReference(Answer.class, id);
		if (item != null) {
			getManager().remove(item);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public Answer update(Answer item) {
		if (item == null) {
			return null;
		}
		item = getManager().merge(item);
		return item;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Answer> findAll() {
		List<Answer> result = null;
		Query query = getManager().createNamedQuery("Answer.findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Answer>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public Answer findById(Long id) {
		if (id == null) {
			return null;
		}
		return (Answer) getManager().find(Answer.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	public Answer findByName(String name) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		Answer result = null;
		Query query = getManager().createNamedQuery("Answer.findByName");
		query.setParameter("param", name);
		try {
			result = (Answer) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Answer> findLikeName(String name) {
		if (name == null || name.isEmpty()) {
			return new ArrayList<Answer>(0);
		}
		List<Answer> result = null;
		Query query = getManager().createNamedQuery("Answer.findLikeName");
		query.setParameter("param", "%" + name + "%");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Answer>(0);
		}
		return result;
	}

	public Answer findByForm(Long uid, Long qid) {
		Answer result = null;
		Query query = getManager().createNamedQuery("Answer.findByForm");
		query.setParameter("uparam", uid);
		query.setParameter("fparam", qid);
		try {
			result = (Answer) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	public List<Answer> findByUser(Long uid) {
		List<Answer> result = null;
		Query query = getManager().createNamedQuery("Answer.findByUser");
		query.setParameter("param", uid);
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Answer>(0);
		}
		return result;
	}

}

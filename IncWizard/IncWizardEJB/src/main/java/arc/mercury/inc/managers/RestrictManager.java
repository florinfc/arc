package arc.mercury.inc.managers;

import arc.mercury.inc.entities.Restrict;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@SuppressWarnings("unchecked")
public class RestrictManager implements IRestrictManager {

	public RestrictManager() {
	}

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;

	/**
	 * {@inheritDoc}
	 */
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Restrict> findAll() {
		List<Restrict> result = null;
		Query query = getManager().createNamedQuery("Restrict.findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Restrict>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public Restrict findById(Long id) {
		if (id == null) {
			return null;
		}
		return (Restrict) getManager().find(Restrict.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	public Restrict findByName(String name) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		Restrict result = null;
		Query query = getManager().createNamedQuery("Restrict.findByName");
		query.setParameter("param", name);
		try {
			result = (Restrict) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Restrict> findLikeName(String name) {
		if (name == null || name.isEmpty()) {
			return new ArrayList<Restrict>(0);
		}
		List<Restrict> result = null;
		Query query = getManager().createNamedQuery("Restrict.findLikeName");
		query.setParameter("param", "%" + name + "%");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Restrict>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Restrict> findByQuestion(Long id) {
		List<Restrict> result = null;
		Query query = getManager().createNamedQuery("Restrict.findByQuestion");
		query.setParameter("param", id);
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Restrict>(0);
		}
		return result;
	}

}

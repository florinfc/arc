package arc.mercury.inc.managers;

import arc.mercury.inc.entities.Help;

import java.util.List;

import javax.ejb.Local;
import javax.persistence.EntityManager;

@Local
public interface IHelpManager {

	public EntityManager getManager();

	public List<Help> findAll();

	public Help findById(Long id);

	public Help findByName(String name);

	public List<Help> findLikeName(String name);

}

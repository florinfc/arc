package arc.mercury.inc.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;

import arc.mercury.com.base.BaseClass;
import arc.mercury.com.base.Constants;
import arc.mercury.com.entities.IState;

@Entity
@Table(name = "TForm")
@NamedQueries({
		@NamedQuery(name = "Form.findAll", query = "select item from Form item order by item.id"),
		@NamedQuery(name = "Form.findById", query = "select item from Form item where item.id = :param"),
		@NamedQuery(name = "Form.findByName", query = "select item from Form item where item.name = :param"),
		@NamedQuery(name = "Form.findLikeName", query = "select item from Form item where item.name like :param order by item.name"),
		@NamedQuery(name = "Form.findByStatus", query = "select item from Form item where item.status.id = :param")

})
@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class Form extends BaseClass<Form> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "name", length = Constants.MAXLEN)
	private String name;
	@Column(name = "code", length = Constants.MAXLEN)
	private String code;
	@Column(name = "description", length = Constants.MAXLEN)
	private String description;
	@Column(name = "active")
	private Integer active;
	@OneToMany(mappedBy = "form", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection(targetClass = Page.class)
	@Type(type = "java.sql.CUBRIDArray")
	private List<Page> pages;
	@XmlTransient
	@ManyToOne()
	@JoinColumn(name = "statusId")
	private Status status;
	@Transient
	// @ManyToOne
	// @JoinColumn(name = "stateId")
	private IState state;

	public Form() {
		super();
		this.code = "";
		this.description = "";
		this.active = 0;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public List<Page> getPages() {
		if (pages == null) {
			pages = new ArrayList<Page>(0);
		}
		return pages;
	}

	public void setPages(List<Page> list) {
		this.pages = list;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public IState getState() {
		return state;
	}

	public void setState(IState state) {
		this.state = state;
	}

}

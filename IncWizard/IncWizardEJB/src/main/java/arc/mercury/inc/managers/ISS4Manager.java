package arc.mercury.inc.managers;

import arc.mercury.inc.entities.SS4;

import java.util.List;

import javax.ejb.Local;
import javax.persistence.EntityManager;

@Local
public interface ISS4Manager {

	public EntityManager getManager();

	public List<SS4> findAll();

	public SS4 findById(Long id);

	public SS4 findByName(String name);

	public List<SS4> findLikeName(String name);
}

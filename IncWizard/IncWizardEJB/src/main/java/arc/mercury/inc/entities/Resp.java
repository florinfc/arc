package arc.mercury.inc.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;

import arc.mercury.com.base.BaseClass;
import arc.mercury.com.entities.IUser;

@SuppressWarnings("serial")
public class Resp extends BaseClass<Resp> {

	private IUser user;
	@OneToMany(mappedBy = "userId", cascade = { CascadeType.ALL })
	@Fetch(value = FetchMode.SUBSELECT)
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection(targetClass = Answer.class)
	@Type(type = "java.sql.CUBRIDArray")
	private List<Answer> answers;

	public Resp() {
		super();
		user = new IUser();
		answers = new ArrayList<Answer>();
	}

	public Long getId() {
		return user.getId();
	}

	public void setId(Long id) {
		user.setId(id);
	}

	public IUser getUser() {
		return user;
	}

	public void setUser(IUser user) {
		this.user = user;
	}

	public List<Answer> getAnswers() {
		if (answers == null) {
			answers = new ArrayList<Answer>(0);
		}
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

}

package arc.mercury.inc.services;

import arc.mercury.inc.entities.*;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService(targetNamespace = "http://dash.server:8081/")
public interface IIncServices {

	@WebMethod
	public List<Naics> getNaics();

	@WebMethod
	public List<Naics> getNaicsLike(String text);

	@WebMethod
	public Naics getNaicsById(Long id);

	@WebMethod
	public List<SS4> getSS4();

	@WebMethod
	public List<Quote> getQuote();

	@WebMethod
	public List<Help> getHelp();

	@WebMethod
	public List<Form> getForms(String code);

	@WebMethod
	public Form getForm(Long formId);

	@WebMethod
	public void saveForm(Form form);

	@WebMethod
	public void submitForm(Form form);

	@WebMethod
	public List<Answer> getAnswers();

	@WebMethod
	public Answer getAnswer(Long uid, Long qid);

	@WebMethod
	public Answer saveAnswer(Answer answer);

}

package arc.mercury.inc.managers;

import arc.mercury.inc.entities.Question;

import java.util.List;

import javax.ejb.Local;
import javax.persistence.EntityManager;

@Local
public interface IQuestionManager {

	public EntityManager getManager();

	public List<Question> findAll();

	public Question findById(Long id);

	public Question findByName(String name);

	public List<Question> findLikeName(String name);

	public List<Question> findByPage(Long id);
}

package arc.mercury.inc.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;

import arc.mercury.com.base.BaseClass;
import arc.mercury.com.base.Constants;

@Entity
@Table(name = "TQuestion")
@NamedQueries({

		@NamedQuery(name = "Question.findAll", query = "select item from Question item order by item.id"),
		@NamedQuery(name = "Question.findById", query = "select item from Question item where item.id = :param"),
		@NamedQuery(name = "Question.findByName", query = "select item from Question item where item.name = :param"),
		@NamedQuery(name = "Question.findLikeName", query = "select item from Question item where item.name like :param order by item.name"),
		@NamedQuery(name = "Question.findByPage", query = "select item from Question item where item.page.id = :param order by item.id")

})
@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class Question extends BaseClass<Question> {

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Id
	@Column(name = "name", length = Constants.MAXLEN)
	private String name;
	@XmlTransient
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pageId")
	private Page page;
	@Column(name = "ord")
	private Integer ord;
	@Column(name = "label", length = 2 * Constants.MAXLEN)
	private String label;
	@Column(name = "pattern", length = Constants.MAXLEN)
	private String pattern;
	@Column(name = "hint", length = Constants.MAXLEN)
	private String hint;
	@Column(name = "defaultValue", length = Constants.MAXLEN)
	private String defaultValue;
	@OneToMany(mappedBy = "question", cascade = { CascadeType.ALL })
	@Fetch(value = FetchMode.SUBSELECT)
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection(targetClass = Restrict.class)
	@Type(type = "java.sql.CUBRIDArray")
	private List<Restrict> restricts;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getHint() {
		return hint;
	}

	public void setHint(String hint) {
		this.hint = hint;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public List<Restrict> getRestricts() {
		if (restricts == null) {
			restricts = new ArrayList<Restrict>(0);
		}
		return restricts;
	}

	public void setRestricts(List<Restrict> restricts) {
		this.restricts = restricts;
	}

}

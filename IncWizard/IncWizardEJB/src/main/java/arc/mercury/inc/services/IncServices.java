package arc.mercury.inc.services;

import arc.mercury.com.enums.StatusType;
import arc.mercury.inc.entities.*;
import arc.mercury.inc.managers.*;
import org.jboss.ws.api.annotation.WebContext;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Stateless
@Remote(IIncServices.class)
@WebService(targetNamespace = "http://dash.server:8081/")
@WebContext(contextRoot = "/incwiz", urlPattern = "/services")
public class IncServices implements IIncServices {

	Logger log = Logger.getLogger("IncServices");
	@EJB(mappedName = "java:module/NaicsManager")
	private INaicsManager nManager;
	@EJB(mappedName = "java:module/SS4Manager")
	private ISS4Manager ss4Manager;
	@EJB(mappedName = "java:module/QuoteManager")
	private IQuoteManager oManager;
	@EJB(mappedName = "java:module/HelpManager")
	private IHelpManager hManager;
	@EJB(mappedName = "java:module/FormManager")
	private IFormManager fManager;
	@EJB(mappedName = "java:module/PageManager")
	private IPageManager pManager;
	@EJB(mappedName = "java:module/QuestionManager")
	private IQuestionManager qManager;
	@EJB(mappedName = "java:module/AnswerManager")
	private IAnswerManager aManager;
	@EJB(mappedName = "java:module/RestrictManager")
	private IRestrictManager rManager;
	@EJB(mappedName = "java:module/StatusManager")
	private IStatusManager tManager;

	public IncServices() {
		super();
	}

	@Override
	@WebMethod
	public List<Naics> getNaics() {
		List<Naics> result = new ArrayList<Naics>();
		List<Naics> items = nManager.findAll();
		for (Naics item : items) {
			result.add(item);
		}
		return result;
	}

	@Override
	@WebMethod
	public Naics getNaicsById(Long id) {
		Naics result = nManager.findById(id);
		return result;
	}

	@Override
	@WebMethod
	public List<Naics> getNaicsLike(String text) {
		List<Naics> result = new ArrayList<Naics>();
		List<Naics> items = nManager.findLikeName(text);
		for (Naics item : items) {
			result.add(item);
		}
		return result;
	}

	@Override
	@WebMethod
	public List<SS4> getSS4() {
		List<SS4> result = new ArrayList<SS4>();
		List<SS4> items = ss4Manager.findAll();
		for (SS4 item : items) {
			result.add(item);
		}
		return result;
	}

	@Override
	@WebMethod
	public List<Quote> getQuote() {
		List<Quote> result = new ArrayList<Quote>();
		List<Quote> items = oManager.findAll();
		for (Quote item : items) {
			result.add(item);
		}
		return result;
	}

	@Override
	@WebMethod
	public List<Help> getHelp() {
		List<Help> result = new ArrayList<Help>();
		List<Help> items = hManager.findAll();
		for (Help item : items) {
			result.add(item);
		}
		return result;
	}

	@Override
	@WebMethod
	public List<Form> getForms(String code) {
		List<Form> result = new ArrayList<Form>();
		List<Form> items = fManager.findByState(code);
		for (Form item : items) {
			result.add(item);
		}
		return result;
	}

	@Override
	@WebMethod
	public Form getForm(Long formId) {
		Form form = fManager.findById(formId);
		if (form.getPages().isEmpty()) {
			form.setPages(pManager.findByForm(formId));
			for (Page page : form.getPages()) {
				page.setQuestions(qManager.findByPage(page.getId()));
			}
		}
		return form;
	}

	@Override
	@WebMethod
	public void submitForm(Form form) {
		if (form != null) {
			Status status = tManager.findById(StatusType.Submit.getValue());
			form.setStatus(status);
		}
	}

	@Override
	@WebMethod
	public void saveForm(Form form) {
		if (form != null) {
		}
	}

	@Override
	@WebMethod
	public List<Answer> getAnswers() {
		return aManager.findAll();
	}

	@Override
	@WebMethod
	public Answer getAnswer(Long uid, Long fid) {
		return aManager.findByForm(uid, fid);
	}

	@Override
	@WebMethod
	public Answer saveAnswer(Answer answer) {
		Long nid = answer.getCom().getPurpose().longValue();
		Naics n = nManager.findById(nid);
		if (n != null)
			answer.getCom().setPurposeName(n.getName());
		Answer newanswer = aManager.update(answer);
		return newanswer;
	}

}

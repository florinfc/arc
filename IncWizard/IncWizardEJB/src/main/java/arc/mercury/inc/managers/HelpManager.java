package arc.mercury.inc.managers;

import arc.mercury.inc.entities.Help;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@SuppressWarnings("unchecked")
public class HelpManager implements IHelpManager {

	public HelpManager() {
	}

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;

	/**
	 * {@inheritDoc}
	 */
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Help> findAll() {
		List<Help> result = null;
		Query query = getManager().createNamedQuery("Help.findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Help>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public Help findById(Long id) {
		if (id == null) {
			return null;
		}
		return (Help) getManager().find(Help.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	public Help findByName(String name) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		Help result = null;
		Query query = getManager().createNamedQuery("Help.findByName");
		query.setParameter("param", name);
		try {
			result = (Help) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Help> findLikeName(String name) {
		if (name == null || name.isEmpty()) {
			return new ArrayList<Help>(0);
		}
		List<Help> result = null;
		Query query = getManager().createNamedQuery("Help.findLikeName");
		query.setParameter("param", "%" + name + "%");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Help>(0);
		}
		return result;
	}

}

package arc.mercury.inc.managers;

import arc.mercury.inc.entities.Resp;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@SuppressWarnings("unchecked")
public class RespManager implements IRespManager {

	public RespManager() {
	}

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;

	/**
	 * {@inheritDoc}
	 */
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	public Resp insert(Resp item) {
		if (item == null) {
			return null;
		}
		getManager().persist(item);
		return item;
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Resp item) {
		if (item == null) {
			return;
		}
		item = getManager().find(Resp.class, item.getId());
		if (item != null) {
			getManager().remove(item);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Long id) {
		if (id == null) {
			return;
		}
		Resp item = getManager().getReference(Resp.class, id);
		if (item != null) {
			getManager().remove(item);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public Resp update(Resp item) {
		if (item == null) {
			return null;
		}
		item = getManager().merge(item);
		return item;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Resp> findAll() {
		List<Resp> result = null;
		Query query = getManager().createNamedQuery("User.findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Resp>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public Resp findById(Long id) {
		if (id == null) {
			return null;
		}
		return (Resp) getManager().find(Resp.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	public Resp findByName(String name) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		Resp result = null;
		Query query = getManager().createNamedQuery("User.findByName");
		query.setParameter("param", name);
		try {
			result = (Resp) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Resp> findLikeName(String name) {
		if (name == null || name.isEmpty()) {
			return new ArrayList<Resp>(0);
		}
		List<Resp> result = null;
		Query query = getManager().createNamedQuery("User.findLikeName");
		query.setParameter("param", "%" + name + "%");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Resp>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public Resp findByMail(String mail) {
		if (mail == null || mail.isEmpty()) {
			return null;
		}
		Resp result = null;
		Query query = getManager().createNamedQuery("User.findByMail");
		query.setParameter("param", mail);
		try {
			result = (Resp) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

}

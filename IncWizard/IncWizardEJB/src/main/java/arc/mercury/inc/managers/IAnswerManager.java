package arc.mercury.inc.managers;

import arc.mercury.inc.entities.Answer;

import java.util.List;

import javax.ejb.Local;
import javax.persistence.EntityManager;

@Local
public interface IAnswerManager {

	public EntityManager getManager();

	public Answer insert(Answer item);

	public Answer update(Answer item);

	public void delete(Answer item);

	public void delete(Long id);

	public List<Answer> findAll();

	public Answer findById(Long id);

	public Answer findByName(String name);

	public List<Answer> findLikeName(String name);

	public Answer findByForm(Long uid, Long qid);

	public List<Answer> findByUser(Long uid);

}

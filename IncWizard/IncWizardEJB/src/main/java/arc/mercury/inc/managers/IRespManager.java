package arc.mercury.inc.managers;

import arc.mercury.inc.entities.Resp;

import java.util.List;

import javax.ejb.Local;
import javax.persistence.EntityManager;

@Local
public interface IRespManager {

	public EntityManager getManager();

	/**
	 * Insert an user item in the system
	 * 
	 * @param item
	 * @return new user
	 */
	public Resp insert(Resp item);

	/**
	 * Update an user item in the system
	 * 
	 * @param item
	 * @return new user
	 */
	public Resp update(Resp item);

	/**
	 * Delete an user item from the system
	 * 
	 * @param item
	 * @return
	 */
	public void delete(Resp item);

	/**
	 * Delete an user item by id from the system
	 * 
	 * @param id
	 * @return
	 */
	public void delete(Long id);

	/**
	 * Find all the users in the system
	 * 
	 * @return List of users
	 */
	public List<Resp> findAll();

	/**
	 * Find user by id
	 * 
	 * @param id
	 * @return Resp
	 */
	public Resp findById(Long id);

	/**
	 * Find user by name (exact match)
	 * 
	 * @param name
	 * @return Resp
	 */
	public Resp findByName(String name);

	/**
	 * Find all users that contains a pattern in the name
	 * 
	 * @param pattern
	 * @return List of users
	 */
	public List<Resp> findLikeName(String name);

	/**
	 * Find user by e-mail
	 * 
	 * @param mail
	 * @return Resp
	 */
	public Resp findByMail(String mail);

}

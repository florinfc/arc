package arc.mercury.inc.managers;

import arc.mercury.inc.entities.Naics;

import java.util.List;

import javax.ejb.Local;
import javax.persistence.EntityManager;

@Local
public interface INaicsManager {

	public EntityManager getManager();

	public List<Naics> findAll();

	public Naics findById(Long id);

	public Naics findByName(String name);

	public List<Naics> findLikeName(String name);
}

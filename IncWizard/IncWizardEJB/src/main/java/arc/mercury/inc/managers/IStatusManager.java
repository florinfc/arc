package arc.mercury.inc.managers;

import arc.mercury.inc.entities.Status;

import java.util.List;

import javax.ejb.Local;
import javax.persistence.EntityManager;

@Local
public interface IStatusManager {

	public EntityManager getManager();

	public List<Status> findAll();

	public Status findById(Long id);

	public Status findByName(String name);

	public List<Status> findLikeName(String name);

}

package arc.mercury.inc.managers;

import arc.mercury.inc.entities.Quote;

import java.util.List;

import javax.ejb.Local;
import javax.persistence.EntityManager;

@Local
public interface IQuoteManager {

	public EntityManager getManager();

	public List<Quote> findAll();

	public Quote findById(Long id);

	public Quote findByName(String name);

	public List<Quote> findLikeName(String name);
}

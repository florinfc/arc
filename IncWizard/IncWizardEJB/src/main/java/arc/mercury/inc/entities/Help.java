package arc.mercury.inc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import arc.mercury.com.base.BaseClass;
import arc.mercury.com.base.Constants;

@Entity
@Table(name = "THelp")
@NamedQueries({
		@NamedQuery(name = "Help.findAll", query = "select item from Help item order by item.id"),
		@NamedQuery(name = "Help.findById", query = "select item from Help item where item.id = :param"),
		@NamedQuery(name = "Help.findByName", query = "select item from Help item where item.name = :param"),
		@NamedQuery(name = "Help.findLikeName", query = "select item from Help item where item.name like :param order by item.name"),

})
@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class Help extends BaseClass<Help> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "name", length = Constants.MAXLEN)
	private String name;
	@Column(name = "adder")
	private String adder;
	@Column(name = "savings")
	private Integer savings;
	@Column(name = "fee")
	private Integer fee;
	@Column(name = "form")
	private String form;
	@Column(name = "doc")
	private String doc;
	@Column(name = "total")
	private Integer total;
	@Transient
	private Boolean show = false;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAdder() {
		return adder;
	}

	public void setAdder(String adder) {
		this.adder = adder;
	}

	public Integer getSavings() {
		return savings;
	}

	public void setSavings(Integer savings) {
		this.savings = savings;
	}

	public Integer getFee() {
		return fee;
	}

	public void setFee(Integer fee) {
		this.fee = fee;
	}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public String getDoc() {
		return doc;
	}

	public void setDoc(String doc) {
		this.doc = doc;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Boolean getShow() {
		return show;
	}

	public void setShow(Boolean show) {
		this.show = show;
	}
}

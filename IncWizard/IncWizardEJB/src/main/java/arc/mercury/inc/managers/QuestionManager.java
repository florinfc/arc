package arc.mercury.inc.managers;

import arc.mercury.inc.entities.Question;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@SuppressWarnings("unchecked")
public class QuestionManager implements IQuestionManager {

	public QuestionManager() {
	}

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;

	/**
	 * {@inheritDoc}
	 */
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Question> findAll() {
		List<Question> result = null;
		Query query = getManager().createNamedQuery("Question.findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Question>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public Question findById(Long id) {
		if (id == null) {
			return null;
		}
		Question result = null;
		Query query = getManager().createNamedQuery("Question.findById");
		query.setParameter("param", id);
		try {
			result = (Question) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public Question findByName(String name) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		Question result = null;
		Query query = getManager().createNamedQuery("Question.findByName");
		query.setParameter("param", name);
		try {
			result = (Question) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Question> findLikeName(String name) {
		if (name == null || name.isEmpty()) {
			return new ArrayList<Question>(0);
		}
		List<Question> result = null;
		Query query = getManager().createNamedQuery("Question.findLikeName");
		query.setParameter("param", "%" + name + "%");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Question>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Question> findByPage(Long id) {
		List<Question> result = null;
		Query query = getManager().createNamedQuery("Question.findByPage");
		query.setParameter("param", id);
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Question>(0);
		}
		return result;
	}

}

package arc.mercury.inc.generators;

import arc.mercury.inc.entities.Restrict;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

/**
 * Generator for more Restrict records
 */
@GeneratorType(className = Restrict.class, fieldType = FieldType.ALL_TYPES)
public final class RestrictCollection extends BeanCollectionGenerator<Restrict> {

	public RestrictCollection() {
		super(Restrict.class, 5);
	}

	public final static RestrictCollection instance() {
		return new RestrictCollection();
	}
}

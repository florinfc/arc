package arc.mercury.inc.entities;

import arc.mercury.inc.generators.*;
import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;

public class QuestionTest extends BaseEntityFixture<Question> {
	public static final Generator<?>[] SPECIAL_GENERATORS = {
			//PageCollection.instance(),
			PageRecord.instance(),
			FormRecord.instance(),
			StatusRecord.instance(),
			RestrictCollection.instance()
	};

	public QuestionTest() {
		super(Question.class, SPECIAL_GENERATORS);
	}
}
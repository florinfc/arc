package arc.mercury.inc.managers;

import arc.mercury.inc.entities.Answer;
import arc.mercury.inc.entities.Form;
import arc.mercury.inc.entities.Page;
import arc.mercury.inc.entities.Question;
import arc.mercury.inc.entities.Restrict;
import arc.mercury.inc.entities.Status;

import com.bm.testsuite.BaseSessionBeanFixture;

public class QuestionManagerTest extends
		BaseSessionBeanFixture<QuestionManager> {
	private static final Class<?>[] beans = { Question.class, Answer.class,
			Page.class, Form.class, Status.class, Restrict.class };

	public QuestionManagerTest() {
		super(QuestionManager.class, beans);
	}

}
package arc.mercury.inc.entities;

import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;
import arc.mercury.inc.generators.*;

public class AnswerTest extends BaseEntityFixture<Answer> {

	private static final Generator<?>[] SPECIAL_GENERATORS = {

			StatusRecord.instance(),
			FormRecord.instance(),
			PageCollection.instance(),
			QuestionCollection.instance(),
			RestrictCollection.instance()
	};

	public AnswerTest() {
		super(Answer.class, SPECIAL_GENERATORS);
	}

}
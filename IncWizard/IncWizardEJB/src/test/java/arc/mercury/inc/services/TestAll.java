package arc.mercury.inc.services;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The class <code>TestAll</code> builds a suite that can be used to run all of
 * the tests within its package as well as within any subpackages of its
 * package.
 * 
 * @generatedBy CodePro at 6/30/13 3:35 PM
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ IncServicesTest.class })
public class TestAll {
}

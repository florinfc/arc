package arc.mercury.inc.entities;

import arc.mercury.inc.generators.*;
import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;

public class StatusTest extends BaseEntityFixture<Status> {
	public static final Generator<?>[] SPECIAL_GENERATORS = {
			FormRecord.instance(),
			PageCollection.instance(),
			QuestionCollection.instance(),
			RestrictCollection.instance()
	};

	public StatusTest() {
		super(Status.class, SPECIAL_GENERATORS);
	}
}
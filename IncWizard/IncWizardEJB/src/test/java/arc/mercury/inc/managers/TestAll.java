package arc.mercury.inc.managers;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The class <code>TestAll</code> builds a suite that can be used to run all of
 * the tests within its package as well as within any subpackages of its
 * package.
 * 
 * @generatedBy CodePro at 6/30/13 3:39 PM
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ StatusManagerTest.class, SS4ManagerTest.class,
		FormManagerTest.class, QuestionManagerTest.class,
		RestrictManagerTest.class,
		RespManagerTest.class, AnswerManagerTest.class, NaicsManagerTest.class,
		PageManagerTest.class })
public class TestAll {
}

package arc.mercury.inc.generators;

import arc.mercury.inc.entities.Status;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.SingleBeanGenerator;

/**
 * Generator for one Status record
 */
@GeneratorType(className = Status.class, fieldType = FieldType.ALL_TYPES)
public final class StatusRecord extends SingleBeanGenerator<Status> {

	public StatusRecord() {
		super(Status.class);
	}

	public final static StatusRecord instance() {
		return new StatusRecord();
	}
}

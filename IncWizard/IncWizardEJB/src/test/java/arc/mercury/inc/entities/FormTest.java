package arc.mercury.inc.entities;

import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;
import arc.mercury.inc.generators.*;

public class FormTest extends BaseEntityFixture<Form> {
    public static final Generator<?>[] SPECIAL_GENERATORS = {
        StatusRecord.instance(),
        PageCollection.instance(),
        QuestionCollection.instance(),
        RestrictCollection.instance()
    };

    public FormTest() {
        super(Form.class, SPECIAL_GENERATORS);
    }
}
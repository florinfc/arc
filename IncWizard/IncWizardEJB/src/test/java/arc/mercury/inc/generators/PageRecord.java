package arc.mercury.inc.generators;

import arc.mercury.inc.entities.Page;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.SingleBeanGenerator;

/**
 * Generator for one Page record
 */
@GeneratorType(className = Page.class, fieldType = FieldType.ALL_TYPES)
public final class PageRecord extends SingleBeanGenerator<Page> {

	public PageRecord() {
		super(Page.class);
	}

	public final static PageRecord instance() {
		return new PageRecord();
	}
}

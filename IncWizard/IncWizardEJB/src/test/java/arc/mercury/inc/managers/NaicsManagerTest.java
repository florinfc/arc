package arc.mercury.inc.managers;

import arc.mercury.inc.entities.Naics;

import com.bm.testsuite.BaseSessionBeanFixture;

public class NaicsManagerTest extends BaseSessionBeanFixture<NaicsManager> {
	private static final Class<?>[] beans = { Naics.class };

	public NaicsManagerTest() {
		super(NaicsManager.class, beans);
	}

}
package arc.mercury.inc.managers;

import arc.mercury.inc.entities.Answer;

import com.bm.testsuite.BaseSessionBeanFixture;

public class AnswerManagerTest extends BaseSessionBeanFixture<AnswerManager> {
	private static final Class<?>[] beans = { Answer.class };

	public AnswerManagerTest() {
		super(AnswerManager.class, beans);
	}

}
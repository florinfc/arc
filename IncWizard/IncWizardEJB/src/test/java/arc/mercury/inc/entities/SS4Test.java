package arc.mercury.inc.entities;

import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;

public class SS4Test extends BaseEntityFixture<SS4> {
	public static final Generator<?>[] SPECIAL_GENERATORS = {

	};

	public SS4Test() {
		super(SS4.class, SPECIAL_GENERATORS);
	}
}
package arc.mercury.inc.generators;

import arc.mercury.inc.entities.Answer;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

/**
 * Generator for more Answer records
 */
@GeneratorType(className = Answer.class, fieldType = FieldType.ALL_TYPES)
public final class AnswerCollection extends BeanCollectionGenerator<Answer> {

	public AnswerCollection() {
		super(Answer.class, 5);
	}

	public final static AnswerCollection instance() {
		return new AnswerCollection();
	}
}

package arc.mercury.inc.managers;

import arc.mercury.inc.entities.SS4;

import com.bm.testsuite.BaseSessionBeanFixture;

public class SS4ManagerTest extends BaseSessionBeanFixture<SS4Manager> {
	private static final Class<?>[] beans = { SS4.class };

	public SS4ManagerTest() {
		super(SS4Manager.class, beans);
	}

}
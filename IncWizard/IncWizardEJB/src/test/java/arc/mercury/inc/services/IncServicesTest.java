package arc.mercury.inc.services;

import arc.mercury.inc.entities.Answer;
import arc.mercury.inc.entities.Form;
import arc.mercury.inc.entities.Naics;
import arc.mercury.inc.entities.Page;
import arc.mercury.inc.entities.Question;
import arc.mercury.inc.entities.Restrict;
import arc.mercury.inc.entities.SS4;
import arc.mercury.inc.entities.Status;

import com.bm.testsuite.BaseSessionBeanFixture;

public class IncServicesTest extends BaseSessionBeanFixture<IncServices> {
	private static final Class<?>[] usedBeans = {

	Answer.class, Form.class, Page.class, Question.class, Restrict.class,
			Status.class, Naics.class, SS4.class

	};

	public IncServicesTest() {
		super(IncServices.class, usedBeans);
	}

}
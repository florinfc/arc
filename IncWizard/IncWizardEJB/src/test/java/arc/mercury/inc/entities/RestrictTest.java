package arc.mercury.inc.entities;

import arc.mercury.inc.generators.*;
import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;

public class RestrictTest extends BaseEntityFixture<Restrict> {
	public static final Generator<?>[] SPECIAL_GENERATORS = {
			QuestionRecord.instance(),
			PageRecord.instance(),
			FormRecord.instance(),
			StatusRecord.instance()
	};

	public RestrictTest() {
		super(Restrict.class, SPECIAL_GENERATORS);
	}
}
package arc.mercury.inc.entities;

import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;
import arc.mercury.inc.generators.*;

public class PageTest extends BaseEntityFixture<Page> {
    public static final Generator<?>[] SPECIAL_GENERATORS = {
        FormRecord.instance(),
        StatusRecord.instance(),
        PageCollection.instance(),
        QuestionCollection.instance(),
        RestrictCollection.instance()
    };

    public PageTest() {
        super(Page.class, SPECIAL_GENERATORS);
    }
}
package arc.mercury.inc.generators;

import arc.mercury.inc.entities.Question;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

/**
 * Generator for more Question records
 */
@GeneratorType(className = Question.class, fieldType = FieldType.ALL_TYPES)
public final class QuestionCollection extends BeanCollectionGenerator<Question> {

	public QuestionCollection() {
		super(Question.class, 5);
	}

	public final static QuestionCollection instance() {
		return new QuestionCollection();
	}
}

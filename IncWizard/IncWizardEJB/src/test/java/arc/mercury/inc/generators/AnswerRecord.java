package arc.mercury.inc.generators;

import arc.mercury.inc.entities.Answer;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.SingleBeanGenerator;

/**
 * Generator for one Answer record
 */
@GeneratorType(className = Answer.class, fieldType = FieldType.ALL_TYPES)
public final class AnswerRecord extends SingleBeanGenerator<Answer> {

	public AnswerRecord() {
		super(Answer.class);
	}

	public final static AnswerRecord instance() {
		return new AnswerRecord();
	}
}

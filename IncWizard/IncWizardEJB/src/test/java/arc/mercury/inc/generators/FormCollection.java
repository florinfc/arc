package arc.mercury.inc.generators;

import arc.mercury.inc.entities.Form;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

/**
 * Generator for more Form records
 */
@GeneratorType(className = Form.class, fieldType = FieldType.ALL_TYPES)
public final class FormCollection extends BeanCollectionGenerator<Form> {

	public FormCollection() {
		super(Form.class, 5);
	}

	public final static FormCollection instance() {
		return new FormCollection();
	}
}

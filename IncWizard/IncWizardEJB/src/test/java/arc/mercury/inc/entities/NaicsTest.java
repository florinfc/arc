package arc.mercury.inc.entities;

import arc.mercury.inc.generators.*;
import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;

public class NaicsTest extends BaseEntityFixture<Naics> {
	public static final Generator<?>[] SPECIAL_GENERATORS = {
			FormRecord.instance(),
			StatusRecord.instance(),
			PageCollection.instance(),
			QuestionCollection.instance(),
			RestrictCollection.instance()
	};

	public NaicsTest() {
		super(Naics.class, SPECIAL_GENERATORS);
	}
}
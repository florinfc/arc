package arc.mercury.inc.managers;

import arc.mercury.inc.entities.Answer;
import arc.mercury.inc.entities.Form;
import arc.mercury.inc.entities.Page;
import arc.mercury.inc.entities.Question;
import arc.mercury.inc.entities.Restrict;
import arc.mercury.inc.entities.Status;

import com.bm.testsuite.BaseSessionBeanFixture;

public class PageManagerTest extends BaseSessionBeanFixture<PageManager> {
	private static final Class<?>[] beans = { Page.class, Form.class,
			Status.class, Question.class, Answer.class, Restrict.class };

	public PageManagerTest() {
		super(PageManager.class, beans);
	}
}
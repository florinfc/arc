package arc.mercury.inc.entities;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ RestrictTest.class, RespTest.class, AnswerTest.class,
		SS4Test.class, NaicsTest.class })
public class TestAll {
}

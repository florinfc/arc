package arc.mercury.inc.generators;

import arc.mercury.inc.entities.Status;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

/**
 * Generator for more Status records
 */
@GeneratorType(className = Status.class, fieldType = FieldType.ALL_TYPES)
public final class StatusCollection extends BeanCollectionGenerator<Status> {

	public StatusCollection() {
		super(Status.class, 5);
	}

	public final static StatusCollection instance() {
		return new StatusCollection();
	}
}
